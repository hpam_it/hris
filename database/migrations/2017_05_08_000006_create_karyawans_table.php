<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaryawansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('karyawans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid')->nullable();
            $table->string('nama_karyawan')->index();
            $table->date('tgl_gabung')->nullable();
            $table->string('status_kerja')->nullable();
            $table->string('npp')->unique();
            $table->integer('jabatan_id')->unsigned();
            $table->integer('divisi_id')->unsigned();
            $table->integer('kantor_id')->unsigned();
            $table->integer('pangkat_id')->unsigned();
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->string('gender');
            $table->string('status_nikah');
            $table->string('status_pajak')->nullable();
            $table->string('agama');
            $table->string('nik')->unique()->index();
            $table->string('npwp')->nullable();
            $table->string('nama_bank')->nullable();
            $table->string('nama_rekening')->nullable();
            $table->string('nomor_rekening_payrol')->nullable();
            $table->string('nomor_cif')->nullable();
            $table->string('email')->nullable();
            $table->string('email_pribadi')->nullable();
            $table->string('password')->nullable();
            $table->string('nomor_telpon')->nullable();
            $table->string('nomor_ponsel')->nullable();
            $table->string('nama_kontak_darurat')->nullable();
            $table->string('nomor_kontak_darurat')->nullable();
            $table->string('hubungan_kontak_darurat')->nullable();
            $table->text('alamat_identitas');
            $table->text('alamat_sekarang');
            $table->string('remember_token')->nullable();
            $table->string('role')->nullable();
            $table->string('jam_kerja')->nullable();
            $table->timestamps();
            //relasi table
            $table->foreign('jabatan_id')->references('id')->on('jabatans')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('divisi_id')->references('id')->on('divisis')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('kantor_id')->references('id')->on('kantors')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('pangkat_id')->references('id')->on('pangkats')->onDelete('restrict')->onUpdate('restrict');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawans');
    }
}
