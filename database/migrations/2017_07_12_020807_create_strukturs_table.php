<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStruktursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('strukturs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jabatan_id')->unsigned();
            $table->integer('divisi_id')->unsigned();
            $table->integer('kantor_id')->unsigned();
            $table->integer('atasan');
            $table->string('kode_struktur');
            $table->timestamps();

            $table->foreign('jabatan_id')->references('id')->on('jabatans')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('divisi_id')->references('id')->on('divisis')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('kantor_id')->references('id')->on('kantors')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('strukturs');
    }
}
