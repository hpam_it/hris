<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJamKerjasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jam_kerjas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_jamkerja')->unique()->index();
            $table->time('masuk');
            $table->time('pulang');
            $table->time('toleransi');
            $table->time('maxpulang');
            $table->date('tgl_mulai');
            $table->date('tgl_selesai');
            $table->time('jam_normal');
            $table->time('istirahat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jam_kerjas');
    }
}
