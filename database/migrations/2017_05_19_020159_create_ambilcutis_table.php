<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmbilcutisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('ambilcutis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('karyawan_id')->unsigned();
            $table->integer('data_cuti_id')->unsigned();
            $table->text('tanggal_cuti');
            $table->integer('jumlah_hari');
            $table->text('keperluan')->nullable();
            $table->text('alamat_cuti')->nullable();
            $table->string('notelpon')->nullable();
            $table->integer('pic_pengganti')->unsigned();
            $table->integer('atasan')->unsigned();
            $table->string('statusatasan')->nullable();
            $table->text('alasan_atasan')->nullable();
            $table->string('statuspic')->nullable();
            $table->text('alasan_pic')->nullable();
            $table->string('statushrd')->nullable();
            $table->text('alasan_hrd')->nullable();
            $table->string('statuscuti')->nullable();
            $table->timestamps();

            //relasi table
              $table->foreign('karyawan_id')->references('id')->on('karyawans')->onDelete('restrict')->onUpdate('restrict');
              $table->foreign('data_cuti_id')->references('id')->on('data_cutis')->onDelete('restrict')->onUpdate('restrict');
              $table->foreign('pic_pengganti')->references('id')->on('karyawans')->onDelete('restrict')->onUpdate('restrict');
              $table->foreign('atasan')->references('id')->on('karyawans')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ambilcutis');
    }
}
