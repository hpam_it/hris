<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengangkatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('pengangkatans', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tgl_pengangkatan');
            $table->integer('karyawan_id')->unsigned()->index();
            $table->integer('jabatan_id')->unsigned();
            $table->integer('pangkat_id')->unsigned();
            $table->integer('kantor_id')->unsigned();
            $table->integer('status_karyawan_id')->unsigned();
            $table->string('nomor_surat');
            $table->string('dokument');//path to file
            $table->text('keterangan');
            $table->timestamps();

            $table->foreign('karyawan_id')->references('id')->on('karyawans')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('jabatan_id')->references('id')->on('jabatans')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('pangkat_id')->references('id')->on('pangkats')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('kantor_id')->references('id')->on('kantors')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('status_karyawan_id')->references('id')->on('status_karyawans')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengangkatans');
    }
}
