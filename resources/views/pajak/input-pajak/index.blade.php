@extends('home')
@section('title')
Input Pajak PPH21
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Input PAJAK</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" class="form-horizontal" id="form_input"  action="{{ url('input-pajak') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                      <div class="tabbable-custom ">
                          <ul class="nav nav-tabs ">
                              <li class="active">
                                  <a href="#data_karyawan" data-toggle="tab">Data Karyawan</a>
                              </li>
                              <li>
                                  <a href="#penghasilan" data-toggle="tab">Penghasilan</a>
                              </li>
                              <li>
                                  <a href="#pengurang" data-toggle="tab">Pengurang</a>
                              </li>
                              <li>
                                  <a href="#ptkp" data-toggle="tab">PTKP</a>
                              </li>
                          </ul>
                          <div class="tab-content">
                              <div class="tab-pane active" id="data_karyawan">
                                <div class="row">
                                  <div class="col-md-12 ">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Karyawan</label>
                                    <div class=" col-md-8" >
                                      <select onchange="pilih_karyawan(this.value)" name="karyawan_id" id="karyawan_id" class="form-control bs-select" data-style="btn-primary" data-live-search="true" required="true" />
                                          <option value="0">Pilih</option>
                                        @foreach($karyawan as $karyawan)
                                          <option value="{{ $karyawan->id }}">{{ $karyawan->nama_karyawan }} / {{ $karyawan->npp }}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">NPWP</label>
                                    <div class=" col-md-8" >
                                      <input type="text" readonly name="npwp" id="npwp" class="form-control" placeholder="NPWP" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">NIK</label>
                                    <div class=" col-md-8" >
                                      <input type="text" readonly name="nik" id="nik" class="form-control" placeholder="Nomor Induk Kependudukan"  >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Status Pernikahan</label>
                                    <div class=" col-md-8" >
                                      <input type="text" readonly name="status_nikah" id="status_nikah" class="form-control" placeholder="K = Kawin, TK = Tidak Kawin" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Jumlah Tanggungan</label>
                                    <div class=" col-md-8" >
                                      <input type="number" readonly name="tanggungan" id="tanggungan" class="form-control" placeholder="Jumlah Tanggungan"  >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Jabatan</label>
                                    <div class=" col-md-8" >
                                      <input type="text" readonly name="jabatan" id="jabatan" class="form-control" placeholder="Jabatan" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Gaji Pokok</label>
                                    <div class=" col-md-8" >
                                      <input type="text" name="gaji_pokok" id="gaji_pokok" class="form-control angka-angka" placeholder="Gaji Pokok"  required >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Bulan Pajak</label>
                                    <div class=" col-md-8" >
                                      <input type="text" readonly name="bulan_pajak" id="bulan_pajak" class="form-control date-picker" placeholder="Bulan Pajak" required >
                                    </div>
                                </div>

                              </div>
                              </div>
                              </div>
                              <div class="tab-pane" id="penghasilan">
                                <div class="row">
                                  <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Adjustmen Gaji</label>
                                        <div class=" col-md-8" >
                                          <input type="text"  name="adjustmen_gaji" id="adjustmen_gaji" class="form-control angka-angka" placeholder="Adjustmen Gaji" required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Tunjangan Makan</label>
                                        <div class=" col-md-8" >
                                          <input type="text"  name="tunjangan_makan" id="tunjangan_makan" class="form-control angka-angka" placeholder="tunjangan makan" required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Insentif</label>
                                        <div class=" col-md-8" >
                                          <input type="text"  name="insentif" id="insentif" class="form-control angka-angka " placeholder="Insentif" required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Bonus</label>
                                        <div class=" col-md-8" >
                                          <input type="text"  name="bonus" id="bonus" class="form-control angka-angka" placeholder="bonus" required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Lembur</label>
                                        <div class=" col-md-8" >
                                          <input type="text"  name="lembur" id="lembur" class="form-control angka-angka" placeholder="Lembur" required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Uang Dinas</label>
                                        <div class=" col-md-8" >
                                          <input type="text"  name="uang_dinas" id="uang_dinas" class="form-control angka-angka" placeholder="Uang Dinas" required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">THR</label>
                                        <div class=" col-md-8" >
                                          <input type="text"  name="thr" id="thr" class="form-control angka-angka" placeholder="THR"  required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">JKK 3%</label>
                                        <div class=" col-md-8" >
                                          <input type="text" name="jkk" id="jkk" class="form-control angka-angka" placeholder="JKK 3%"  >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">JK 1%</label>
                                        <div class=" col-md-8" >
                                          <input type="text" name="jk" id="jk" class="form-control angka-angka" placeholder="JK 1%"  >
                                        </div>
                                    </div>

                                  </div>
                                </div>
                              </div>
                              <div class="tab-pane" id="pengurang">
                                <div class="row">
                                  <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Biaya Jabatan</label>
                                        <div class=" col-md-8" >
                                          <input type="hidden" id="total_penghasilan" name="total_penghasilan" />
                                          <input type="text" readonly name="biaya_jabatan" id="biaya_jabatan" class="form-control angka-angka" required="true" placeholder="Biaya Jabatan"  >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Iuran JHT</label>
                                        <div class=" col-md-8" >
                                          <input type="text"  name="iuran_jht" id="iuran_jht" class="form-control angka-angka"  placeholder="Iuran JHT" required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">Iuran Pensiun</label>
                                        <div class=" col-md-8" >
                                          <input type="text"  name="iuran_pensiun" id="iuran_pensiun" class="form-control angka-angka" placeholder="Iuran Pensiun" required  >
                                        </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="tab-pane" id="ptkp">
                                <div class="row">
                                  <div class="col-md-12 ">
                                    @foreach($ptkp as $ptkp)
                                    <div class="form-group">
                                        <label class="control-label col-md-4">{{ $ptkp->nama }}</label>
                                        <div class=" col-md-8" >
                                          <input onchange="hitung_ptkp()" id="ptkp{{ $ptkp->id }}" type="number"  name="data_ptkp[{{ $ptkp->id }}]" data-id="{{  $ptkp->id }}" data-nilai="{{ $ptkp->nilai }}" class="form-control data_ptkp" placeholder="Jumlah PTKP" required />
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="form-group">
                                      <label class="control-label col-md-4">Total PTKP</label>
                                      <div class="col-md-8">
                                        <input type="text" id="total_ptkp" name="total_ptkp" class="form-control angka-angka" readonly placeholder="Total PTKP" />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                      </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_pajak();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>List Data Pajak</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer" },
                      { "data": "nama_karyawan" },
                      { "data": "bulan_pajak" },
                      { "data": "total_penghasilan", render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" )},
                      { "data": "total_pengurang", render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" )},
                      { "data": "penghasilan_netto", render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" )},
                      { "data": "total_ptkp", render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" )},
                      { "data": "pkp", render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" ) },
                      { "data": "pph_total", render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" ) },
                      { "data": "pph_bulansebelum", render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" ) },
                      { "data": "pph_bulanini", render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" ) },
                      { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">NO</th>
                        <th class="min-mobile-p">Nama Karyawan</th>
                        <th class="min-tablet-l">Bulan Pajak</th>
                        <th class="none">Penghasilan</th>
                        <th class="none">Pengurang</th>
                        <th class="none">Penghasilan Neto</th>
                        <th class="none">Ptkp</th>
                        <th class="none">Total PKP</th>
                        <th class="none">PPH Total</th>
                        <th class="none">PPH Bulan Sebelum</th>
                        <th class="min-mobile-p">PPH Bulan Ini</th>
                        <th class="none">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
