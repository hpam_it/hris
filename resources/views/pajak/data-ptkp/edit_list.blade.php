<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data PTKP</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  action="{{ url('data-ptkps') }}/{{ $item->id }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="update_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama PTKP</label>
                            <input type="text" value="{{ $item->nama }}" name="nama" id="nama" class="pertama form-control" placeholder="Nama PTKP" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nilai PTKP</label>
                            <input type="text" value="{{ $item->nilai }}" name="nilai" id="nilai" class="form-control" placeholder="Nilai PTKP" required="true">
                        </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
