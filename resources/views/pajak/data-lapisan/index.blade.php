@extends('home')
@section('title')
Data PTKPS
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Input Data Lapisan PAJAK</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  action="{{ url('data-lapisan') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Urutan</label>
                            <input type="number" name="urutan" id="urutan" class="pertama form-control" placeholder="Urutan Lapisan" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nilai Lapisan</label>
                            <input type="text" name="lapisan" id="lapisan" class="form-control" placeholder="Nilai Lapisan" required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tarif</label>
                            <input type="number" name="tarif" id="tarif" class="form-control" placeholder="% tarif Lapisan" required="true">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>List Data Lapisan</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer" },
                      { "data": "urutan" },
                      { "data": "lapisan", render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" ) },
                      { "data": "tarif" },
                      { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">NO</th>
                        <th class="min-mobile-p">Urutan</th>
                        <th class="min-tablet-l">Nilai Lapisan</th>
                        <th class="min-tablet-l">Tarif (%)</th>
                        <th class="none">Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
