<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data PTKP</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  action="{{ url('data-lapisan') }}/{{ $item->id }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="update_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="control-label">Urutan</label>
                          <input type="number" name="urutan" id="urutan" value="{{ $item->urutan }}" class="pertama form-control" placeholder="Urutan Lapisan" autofocus required="true">
                      </div>
                      <div class="form-group">
                          <label class="control-label">Nilai Lapisan</label>
                          <input type="text" value="{{ $item->lapisan }}" name="lapisan" id="lapisan" class="form-control" placeholder="Nilai Lapisan" required="true">
                      </div>
                      <div class="form-group">
                          <label class="control-label">Tarif</label>
                          <input type="number" name="tarif" id="tarif" value="{{ $item->tarif }}"  class="form-control" placeholder="% tarif Lapisan" required="true">
                      </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
