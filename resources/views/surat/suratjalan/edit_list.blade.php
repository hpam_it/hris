<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Jenis Cuti</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  action="{{ url('surat-jalan') }}/{{ $item->id }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="update_item">
                    <input type="hidden" name="id" value="{{ $item->id }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Kepada </label>
                            <input type="text" value="{{ $item->tujuan }}" name="tujuan" id="nama_cuti" class="pertama form-control" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Dari</label>
                            <input type="text" value="{{ $item->dari }}" name="dari" id="dari" class="form-control"  required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tipe</label>
                            <input type="text" value="{{ $item->tipe }}" name="tipe" id="tipe" class="form-control"  required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Keterangan</label>
                            <textarea name="keterangan" id="keterangan" class="form-control"  required="true">{{ $item->keterangan }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tanggal</label>
                            <input type="text" name="tanggal" value="{{ $item->tanggal }}" id="tanggal" class="form-control date-picker"  required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Di Kirim Oleh</label>
                            <input type="text" name="dikirim" value="{{ $item->dikirim }}" id="dikirim" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label class="control-label">Diterima Oleh</label>
                            <input type="text" name="diterima" value="{{ $item->diterima }}" id="diterima" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kota</label>
                            <input type="text" name="kota" value="{{ $item->kota }}" id="kota" class="form-control" required >
                        </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
