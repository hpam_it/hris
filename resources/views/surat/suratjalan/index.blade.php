@extends('home')
@section('title')
Surat Jalan
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Input Tanda Terima</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  action="{{ url('surat-jalan') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Kepada </label>
                            <input type="text" name="tujuan" id="nama_cuti" class="pertama form-control" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Dari</label>
                            <input type="text" name="dari" id="dari" class="form-control"  required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tipe</label>
                            <input type="text" name="tipe" id="tipe" class="form-control"  required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Keterangan</label>
                            <textarea name="keterangan" id="keterangan" class="form-control"  required="true"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tanggal</label>
                            <input type="text" name="tanggal" id="tanggal" class="form-control date-picker"  required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Di Kirim Oleh</label>
                            <input type="text" name="dikirim" id="dikirim" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label class="control-label">Diterima Oleh</label>
                            <input type="text" name="diterima" id="diterima" class="form-control" >
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kota</label>
                            <input type="text" name="kota" id="kota" class="form-control" required >
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>List Data Tanda terima</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer" },
                      { "data": "dari" },
                      { "data": "tujuan" },
                      { "data": "tanggal" },
                      { "data": "keterangan" },
                      { "data": "dikirim" },
                      { "data": "diterima" },
                      { "data": "kota" },
                      { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">NO</th>
                        <th class="min-mobile-p">Dari</th>
                        <th class="min-tablet-l">Tujuan</th>
                        <th class="min-tablet-l">Tanggal</th>
                        <th class="none">Keterangan</th>
                        <th class="none">Pengirim</th>
                        <th class="none">Penerima</th>
                        <th class="none">Kota</th>
                        <th class="none">Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
