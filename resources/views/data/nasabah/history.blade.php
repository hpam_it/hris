<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">List Followup Nasabah</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
          <table id="table-modal" class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
          <thead>
          <tr>
              <th class="all">Tanggal</th>
              <th class="min-mobile-p">Progress</th>
              <th class="min-tablet-l">Hasil</th>
              <th class="min-tablet-l">berikutnya</th>
              <th class="min-tablet-l">nominal</th>
              <th class="min-tablet-l">Keterangan</th>
          </tr>
          </thead>
          <tbody>

            @foreach($progress as $progress)
            <tr>
              <td>{{ $progress->tanggal_progress }}</td>
              <td>{{ $progress->nama_progress }}</td>
              <td>{{ $progress->hasil_progress }}</td>
              <td>{{ $progress->tanggal_progress_berikutnya }}</td>
              <td>{{ $progress->nominal_progress }}</td>
              <td>{{ $progress->keterangan_progress }}</td>
           </tr>
            @endforeach
          </tbody>
          </table>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
<script>
var modaltable = $("#table-modal").dataTable();
</script>
