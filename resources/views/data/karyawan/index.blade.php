@extends('home')
@section('title')
Data Karyawan
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Input Data Karyawan</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" class="form-horizontal" id="form_input"  action="{{ url('data-karyawan') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                      <div class="tabbable-custom ">
                                          <ul class="nav nav-tabs ">
                                              <li class="active">
                                                  <a href="#data_diri" data-toggle="tab">Data Diri</a>
                                              </li>
                                              <li>
                                                  <a href="#info_kontak" data-toggle="tab">Kontak</a>
                                              </li>
                                              <li>
                                                  <a href="#info_kepegawaian" data-toggle="tab">Kepegawaian</a>
                                              </li>
                                              <li>
                                                  <a href="#info_bank" data-toggle="tab">Bank</a>
                                              </li>
                                          </ul>
                                          <div class="tab-content">
                                              <div class="tab-pane active" id="data_diri">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Nama Sesuai ID</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nama_karyawan" id="nama_karyawan" class="pertama form-control" placeholder="Nama Sesuai ID" autofocus required="true">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tempat Lahir</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" placeholder="Tempat Lahir" autofocus required="true">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tanggal Lahir</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="tgl_lahir" id="tgl_lahir" class="date-picker form-control" placeholder="Tanggal Lahir" autofocus required="true">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">NIK</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nik" id="nik" class="form-control" placeholder="Nomor Induk Kependudukan"  required="true">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Alamat Sesuai ID</label>
                                                    <div class=" col-md-8" >
                                                      <textarea class="form-control" name="alamat_identitas" required></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Alamat Sekarang</label>
                                                    <div class=" col-md-8" >
                                                      <textarea class="form-control" name="alamat_sekarang" required></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">NPWP</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="npwp" id="npwp" class="form-control" placeholder="NPWP" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status Pajak</label>
                                                    <div class="col-md-8" >
                                                      <select name="status_pajak" id="status_pajak" >
                                                            @foreach($pajak as $pajak)
                                                              <option value="{{ $pajak->pajak }}">{{ $pajak->pajak }}</option>
                                                            @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tanggungan</label>
                                                    <div class=" col-md-8" >
                                                      <input type="number" name="tanggungan" id="tanggungan" class="form-control" placeholder="Banyaknya Tanggungan" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Jenis Kelamin</label>
                                                    <div class="col-md-8" >
                                                      <select name="gender" id="gender" class="form-control" required >
                                                          <option value="L">Laki-Laki</option>
                                                          <option value="P">Perempuan</option>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status Nikah</label>
                                                    <div class=" col-md-8" >
                                                      <select name="status_nikah" id="status_nikah" class="form-control" >
                                                          <option  value="Lajang">Lajang</option>
                                                          <option  value="Menikah">Menikah</option>
                                                          <option  value="Sendiri">Sendiri</option>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Agama</label>
                                                    <div class=" col-md-8" >
                                                      <select name="agama" id="agama" class="form-control" required>
                                                          <option value="Islam">Islam</option>
                                                          <option value="Kristen">Kristen</option>
                                                          <option value="Katholik">Katolik</option>
                                                          <option value="Budha">Buda</option>
                                                          <option value="Hindu">Hindu</option>
                                                          <option value="Khonghucu">Khonghucu</option>
                                                          <option value="Aliran Kepercayaan">Aliran Kepercayaan</option>
                                                      </select>
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_kontak">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">No Telpon</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nomor_telpon" id="nomor_telpon" class="form-control" placeholder="Nomor Telpon" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">No Handphone</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nomor_ponsel" id="nomor_ponsel" class="form-control" placeholder="Nomor Handphone"  required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Email Pribadi</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="email_pribadi" id="email_pribadi" class="form-control" placeholder="Email Pribadi"  required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nama_kontak_darurat" id="nama_kontak_darurat" class="form-control" placeholder="Nama Kontak Darurat" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input type="tel" name="nomor_kontak_darurat" id="nomor_kontak_darurat" class="form-control" placeholder="Nomor Kontak Darurat" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Hubungan Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="hubungan_kontak_darurat" id="hubungan_kontak_darurat" class="form-control" placeholder="Hubungan Kontak Darurat"  required >
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_kepegawaian">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Pegawai</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="npp" id="npp" class="form-control" placeholder="NPP" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">User Absen</label>
                                                    <div class=" col-md-8" >
                                                      <select class="form-control bs-select" data-live-search='true' name="userid">
                                                            <option value="0">Belum Ada</option>
                                                          @foreach($userinfo as $userinfo)
                                                            <option value="{{ $userinfo->userid }}">{{ $userinfo->badgenumber }}/{{ $userinfo->nama }}</option>
                                                          @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tanggal Bergabung</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="tgl_gabung" id="tgl_gabung" class="date-picker  form-control" placeholder="Tanggal Bergabung">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Email Kantor</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" autocomplete="new-password" name="email" id="email" class="form-control" placeholder="Email Kantor" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Password</label>
                                                    <div class=" col-md-8" >
                                                      <input type="password" autocomplete="new-password" name="passwordnya" id="passwordnya" class="form-control" placeholder="Password untuk akses sistem" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Jabatan</label>
                                                    <div class=" col-md-8" >
                                                      <select name="jabatan_id" id="jabatan_id" class="form-control" required>
                                                          <option value="0">Pilih</option>
                                                          @foreach($jabatan as $jabatan)
                                                            <option value="{{ $jabatan->id }}">{{ $jabatan->nama_jabatan }}</option>
                                                          @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Divisi</label>
                                                    <div class=" col-md-8" >
                                                      <select name="divisi_id" id="divisi_id" class="form-control" required>
                                                          <option value="0">Pilih</option>
                                                          @foreach($divisi as $divisi)
                                                            <option value="{{ $divisi->id }}">{{ $divisi->nama_divisi }}</option>
                                                          @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Kantor</label>
                                                    <div class=" col-md-8" >
                                                      <select name="kantor_id" id="kantor_id" class="form-control" required>
                                                          <option value="0">Pilih</option>
                                                          @foreach($kantor as $kantor)
                                                            <option value="{{ $kantor->id }}">{{ $kantor->nama_kantor }}</option>
                                                          @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Pangkat</label>
                                                    <div class=" col-md-8" >
                                                      <select name="pangkat_id" id="pangkat_id" class="form-control" required>
                                                          <option value="0">Pilih</option>
                                                          @foreach($pangkat as $pangkat)
                                                            <option value="{{ $pangkat->id }}">{{ $pangkat->nama_pangkat }}</option>
                                                          @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Role</label>
                                                    <div class=" col-md-8" >
                                                      <select name="role" id="role" class="form-control" required>
                                                            <option value="user">User</option>
                                                            <option value="admin">Admin</option>
                                                            <option value="sekretaris">Sekretaris</option>
                                                            <option value="finance">Finance</option>
                                                            <option value="marketing">Marketing</option>
                                                            <option value="direktur">Direktur</option>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status</label>
                                                    <div class=" col-md-8" >
                                                      <select name="status_kerja" id="status_kerja" class="form-control" required>
                                                          @foreach($status as $status)
                                                            <option value="{{ $status->status_karyawan }}">{{ $status->status_karyawan }}</option>
                                                          @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_bank">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nama Bank</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nama_bank" id="nama_bank" class="form-control" placeholder="Nama Bank"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nama Rekening</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nama_rekening" id="nama_rekening" class="form-control" placeholder="Nama Rekening"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Rekening</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nomor_rekening_payrol" id="nomor_rekening_payrol" class="form-control" placeholder="Nomor Rekening"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor CIF</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nomor_cif" id="nomor_cif" class="form-control" placeholder="Nomor CIF" >
                                                    </div>
                                                </div>
                                              </div>
                                          </div>
                                      </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_karyawan();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Daftar karyawan</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer","orderable":false },
                      { "data": "nama_karyawan","orderable":false },
                      { "data": "npp","orderable":false },
                      { "data": "nomor_ponsel","orderable":false},
                      { "data": "tambahan","orderable":false},
                      { "data": "action","orderable":false }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">No.</th>
                        <th class="min-mobile-p">Nama</th>
                        <th class="min-tablet-l">NPP</th>
                        <th class="min-tablet-l">Ponsel</th>
                        <th class="none"></th>
                        <th class="none">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                      <a href="{{ url('/') }}/karyawan/export" type="button" class="simpan_input btn green">Excel</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
