<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Detail Karyawan</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" class="form-horizontal" id="form_update_karyawan" >
                    {{ csrf_field() }}
                    <div class="form-body">
                      <div class="tabbable-custom ">
                                          <ul class="nav nav-tabs ">
                                              <li class="active">
                                                  <a href="#data_diri2" data-toggle="tab">Data Diri</a>
                                              </li>
                                              <li>
                                                  <a href="#info_kontak2" data-toggle="tab">Kontak</a>
                                              </li>
                                              <li>
                                                  <a href="#info_kepegawaian2" data-toggle="tab">Kepegawaian</a>
                                              </li>
                                              <li>
                                                  <a href="#info_bank2" data-toggle="tab">Bank</a>
                                              </li>
                                          </ul>
                                          <div class="tab-content">
                                              <div class="tab-pane active" id="data_diri2">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Nama Sesuai ID</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" value="{{ $karyawan->nama_karyawan }}" name="nama_karyawan" id="nama_karyawan" class="pertama form-control" placeholder="Nama Lengkap" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tempat Lahir</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" name="tempat_lahir" value="{{ $karyawan->tempat_lahir }}" id="tempat_lahir" class="form-control" placeholder="Tempat Lahir" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tanggal Lahir</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" name="tgl_lahir" value="{{ $karyawan->tgl_lahir }}" id="tgl_lahir" class="date-picker form-control" placeholder="Tanggal Lahir" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">NIK</label>
                                                    <div class=" col-md-8" >
                                                      <input  readonly type="text" name="nik" id="nik" value="{{ $karyawan->nik }}" class="form-control" placeholder="Nomor Induk Kependudukan"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Alamat Sesuai ID</label>
                                                    <div class=" col-md-8" >
                                                      <textarea readonly class="form-control"  name="alamat_identitas" >{{ $karyawan->alamat_identitas }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Alamat Sekarang</label>
                                                    <div class=" col-md-8" >
                                                      <textarea readonly class="form-control"  name="alamat_sekarang" >{{ $karyawan->alamat_sekarang }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">NPWP</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" name="npwp" value="{{ $karyawan->npwp }}" id="npwp" class="form-control" placeholder="NPWP"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status Pajak</label>
                                                    <div class="col-md-8" >
                                                      <input type="text" value="{{ $karyawan->status_pajak }}" readonly="true" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tanggungan</label>
                                                    <div class=" col-md-8" >
                                                      <input type="number" value="{{ $karyawan->tanggungan }}"  class="form-control" placeholder="Banyaknya Tanggungan" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Jenis Kelamin</label>
                                                    <div class="col-md-8" >
                                                      <select disabled name="gender" id="gender" class="form-control" >
                                                          <option @if($karyawan->gender=='L') selected @endif value="L">Laki-Laki</option>
                                                          <option @if($karyawan->gender=='P') selected @endif  value="P">Perempuan</option>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status Nikah</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" value="{{ $karyawan->status_nikah }}" readonly="true" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Agama</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" value="{{ $karyawan->agama }}" readonly="true" />
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_kontak2">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">No Telpon</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" value="{{ $karyawan->nomor_telpon }}" name="nomor_telpon" id="nomor_telpon" class="form-control" placeholder="Nomor Telpon" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">No Handphone</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" value="{{ $karyawan->nomor_ponsel }}" name="nomor_ponsel" id="nomor_ponsel" class="form-control" placeholder="Nomor Handphone" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Email Pribadi</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" name="email_pribadi" value="{{ $karyawan->email_pribadi }}" id="email_pribadi" class="form-control" placeholder="Email Pribadi"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" name="nama_kontak_darurat" value="{{ $karyawan->nama_kontak_darurat }}" id="nama_kontak_darurat" class="form-control" placeholder="Nama Kontak Darurat" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="tel" name="nomor_kontak_darurat" value="{{ $karyawan->nomor_kontak_darurat  }}" id="nomor_kontak_darurat" class="form-control" placeholder="Nomor Kontak Darurat" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Hubungan Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" name="hubungan_kontak_darurat" value="{{ $karyawan->hubungan_kontak_darurat }}" id="hubungan_kontak_darurat" class="form-control" placeholder="Hubungan Kontak Darurat" >
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_kepegawaian2">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Pegawai</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" name="npp" id="npp" value="{{ $karyawan->npp }}" class="form-control" placeholder="NPP" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Email Kantor</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" name="email" id="email" value="{{ $karyawan->email }}" class="form-control" placeholder="Email Kantor" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Password</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="password" name="passwordnya" id="passwordny" class="form-control" placeholder="Password untuk akses sistem" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Jabatan</label>
                                                    <div class=" col-md-8" >
                                                      <select disabled name="jabatan_id" id="jabatan_id" class="form-control" >
                                                          <option value="0">Pilih</option>
                                                          @foreach($jabatan as $jabatan)
                                                            <option @if($jabatan->id==$karyawan->jabatan_id) selected @endif value="{{ $jabatan->id }}">{{ $jabatan->nama_jabatan }}</option>
                                                          @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Divisi</label>
                                                    <div class=" col-md-8" >
                                                      <select disabled name="divisi_id" id="divisi_id" class="form-control" >
                                                          <option value="0">Pilih</option>
                                                          @foreach($divisi as $divisi)
                                                            <option @if($divisi->id==$karyawan->divisi_id) selected @endif value="{{ $divisi->id }}">{{ $divisi->nama_divisi }}</option>
                                                          @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Kantor</label>
                                                    <div class=" col-md-8" >
                                                      <select disabled name="kantor_id" id="kantor_id" class="form-control" >
                                                          <option value="0">Pilih</option>
                                                          @foreach($kantor as $kantor)
                                                            <option @if($kantor->id==$karyawan->kantor_id) selected @endif value="{{ $kantor->id }}">{{ $kantor->nama_kantor }}</option>
                                                          @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Role</label>
                                                    <div class=" col-md-8" >
                                                      <select disabled name="role" id="role" class="form-control" required>
                                                            <option @if($karyawan->role=='user') selected @endif value="user">User</option>
                                                            <option  @if($karyawan->role=='admin') selected @endif value="admin">Admin</option>
                                                            <option  @if($karyawan->role=='sekretaris') selected @endif value="sekretaris">Sekretaris</option>
                                                            <option  @if($karyawan->role=='marketing') selected @endif value="marketing">Marketing</option>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Pangkat</label>
                                                    <div class=" col-md-8" >
                                                      <select disabled name="pangkat_id" id="pangkat_id" class="form-control">
                                                          <option value="0">Pilih</option>
                                                          @foreach($pangkat as $pangkat)
                                                            <option @if($pangkat->id==$karyawan->pangkat_id) selected @endif value="{{ $pangkat->id }}">{{ $pangkat->nama_pangkat }}</option>
                                                          @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status</label>
                                                    <div class=" col-md-8" >
                                                      <select disabled name="status_kerja" id="status_kerja" class="form-control" required>
                                                          @foreach($status as $status)
                                                            <option @if($karyawan->status_kerja==$status->status_karyawan) selected @endif value="{{ $status->status_karyawan }}">{{ $status->status_karyawan }}</option>
                                                          @endforeach
                                                      </select>
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_bank2">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nama Bank</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" value="{{ $karyawan->nama_bank }}" name="nama_bank" id="nama_bank" class="form-control" placeholder="Nama Bank"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nama Rekening</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" value="{{ $karyawan->nama_rekening }}" name="nama_rekening" id="nama_rekening" class="form-control" placeholder="Nama Rekening"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Rekening</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" value="{{ $karyawan->nomor_rekening_payrol }}" name="nomor_rekening_payrol" id="nomor_rekening_payrol" class="form-control" placeholder="Nomor Rekening"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor CIF</label>
                                                    <div class=" col-md-8" >
                                                      <input readonly type="text" value="{{ $karyawan->nomor_cif }}" name="nomor_cif" id="nomor_cif" class="form-control" placeholder="Nomor CIF" >
                                                    </div>
                                                </div>
                                              </div>
                                          </div>
                                      </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" data-dismiss="modal" class="btn default">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
