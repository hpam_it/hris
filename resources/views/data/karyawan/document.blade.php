<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Document Pendukung</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
          <form method="POST" class="form-horizontal" enctype="multipart/form-data" id="form_modal"  action="{{ url('data-karyawan') }} ">
               {{ csrf_field() }}
               <input type="hidden" name="aksi" value="simpan_dokumen" />
               <input type="hidden" name="karyawan_id" value="{{ $id_karyawan }}" />
               <div class="form-body">
                 <div class="form-group">
                     <label class="control-label col-md-4">Nama Dokument</label>
                     <div class=" col-md-8" >
                       <input type="text" name="nama_dokumen" id="nama_dokumen" class="form-control" placeholder="Nama Dokument" required >
                     </div>
                 </div>
                 <div class="form-group">
                   <label class="control-label col-md-4">Nomor</label>
                     <div class=" col-md-8" >
                       <input type="text" name="nomor_dokumen" id="nomor_dokumen" class="form-control" placeholder="Nomor Dokument" required >
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="control-label col-md-4">Scan</label>
                     <div class=" col-md-8" >
                       <input type="file" name="scan_dokumen" id="scan_dokumen" class="form-control"  required >
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="control-label col-md-4">Tanggal berakhir</label>
                     <div class=" col-md-8" >
                       <input type="text" name="tanggal_berakhir" id="tanggal_berakhir" class="form-control date-picker"  required >
                     </div>
                 </div>
               </div>
               <div class="form-actions">
                   <button type="button" onclick="simpan_dokumen();" class="simpan_input btn green">Simpan</button>
                   <button type="button" class="btn default">Reset</button>
               </div>
           </form>

         </div>
       </div>
       <div class="row">
         <div class="col-md-12 tempat-table-modal">
           List Dokumen Pendukung
           <table id="table-modal"
             class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
           <thead>
           <tr>
               <th class="all">NO</th>
               <th class="min-mobile-p">Nama Document</th>
               <th class="min-tablet-l">Nomor</th>
               <th class="min-tablet-l">Berakhir</th>
               <th class="none">Scan document</th>
               <th class="min-tablet-l">Action</th>
           </tr>
           </thead>
           <?php $nomor=1; ?>
           <tbody>

             @foreach($dokument as $dokument)
             <tr>
              <td>{{ $nomor++ }}</td>
              <td>{{ $dokument->nama_dokumen }}</td>
              <td>{{ $dokument->nomor_dokumen }}</td>
              <td>{{ $dokument->tanggal_berakhir }}</td>
              <td><a target="_blank" href="{{  Storage::url($dokument->scan_dokumen) }}" >Download</a></td>
              <td>
                <button  data-title="Hapus Dokumen ?" data-toggle="confirmation" data-placement="left" data-url="{{ url("data-karyawan") }}/{{ $dokument->id }}?aksi=hapus_dokumen" class="konfirmasi hapus-dokumen btn btn-md btn-icon-only red">
                    <i class="fa fa-trash"></i>
                </button>
              </td>
            </tr>
             @endforeach
           </tbody>
           </table>
         </div>
       </div>
</div>
<div class="modal-footer">

</div>
<script>
$("#table-modal").dataTable();
$('.date-picker').datepicker({
  rtl: App.isRTL(),
  orientation: "left",
  format:"yyyy-mm-dd",
  autoclose: true
});
$('.konfirmasi').on('click', function () {
  $(this).confirmation('show');

  $('.hapus-dokumen').on('confirmed.bs.confirmation', function () {
      var url=$(this).attr('data-url');
      hapus_input_modal(url);
      //alert(url);
  });

} );
</script>
