<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Data Keluarga</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
          <form method="POST" class="form-horizontal" id="form_modal"  action="{{ url('data-karyawan') }} ">
               {{ csrf_field() }}
               <input type="hidden" name="aksi" value="simpan_keluarga">
               <input type="hidden" name="karyawan_id" value="{{ $id_karyawan }}" />
               <div class="form-body">
                 <div class="form-group">
                     <label class="control-label col-md-4">Nama</label>
                     <div class=" col-md-8" >
                       <input type="text" name="nama_anggota" id="nama_anggota" class="form-control" placeholder="Nama Anggota" required >
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="control-label col-md-4">NIK</label>
                     <div class=" col-md-8" >
                       <input type="text" name="nik" id="nik_modal" class="form-control" placeholder="NIK" required >
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="control-label col-md-4">Hubungan</label>
                     <div class=" col-md-8" >
                       <select name="hubungan" id="hubungan" >
                        <option value="Suami">Suami</option>
                        <option value="Istri">Istri</option>
                        <option value="Anak">Anak</option>
                        <option Value="Bapak">Bapak</option>
                        <option Value="Ibu">Ibu</option>
                        <option value="Saudara">Saudara</option>
                        <option value="Cucu">Cucu</option>
                        <option value="Kakek">Kakek</option>
                        <option value="Nenek">Nenek</option>
                      </select>
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="control-label col-md-4">Urutan</label>
                     <div class=" col-md-8" >
                       <input type="number" name="urutan" id="urutan" class="form-control" placeholder="" required >
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="control-label col-md-4">Jenis Kelamin</label>
                     <div class=" col-md-8" >
                       <select name="gender" id="gender" >
                          <option value="Laki-laki"> Laki - Laki</option>
                          <option value="Perempuan"> Perempuan</option>
                       </select>
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="control-label col-md-4">Tempat Lahir</label>
                     <div class=" col-md-8" >
                       <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" placeholder="Tempat Lahir" required >
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="control-label col-md-4">Tanggal Lahir</label>
                     <div class=" col-md-8" >
                       <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control date-picker" placeholder="Tanggal Lahir" required >
                     </div>
                 </div>

               </div>
               <div class="form-actions">
                   <button type="button" onclick="simpan_keluarga();" class="simpan_input btn green">Simpan</button>
                   <button type="button" class="btn default">Reset</button>
               </div>
           </form>

         </div>
       </div>
       <div class="row">
         <div class="col-md-12 tempat-table-modal">
           List Anggota Keluarga
           <table id="table-modal"
             class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
           <thead>
           <tr>
               <th class="all">NO</th>
               <th class="min-mobile-p">Nama</th>
               <th class="min-tablet-l">Hubungan</th>
               <th class="none">Tempat Lahir</th>
               <th class="none">Tanggal lahir</th>
               <th class="none">Jenis Kelamin</th>
               <th class="none">Action</th>
           </tr>
           </thead>
           <tbody>
             <?php $nomor=1; ?>
             @foreach($keluarga as $keluarga)
             <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $keluarga->nama_anggota }}</td>
                <td>{{ $keluarga->hubungan }}</td>
                <td>{{ $keluarga->tempat_lahir }}</td>
                <td>{{ $keluarga->tgl_lahir }}</td>
                <td>{{ $keluarga->gender }}</td>
                <td>
                  <button onclick="hapus_input_modal('{{url('data-karyawan') }}/{{$keluarga->id}}?aksi=hapus_keluarga');" class="btn btn-md btn-icon-only red">
                      <i class="fa fa-trash"></i>
                  </button>
                </td>
             </tr>
             @endforeach
           </tbody>
           </table>
         </div>
       </div>
</div>
<div class="modal-footer">

</div>
<script>
$("#table-modal").dataTable();
$('.date-picker').datepicker({
rtl: App.isRTL(),
orientation: "left",
format:"yyyy-mm-dd",
autoclose: true
});
</script>
