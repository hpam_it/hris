@extends('home')
@section('title')
Data Diri Karyawan
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-user"></i>Data Diri</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" class="form-horizontal" id="form_update"  action="{{ url('data-diri') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="update_item">
                    <div class="form-body">
                      <div class="tabbable-custom ">
                                          <ul class="nav nav-tabs ">
                                              <li class="active">
                                                  <a href="#data_diri" data-toggle="tab">Data Diri</a>
                                              </li>
                                              <li>
                                                  <a href="#info_kontak" data-toggle="tab">Kontak</a>
                                              </li>
                                              <li>
                                                  <a href="#info_kepegawaian" data-toggle="tab">Kepegawaian</a>
                                              </li>
                                              <li>
                                                  <a href="#info_bank" data-toggle="tab">Bank</a>
                                              </li>
                                          </ul>
                                          <div class="tab-content">
                                              <div class="tab-pane active" id="data_diri">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Nama Sesuai ID</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  value="{{ $karyawan->nama_karyawan }}" class="pertama form-control" placeholder="Nama Lengkap" readonly />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tempat Lahir</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->tempat_lahir }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tanggal Lahir</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->tgl_lahir }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">NIK</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->nik }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Alamat Sesuai ID</label>
                                                    <div class=" col-md-8" >
                                                      <textarea class="form-control" readonly >{{ $karyawan->alamat_identitas }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Alamat Sekarang</label>
                                                    <div class=" col-md-8" >
                                                      <textarea class="form-control" readonly>{{ $karyawan->alamat_sekarang }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">NPWP</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->npwp }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Jenis Kelamin</label>
                                                    <div class="col-md-8" >
                                                      <select class="form-control" readonly >
                                                          <option value="L" @if($karyawan->gender=='L') selected @endif >Laki-Laki</option>
                                                          <option value="P" @if($karyawan->gender=='P') selected @endif >Perempuan</option>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status Nikah</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->status_nikah }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Agama</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->agama }}" readonly>
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_kontak">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">No Telpon</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->nomor_telpon }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">No Handphone</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->nomor_ponsel }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Email Pribadi</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->email_pribadi }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->nama_kontak_darurat }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->nomor_kontak_darurat }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Hubungan Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->hubungan_kontak_darurat }}" readonly>
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_kepegawaian">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Pegawai</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->npp }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Email Kantor</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->email }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Password</label>
                                                    <div class=" col-md-8" >
                                                      <input type="password" name="passwordnya" id="passwordnya" class="form-control" placeholder="Isi Form Ini Untuk Ganti Password Anda" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Jabatan</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $jabatan->nama_jabatan }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Divisi</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $divisi->nama_divisi }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Kantor</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $kantor->nama_kantor }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Pangkat</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $pangkat->nama_pangkat }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->status_kerja }}" readonly>
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_bank">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nama Bank</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->nama_bank }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nama Rekening</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->nama_rekening }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Rekening</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->nomor_rekening_payrol }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor CIF</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text"  class="form-control"  value="{{ $karyawan->nomor_cif }}" readonly>
                                                    </div>
                                                </div>
                                              </div>
                                          </div>
                                      </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="update_password();" class="simpan_input btn green">Update</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Daftar Dokumen Karyawan</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="min-mobile-p">Nama Dokumen</th>
                        <th class="min-tablet-l">Keterangan</th>
                        <th class="min-tablet-l">Data</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php $nomor=1;?>
                      @foreach($dokument as $dokument)
                      <tr>
                       <td>{{ $dokument->nama_dokumen }}</td>
                       <td>{{ $dokument->nomor_dokumen }}</td>
                       <td><a target="_blank" href="{{  Storage::url($dokument->scan_dokumen) }}" >Download</a></td>
                     </tr>
                      @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
