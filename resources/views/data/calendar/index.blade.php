@extends('home')
@section('title')
Data Calendar
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-5">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Input Data Kalender</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  class="form-horizontal" action="{{ url('data-calendar') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal</label>
                            <div class="col-md-9">
                              <input type="text" name="tanggal" id="tanggal" class="date-picker pertama form-control" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Keterangan</label>
                            <div class="col-md-9">
                              <input type="text" name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan Libur" required="true">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_calendar();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
            <!-- list calender -->

            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Daftar Hari Libur</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer" },
                      { "data": "tanggal",render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" ) },
                      { "data": "keterangan" },
                      { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">NO</th>
                        <th class="min-mobile-p">Tanggal</th>
                        <th class="min-tablet-l">Keterangan</th>
                        <th class="none">Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div class="col-md-7">
            <div class="portlet box blue" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Kalendar</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <div id="calendar" class="has-toolbar fc fc-ltr fc-unthemed">

	                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
