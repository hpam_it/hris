<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Libur</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
          <form method="PATCH" id="form_update"  action="{{ url('data-calendar') }}/{{ $item->id }} ">
               {{ csrf_field() }}
               <input type="hidden" name="aksi" value="update_item">
               <div class="form-body">
                   <div class="form-group">
                       <label class="control-label col-md-3">Tanggal</label>
                       <div class="col-md-9">
                         <input type="text" name="tanggal" value="{{ $item->tanggal }}" id="tanggal" class="date-picker pertama form-control" required="true">
                       </div>
                   </div>
                   <div class="form-group">
                       <label class="control-label col-md-3">Keterangan</label>
                       <div class="col-md-9">
                         <input type="text" name="keterangan" id="keterangan" value="{{ $item->keterangan }}" class="form-control" placeholder="Keterangan Libur" required="true">
                       </div>
                   </div>
               </div>
               <div class="form-actions">
                   <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
               </div>
           </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
<script>
$('.date-picker').datepicker();
</script>
