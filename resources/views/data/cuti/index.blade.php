@extends('home')
@section('title')
Data Jenis Cuti
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Input Data Jenis Cuti</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  action="{{ url('data-cuti') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Cuti</label>
                            <input type="text" name="nama_cuti" id="nama_cuti" class="pertama form-control" placeholder="Nama Cuti" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Jumlah Hari</label>
                            <input type="number" name="jumlah" id="jumlah" class="form-control" placeholder="Jumlah" required="true">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Daftar Jenis Cuti</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer" },
                      { "data": "nama_cuti" },
                      { "data": "jumlah" },
                      { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">No.</th>
                        <th class="min-mobile-p">Nama Cuti</th>
                        <th class="min-tablet-l">Jumlah Hari</th>
                        <th class="none">Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
