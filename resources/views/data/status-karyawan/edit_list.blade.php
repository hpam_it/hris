<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Status Karyawan</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  action="{{ url('data-status-karyawan') }}/{{ $item->id }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="update_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="control-label">Status Karywan</label>
                          <input type="text" value="{{ $item->status_karyawan }}" name="status_karyawan" id="status_karyawan" class="pertama form-control" placeholder="Status karyawan" autofocus required="true">
                      </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
