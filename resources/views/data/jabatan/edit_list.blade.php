<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Jabatan</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  action="{{ url('data-jabatan') }}/{{ $item->id }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="update_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="control-label">Nama Jabatan</label>
                          <input type="text" value="{{ $item->nama_jabatan }}" name="nama_jabatan" id="nama_jabatan" class="pertama form-control" placeholder="Nama Jabatan" autofocus required="true">
                      </div>
                      <div class="form-group">
                          <label class="control-label">Kode Jabatan</label>
                          <input type="text" value="{{ $item->kode_jabatan }}" name="kode_jabatan" id="kode_jabatan" class="form-control" placeholder="Kode Jabatan" required="true">
                      </div>
                      <div class="form-group">
                          <label class="control-label">Wewenang</label>
                          <textarea  name="wewenang" id="wewenang" class="form-control" required="true">{{ $item->wewenang }}</textarea>
                      </div>
                      <div class="form-group">
                          <label class="control-label">Tanggung Jawab</label>
                          <textarea name="tanggung_jawab" id="tanggung_jawab" class="form-control" required="true">{{ $item->tanggung_jawab }}</textarea>
                      </div>
                      <!-- <div class="form-group">
                          <label class="control-label">Atasan</label>
                          <select name="atasan" id="atasan" class="form-control bs-select">
                              <option value="0">Pilih</option>
                              @foreach($jabatan as $jabatan)
                                <option @if($jabatan->id==$item->atasan ) selected @endif value="{{ $jabatan->id }}">{{ $jabatan->nama_jabatan }}</option>
                              @endforeach
                          </select>
                      </div> -->
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
