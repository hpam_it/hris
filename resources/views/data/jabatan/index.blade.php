@extends('home')
@section('title')
Data Jabatan
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Input Data Jabatan</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  action="{{ url('data-jabatan') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Jabatan</label>
                            <input type="text" name="nama_jabatan" id="nama_jabatan" class="pertama form-control" placeholder="Nama Jabatan" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kode Jabatan</label>
                            <input type="text" name="kode_jabatan" id="kode_jabatan" class="form-control" placeholder="Kode Jabatan" required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Wewenang</label>
                            <textarea  name="wewenang" id="wewenang" class="form-control" required="true"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tanggung Jawab</label>
                            <textarea name="tanggung_jawab" id="tanggung_jawab" class="form-control" required="true"></textarea>
                        </div>
                        <!-- <div class="form-group">
                            <label class="control-label">Atasan</label>
                            <select name="atasan" id="atasan" class="form-control bs-select">
                                <option value="0">Pilih</option>
                                @foreach($jabatan as $jabatan)
                                  <option value="{{ $jabatan->id }}">{{ $jabatan->nama_jabatan }}</option>
                                @endforeach
                            </select>
                        </div> -->
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Daftar Jabatan</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer" },
                      { "data": "nama_jabatan" },
                      { "data": "kode_jabatan" },
                      { "data": "wewenang" },
                      { "data": "tanggung_jawab" },
                      { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">No.</th>
                        <th class="min-mobile-p">Nama Jabatan</th>
                        <th class="min-tablet-l">Kode</th>
                        <th class="none">Wewenang</th>
                        <th class="none">Tanggung Jawab</th>
                        <th class="none">Action</th>


                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
