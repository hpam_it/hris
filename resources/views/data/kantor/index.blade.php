@extends('home')
@section('title')
Data Kantor
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Input Data Kantor</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  action="{{ url('data-kantor') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Kantor</label>
                            <input type="text" name="nama_kantor" id="nama_kantor" class="pertama form-control" placeholder="Nama Kantor" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kode Kantor</label>
                            <input type="text" name="kode_kantor" id="kode_kantor" class="form-control" placeholder="Kode Kantor" required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kota</label>
                            <input type="text" name="nama_kota" id="nama_kota" class="form-control" placeholder="Kota" required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Alamat Kantor</label>
                            <textarea name="alamat_kantor" id="alamat_kantor" class="form-control" required="true"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Telpon Kantor</label>
                            <input type="tel" name="telpon_kantor" id="telpon_kantor" class="form-control"  required="true" />
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Daftar Kantor</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer" },
                      { "data": "nama_kantor" },
                      { "data": "kode_kantor" },
                      { "data": "nama_kota"},
                      { "data": "alamat_kantor"},
                      { "data": "telpon_kantor"},
                      { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">No.</th>
                        <th class="min-mobile-p">Nama Kantor</th>
                        <th class="min-tablet-l">Kode</th>
                        <th class="min-tablet-l">Kota</th>
                        <th class="min_tablet-l">Alamat</th>
                        <th class="min-tablet-l">telpon</th>
                        <th class="none">Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
