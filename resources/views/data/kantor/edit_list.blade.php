<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Kantor</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  action="{{ url('data-kantor') }}/{{ $item->id }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="update_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Kantor</label>
                            <input type="text" value="{{ $item->nama_kantor }}" name="nama_kantor" id="nama_kantor" class="pertama form-control" placeholder="Nama Kantor" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kode Kantor</label>
                            <input type="text" value="{{ $item->kode_kantor }}" name="kode_kantor" id="kode_kantor" class="form-control" placeholder="Kode Kantor" required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kota</label>
                            <input type="text" value="{{ $item->nama_kota }}" name="nama_kota" id="nama_kota" class="form-control" placeholder="Kota" required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Alamat Kantor</label>
                            <textarea name="alamat_kantor" id="alamat_kantor" class="form-control" required="true">{{ $item->alamat_kantor }}</textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Telpon Kantor</label>
                            <input type="tel" value="{{ $item->telpon_kantor }}" name="telpon_kantor" id="telpon_kantor" class="form-control"  required="true" />
                        </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
