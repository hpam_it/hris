<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Divisi</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  action="{{ url('data-divisi') }}/{{ $item->id }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="update_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Divisi</label>
                            <input type="text" value="{{ $item->nama_divisi }}" name="nama_divisi" id="nama_divisi" class="pertama form-control" placeholder="Nama Divisi" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kode Divisi</label>
                            <input type="text" value="{{ $item->kode_divisi }}" name="kode_divisi" id="kode_divisi" class="form-control" placeholder="Kode Divisi" required="true">
                        </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
