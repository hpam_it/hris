<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Struktur</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  action="{{ url('data-struktur') }}/{{ $struktur->id }} ">
             {{ csrf_field() }}
             <input type="hidden" name="aksi" value="simpan_item">
             <div class="form-body">
                 <div class="form-group">
                     <label class="control-label">Nama Jabatan</label>
                     <select name="jabatan_id" id="jabatan_id" class="form-control bs-select" data-live-search="true" >
                         <option value="0">Pilih</option>
                         @foreach($jabatan as $jabatans)
                           <option @if($struktur->jabatan_id == $jabatans->id ) selected @endif value="{{ $jabatans->id }}">{{ $jabatans->nama_jabatan }}</option>
                         @endforeach
                     </select>
                 </div>
                 <div class="form-group">
                     <label class="control-label">Nama Divisi</label>
                     <select name="divisi_id" id="divisi_id" class="form-control bs-select" data-live-search="true" >
                         <option value="0">Pilih</option>
                         @foreach($divisi as $divisis)
                           <option @if($struktur->divisi_id == $divisis->id ) selected @endif value="{{ $divisis->id }}">{{ $divisis->nama_divisi }}</option>
                         @endforeach
                     </select>
                 </div>
                 <div class="form-group">
                     <label class="control-label">Kantor</label>
                     <select name="kantor_id" id="kantor_id" class="form-control bs-select" data-live-search="true" >
                         <option value="0">Pilih</option>
                         @foreach($kantor as $kantors)
                           <option @if($struktur->kantor_id == $kantors->id ) selected @endif value="{{ $kantors->id }}">{{ $kantors->nama_kantor }}</option>
                         @endforeach
                     </select>
                 </div>
                 <h3 class="form-section">Atasan</h3>
                 <div class="form-group">
                     <label class="control-label">Nama Jabatan</label>
                     <select name="jabatan_atasan" id="jabatan_atasan" class="form-control bs-select" data-live-search="true" >
                         <option value="0">Pilih</option>
                         @foreach($jabatan as $jabatan)
                           <option @if($struktur->jabatan_atasan == $jabatan->id ) selected @endif value="{{ $jabatan->id }}">{{ $jabatan->nama_jabatan }}</option>
                         @endforeach
                     </select>
                 </div>
                 <div class="form-group">
                     <label class="control-label">Nama Divisi</label>
                     <select name="divisi_atasan" id="divisi_atasan" class="form-control bs-select" data-live-search="true" >
                         <option value="0">Pilih</option>
                         @foreach($divisi as $divisi)
                           <option @if($struktur->divisi_atasan == $divisi->id ) selected @endif value="{{ $divisi->id }}">{{ $divisi->nama_divisi }}</option>
                         @endforeach
                     </select>
                 </div>
                 <div class="form-group">
                     <label class="control-label">Kantor</label>
                     <select name="kantor_atasan" id="kantor_atasan" class="form-control bs-select" data-live-search="true" >
                         <option value="0">Pilih</option>
                         @foreach($kantor as $kantor)
                           <option @if($struktur->kantor_atasan == $kantor->id ) selected @endif value="{{ $kantor->id }}">{{ $kantor->nama_kantor }}</option>
                         @endforeach
                     </select>
                 </div>
             </div>
             <div class="form-actions">
                 <button type="button" onclick="update_input();" class="simpan_input btn green">Submit</button>
                 <button data-dismiss="modal" type="button" class="btn default">Cancel</button>
             </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
