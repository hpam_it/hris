@extends('home')
@section('title')
Log Absensi | {{ $nama_karyawan }} | {{ $npp }}
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-md-offset-1 col-md-10">
        <div class="portlet box blue" >
            <div class="portlet-title">
                <div class="caption">
                <i class=" icon-grid"></i>Kalender Absensi</div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body" id="list_input" >
                <div id="calendar-absen" class="has-toolbar fc fc-ltr fc-unthemed">
                 </div>
            </div>
        </div>
      </div>
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Log Absensi</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-absen"
                    data-kolom='[
                        {"data":"tanggal","orderable":false,render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" )},
                        {"data":"hari","orderable":false},
                        {"data":"masuk","orderable":false},
                        {"data":"pulang","orderable":false},
                        {"data":"cek_in","orderable":false},
                        {"data":"cek_out","orderable":false},
                        {"data":"jam_kerja","orderable":false},
                        {"data":"keterangan","orderable":false}
                    ]'
                      action="{{ url('logabsen') }}"
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                      <tr>
                          <th colspan="6">
                            Tanggal :
                            <input style="padding:4px;" type="text" class="date-picker" id="absen_start">
                            S/D
                            <input style="padding:4px; margin-right:10px;"Type="text" class="date-picker" id="absen_end">

                            <button type="button" onclick="filter_absen(listabsen)" class="btn btn-md blue" >Filter</button>
                          </th>
                      </tr>
                      <tr>
                          <th class="min-mobile-p">Tanggal</th>
                          <th class="min-mobile-p">Hari</th>
                          <th class="none">Masuk</th>
                          <th class="none">Pulang</th>
                          <th class="min-mobile-p">Jam Masuk</th>
                          <th class="min-mobile-p">Jam Keluar</th>
                          <th class="min-tablet-l">Jam Kerja</th>
                          <th class="min-tablet-l">Keterangan</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>
</div>
@endsection
