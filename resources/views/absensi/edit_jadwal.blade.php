<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Jadwal</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
          <form class="form-horizontal" method="PATCH" id="form_update"  action="{{ url('absensi') }}/{{ $jadwal->id }} ">
               {{ csrf_field() }}
               <input type="hidden"  name="aksi" value="update_jadwal">
               <div class="form-body">
                 <div class="col-md-6">
                     <div class="form-group">
                         <label class="control-label col-md-3">Jam Kerja</label>
                         <div class="col-md-9">
                             <input type="text" value="{{ $jadwal->nama_jamkerja }}" class="form-control pertama" placeholder="Nama Jam Kerja" name="nama_jamkerja" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="control-label col-md-3">Masuk (Jam)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" value="{{ $jadwal->masuk }}" name="masuk" required=""/>
                         </div>
                     </div>
                     <div class=form-group>
                         <label class="control-label col-md-3">Pulang (Jam)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" value="{{ $jadwal->pulang }}" name="pulang" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="control-label col-md-3">Toleransi (Jam)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" value="{{ $jadwal->toleransi }}" name="toleransi" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="control-label col-md-3">Max-Pulang (Jam)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" value="{{ $jadwal->maxpulang }}" name="maxpulang" required=""/>
                         </div>
                     </div>

                   </div>
                   <div class="col-md-6">

                     <div class="form-group">
                         <label class="control-label col-md-3">Istirahat (waktu)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" value="{{ $jadwal->istirahat }}" name="istirahat" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="control-label col-md-3">Mulai Istirahat (Jam)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" name="jam_istirahat" value="{{ $jadwal->jam_istirahat }}" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="control-label col-md-3">Total Kerja (waktu)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" value="{{ $jadwal->jam_normal }}" name="jam_normal" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="control-label col-md-3">Masa Berlaku</label>
                         <div class="col-md-4">
                             <input type="text" class="form-control date-picker" value="{{ $jadwal->tgl_mulai }}" name="tgl_mulai" required=""/>
                         </div>
                         <div class="col-md-1 text-center">
                           S/D
                         </div>
                         <div class="col-md-4">
                             <input type="text" class="form-control date-picker" value="{{ $jadwal->tgl_selesai }}" name="tgl_selesai" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                       <div class="col-md-12 text-right">
                         <button type="button" onclick="update_input();" class="simpan_input btn green">Submit</button>
                         <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                       </div>
                     </div>
                   </div>
             </div>

           </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
<script>
$('.date-picker').datepicker({
rtl: App.isRTL(),
orientation: "left",
format:"yyyy-mm-dd",
autoclose: true
});

$('.timepicker').timepicker({
      autoclose: true,
      minuteStep: 5,
      showSeconds: false,
      showMeridian: false
  });
</script>
