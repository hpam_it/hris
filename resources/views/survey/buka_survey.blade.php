<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">{{$nama_survey}}</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="POST" id="ikut_survey"  class="form-horizontal" action="{{ url('ikut-survey') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="terima" />
                    <input type="hidden" name="group_id" value="{{ $group_id }}" />
                    <input type="hidden" name="target" value="{{ $target }}" />
                    <div class="form-body">


                      @foreach($survey as $survey)
                      <div class="form-group form-md-radios">
                          <?php
                            $label_soal = str_replace("{nama}","<b>".$nama_target."</b>",$survey->pertanyaan);
                          ?>
                          <div class="col-md-12"><?= $label_soal ?></div>
                          @if($survey->option == NULL)
                          <?php
                            $idnya = "soal-".$survey->id_soal;
                              $nomor++;
                          ?>
                            <div class="col-md-12">
                              <textarea id="<?= $idnya."-".$nomor; ?>" name="<?= $idnya; ?>" class="form-control" required="true"></textarea>
                            </div>
                          @else
                            <div class="col-md-12">
                              <label class="radio-inline">
                                <b>{{ $survey->label_bawah }}</b>
                              </label>
                              <?php
                                $nomor=0;
                                $idnya = "soal-".$survey->id_soal;
                                $option = explode('|',$survey->option);
                                foreach ($option as $key) {
                                $nomor++;
                                $jawaban=explode(':',$key);

                              ?>
                                <label class="radio-inline">
                                  <input type="radio" id="<?= $idnya."-".$nomor; ?>" name="<?= $idnya; ?>" value="<?= $jawaban[0]; ?>" >
                                  <?= $jawaban[1] ?>
                                </label>
                              <?php
                                }

                               ?>
                               <label class="radio-inline">
                                 <b>{{ $survey->label_atas }}</b>
                               </label>
                            </div>
                          @endif


                      </div>
                      @endforeach
                    </div>

            </form>
        </div>
    </div>
</div>
<div class="modal-footer">
  <div class="form-actions">
  <button onclick="ikut_survey();" type="button" class="simpan_edit btn blue">Save</button>
  <button  type="button" class="btn default" data-dismiss="modal">Close</button>
  </div>
</div>
