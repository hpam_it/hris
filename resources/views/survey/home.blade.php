<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>HRIS | @yield('title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{ url('/') }}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ url('/') }}/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ url('/') }}/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ url('/') }}/assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ url('/') }}/assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="{{ url('/') }}/img/favicon.png" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-menu-fixed">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <!-- BEGIN HEADER -->
                    <div class="page-header">
                        <div class="page-header-top">
                            <div class="container">
                                <!-- BEGIN LOGO -->
                                <div class="page-logo">
                                       <span class="judul-logo"> Human Resource Database</span>
                                </div>
                                <!-- END LOGO -->
                                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                                <a href="javascript:;" class="menu-toggler"></a>
                                <!-- END RESPONSIVE MENU TOGGLER -->
                            </div>
                        </div>
                        <!-- BEGIN HEADER MENU -->
                        <div class="page-header-menu">
                            <div class="container-fluid">
                                <!-- BEGIN HEADER SEARCH BOX -->
                                <!-- END HEADER SEARCH BOX -->
                                <!-- BEGIN MEGA MENU -->
                                <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                                <div class="hor-menu  ">
                                    <ul class="nav navbar-nav">

                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ($halaman == 'dashboard') active @endif">
                                            <a href="{{ url('dashboard') }}"> <i class="glyphicon glyphicon-home"></i> Dashboard</a>
                                        </li>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ($halaman == 'data-diri') active @endif">
                                            <a href="{{ url('data-diri') }}"> <i class="glyphicon glyphicon-user"></i> Data Diri</a>
                                        </li>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ($halaman == 'survey' || $halaman == 'target-survey' || $halaman == 'hasil-survey') active @endif">
                                            <a href="javascript:;"><i class="glyphicon glyphicon-folder-open"></i>  Survey
                                                <span class="arrow"></span>
                                            </a>

                                              <ul class="dropdown-menu pull-left">
                                                @if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' )
                                                  <li aria-haspopup="true" class=" @if($halaman=='target-survey') active @endif ">
                                                      <a href="{{ url('target-survey') }}" class="nav-link  ">
                                                          <i class="icon-calendar"></i> Target Survey
                                                      </a>
                                                  </li>
                                                  <li aria-haspopup="true" class=" @if($halaman=='daftar-survey') active @endif ">
                                                      <a href="{{ url('daftar-survey') }}" class="nav-link  ">
                                                          <i class="icon-calendar"></i> Data Survey
                                                      </a>
                                                  </li>
                                                @endif
                                                <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ($halaman == 'survey') active @endif">
                                                    <a href="{{ url('survey') }}"> <i class="glyphicon glyphicon-signal"></i>Isi Survey</a>
                                                </li>
                                            </ul>
                                        </li>

                                        @if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' || Auth::user()->role=='marketing')
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ($halaman == 'data-divisi' || $halaman == 'data-jabatan' || $halaman == 'data-pangkat' || $halaman == 'data-kantor' || $halaman == 'data-status-karyawan' || $halaman == 'data-karyawan' ) active @endif">
                                            <a href="javascript:;"><i class="glyphicon glyphicon-folder-open"></i>  Data
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                              @if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' )
                                                <li aria-haspopup="true" class=" @if($halaman=='data-calendar') active @endif ">
                                                    <a href="{{ url('data-calendar') }}" class="nav-link  ">
                                                        <i class="icon-calendar"></i> Kalender
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='data-cuti') active @endif ">
                                                    <a href="{{ url('data-cuti') }}" class="nav-link  ">
                                                        <i class="icon-share-alt"></i> Cuti
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='data-ijin') active @endif ">
                                                    <a href="{{ url('data-ijin') }}" class="nav-link  ">
                                                        <i class="icon-plane"></i> Izin
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='data-divisi') active @endif ">
                                                    <a href="{{ url('data-divisi') }}" class="nav-link  ">
                                                        <i class="icon-direction"></i> Divisi
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='data-jabatan') active @endif ">
                                                    <a href="{{ url('data-jabatan') }}" class="nav-link  ">
                                                        <i class="icon-badge"></i> Jabatan
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='data-struktur') active @endif ">
                                                    <a href="{{ url('data-struktur') }}" class="nav-link  ">
                                                        <i class="icon-directions"></i> Struktur
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='data-pangkat') active @endif ">
                                                    <a href="{{ url('data-pangkat') }}" class="nav-link  ">
                                                        <i class="icon-notebook"></i> Pangkat </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='data-status-karyawan') active @endif ">
                                                    <a href="{{ url('data-status-karyawan') }}" class="nav-link  ">
                                                        <i class="icon-star"></i> Status Karyawan
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='data-pajak') active @endif ">
                                                    <a href="{{ url('data-pajak') }}" class="nav-link  ">
                                                        <i class="icon-target"></i> Status Pajak
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='data-kantor') active @endif ">
                                                    <a href="{{ url('data-kantor') }}" class="nav-link  ">
                                                        <i class="glyphicon glyphicon-pushpin" ></i> Kantor
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='data-karyawan') active @endif ">
                                                    <a href="{{ url('data-karyawan') }}" class="nav-link  ">
                                                        <i class="icon-user"></i> Karyawan
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='data-progress') active @endif ">
                                                    <a href="{{ url('data-progress') }}" class="nav-link  ">
                                                        <i class="icon-speedometer"></i> Progress
                                                    </a>
                                                </li>
                                                @endif
                                                @if(Auth::user()->role=='superadmin' || Auth::user()->role=='marketing' )
                                                <li aria-haspopup="true" class=" @if($halaman=='data-nasabah') active @endif ">
                                                    <a href="{{ url('data-nasabah') }}" class="nav-link  ">
                                                        <i class="icon-users"></i> Nasabah
                                                    </a>
                                                </li>
                                                @endif
                                            </ul>
                                        </li>
                                        @endif
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ($halaman == 'purchase-request'  || $halaman=='list-request' || $halaman=='approval-request' ) active @endif">
                                            <a href="javascript:;"> <i class="fa fa-bolt" ></i> Purchase Request
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                              @if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' )
                                                <li aria-haspopup="true" class=" @if($halaman=='list-request') active @endif ">
                                                    <a href="{{ url('daftar-request') }}" class="nav-link  ">
                                                        <i class="icon-calendar"></i> Daftar Request
                                                    </a>
                                                </li>
                                              @endif
                                              <li aria-haspopup="true" class=" @if($halaman=='purchase-request') active @endif ">
                                                  <a href="{{ url('purchase-request') }}" class="nav-link  ">
                                                      <i class="fa fa-heart"></i> Request
                                                  </a>
                                              </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='approval-request') active @endif ">
                                                    <a href="{{ url('approval-request') }}" class="nav-link  ">
                                                        <i class="fa fa-check"></i> Approval
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        @if(Auth::user()->role=='superadmin' || Auth::user()->role=='marketing' )
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ($halaman == 'followup') active @endif">
                                            <a href="{{ url('followup') }}"><i class="glyphicon glyphicons-forward"></i> Followup </a>
                                        </li>
                                        @endif
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ($halaman == 'ambilcuti') active @endif">
                                            <a href="{{ url('ambilcuti') }}"><i class="glyphicon glyphicon-plane"></i> Pengajuan Cuti</a>
                                        </li>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ($halaman == 'ijin') active @endif">
                                            <a href="{{ url('ijin') }}"><i class="icon-share-alt"></i> Pengajuan Izin</a>
                                        </li>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ($halaman == 'logabsen') active @endif">
                                            <a href="{{ url('logabsen') }}"><i class="icon-clock"></i> Log Absen</a>
                                        </li>
                                        @if(Auth::user()->role=='finance' || Auth::user()->role=='superadmin')
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ( $halaman =='data-lapisan' || $halaman=='data_ptkp' || $halaman=='input-pajak' ) active @endif">
                                            <a href="javascript:;"><i class=" icon-settings" ></i> PPH
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li aria-haspopup="true" class=" @if($halaman=='data-lapisan') active @endif ">
                                                    <a href="{{ url('data-lapisan') }}" class="nav-link  ">
                                                        <i class="icon-check"></i> Data Lapisan Pajak
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='data-ptkp') active @endif ">
                                                    <a href="{{ url('data-ptkp') }}" class="nav-link  ">
                                                        <i class="icon-check"></i> Data PTKP
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='input-pajak') active @endif ">
                                                    <a href="{{ url('input-pajak') }}" class="nav-link  ">
                                                        <i class="icon-check"></i> Input Pajak
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        @endif
                                        @if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' || Auth::user()->role=='sekretaris')
                                          <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ($halaman == 'absensi') active @endif">
                                              <a href="{{ url('absensi') }}"><i class=" icon-hourglass" ></i> Absensi</a>
                                          </li>
                                        @endif
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ( $halaman =='approve-cuti' || $halaman=='daftar-cuti' || $halaman=='daftar-ijin' ) active @endif">
                                            <a href="javascript:;"><i class=" icon-settings" ></i> Administrasi
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li aria-haspopup="true" class=" @if($halaman=='approve-cuti') active @endif ">
                                                    <a href="{{ url('approve-cuti') }}" class="nav-link  ">
                                                        <i class="icon-check"></i> Persetujuan Cuti
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='approve-ijin') active @endif ">
                                                    <a href="{{ url('approve-ijin') }}" class="nav-link  ">
                                                        <i class="icon-check"></i> Persetujuan Izin
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='approve-followup') active @endif ">
                                                    <a href="{{ url('approve-followup') }}" class="nav-link  ">
                                                        <i class="icon-check"></i> Persetujuan Followup
                                                    </a>
                                                </li>
                                                @if(Auth::user()->role=='admin' || Auth::user()->role=='sekretaris' || Auth::user()->role=='superadmin' )
                                                <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown @if ($halaman == 'surat-jalan') active @endif">
                                                  <a href="{{ url('surat-jalan') }}"><i class="icon-folder"></i> Surat Jalan</a>
                                                </li>
                                                @endif
                                                @if(Auth::user()->role=='admin' || Auth::user()->role=='sekretaris' || Auth::user()->role=='superadmin' || Auth::user()->jabatan_id=='23' || Auth::user()->jabatan_id=='22' || Auth::user()->jabatan_id=='21' || Auth::user()->jabatan_id=='15' || Auth::user()->jabatan_id=='11')
                                                <li aria-haspopup="true" class=" @if($halaman=='daftar-cuti') active @endif ">
                                                    <a href="{{ url('daftar-cuti') }}" class="nav-link  ">
                                                        <i class="icon-paper-plane"></i> Daftar Cuti
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='daftar-ijin') active @endif ">
                                                    <a href="{{ url('daftar-ijin') }}" class="nav-link  ">
                                                        <i class="icon-list"></i> Daftar Izin
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" @if($halaman=='daftar-visit') active @endif ">
                                                    <a href="{{ url('daftar-visit') }}" class="nav-link  ">
                                                        <i class="icon-list"></i> Daftar Visit
                                                    </a>
                                                </li>
                                                @endif

                                            </ul>
                                        </li>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                                            <a href="{{ url('logout')}}"><i class="icon-logout"></i> Logout
                                                <span class="arrow"></span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                                <!-- END MEGA MENU -->
                            </div>
                        </div>
                        <!-- END HEADER MENU -->
                    </div>
                    <!-- END HEADER -->
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">
                                @yield('content')
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            <div class="page-wrapper-row">
                <div class="page-wrapper-bottom">
                    <!-- BEGIN FOOTER -->
                    <!-- BEGIN INNER FOOTER -->
                    <div class="page-footer">
                        <div class="container-fluid"> <?= date('Y'); ?> &copy; PT HPAM
                        </div>
                    </div>
                    <div class="scroll-to-top">
                        <i class="icon-arrow-up"></i>
                    </div>
                    <!-- END INNER FOOTER -->
                    <!-- END FOOTER -->
                </div>
            </div>
        </div>
        <!-- modal container  -->
        <div class="modal fade" id="ajax" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content modal-lg">
                    <div class="modal-body">
                        <img src="{{ url('/') }}/assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
                        <span> &nbsp;&nbsp;Loading... </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="full" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="{{ url('/') }}/assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
                    <span> &nbsp;&nbsp;Loading... </span>
                </div>
            </div>
              <!-- /.modal-content -->
          </div>                          <!-- /.modal-dialog -->
        </div>
        <!-- BEGIN QUICK NAV -->

        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<script src="../assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ url('/') }}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
          <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ url('/') }}/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/fullcalendar/lang/id.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/jquery-repeater/jquery.repeater.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/js/jquery.number.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ url('/') }}/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
         <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ url('/') }}/js/gh.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ url('/') }}/assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="{{ url('/') }}/assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>

        <!-- END THEME LAYOUT SCRIPTS -->

    </body>
    <!-- zuliantoe@gmail.com -->
</html>
