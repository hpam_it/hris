@extends('home')
@section('title')
Data Target Survey
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Input Target</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  action="{{ url('target-survey') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Karyawan</label>
                            <select class="form-control selectpicker" data-live-search="true" name="karyawan_id" id="karyawan_id" >
                                  @foreach($karyawan as $karyawan)
                                    <option value="{{ $karyawan->id }}">{{ $karyawan->nama_karyawan }}</option>
                                  @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Group Survey</label>
                            <select class="form-control" name="group_id" id="group_id" >
                                  @foreach($group as $group)
                                    <option value="{{ $group->id }}">{{ $group->nama_group }}</option>
                                  @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            <select class="form-control" name="status" id="status" >
                                    <option value="1">Aktif</option>
                                    <option value="0">Off</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Daftar Target Survey</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nama_karyawan" },
                      { "data": "nama_group" },
                      { "data": "status_target" },
                      { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">Nama karyawan</th>
                        <th class="min-mobile-p">Nama group</th>
                        <th class="min-tablet-l">Status</th>
                        <th class="min-tablet-l">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
