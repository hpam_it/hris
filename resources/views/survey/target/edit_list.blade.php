<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit {{ $nama_target }}</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  action="{{ url('target-survey') }}/{{ $item->id }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="update_item">
                    <input type="hidden" name="karyawan_id" value="{{ $item->karyawan_id }}">
                    <input type="hidden" name="group_id" value="{{ $item->group_id }}">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            <select class="form-control" name="status" id="status" >
                                    <option @if($item->status=='1') selected @endif value="1">Aktif</option>
                                    <option @if($item->status=='0') selected @endif value="0">Off</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
