<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">{{$nama_survey}}</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
          <table class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
            <thead>
            <tr>
                <th class="min-mobile-p">Nama</th>
                <th class="min-mobile-p">Soal 1</th>
                <th class="min-mobile-p">Soal 2</th>
                <th class="min-tablet-l">Rata-rata</th>
                <!-- <th class="min-tablet-l">Responden</th> -->
            </tr>
            </thead>
            <tbody>
              @foreach($hasilnya as $hasilnya)
                <tr>
                    <td>{{ $hasilnya->nama_karyawan }}</td>
                    <td>{{ $hasilnya->soal1 }}</td>
                    <td>{{ $hasilnya->soal2 }}</td>
                    <td>{{ $hasilnya->nilai_akhir }}</td>
                    <!-- <td>{{ $hasilnya->responder }}</td> -->
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
    </div>
</div>
<div class="modal-footer">
  <div class="form-actions">
  <button  type="button" class="btn default" data-dismiss="modal">Close</button>
  </div>
</div>
