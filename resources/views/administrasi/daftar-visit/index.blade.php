@extends('home')
@section('title')
Daftar Visit
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Daftar Visit</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                  <table id="table-list-aja" action="{{ url('daftar-visit') }}"  data-kolom='[
                    { "data": "nomer","orderable":false },
                    { "data": "nama_karyawan","orderable":false },
                    { "data": "nama_nasabah","orderable":false },
                    { "data": "alamat_nasabah","orderable":false },
                    { "data": "tanggal_ijin","orderable":false,render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" ) },
                    { "data": "nama_progress","orderable":false},
                    { "data": "tanggal_progress","orderable":false,render: $.fn.dataTable.render.moment( "YYYY-MM-DD HH:mm:ss", "Do MMM YYYY", "id" )},
                    { "data": "nominal","orderable":false,render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" )},
                    { "data": "alasan_progress","orderable":false},
                    ]'
                    class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                  <thead>
                  <tr>
                      <th class="all">No.</th>
                      <th class="min-mobile-p">Nama Karyawan</th>
                      <th class="min-mobile-p">Nama Nasabah</th>
                      <th class="min-mobile-p">Alamat Visit</th>
                      <th class="min-mobile-p">Tanggal Visit</th>
                      <th class="min-mobile-p">Progress</th>
                      <th class="min-mobile-p">Tanggal Progress Berikutnya</th>
                      <th class="min-mobile-p">Nominal</th>
                      <th class="min-mobile-p">Alasan Progress</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  </table>
                  @if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' || Auth::user()->role=='sekretaris' )
                  <a href="{{ url('/') }}/daftarvisit/export" type="button" class="simpan_input btn green">Excel</a>
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
