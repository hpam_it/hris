<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Ijin</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  class="form-horizontal" action="{{ url('daftar-ijin') }}/{{ $item->id }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="update_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-4 control-label">Karyawan</label>
                          <div class=" col-md-8" >
                            <select name="karyawan_id" id="karyawan_id" class="form-control bs-select" >
                                  <option value="{{ $karyawan->id }}">{{ $karyawan->nama_karyawan }} / {{ $karyawan->npp }}</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-4 control-label">Jenis Ijin</label>
                          <div class=" col-md-8" >
                            <select name="data_ijin_id" id="data_ijin_id" class="form-control bs-select" >
                                <option value="">Pilih</option>
                              @foreach($jenis_ijin as $jenis_ijin)
                                <option @if($item->data_ijin_id==$jenis_ijin->id) selected @endif value="{{ $jenis_ijin->id }}">{{ $jenis_ijin->nama_ijin }}</option>
                              @endforeach
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Tanggal Ijin</label>
                          <div class=" col-md-8" >
                            <div class="input-group input-normal date date-picker" data-date="" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-calendar"></i>
                                  </button>
                            </span>
                            <input type="text"  value="{{ $item->tanggal_ijin }}" readonly name="tanggal_ijin" id="tanggal_ijin" class="form-control" required>

                          </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Jam Mulai</label>
                          <div class=" col-md-8" >
                            <div class="input-group input-normal" >
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-clock-o"></i>
                                  </button>
                            </span>
                            <input type="text" value="{{ $item->jam_ijin }}" readonly name="jam_ijin" id="jam_ijin" class="form-control timepicker" required>

                          </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Jam selesai</label>
                          <div class=" col-md-8" >
                            <div class="input-group input-normal" >
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-clock-o"></i>
                                  </button>
                            </span>
                            <input type="text" value="{{ $item->sampai_dengan }}" readonly name="sampai_dengan" id="sampai_dengan" class="form-control timepicker" required>

                          </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Alasan Ijin</label>
                          <div class=" col-md-8" >
                            <input type="text" name="alasan" value="{{ $item->tanggal_ijin }}" id="alasan" class="form-control" placeholder="Alasan Ijin" required="true">
                          </div>
                      </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
<script>
if (jQuery().datepicker) {
$('.date-picker').datepicker({
  rtl: App.isRTL(),
  orientation: "left",
  format:"yyyy-mm-dd",
  autoclose: true
});

$('.date-picker-multi').datepicker({
 rtl: App.isRTL(),
 orientation: "left",
 format:"yyyy-mm-dd",
 multidate:true,
 multidateSeparator:",",
 autoclose: true
});

$( document ).scroll(function(){
          $('#ajax .date-picker').datepicker('place'); //#modal is the id of the modal
      });
}
if (jQuery().timepicker) {
$('.timepicker').timepicker({
              autoclose: true,
              minuteStep: 5,
              showSeconds: false,
              showMeridian: false
          });
$('.timepicker').parent('.input-group').on('click', '.input-group-btn', function(e){
      e.preventDefault();
      $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
});

$( document ).scroll(function(){
           $('#ajax .timepicker').datepicker('place'); //#modal is the id of the modal
       });
}
</script>
