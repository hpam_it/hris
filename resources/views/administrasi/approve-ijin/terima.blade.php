<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Terima Permohonan Ijin</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="terimacuti"  class="form-horizontal" action="{{ url('approve-ijin') }}/{{ $item->id }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="terima">
                    <input type="hidden" name="status" value="{{ $status }}">
                    <table class="table">
                        <tr>
                            <td width="30%" >Nama Karyawan</td>
                            <td> : {{ $item->nama_karyawan }}</td>
                        </tr>
                        <tr>
                            <td>Atasan Langsung</td>
                            <td> : {{ $item->nama_atasan }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Ijin</td>
                            <td> : {{ $item->tanggal_ijin }}</td>
                        </tr>
                        <tr>
                            <td>Jam Mulai</td>
                            <td> : {{ $item->jam_ijin }}</td>
                        </tr>
                        <tr>
                            <td>Selesai</td>
                            <td> : {{ $item->sampai_dengan }}</td>
                        </tr>
                        <tr>
                            <td>Jenis Ijin</td>
                            <td> : {{ $item->nama_ijin }}</td>
                        </tr>
                        <tr>
                            <td>Keperluan</td>
                            <td> : {{ $item->alasan }}</td>
                        </tr>
                        @if($item->hanya_marketing=='Ya')
                        <tr>
                            <td>Nama Nasabah</td>
                            <td> : {{ $item->nama_nasabah }}</td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td> : {{ $item->alamat }}</td>
                        </tr>
                        <tr>
                            <td>Progress</td>
                            <td> : {{ $item->nama_progress }}</td>
                        </tr>
                        <tr>
                            <td>Nominal</td>
                            <td> : Rp {{ number_format($item->nominal),2,",","." }}</td>
                        </tr>
                        <tr>
                            <td>Alasan Progress</td>
                            <td> : {{ $item->nama_nasabah }}</td>
                        </tr>
                        @endif

                    </table>
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-5 control-label">Apakah Anda Menyetujui ?</label>
                          <div class="col-md-3">
                            <select name="keputusan" id="keputusan" class="form-control">
                              <option value="Diterima" >YA</option>
                              <option value="Ditolak" >TIDAK</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">Alasan (optional)</label>
                          <div class="col-md-9">
                            <textarea class="form-control" name="alasan" id="alasan"></textarea>
                          </div>
                      </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="terimacuti();" type="button" class="simpan_edit btn blue">Save</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
