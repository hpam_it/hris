@extends('home')
@section('title')
Daftar Cuti
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>List Cuti</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list-aja" action="{{ url('daftar-cuti') }}"  data-kolom='[
                      { "data": "nomer","orderable":false },
                      { "data": "nama_karyawan","orderable":false },
                      { "data": "tanggal_cuti","orderable":false },
                      { "data": "jumlah_hari","orderable":false},
                      { "data": "keperluan","orderable":false},
                      { "data": "alamat_cuti","orderable":false},
                      { "data": "notelpon","orderable":false},
                      { "data": "pengganti","orderable":false},
                      { "data": "action","orderable":false }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">NO</th>
                        <th class="min-mobile-p">Nama</th>
                        <th class="min-tablet-l">Tanggal Cuti</th>
                        <th class="min-tablet-l">Hari</th>
                        <th class="none">Keperluan</th>
                        <th class="none">Alamat Cuti</th>
                        <th class="none">Telpon</th>
                        <th class="min-tablet-l">PIC Pengganti</th>
                        <th class="none">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
