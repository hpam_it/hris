<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Terima Permohonan Cuti</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="terimacuti"  class="form-horizontal" action="{{ url('approve-cuti') }}/{{ $item->id }} ">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <input type="hidden" name="aksi" value="terima">
                    <input type="hidden" name="status" value="{{ $status }}">
                    <table class="table">
                        <tr>
                            <td width="30%" >Nama Karyawan</td>
                            <td> : {{ $item->nama_karyawan }}</td>
                        </tr>
                        <tr>
                            <td>PIC Pengganti</td>
                            <td> : {{ $item->nama_pengganti }}</td>
                        </tr>
                        <tr>
                            <td>Atasan Langsung</td>
                            <td> : {{ $item->nama_atasan }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Cuti</td>
                            <td> : {{ $item->tanggal_tanggal }}</td>
                        </tr>
                        <tr>
                            <td>Jumlah Hari Cuti</td>
                            <td> : {{ $item->jumlah_hari }}</td>
                        </tr>
                        <tr>
                            <td>Keperluan</td>
                            <td> : {{ $item->keperluan }}</td>
                        </tr>
                        <tr>
                            <td>Jenis Cuti</td>
                            <td> : {{ $item->nama_cuti }}</td>
                        </tr>

                    </table>
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-5 control-label">Apakah Anda Menyetujui ?</label>
                          <div class="col-md-3">
                            <select name="keputusan" id="keputusan" class="form-control">
                              <option value="Diterima" >YA</option>
                              <option value="Ditolak" >TIDAK</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">Alasan</label>
                          <div class="col-md-9">
                            <textarea class="form-control" name="alasan" id="alasan"></textarea>
                          </div>
                      </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="terimacuti();" type="button" class="simpan_edit btn blue">Save</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
            <div class="table-responsive" style="padding:10px;">
              <table class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
              <thead>
              <tr>
                  <th class="all" colspan="2" rowspan="2">Ketentuan</th>
                  <th colspan="8">Periode</th>
              </tr>
              <tr>
                  <th class="all text-center" colspan="4">Lalu</th>
                  <th class="all text-center" colspan="4">Sekarang</th>
              </tr>
              <tr>
                  <th class="all">Jenis Cuti</th>
                  <th class="min-tablet-l text-center">Jumlah</th>
                  <th class="min-tablet-l text-center">Jumlah</th>
                  <th class="min-tablet-l text-center">Terpakai</th>
                  <th class="min-tablet-l text-center">Berakhir</th>
                  <th class="min-tablet-l text-center">Sisa</th>
                  <th class="min-tablet-l text-center">Jumlah</th>
                  <th class="min-tablet-l text-center">Terpakai</th>
                  <th class="min-tablet-l text-center">Berakhir</th>
                  <th class="min-tablet-l text-center">Sisa</th>
              </tr>
              </thead>
              <tbody>
                @foreach($cuti_oke as $oke)
                <tr>
                    <th class="all">{{ $oke->nama_cuti }}</th>
                    <th class="min-mobile-p text-center">{{ $oke->jumlah }}</th>
                    <th class="min-mobile-p text-center">{{ $oke->kemarin_awal }}</th>
                    <th class="min-tablet-l text-center">{{ $oke->kemarin_ambil }}</th>
                    <?php $tgl_expired = date_create($oke->kemarin_tambahan); $sekarang=date('Y-m-d'); ?>
                    <th class="min-tablet-l text-center">@if($oke->hitung==1) {{ date_format($tgl_expired,"d-m-Y")  }} @endif</th>
                    <th class="min-tablet-l text-center">{{ $oke->kemarin_sisa }}</th>
                    <th class="min-mobile-p text-center">@if($oke->hitung==1) {{ $oke->sekarang_awal }} @endif</th>
                    <th class="min-tablet-l text-center">@if($oke->hitung==1) {{ $oke->sekarang_ambil }} @else {{ $oke->total_cuti }}@endif</th>
                    <?php $tgl_expired = date_create($oke->sekarang_tambahan); $sekarang=date('Y-m-d'); ?>
                    <th class="min-tablet-l text-center">@if($oke->hitung==1) {{ date_format($tgl_expired,"d-m-Y")  }} @endif</th>
                    <th class="min-tablet-l text-center">{{ $oke->sekarang_sisa }}</th>
                </tr>
                @endforeach
                <tr>
                  <td colspan="10"><p>{{ $pesan }}</p></td>
                </tr>

              </tbody>
              </table>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
