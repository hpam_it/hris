@extends('home')
@section('title')
Followup Progress Nasabah
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-5">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Input Followup Nasabah/Calon Nasabah</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" class="form-horizontal" id="form_input"  action="{{ url('followup') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-4 control-label">Karyawan</label>
                          <div class=" col-md-8" >
                            <select name="karyawan_id" id="karyawan_id" class="form-control bs-select" required />
                              @foreach($karyawan as $karyawan)
                                <option value="{{ $karyawan->id }}">{{ $karyawan->nama_karyawan }} / {{ $karyawan->npp }}</option>
                              @endforeach
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Nama Nasabah</label>
                          <div class=" col-md-8" >
                            <select name="nasabah_id" id="nasabah_id" data-live-search="true" class="form-control bs-select" >
                                <option value="">Pilih</option>
                              @foreach($nasabah as $nasabah)
                                <option value="{{ $nasabah->id }}">{{ $nasabah->nama_nasabah }}</option>
                              @endforeach
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-4 control-label">Progress Followup</label>
                          <div class=" col-md-8" >
                            <select name="progress_id" id="progress_id" class="form-control bs-select" required  />
                                <option value="">Pilih</option>
                              @foreach($progress as $progress)
                                <option value="{{ $progress->id }}">{{ $progress->nama_progress }}</option>
                              @endforeach
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Tanggal Followup</label>
                          <div class=" col-md-8" >
                            <div class="input-group input-normal date date-picker" data-date="" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-calendar"></i>
                                  </button>
                            </span>
                            <input type="text" readonly name="tanggal_progress" id="tanggal_progress" class="form-control" required>

                          </div>
                          </div>
                      </div>
                      <div class="form-group " >
                          <label class="control-label col-md-4">Hasil Progress</label>
                          <div class=" col-md-8" >
                            <select name="hasil_progress_id" id="hasil_progress_id" onchange="pilih_followup()"  class="form-control bs-select" >
                                <option value="">Pilih</option>
                              @foreach($hasil_progress as $hasil_progress)
                                <option butuh-nominal="{{ $hasil_progress->butuh_nominal }}"  butuh-tanggal="{{ $hasil_progress->butuh_tanggal }}"  value="{{ $hasil_progress->id }}">{{ $hasil_progress->nama_progress }}</option>
                              @endforeach
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Keterangan Progress</label>
                          <div class=" col-md-8" >
                            <input type="text" name="keterangan_progress" id="keterangan_progress" class="form-control" placeholder="Keterangan Progress">
                          </div>
                      </div>
                      <div class="form-group nominal " style="display:none">
                          <label class="control-label col-md-4">Nominal</label>
                          <div class=" col-md-8" >
                            <input type="text" name="nominal_progress" id="nominal_progress" class="form-control angka-angka" placeholder="Nominal Progress" >
                          </div>
                      </div>
                      <div class="form-group tanggal-progress " style="display:none">
                          <label class="control-label col-md-4">Tanggal Progres Berikutnya</label>
                          <div class=" col-md-8" >
                            <div class="input-group input-normal date date-picker" data-date="" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-calendar"></i>
                                  </button>
                            </span>
                            <input type="text" readonly name="tanggal_progress_berikutnya" id="tanggal_progress_berikutnya" class="form-control">

                          </div>
                          </div>
                      </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-7">
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>List Followup</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                  <div class="tabbable-custom ">
                      <ul class="nav nav-tabs ">
                          <li class="active">
                              <a href="#followup-pending" data-toggle="tab">Pending</a>
                          </li>
                          <li>
                              <a href="#followup-batal" data-toggle="tab">Batal</a>
                          </li>
                          <li>
                              <a href="#followup-ok" data-toggle="tab">Disetujui</a>
                          </li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="followup-pending">
                          <table id="table-ijin-pending" data-kolom='[
                            { "data": "nomer","orderable":false },
                            { "data": "tanggal_progress","orderable":false,render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" ) },
                            { "data": "nama_nasabah","orderable":false },
                            { "data": "nama_progress","orderable":false },
                            { "data": "hasil_progress","orderable":false },
                            { "data": "nominal_progress","orderable":false,render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" )},
                            { "data": "tanggal_progress_berikutnya","orderable":false,render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" )},
                            { "data": "keterangan_progress"}
                            ]'
                            action="{{ url('followup') }}"

                            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                          <thead>
                          <tr>
                              <th class="all">No</th>
                              <th class="min-mobile-p">Tanggal</th>
                              <th class="min-tablet-l">Nasabah</th>
                              <th class="min-tablet-l">Progress</th>
                              <th class="min-tablet-l">Hasil</th>
                              <th class="none">Nominal</th>
                              <th class="none">Progres Berikutnya</th>
                              <th class="none">Keterangan</th>
                          </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          </table>
                        </div>
                        <div class="tab-pane" id="followup-batal">
                          <table id="table-ijin-batal" data-kolom='[
                            { "data": "nomer","orderable":false },
                            { "data": "tanggal_progress","orderable":false,render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" ) },
                            { "data": "nama_nasabah","orderable":false },
                            { "data": "nama_progress","orderable":false },
                            { "data": "hasil_progress","orderable":false },
                            { "data": "nominal_progress","orderable":false,render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" )},
                            { "data": "tanggal_progress_berikutnya","orderable":false,render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" )},
                            { "data": "keterangan_progress"}
                            ]'
                            action="{{ url('followup') }}"

                            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                          <thead>
                            <tr>
                                <th class="all">No</th>
                                <th class="min-mobile-p">Tanggal</th>
                                <th class="min-tablet-l">Nasabah</th>
                                <th class="min-tablet-l">Progress</th>
                                <th class="min-tablet-l">Hasil</th>
                                <th class="none">Nominal</th>
                                <th class="none">Progres Berikutnya</th>
                                <th class="none">Keterangan</th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          </table>
                        </div>
                        <div class="tab-pane" id="followup-ok">
                          <table id="table-ijin-oke" data-kolom='[
                            { "data": "nomer","orderable":false },
                            { "data": "tanggal_progress","orderable":false,render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" ) },
                            { "data": "nama_nasabah","orderable":false },
                            { "data": "nama_progress","orderable":false },
                            { "data": "hasil_progress","orderable":false },
                            { "data": "nominal_progress","orderable":false,render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" )},
                            { "data": "tanggal_progress_berikutnya","orderable":false,render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" )},
                            { "data": "keterangan_progress"}
                            ]'
                            action="{{ url('followup') }}"

                            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                          <thead>
                            <tr>
                                <th class="all">No</th>
                                <th class="min-mobile-p">Tanggal</th>
                                <th class="min-tablet-l">Nasabah</th>
                                <th class="min-tablet-l">Progress</th>
                                <th class="min-tablet-l">Hasil</th>
                                <th class="none">Nominal</th>
                                <th class="none">Progres Berikutnya</th>
                                <th class="none">Keterangan</th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          </table>
                        </div>
                      </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
