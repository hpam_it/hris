@extends('home')
@section('title')
Pengajuan Cuti
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="icon-grid"></i>Pengajuan Cuti</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" class="form-horizontal" id="form_input"  action="{{ url('ambilcuti') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-4 control-label">Karyawan</label>
                          <div class=" col-md-8" >
                            <select name="karyawan_id" id="karyawan_id" class="form-control bs-select" />
                              @foreach($karyawan2 as $karyawan2)
                                <option value="{{ $karyawan2->id }}">{{ $karyawan2->nama_karyawan }} / {{ $karyawan2->npp }}</option>
                              @endforeach
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Jenis Cuti</label>
                          <div class=" col-md-8" >
                            <select name="data_cuti_id" id="data_cuti_id" class="form-control bs-select" />
                                <option value="">Pilih</option>
                              @foreach($data_cuti as $data_cuti)
                                <option value="{{ $data_cuti->id }}">{{ $data_cuti->nama_cuti }}</option>
                              @endforeach
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Pilihan Tanggal</label>
                          <div class=" col-md-8" >
                            <select onchange="pilih_tanggal(this.value)" name="pilihan_tanggal" id="pilihan_tanggal" class="form-control bs-select" required />
                                <option value="">Pilih</option>
                                <option value="tanggal">Pilih Tanggal</option>
                                <option value="range">Pilih Range</option>
                            </select>
                          </div>
                      </div>
                      <div class="cuti-tanggal form-group hidden">
                          <label class="control-label col-md-4">Tanggal Cuti</label>
                          <div class=" col-md-8" >
                            <div class="input-group input-normal date date-picker-multi" data-date="" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-calendar"></i>
                                  </button>
                            </span>
                            <input type="text" readonly onchange="hitung_hari(this.value)" name="tanggal_cuti" id="tanggal_cuti" class="form-control">

                          </div>
                          </div>
                      </div>

                      <div class="cuti-range form-group hidden">
                          <label class="control-label col-md-4">Tanggal awal cuti</label>
                          <div class=" col-md-8" >
                            <div class="input-group input-normal date date-picker-multi" data-date="" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                              <span class="input-group-btn">
                                    <button  class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                    </button>
                              </span>
                              <input type="text" readonly name="tgl_awal" id="tgl_awal" class="form-control">
                            </div>
                          </div>
                      </div>
                      <div class="cuti-range form-group hidden">
                          <label class="control-label col-md-4">Tanggal akhir Cuti</label>
                          <div class=" col-md-8" >
                            <div class="input-group input-normal date date-picker-multi" data-date="" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-calendar"></i>
                                  </button>
                            </span>
                            <input type="text" readonly name="tgl_akhir" id="tgl_akhir" class="form-control">

                          </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="control-label col-md-4">Jumlah Hari Cuti (Hari Kerja)</label>
                          <div class=" col-md-8" >
                            <input readonly type="number" name="jumlah_hari" id="jumlah_hari" class="form-control" placeholder="Jumlah Hari Kerja" >
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Keperluan</label>
                          <div class=" col-md-8" >
                            <input type="text" name="keperluan" id="keperluan" class="form-control" placeholder="Keperluan Cuti"  required="true">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Alamat Cuti</label>
                          <div class=" col-md-8" >
                            <textarea name="alamat_cuti" id="alamat_cuti" class="form-control" required="true"></textarea>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">No. Telpon</label>
                          <div class=" col-md-8" >
                            <input type="text" name="notelpon" id="notelpon" class="form-control" placeholder="Nomor Telpon Yang Bisa Dihubungi Saat Cuti"  required="true">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">PIC Pengganti</label>
                          <div class=" col-md-8" >
                            <select name="pic_pengganti" id="pic_pengganti" class="form-control bs-select" />
                                <option value="">Pilih</option>
                              @foreach($karyawan as $karyawan)
                                <option value="{{ $karyawan->id }}">{{ $karyawan->nama_karyawan }} / {{ $karyawan->npp }}</option>
                              @endforeach
                            </select>
                          </div>
                      </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_cuti();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Rincian Cuti</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all" colspan="2" rowspan="2">Ketentuan</th>
                        <th colspan="8">Periode</th>
                    </tr>
                    <tr>
                        <th class="all text-center" colspan="4">Lalu</th>
                        <th class="all text-center" colspan="4">Sekarang</th>
                    </tr>
                    <tr>
                        <th class="all">Jenis Cuti</th>
                        <th class="min-tablet-l text-center">Jumlah</th>
                        <th class="min-tablet-l text-center">Jumlah</th>
                        <th class="min-tablet-l text-center">Terpakai</th>
                        <th class="min-tablet-l text-center">Berakhir</th>
                        <th class="min-tablet-l text-center">Sisa</th>
                        <th class="min-tablet-l text-center">Jumlah</th>
                        <th class="min-tablet-l text-center">Terpakai</th>
                        <th class="min-tablet-l text-center">Berakhir</th>
                        <th class="min-tablet-l text-center">Sisa</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($cuti_oke as $oke)
                      <tr>
                          <th class="all">{{ $oke->nama_cuti }}</th>
                          <th class="min-mobile-p text-center">{{ $oke->jumlah }}</th>
                          <th class="min-mobile-p text-center">{{ $oke->kemarin_awal }}</th>
                          <th class="min-tablet-l text-center">{{ $oke->kemarin_ambil }}</th>
                          <?php $tgl_expired = date_create($oke->kemarin_tambahan); $sekarang=date('Y-m-d'); ?>
                          <th class="min-tablet-l text-center">@if($oke->hitung==1) {{ date_format($tgl_expired,"d-m-Y")  }} @endif</th>
                          <th class="min-tablet-l text-center">{{ $oke->kemarin_sisa }}</th>
                          <th class="min-mobile-p text-center">@if($oke->hitung==1) {{ $oke->sekarang_awal }} @endif</th>
                          <th class="min-tablet-l text-center">@if($oke->hitung==1) {{ $oke->sekarang_ambil }} @else {{ $oke->total_cuti }}@endif</th>
                          <?php $tgl_expired = date_create($oke->sekarang_tambahan); $sekarang=date('Y-m-d'); ?>
                          <th class="min-tablet-l text-center">@if($oke->hitung==1) {{ date_format($tgl_expired,"d-m-Y")  }} @endif</th>
                          <th class="min-tablet-l text-center">{{ $oke->sekarang_sisa }}</th>
                      </tr>
                      @endforeach
                    </tbody>
                    </table>
                  </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Daftar Pengajuan Cuti</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                  <div class="tabbable-custom ">
                      <ul class="nav nav-tabs ">
                          <li class="active">
                              <a href="#ijin-pending" data-toggle="tab">Pending</a>
                          </li>
                          <li>
                              <a href="#ijin-batal" data-toggle="tab">Batal</a>
                          </li>
                          <li>
                              <a href="#ijin-ok" data-toggle="tab">Disetujui</a>
                          </li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="ijin-pending">
                          <table id="table-ijin-pending" data-kolom='[
                            { "data": "nomer","orderable":false },
                            { "data": "nama_karyawan","orderable":false },
                            { "data": "tanggal_tanggal","orderable":false },
                            { "data": "jumlah_hari","orderable":false},
                            { "data": "keperluan","orderable":false},
                            { "data": "alamat_cuti","orderable":false},
                            { "data": "notelpon","orderable":false},
                            { "data": "pengganti","orderable":false},
                            { "data": "statushrd","orderable":false},
                            { "data": "statusatasan","orderable":false}
                            ]'
                            action="{{ url('ambilcuti') }}"
                            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                          <thead>
                          <tr>
                              <th class="all">No.</th>
                              <th class="min-mobile-p">Nama</th>
                              <th class="min-tablet-l">Tanggal Cuti</th>
                              <th class="none">HK</th>
                              <th class="none">Keperluan</th>
                              <th class="none">Alamat Cuti</th>
                              <th class="none">Telpon</th>
                              <th class="none">PIC Pengganti</th>
                              <th class="min-tablet-l">HRD</th>
                              <th class="min-tablet-l">Atasan</th>
                          </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          </table>
                        </div>
                        <div class="tab-pane" id="ijin-batal">
                          <table id="table-ijin-batal" data-kolom='[
                            { "data": "nomer","orderable":false },
                            { "data": "nama_karyawan","orderable":false },
                            { "data": "tanggal_tanggal","orderable":false },
                            { "data": "jumlah_hari","orderable":false},
                            { "data": "keperluan","orderable":false},
                            { "data": "alamat_cuti","orderable":false},
                            { "data": "notelpon","orderable":false},
                            { "data": "pengganti","orderable":false},
                            { "data": "statushrd","orderable":false},
                            { "data": "statusatasan","orderable":false}
                            ]'
                            action="{{ url('ambilcuti') }}"
                            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                          <thead>
                          <tr>
                              <th class="all">No.</th>
                              <th class="min-mobile-p">Nama</th>
                              <th class="min-tablet-l">Tanggal Cuti</th>
                              <th class="none">HK</th>
                              <th class="none">Keperluan</th>
                              <th class="none">Alamat Cuti</th>
                              <th class="none">Telpon</th>
                              <th class="none">PIC Pengganti</th>
                              <th class="min-tablet-l">HRD</th>
                              <th class="min-tablet-l">Atasan</th>
                          </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          </table>
                        </div>
                        <div class="tab-pane" id="ijin-ok">
                          <table id="table-ijin-oke" data-kolom='[
                            { "data": "nomer","orderable":false },
                            { "data": "nama_karyawan","orderable":false },
                            { "data": "tanggal_tanggal","orderable":false },
                            { "data": "jumlah_hari","orderable":false},
                            { "data": "keperluan","orderable":false},
                            { "data": "alamat_cuti","orderable":false},
                            { "data": "notelpon","orderable":false},
                            { "data": "pengganti","orderable":false},
                            { "data": "statushrd","orderable":false},
                            { "data": "statusatasan","orderable":false}
                            ]'
                            action="{{ url('ambilcuti') }}"
                            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                          <thead>
                          <tr>
                              <th class="all">No.</th>
                              <th class="min-mobile-p">Nama</th>
                              <th class="min-tablet-l">Tanggal Cuti</th>
                              <th class="none">HK</th>
                              <th class="none">Keperluan</th>
                              <th class="none">Alamat Cuti</th>
                              <th class="none">Telpon</th>
                              <th class="none">PIC Pengganti</th>
                              <th class="min-tablet-l">HRD</th>
                              <th class="min-tablet-l">Atasan</th>
                          </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          </table>
                        </div>
                      </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
