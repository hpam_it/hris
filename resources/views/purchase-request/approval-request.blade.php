@extends('home')
@section('title')
Approval Request
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>List Purchase Request</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                  <table id="table-modal" class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                  <thead>
                  <tr>
                      <th >Kantor</th>
                      <th >Divisi</th>
                      <th >Karyawan</th>
                      <th >Tanggal Request</th>
                      <th >Kategori</th>
                      <th >Keterangan</th>
                      <th >Alasan</th>
                      <th >Action</th>
                  </tr>

                  </thead>
                  <tbody>
                    @foreach($item as $item)
                      <tr>
                        <td>{{ $item->nama_kantor }}</td>
                        <td>{{ $item->nama_divisi }}</td>
                        <td>{{ $item->nama_karyawan }}</td>
                        <td>{{ $item->tanggal_request }}</td>
                        <td>{{ $item->kategori }}</td>
                        <td><?php echo $item->keterangan; ?></td>
                        <td>{{ $item->alasan }}</td>
                        <td><?php echo $item->action ?></td>
                      </tr>
                    @endforeach
                  </tbody>
                  </table>
                </div>
            </div>
          </div>
    </div>
</div>
@endsection
