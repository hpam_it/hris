@extends('home')
@section('title')
Purchase Request
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="icon-grid"></i>Pengajuan Request</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" class="form-horizontal" id="form_input"  action="{{ url('simpan-request') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-4 control-label">Kategori</label>
                          <div class=" col-md-8" >
                            <select name="kategori" id="kategori" class="form-control bs-select" />
                                <option value="Komputer">Komputer</option>
                                <option value="Kendaraan">Kendaraan</option>
                                <option value="Furniture">Furniture</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Keterangan</label>
                          <div class=" col-md-8" >
                            <textarea name="keterangan" id="keterangan" class="form-control summernote" ></textarea>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Alasan</label>
                          <div class=" col-md-8" >
                            <textarea name="alasan" id="alasan" class="form-control" ></textarea>
                          </div>
                      </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Daftar Pengajuan Request</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                  <table id="table-list-aja" data-kolom='[
                    { "data": "tanggal_request","orderable":false },
                    { "data": "kategori","orderable":false },
                    { "data": "alasan","orderable":false },
                    { "data": "keterangan","orderable":false},
                    { "data": "status","orderable":false}
                    ]'
                    action="{{ url('table-request') }}"
                    class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                  <thead>
                  <tr>
                      <th class="all">Tanggal</th>
                      <th class="min-mobile-p">Kategori</th>
                      <th class="none">Alasan</th>
                      <th class="none">Keterangan</th>
                      <th class="min-tablet-l">Status</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  </table>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
