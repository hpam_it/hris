<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Terima Purchase Request</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="terimacuti"  class="form-horizontal" action="{{ url('update-request') }}/{{ $item->id }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="terima">
                    <table class="table">
                        <tr>
                            <td width="30%" >Nama Karyawan</td>
                            <td> : {{ $item->nama_karyawan }}</td>
                        </tr>
                        <tr>
                            <td>Divisi</td>
                            <td> : {{ $item->nama_divisi }}</td>
                        </tr>
                        <tr>
                            <td>Kantor</td>
                            <td> : {{ $item->nama_kantor }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Request</td>
                            <td> : {{ $item->tanggal_request }}</td>
                        </tr>
                        <tr>
                            <td>Kategori</td>
                            <td> : {{ $item->kategori }}</td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td> : <?php echo $item->keterangan; ?></td>
                        </tr>
                        <tr>
                            <td>Alasan</td>
                            <td> : {{ $item->alasan }}</td>
                        </tr>

                    </table>
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-5 control-label">Apakah Anda Menyetujui ?</label>
                          <div class="col-md-3">
                            <select name="keputusan" id="keputusan" class="form-control">
                              <option value="Diterima" >YA</option>
                              <option value="Ditolak" >TIDAK</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">Alasan</label>
                          <div class="col-md-9">
                            <textarea class="form-control" name="alasan" id="alasan"></textarea>
                          </div>
                      </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="terima_request();" type="button" class="simpan_edit btn blue">Save</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
