<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Detail Perjalanan Dinas</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
              <h3>Perjalanan Dinas</h3>
                <div class="table-responsive">
                    <table class="table table-bordered">
                      <tr>
                          <th>Tujuan</th>
                          <th>Mulai</th>
                          <th>Datang</th>
                          <th>Selesai</th>
                          <th>Pulang</th>
                          <th>Keterangan</th>
                      </tr>
                      <tr>
                          <td>{{ $item->tujuan_dinas }}</td>
                          <td>{{ $item->mulai_dinas }}</td>
                          <td>{{ $item->kedatangan }}</td>
                          <td>{{ $item->selesai_dinas }}</td>
                          <td>{{ $item->kepulangan }}</td>
                          <td>{{ $item->keterangan }}</td>
                      </tr>
                    </table>
                </div>
        </div>
        <div class="col-md-12">
          <h3>Aktifitas Dinas</h3>
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Tanggal</th>
                    <th>Aktifitas</th>
                    <th>PIC Dinas</th>
                    <th>Status PIC</th>
                    <th>Status Atasan</th>
                </tr>
                @foreach($aktifitas as $aktifitas)
                <tr>
                    <td>{{ $aktifitas->tanggal_aktifitas }}</td>
                    <td>{{ $aktifitas->aktivitas }}</td>
                    <td>{{ $aktifitas->nama_karyawan }}</td>
                    <td>{{ $aktifitas->status_pic_dinas }}</td>
                    <td>{{ $aktifitas->status_atasan}}</td>
                </tr>
                @endforeach
            </table>
          </div>
        </div>
        <div class="col-md-12">
          <h3>Transportasi</h3>
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Jenis Transportasi</th>
                    <th>Maskapai/Moda</th>
                    <th>Biaya</th>
                    <th>Keterangan</th>
                </tr>
                @foreach($transportasi as $transportasi)
                <tr>
                    <td>{{ $transportasi->jenis_transportasi }}</td>
                    <td>{{ $transportasi->maskapai }}</td>
                    <td>{{ number_format($transportasi->biaya) }}</td>
                    <td>{{ $transportasi->keterangan }}</td>
                </tr>
                @endforeach
            </table>
          </div>
        </div>
        <div class="col-md-12">
          <h3>Akomodasi</h3>
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Jenis Akomodasi</th>
                    <th>Nama Akomodasi</th>
                    <th>Total hari</th>
                    <th>Biaya Perhari</th>
                    <th>Total Biaya</th>
                    <th>Keterangan</th>
                </tr>
                @foreach($akomodasi as $akomodasi)
                <tr>
                    <td>{{ $akomodasi->jenis_akomodasi }}</td>
                    <td>{{ $akomodasi->nama_akomodasi }}</td>
                    <td>{{ $akomodasi->total_hari }}</td>
                    <td class="text-right">{{ number_format($akomodasi->biaya_perhari) }}</td>
                    <td class="text-right">{{ number_format($akomodasi->total_biaya) }}</td>
                    <td>{{ $akomodasi->keterangan }}</td>
                </tr>
                @endforeach
            </table>
          </div>
        </div>
        <div class="col-md-12">
          <h3>Persetujuan</h3>
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Persetujuan</th>
                    <th>Atasan</th>
                    <th>HRD</th>
                    <th>Direksi</th>
                </tr>
                <tr>
                    <td>Oleh</td>
                    <td>{{ $item->nama_atasan }}</td>
                    <td>HRD</td>
                    <td>{{ $item->nama_direksi }}</td>
                </tr>
                <tr>
                    <td>Sebelum Dinas</td>
                    <td>@if($item->before_atasan==1) Diterima @elseif($item->before_atasan==2) Pending @else($item->before_atasan==0) Ditolak @endif</td>
                    <td>@if($item->before_hrd==1) Diterima @elseif($item->before_hrd==2) Pending @else($item->before_hrd==0) Ditolak @endif</td>
                    <td>@if($item->before_direksi==1) Diterima @elseif($item->before_direksi==2) Pending @else($item->before_direksi==0) Ditolak @endif</td>
                </tr>
                <tr>
                    <td>Setelah Dinas</td>
                    <td>@if($item->after_atasan==1) Diterima @elseif($item->after_atasan==2) Pending @else($item->after_atasan==0) Ditolak @endif</td>
                    <td>@if($item->after_hrd==1) Diterima @elseif($item->after_hrd==2) Pending @else($item->after_hrd==0) Ditolak @endif</td>
                    <td>@if($item->after_direksi==1) Diterima @elseif($item->after_direksi==2) Pending @else($item->after_direksi==0) Ditolak @endif</td>
                </tr>
            </table>
          </div>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
