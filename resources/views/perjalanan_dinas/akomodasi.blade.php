@extends('home')
@section('title')
Akomodasi Dinas
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="icon-grid"></i>Akomodasi Dinas</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" class="form-horizontal" id="form_input"  action="{{ url('simpan-akomodasi') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="control-label col-md-2">Perjalanan Dinas</label>
                          <div class=" col-md-8" >
                            <select onchange="pilih_sesuatu(this.value,'{{url('pilih-dinas')}}','detail-dinas')" name="perjalanan_dinas_id" class="selectpicker form-control" title="Pilih Perjalanan Dinas" data-live-search="true">
                                @foreach($dinas as $perjalanan_dinas)
                                  <option value="{{ $perjalanan_dinas->id }}">{{ $perjalanan_dinas->nama_karyawan }}/{{ $perjalanan_dinas->tujuan_dinas }}/{{ $perjalanan_dinas->mulai_dinas }}</option>
                                @endforeach
                            </select>
                          </div>
                      </div>
                      <div class="form-group" id="detail-dinas">

                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Kedatangan</label>
                        <div class="col-md-4">
                          <div class="input-group input-normal date date-picker" data-date="" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-calendar"></i>
                                  </button>
                            </span>
                            <input type="text" readonly name="tgl_kedatangan" id="tgl_kedatangan" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="input-group input-normal">
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-clock-o"></i>
                                  </button>
                            </span>
                            <input type="text" readonly name="jam_kedatangan" id="jam_kedatangan" class="form-control timepicker">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Kepulangan</label>
                        <div class="col-md-4">
                          <div class="input-group input-normal date date-picker" data-date="" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-calendar"></i>
                                  </button>
                            </span>
                            <input type="text" readonly name="tgl_kepulangan" id="tgl_kepulangan" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="input-group input-normal">
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-clock-o"></i>
                                  </button>
                            </span>
                            <input type="text" readonly name="jam_kepulangan" id="jam_kepulangan" class="form-control timepicker">
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                            <label class="col-md-2 control-label">Akomodasi</label>
                            <div class="col-md-8">
                                <div class="mt-repeater">
                                    <div data-repeater-list="list_akomodasi">
                                        <div data-repeater-item class="row" style="border-bottom:1px solid #000000; padding-bottom:10px;">
                                            <div class="col-md-4">
                                                <label class="control-label">Jenis Akomodasi</label>
                                                <select title="Pilih Jenis Akomodasi" name="jenis_akomodasi" class="form-control">
                                                  <option value="Penginapan">Penginapan</option>
                                                  <option value="Uang Saku">Uang Saku</option>
                                                </select>
                                            </div>
                                            <div class="col-md-8">
                                                <label class="control-label">Akomodasi</label>
                                                <input name="nama_akomodasi" class="form-control" />
                                            </div>
                                            <div class="col-md-3">
                                                <label class="control-label">Jumlah Hari</label>
                                                <input name="total_hari" class="form-control" />
                                            </div>
                                            <div class="col-md-4">
                                                <label class="control-label">Biaya /Hari</label>
                                                <input name="biaya_perhari" class="form-control angka-angka" />
                                            </div>
                                            <div class="col-md-4">
                                                <label class="control-label">Total Biaya</label>
                                                <input name="total_biaya" class="form-control angka-angka" />
                                            </div>
                                            <div class="col-md-8">
                                                <label class="control-label">Keterangan</label>
                                                <input name="keterangan" class="form-control" />
                                            </div>
                                            <div class="col-md-1">
                                                <label class="control-label">&nbsp;</label>
                                                <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                        <i class="fa fa-plus"></i> Tambah Lagi</a>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                              <label class="col-md-2 control-label">Transportasi</label>
                              <div class="col-md-8">
                                  <div class="mt-repeater">
                                      <div data-repeater-list="list_transportasi">
                                          <div data-repeater-item class="row" style="border-bottom:1px solid #000000; padding-bottom:10px;">
                                              <div class="col-md-7">
                                                  <label class="control-label">Jenis Transportasi</label>
                                                  <select title="Pilih Jenis Transportasi" name="jenis_transportasi" class="form-control">
                                                    <option value="Transportasi Pulang Pergi Bandara">Transportasi Pulang Pergi Bandara</option>
                                                    <option value="Transportasi Penerbangan (Berangkat)">Transportasi Penerbangan (Berangkat)</option>
                                                    <option value="Transportasi Penerbangan (Transit)">Transportasi Penerbangan (Transit)</option>
                                                    <option value="Transportasi Penerbangan (Kembali)">Transportasi Penerbangan (Kembali)</option>
                                                  </select>
                                              </div>
                                              <div class="col-md-5">
                                                  <label class="control-label">Maskapai/Moda</label>
                                                  <input name="maskapai" class="form-control" />
                                              </div>
                                              <div class="col-md-3">
                                                  <label class="control-label">Biaya</label>
                                                  <input name="biaya" class="form-control angka-angka" />
                                              </div>
                                              <div class="col-md-8">
                                                  <label class="control-label">Keterangan</label>
                                                  <input name="keterangan" class="form-control" />
                                              </div>
                                              <div class="col-md-1">
                                                  <label class="control-label">&nbsp;</label>
                                                  <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                      <i class="fa fa-close"></i>
                                                  </a>
                                              </div>
                                          </div>
                                      </div>
                                      <hr>
                                      <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                          <i class="fa fa-plus"></i> Tambah Lagi</a>
                                      <br>
                                      <br>
                                  </div>
                              </div>
                          </div>
                      </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_akomodasi();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>

              <!-- END FORM-->
            </div>
          </div>
        </div>
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Daftar Perjalanan Dinas</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                  <div class="tabbable-custom ">
                      <ul class="nav nav-tabs ">
                          <li class="active">
                              <a href="#dinas-progress" data-toggle="tab">Progress</a>
                          </li>
                          <li>
                              <a href="#dinas-selesai" data-toggle="tab">Selesai</a>
                          </li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="dinas-progress">
                          <table id="table-akomodasi-progress" data-kolom='[
                            { "data": "nomer","orderable":false },
                            { "data": "tujuan_dinas","orderable":false },
                            { "data": "mulai_dinas","orderable":false },
                            { "data": "selesai_dinas","orderable":false},
                            { "data": "keterangan","orderable":false},
                            { "data": "action","orderable":false},
                            ]'
                            action="{{ url('perjalanan-dinas') }}"
                            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                          <thead>
                          <tr>
                              <th class="all">No.</th>
                              <th class="all">Tujuan Dinas</th>
                              <th class="all">Mulai Dinas</th>
                              <th class="all">Selesai Dinas</th>
                              <th class="all">keterangan</th>
                              <th class="all">Action</th>
                          </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          </table>
                        </div>
                        <div class="tab-pane" id="dinas-selesai">
                          <table id="table-akomodasi-selesai" data-kolom='[
                            { "data": "nomer","orderable":false },
                            { "data": "tujuan_dinas","orderable":false },
                            { "data": "mulai_dinas","orderable":false },
                            { "data": "selesai_dinas","orderable":false},
                            { "data": "keterangan","orderable":false},
                            { "data": "action","orderable":false},
                            ]'
                            action="{{ url('perjalanan-dinas') }}"
                            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                          <thead>
                            <tr>
                                <th class="all">No.</th>
                                <th class="all">Tujuan Dinas</th>
                                <th class="all">Mulai Dinas</th>
                                <th class="all">Selesai Dinas</th>
                                <th class="all">keterangan</th>
                                <th class="all">Action</th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          </table>
                        </div>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
