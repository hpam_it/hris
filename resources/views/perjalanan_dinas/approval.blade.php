@extends('home')
@section('title')
Approval Dinas
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Daftar Perjalanan Dinas</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                  <table id="table-approval-sebelum" data-kolom='[
                    { "data": "nomer","orderable":false },
                    { "data": "tujuan_dinas","orderable":false },
                    { "data": "mulai_dinas","orderable":false },
                    { "data": "selesai_dinas","orderable":false},
                    { "data": "keterangan","orderable":false},
                    { "data": "action","orderable":false},
                    ]'
                    action="{{ url('approval-dinas') }}"
                    class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                  <thead>
                  <tr>
                      <th class="all">No.</th>
                      <th class="all">Tujuan Dinas</th>
                      <th class="all">Mulai Dinas</th>
                      <th class="all">Selesai Dinas</th>
                      <th class="all">keterangan</th>
                      <th class="all">Action</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  </table>
                </div>
          </div>
        </div>
    </div>
</div>
@endsection
