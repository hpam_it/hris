@extends('home')
@section('title')
Perjalanan Dinas
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="icon-grid"></i>Perjalanan Dinas</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" class="form-horizontal" id="form_input"  action="{{ url('perjalanan-dinas') }} ">
                    {{ csrf_field() }}
                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="control-label col-md-2">Mulai Dinas</label>
                          <div class=" col-md-10" >
                            <div class="input-group input-normal date date-picker" data-date="" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                              <span class="input-group-btn">
                                    <button  class="btn default" type="button">
                                            <i class="fa fa-calendar"></i>
                                    </button>
                              </span>
                              <input type="text" readonly name="mulai_dinas" id="mulai_dinas" class="form-control">
                            </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-2">Selesai Dinas</label>
                          <div class=" col-md-10" >
                            <div class="input-group input-normal date date-picker" data-date="" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-calendar"></i>
                                  </button>
                            </span>
                            <input type="text" readonly name="selesai_dinas" id="selesai_dinas" class="form-control">

                          </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-2">Destinasi Dinas</label>
                          <div class=" col-md-10" >
                            <input type="text" name="tujuan_dinas" id="tujuan_dinas" class="form-control" required="true"/>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-2">keterangan</label>
                          <div class=" col-md-10" >
                            <textarea name="keterangan" id="keterangan" class="form-control" required="true"></textarea>
                          </div>
                      </div>
                      <div class="form-group">
                            <label class="col-md-2 control-label">Aktivitas Dinas</label>
                            <div class="col-md-10">
                                <div class="mt-repeater">
                                    <div data-repeater-list="list_aktivitas">
                                        <div data-repeater-item class="row">
                                            <div class="col-md-5">
                                                <label class="control-label">Tanggal</label>
                                                <div class="input-group input-normal date date-picker" data-date="" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
                                                <span class="input-group-btn">
                                                      <button  class="btn default" type="button">
                                                              <i class="fa fa-calendar"></i>
                                                      </button>
                                                </span>
                                                <input type="text" name="tanggal_aktifitas" readonly class="form-control">

                                              </div>
                                            </div>
                                            <div class="col-md-5">
                                                <label class="control-label">Aktivitas</label>
                                                <textarea name="aktivitas" class="form-control" ></textarea>
                                            </div>
                                            <div class="col-md-1">
                                                <label class="control-label">&nbsp;</label>
                                                <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                    <i class="fa fa-close"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                        <i class="fa fa-plus"></i> Tambah Lagi</a>
                                    <br>
                                    <br>
                                  </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_dinas();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Daftar Perjalanan Dinas</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                  <div class="tabbable-custom ">
                      <ul class="nav nav-tabs ">
                          <li class="active">
                              <a href="#dinas-progress" data-toggle="tab">Progress</a>
                          </li>
                          <li>
                              <a href="#dinas-selesai" data-toggle="tab">Selesai</a>
                          </li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="dinas-progress">
                          <table id="table-dinas-progress" data-kolom='[
                            { "data": "nomer","orderable":false },
                            { "data": "tujuan_dinas","orderable":false },
                            { "data": "mulai_dinas","orderable":false },
                            { "data": "selesai_dinas","orderable":false},
                            { "data": "keterangan","orderable":false},
                            { "data": "action","orderable":false},
                            ]'
                            action="{{ url('perjalanan-dinas') }}"
                            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                          <thead>
                          <tr>
                              <th class="all">No.</th>
                              <th class="all">Tujuan Dinas</th>
                              <th class="all">Mulai Dinas</th>
                              <th class="all">Selesai Dinas</th>
                              <th class="all">keterangan</th>
                              <th class="all">Action</th>
                          </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          </table>
                        </div>
                        <div class="tab-pane" id="dinas-selesai">
                          <table id="table-dinas-selesai" data-kolom='[
                            { "data": "nomer","orderable":false },
                            { "data": "tujuan_dinas","orderable":false },
                            { "data": "mulai_dinas","orderable":false },
                            { "data": "selesai_dinas","orderable":false},
                            { "data": "keterangan","orderable":false},
                            { "data": "action","orderable":false},
                            ]'
                            action="{{ url('perjalanan-dinas') }}"
                            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                          <thead>
                            <tr>
                                <th class="all">No.</th>
                                <th class="all">Tujuan Dinas</th>
                                <th class="all">Mulai Dinas</th>
                                <th class="all">Selesai Dinas</th>
                                <th class="all">keterangan</th>
                                <th class="all">Action</th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          </table>
                        </div>
                      </div>
                  </div>
          </div>
        </div>
    </div>
</div>
@endsection
