    <div class="col-md-12">
          <h3>Perjalanan Dinas</h3>
            <div class="table-responsive">
                <table class="table table-bordered">
                  <tr>
                      <th>Tujuan</th>
                      <th>Mulai</th>
                      <th>Selesai</th>
                      <th>Keterangan</th>
                  </tr>
                  <tr>
                      <td>{{ $item->tujuan_dinas }}</td>
                      <td>{{ $item->mulai_dinas }}</td>
                      <td>{{ $item->selesai_dinas }}</td>
                      <td>{{ $item->keterangan }}</td>
                  </tr>
                </table>
            </div>
    </div>
    <div class="col-md-12">
      <h3>Aktifitas Dinas</h3>
      <div class="table-responsive">
        <table class="table table-striped table-bordered">
            <tr>
                <th>Tanggal</th>
                <th>Aktifitas</th>
                <th>PIC Dinas</th>
                <th>Status PIC</th>
                <th>Status Atasan</th>
            </tr>
            @foreach($aktifitas as $aktifitas)
            <tr>
                <td>{{ $aktifitas->tanggal_aktifitas }}</td>
                <td>{{ $aktifitas->aktivitas }}</td>
                <td>{{ $aktifitas->nama_karyawan }}</td>
                <td>{{ $aktifitas->status_pic_dinas }}</td>
                <td>{{ $aktifitas->status_atasan}}</td>
            </tr>
            @endforeach
        </table>
      </div>
    </div>
