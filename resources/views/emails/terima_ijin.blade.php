@component('mail::message')
# {{ $content['title'] }}
{{ $content['text'] }}

@component('mail::panel')
  Nama : {{ $content['nama'] }}<br>
  Tanggal Izin : {{ $content['tanggal'] }}<br>
  Jenis Izin : {{ $content['jenis_ijin'] }}<br>
  Keperluan : {{ $content['alasan'] }}<br>
  @if($content['hanya_marketing']=='Ya')
    Nasabah : {{ $content['nasabah'] }}<br>
    Alamat : {{ $content['alamat'] }}<br>
    Progress : {{ $content['progress'] }}<br>
    Nominal : {{ $content['nominal'] }}<br>
    Tanggal Progress Selanjutnya : {{ $content['tanggal_progress'] }}<br>
    Alasan progress : {{ $content['alasan_progress'] }}
  @endif

@endcomponent

@component('mail::button', ['url' => 'https://hris.hpam.co.id/approve-ijin' ])
{{ $content['button'] }}
@endcomponent

Terima Kasih,

{{ config('app.name') }}
@endcomponent
