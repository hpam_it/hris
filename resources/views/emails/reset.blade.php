@component('mail::message')
# {{ $content['title'] }}
{{ $content['text'] }}
@component('mail::panel')
  Gunakan link dibawah untuk mereset password anda
  <br><a href="{{ config('app.url').$content['link'] }}" >Reset Now</a>
  <br>Atau Copy dan Paste link berikut ke browser anda :
  <br>{{ config('app.url').$content['link'] }}
  <br>Link akan expired dalam 24 jam.
  <br>Sementar link hanya bisa di akses dari jaringan lokal PT.HPAM
@endcomponent

@component('mail::button', ['url' => 'https://hris.hpam.co.id/' ])
{{ $content['button'] }}
@endcomponent

Thanks,
{{ config('app.name') }}
@endcomponent
