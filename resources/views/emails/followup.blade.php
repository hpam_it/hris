@component('mail::message')
# {{ $content['title'] }}
{{ $content['text'] }}

@component('mail::panel')
  Nama : {{ $content['nama'] }}<br>
  Tanggal Progress : {{ $content['tanggal'] }}<br>
  Nama Progress : {{ $content['progress'] }}<br>
  Nasabah : {{ $content['nasabah'] }}<br>
  Nama Progress : {{ $content['progress'] }}<br>
  Hasil Progress : {{ $content['hasil_progress'] }}<br>
  Nominal : {{ $content['nominal'] }}<br>
  Tanggal Progress Selanjutnya : {{ $content['tanggal_progress'] }}<br>
  Keterangan : {{ $content['keterangan'] }}

@endcomponent

@component('mail::button', ['url' => 'https://hris.hpam.co.id/approve-ijin' ])
{{ $content['button'] }}
@endcomponent

Terima Kasih,

{{ config('app.name') }}
@endcomponent
