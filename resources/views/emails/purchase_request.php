@component('mail::message')
# {{ $content['title'] }}
{{ $content['text'] }}
@component('mail::panel')
  Nama Karyawan : {{ $content['nama_karyawan'] }}<br>
  Kantor : {{ $content['nama_kantor'] }}<br>
  Divisi : {{ $content['nama_divisi'] }}<br>
  Tanggal Request : {{ $content['tanggal_request'] }}<br>
  Keterangan : {{ $content['keterangan'] }}<br>
  Alasan : {{ $content['alasan'] }}<br>
@endcomponent

@component('mail::button', ['url' => 'https://hris.hpam.co.id/list-request' ])
{{ $content['button'] }}
@endcomponent

Thanks,
{{ config('app.name') }}
@endcomponent
