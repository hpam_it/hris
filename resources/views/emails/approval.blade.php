@component('mail::message')
# {{ $content['title'] }}
{{ $content['text'] }}
@component('mail::panel')
  Nama : {{ $content['nama'] }}<br>
  Tanggal Cuti : {{ $content['tanggal'] }}<br>
  Jenis Cuti : {{ $content['cuti'] }}<br>
  Keperluan : {{ $content['keperluan'] }}<br>
  Alamat Cuti : {{ $content['alamat_cuti'] }}<br>
  Kontak : {{ $content['kontak_cuti'] }}<br>
  Pengganti : {{ $content['pengganti'] }}<br>
@endcomponent



Thanks,
{{ config('app.name') }}
@endcomponent
