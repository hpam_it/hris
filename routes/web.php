<?php
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {

  Route::resource('dashboard','Dashboard');
  Route::resource('ambilcuti','ambil_cuti');
  Route::resource('perjalanan-dinas','perjalananDinas');
  Route::get('perjalanan-dinas/print/{id_dinas}','perjalananDinas@print_pdf');
  Route::get('approval-dinas-list','perjalananDinas@approval_dinas');
  Route::get('approval-dinas','perjalananDinas@approvaldinas');
  Route::resource('logabsen','logabsen');
  Route::resource('ijin','ijin');
  Route::resource('approve-cuti','approvalcuti');
  Route::resource('approve-ijin','approveijin');
  Route::resource('approve-followup','approvefollowup');
  Route::resource('data-diri','datadiri');
  Route::resource('daftar-visit','daftarvisit');
  Route::resource('daftar-cuti','daftarcuti');
  Route::resource('daftar-ijin','daftarijin');
  Route::get('purchase-request','purchaseRequest@index');
  Route::get('list-request/{idnya}','purchaseRequest@list_request');
  Route::get('daftar-request','purchaseRequest@daftar_request');
  Route::get('approval-request','purchaseRequest@approval');
  Route::get('approve-request/{request_id}','purchaseRequest@approve');
  Route::post('update-request','purchaseRequest@update');
  Route::post('simpan-request','purchaseRequest@simpan');
  Route::get('table-request/{idnya}','purchaseRequest@list_table');
  //survey
  Route::get('survey','surveyController@index');
  Route::get('list-survey/{idnya}','surveyController@list_survey');
  Route::get('buka-survey/{idsurvey}','surveyController@buka_survey');
  Route::post('ikut-survey','surveyController@ikut_survey');
  Route::post('simpan-survey','surveyController@simpan_survey');
  Route::get('daftar-survey','surveyController@daftar_survey');
  Route::resource('target-survey','targetSurvey');
  Route::get('simpan-target','surveyController@simpan_target');
  Route::get('update-target','surveyController@update_target');
  Route::get('data-survey/{oke}','surveyController@data_survey');
  Route::get('hasil-survey/{idsurvey}','surveyController@nilai_survey');
});
Route::get('lihat-file/{id_ijin}','ijin@lihat_file');
//jangan lupa tambah middleware dan masukin ke kernel
Route::group(['middleware' => ['auth', 'role:admin,admin,superadmin,superadmin']], function(){
  Route::post('hitung-cuti','approvalcuti@putar_cuti2');
  Route::resource('data-divisi','data_divisi');
  Route::resource('data-jabatan','data_jabatan');
  Route::resource('data-pangkat','data_pangkat');
  Route::resource('data-pajak','datapajak');
  Route::resource('data-status-karyawan','data_status_karyawan');
  Route::resource('data-kantor','data_kantor');
  Route::resource('data-karyawan','data_karyawan');
  Route::resource('data-cuti','datacuti');
  Route::resource('data-calendar','data_calendar');
  Route::resource('data-struktur','strukturController');
  Route::resource('data-ijin','dataijin');
  Route::resource('data-progress','progress');

});

Route::group(['middleware' => ['auth', 'role:sekretaris,superadmin,admin,direktur']], function(){
    Route::resource('absensi','absensi');
  Route::get('/karyawan/export','data_karyawan@export_excel');//ini export excel data karyawan
  Route::get('/daftarcuti/export','daftarcuti@export_excel');//ini export excel data cuti
  Route::get('/daftarijin/export','daftarijin@export_excel');//ini export excel data ijin
  Route::get('/daftarvisit/export','daftarvisit@export_excel');
  Route::get('/rasio_karyawan','absensi@rasio_karyawan');//ini untuk rekap absensi
  Route::get('rekap-cuti','approvalcuti@rekap_cuti',['middleware' => ['auth']]);
});

Route::group(['middleware' => ['auth', 'role:sekretaris,superadmin,admin,admin']], function(){
  Route::resource('surat-jalan','surat_jalan');
  Route::get('akomodasi-dinas','perjalananDinas@akomodasi');
  Route::post('pilih-dinas','perjalananDinas@pilih_dinas');
  Route::post('simpan-akomodasi','perjalananDinas@simpan_akomodasi');
  //Route::resource('daftar-cuti','daftarcuti');
  //Route::resource('daftar-ijin','daftarijin');
  //Route::resource('daftar-visit','daftarvisit');
});

Route::group(['middleware' => ['auth', 'role:marketing,superadmin,superadmin,marketing']], function(){
  Route::resource('data-nasabah','nasabah');
  Route::resource('followup','followupController');
});

//routing aplikasi pph21

Route::group(['middleware' => ['auth', 'role:finance,superadmin,superadmin,finance']], function(){
  Route::resource('data-lapisan','datalapisan');
  Route::resource('data-ptkp','dataptkp');
  Route::resource('input-pajak','inputpajak');
});


//sementara di Luar



//Auth::routes();
// Route::group(['middleware' => ['auth', 'role:superadmin']], function(){
//     Route::resource('barang_masuk_spk','barang_masuk_spk');
//     Route::resource('barang_keluar_spk','barang_keluar_spk');
//     Route::resource('barang_masuk_lain','barang_masuk_lain');
//     Route::resource('barang_keluar_lain','barang_keluar_lain');
//     Route::resource('master_barang','master_barang');
//     Route::resource('master_spk','master_spk');
//     Route::resource('master_suplyer','master_suplyer');
//     Route::resource('master_bagian','master_bagian');
// });
/* auth route */
// Login Routes...
Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
//reset password
Route::post('/password/reset','passwordController@reset');//ini kirim link reset
Route::get('/password/show/{token}','passwordController@show');//ini nampilin reset form
Route::post('/password/ganti','passwordController@ganti');

Route::get('/',function(){
    return redirect('login');
});
Route::get('logout',function(){
    Auth::logout();
    return redirect('login');
});
