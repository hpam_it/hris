<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kelengkapan_document extends Model
{
    protected $fillable = [
      'karyawan_id',
      'nama_dokument',
      'nomor_dokument',
      'scan_dokument',
      'tanggal_berakhir'
    ];
}
