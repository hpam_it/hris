<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class data_asuransi extends Model
{
    protected $fillable = [
      'karyawan_id',
      'nama_asuransi',
      'nomor_asuransi',
      'scan_kartu'
    ];
}
