<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pengeluaran extends Model
{
    protected $fillable=['karyawan_id','biaya_jabatan','iuran_jht','iuran_pensiun','bulan_pajak'];
}
