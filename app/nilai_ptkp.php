<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nilai_ptkp extends Model
{
    protected $fillable=['karyawan_id','data_ptkp_id','qty','total','nilai','bulan_pajak'];
}
