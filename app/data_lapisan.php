<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class data_lapisan extends Model
{
    protected $fillable=['urutan','lapisan','tarif'];
}
