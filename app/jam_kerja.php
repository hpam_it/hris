<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jam_kerja extends Model
{
    protected $fillable=['nama_jamkerja','masuk','pulang','toleransi','maxpulang','tgl_mulai','tgl_selesai','jam_normal','istirahat','jam_istirahat'];
}
