<?php

namespace App\Http\Middleware;

use Closure;

class HasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     **/
    public function handle($request, Closure $next, $role1,$role2,$role3,$role4)
    {
       if ($request->user()->role == $role1  || $request->user()->role == $role2 || $request->user()->role == $role3 || $request->user()->role == $role4 ) {
                return $next($request);
            }
            return redirect('/')
            ->with('message', 'Anda tidak memiliki akses untuk halaman tersebut');
    }
}
