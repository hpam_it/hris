<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\karyawan;
use App\data_cuti;
use App\ambilcuti;
use App\jabatan;
use App\leave;
use App\data_ijin;
use App\data_nasabah;
use App\data_progress;
use App\followup;
use DB;
use App\Mail\mailfollowup;
use Mail;


class approvefollowup extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['halaman']='approve-followup';
        return view('administrasi/approve-followup/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=="list-all"){
          $role=Auth::user()->role;
          $karyawan_id= Auth::id();
          $divisi_id = Auth::user()->divisi_id;
          $kantor_id = Auth::user()->kantor_id;
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
                $item=DB::table('followups')
                        ->select('followups.*','data_nasabahs.nama_nasabah','karyawan.nama_karyawan','progress.nama_progress','hasil_progress.nama_progress as hasil_progres')
                        ->where(function($query) use ($keyword){
                          $query->where('progress.nama_progress','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('hasil_progress.nama_progress','like',$keyword);

                        })
                        ->where('followups.atasan',$karyawan_id)
                        ->where(function($query) {
                          $query->where('followups.statusatasan','pending');
                        })
                        ->leftjoin('karyawans as karyawan','followups.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_progresses as progress','followups.progress_id','=','progress.id')
                        ->leftjoin('data_progresses as hasil_progress','followups.hasil_progress_id','=','hasil_progress.id')
                        ->leftjoin('data_nasabahs','followups.nasabah_id','=','data_nasabahs.id')
                        ->orderBy('followups.tanggal_progress','asc')
                        ->get();

              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';

                $item=DB::table('followups')
                        ->select('followups.*','data_nasabahs.nama_nasabah','karyawan.nama_karyawan','progress.nama_progress','hasil_progress.nama_progress as hasil_progres')
                        ->where(function($query) use ($keyword){
                          $query->where('progress.nama_progress','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('hasil_progress.nama_progress','like',$keyword);

                        })
                        ->where('followups.atasan',$karyawan_id)
                        ->where(function($query) {
                          $query->where('followups.statusatasan','pending');
                        })
                        ->leftjoin('karyawans as karyawan','followups.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_progresses as progress','followups.progress_id','=','progress.id')
                        ->leftjoin('data_progresses as hasil_progress','followups.hasil_progress_id','=','hasil_progress.id')
                        ->leftjoin('data_nasabahs','followups.nasabah_id','=','data_nasabahs.id')
                        ->orderBy('followups.tanggal_progress','asc')
                        ->skip($awal)
                        ->take($banyak)
                        ->get();
                $total=DB::table('followups')
                        ->select('followups.*','data_nasabahs.nama_nasabah','karyawan.nama_karyawan','progress.nama_progress','hasil_progress.nama_progress as hasil_progres')
                        ->where(function($query) use ($keyword){
                          $query->where('progress.nama_progress','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('hasil_progress.nama_progress','like',$keyword);

                        })
                        ->where('followups.atasan',$karyawan_id)
                        ->where(function($query) {
                          $query->where('followups.statusatasan','pending');
                        })
                        ->leftjoin('karyawans as karyawan','followups.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_progresses as progress','followups.progress_id','=','progress.id')
                        ->leftjoin('data_progresses as hasil_progress','followups.hasil_progress_id','=','hasil_progress.id')
                        ->leftjoin('data_nasabahs','followups.nasabah_id','=','data_nasabahs.id')
                        ->orderBy('followups.tanggal_progress','asc')
                        ->get();

              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);

          } else if($banyak<0) {
              $item=DB::table('followups')
                      ->select('followups.*','data_nasabahs.nama_nasabah','karyawan.nama_karyawan','progress.nama_progress','hasil_progress.nama_progress as hasil_progres')
                      ->where('followups.atasan',$karyawan_id)
                      ->where(function($query) {
                        $query->where('followups.statusatasan','pending');
                      })
                      ->leftjoin('karyawans as karyawan','followups.karyawan_id','=','karyawan.id')
                      ->leftjoin('data_progresses as progress','followups.progress_id','=','progress.id')
                      ->leftjoin('data_progresses as hasil_progress','followups.hasil_progress_id','=','hasil_progress.id')
                      ->leftjoin('data_nasabahs','followups.nasabah_id','=','data_nasabahs.id')
                      ->orderBy('followups.tanggal_progress','asc')
                      ->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
                $item=DB::table('followups')
                        ->select('followups.*','data_nasabahs.nama_nasabah','karyawan.nama_karyawan','progress.nama_progress','hasil_progress.nama_progress as hasil_progres')
                        ->where('followups.atasan',$karyawan_id)
                        ->where(function($query) {
                          $query->where('followups.statusatasan','pending');
                        })
                        ->leftjoin('karyawans as karyawan','followups.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_progresses as progress','followups.progress_id','=','progress.id')
                        ->leftjoin('data_progresses as hasil_progress','followups.hasil_progress_id','=','hasil_progress.id')
                        ->leftjoin('data_nasabahs','followups.nasabah_id','=','data_nasabahs.id')
                        ->orderBy('followups.tanggal_progress','asc')
                        ->skip($awal)
                        ->take($banyak)
                        ->get();

                $total=DB::table('followups')
                        ->select('followups.*','data_nasabahs.nama_nasabah','karyawan.nama_karyawan','progress.nama_progress','hasil_progress.nama_progress as hasil_progres')
                        ->where('followups.atasan',$karyawan_id)
                        ->where(function($query) {
                          $query->where('followups.statusatasan','pending');
                        })
                        ->leftjoin('karyawans as karyawan','followups.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_progresses as progress','followups.progress_id','=','progress.id')
                        ->leftjoin('data_progresses as hasil_progress','followups.hasil_progress_id','=','hasil_progress.id')
                        ->leftjoin('data_nasabahs','followups.nasabah_id','=','data_nasabahs.id')
                        ->orderBy('followups.tanggal_progress','asc')
                        ->get();
              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x,$karyawan_id) {
              //$item->setAttribute('nomer',$gh_x++);
              $item->nomer=$gh_x++;
              $item->action='';
                if($item->atasan==$karyawan_id){
                  if($item->statusatasan=='pending'){
                    $item->action.='<a href="'.url("approve-followup").'/'.$item->id.'?aksi=terima&status=atasan" data-target="#ajax" data-toggle="modal" class="btn btn-md purple">
                        <i class="fa fa-check"></i> Atasan
                    </a>';
                  }
                }
                /*
                if(Auth::user()->role=='admin'){
                  if($item->statushrd=='pending'){
                    $item->action.='<a href="'.url("approve-ijin").'/'.$item->id.'?aksi=terima&status=hrd" data-target="#ajax" data-toggle="modal" class="btn btn-md yellow">
                        <i class="fa fa-check"></i> HRD
                    </a>';
                  }
                }
                */
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }

      if($request->aksi='terima'){
        $status=$request->status;
        $data['status']=$status;
        $data['item']=DB::table('followups')
                      ->select('followups.*','hasil_progress.nama_progress as hasil_progres','karyawan.nama_karyawan as nama_karyawan','atasan.nama_karyawan as nama_atasan','nasabah.nama_nasabah','progress.nama_progress')
                      ->where('followups.id',$id)
                      ->leftjoin('data_progresses as progress','followups.progress_id','=','progress.id')
                      ->leftjoin('data_progresses as hasil_progress','followups.hasil_progress_id','=','hasil_progress.id')
                      ->leftjoin('karyawans as karyawan','followups.karyawan_id','=','karyawan.id')
                      ->leftjoin('karyawans as atasan','followups.atasan','=','atasan.id')
                      ->leftjoin('data_nasabahs as nasabah','followups.nasabah_id','=','nasabah.id')
                      ->first();
        return view('administrasi/approve-followup/terima',$data);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->aksi=='terima'){
          $followup=followup::find($id);
          $status=$request->status;
          $id_pemutus=Auth::user()->id;
          $keputusan=$request->keputusan;
          $alasan=$request->alasan;
          $progress=data_progress::find($followup->progress_id);
          $hasil_progress=data_progress::find($followup->hasil_progress_id);
          $karyawan=karyawan::find($followup->karyawan_id);

          //akhir tentukan periode_awal
        if($status=='atasan'){
            if($id_pemutus==$followup->atasan){

              $pelaksana=karyawan::find($id_pemutus);
              $email = $karyawan->email;
              $nama_pelaksana = $pelaksana->nama_karyawan;
              // kirim email notif kalo pic bersedia
              $nasabah=data_nasabah::find($followup->nasabah_id);
              $content = [
                    'title' => 'Status Followup '.$karyawan->nama_karyawa,
                    'button' => 'Go To Apps',
                    'text' => 'Pengajuan Follow Anda untuk nasabah '.$nasabah->nama_nasabah.' '.$keputusan.' Oleh Atasan ',
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $followup->tanggal_progress,
                    'keterangan' => $followup->keterangan_progress
                    ];
              $content_lap = [
                    'title' => 'Status Followup '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Pengajuan followup untuk nasabah '.$nasabah->nama_nasabah.' '.$keputusan.' Oleh Atasan',
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $followup->tanggal_progress,
                    'keterangan' => $followup->keterangan_progress
                    ];


                $content['nasabah'] = $nasabah->nama_nasabah;
                $content['progress'] = $progress->nama_progress;
                $content['hasil_progress'] = $progress->hasil_progress;
                $content['nominal'] = number_format($followup->nominal,0,",",".");
                $content['keterangan'] = $followup->keterangan_progress;
                $content['tanggal_progress'] = $followup->tanggal_progress;
                $content['tanggal_progress_berikutnya'] = $followup->tanggal_progress_berikutnya;
                //hrd
                $content_lap['nasabah'] = $nasabah->nama_nasabah;
                $content_lap['progress'] = $progress->nama_progress;
                $content_lap['hasil_progress'] = $progress->hasil_progress;
                $content_lap['nominal'] = number_format($followup->nominal,0,",",".");
                $content_lap['keterangan'] = $followup->keterangan_progress;
                $content_lap['tanggal_progress'] = $followup->tanggal_progress;
                $content_lap['tanggal_progress_berikutnya'] = $followup->tanggal_progress_berikutnya;

             Mail::to($karyawan->email)->send(new mailfollowup($content));
             Mail::to('hris.absensi@hpam.co.id')->send(new mailfollowup($content_lap));

             // Mail::to('zuliantoe@gmail.com')->send(new mailfollowup($content));
             // Mail::to('zuliantoe@gmail.com')->send(new mailfollowup($content_lap));
             //update
             DB::table('followups')
                 ->where('id', $followup->id)
                 ->update(['statusatasan' => $keputusan]);
              return 'ok';
            }else{
              return 'atasan-salah';
            }

          }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
