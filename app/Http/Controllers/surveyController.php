<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use App\divisi;
use App\karyawan;
use App\soal;
use App\kantor;
use App\jawaban;
use App\target_survey;
use App\group_survey;
class surveyController extends Controller
{
  public function index()
  {
      $data['halaman']='survey';
      $id = Auth::id();
      $divisi_id = Auth::user()->divisi_id;
      $kantor_id = Auth::user()->kantor_id;
      $jabatan = Auth::user()->jabatan_id;
      $divisi=divisi::find($divisi_id);
      $kantor=kantor::find($kantor_id);
      $data['divisi']=$divisi->nama_divisi;
      $data['kantor']=$kantor->nama_kantor;
      return view('survey/index',$data);
  }

  public function buka_survey($id_survey){
    $data['halaman']='buka-survey';
    $id_karyawan = Auth::id();
    $group = group_survey::find($id_survey);
    $survey = group_survey::select('group_surveys.id','group_surveys.untuk','group_surveys.nama_group','soals.id as id_soal','soals.pertanyaan','soals.option','soals.label_bawah','soals.label_atas')
              ->leftjoin('soals','soals.group_id','group_surveys.id')
              ->where('group_surveys.id',$id_survey)
              ->get();
    $data['survey']=$survey;
    $data['group_id']=$id_survey;

    if($group->untuk == 'semua'){
      $data['target'] = 0;
      $data['nama_target'] = 'Survey Untuk Semua';
    }else{
      $target=target_survey::select('target_surveys.id as id_target','karyawans.id','karyawans.nama_karyawan')
                                      ->leftjoin('karyawans','target_surveys.karyawan_id','karyawans.id')
                                      ->where('status','1')
                                      ->where('group_id',$id_survey)
                                      ->first();
      $data['target'] = $target->id;
      $data['nama_target'] = $target->nama_karyawan;
    }

    $data['nama_survey']=$group->nama_group;
    return view('survey/buka_survey',$data);
  }

  public function ikut_survey(Request $request){
    $data['halaman']='ikut-survey';
    $target = $request->target;
    $group = $request->group_id;
    $responden = Auth::id();
    $soals=soal::where('group_id',$group)->get();
    $soo=soal::where('group_id',$group)->get();
    $groupnya = group_survey::find($group);
    $jumlah = count($soals);
    $oke=0;
    foreach ($soo as $sss) {
      if(!isset($_POST['soal-'.$sss->id])){
      $oke++;
      }
    }
    if($oke > 0){
      return 0;
    }else{
    //simpan jawaban
    foreach ($soals as $key ) {
      //cek apa sudah ada

      //cek soal apakah dengan option
      $cek_option = soal::find($key->id);
      if($groupnya->untuk == 'semua'){
        $cek = jawaban::where('soal_id',$key->id)->where('responden',$responden)->count();
      }else{
        $cek = jawaban::where('soal_id',$key->id)->where('target',$target)->where('responden',$responden)->count();
      }
      if($cek > 0){
        //update
        if($groupnya->untuk == 'semua'){
          $j=jawaban::where('soal_id',$key->id)->where('responden',$responden)->first();
        }else{
          $j=jawaban::where('soal_id',$key->id)->where('target',$target)->where('responden',$responden)->first();
        }
        $jawaban = jawaban::find($j->id);
        if($cek_option->option == NULL){
          $jawaban->jawaban = '0';
          $jawaban->keterangan = $_POST['soal-'.$key->id];
        }else{
          $jawaban->jawaban = $_POST['soal-'.$key->id];
          $jawaban->keterangan = '';
        }
        $jawaban->soal_id = $key->id;
        $jawaban->target = $target;
        $jawaban->responden = $responden;
        $jawaban->save();
      }else{
        $jawaban = new jawaban;
        if($cek_option->option == NULL){
          $jawaban->jawaban = '0';
          $jawaban->keterangan = $_POST['soal-'.$key->id];
        }else{
          $jawaban->jawaban = $_POST['soal-'.$key->id];
          $jawaban->keterangan = '';
        }
        $jawaban->soal_id = $key->id;
        $jawaban->target = $target;
        $jawaban->responden = $responden;
        $jawaban->save();
      }

    }

    return 'ok';
  }

  }
  public function daftar_survey(Request $request){
      $data['halaman']='daftar-survey';
      $id = Auth::id();
      $divisi_id = Auth::user()->divisi_id;
      $kantor_id = Auth::user()->kantor_id;
      $jabatan = Auth::user()->jabatan_id;
      $divisi=divisi::find($divisi_id);
      $kantor=kantor::find($kantor_id);
      $data['divisi']=$divisi->nama_divisi;
      $data['kantor']=$kantor->nama_kantor;
      return view('survey/daftar-survey',$data);
  }
  public function data_survey(Request $request){
    $data['halaman']='daftar-survey';
    $id = Auth::id();
    $divisi_id = Auth::user()->divisi_id;
    $kantor_id = Auth::user()->kantor_id;
    $jabatan = Auth::user()->jabatan_id;
    $divisi=divisi::find($divisi_id);
    $kantor=kantor::find($kantor_id);
    $data['divisi']=$divisi->nama_divisi;
    $data['kantor']=$kantor->nama_kantor;


    //list table

    $awal=$request->start;
    $banyak=$request->length;
    $banyak_colom=$request->iColumns;
    $kata_kunci_global=$request->sSearch;
    $echo=$request->draw;
    $kata_kunci=$request->search['value'];
    if( ($banyak<0) AND ($kata_kunci != "") ){
        $keyword='%'.$kata_kunci.'%';
        $item=group_survey::select('id','nama_group','keterangan')
                              ->where(function($query)  {
                                    $query->where('nama_group','like',$keyword)
                                          ->orWhere('keterangan','like',$keyword);
                                })
                              ->orderBy('id','desc')->get();
        $data['recordsTotal']=count($item);
        $data['recordsFiltered']=count($item);
    }else if($kata_kunci != "" ) {
        $keyword='%'.$kata_kunci.'%';
        $item=group_survey::select('id','nama_group','keterangan')
                              ->where(function($query)  {
                                    $query->where('nama_group','like',$keyword)
                                          ->orWhere('keterangan','like',$keyword);
                                })
                              ->skip($awal)
                              ->take($banyak)
                              ->orderBy('id','desc')
                              ->get();
        $total=group_survey::select('id','nama_group','keterangan')
                              ->where(function($query)  {
                                    $query->where('nama_group','like',$keyword)
                                          ->orWhere('keterangan','like',$keyword);
                                })
                              ->skip($awal)
                              ->take($banyak)
                              ->orderBy('id','desc')
                              ->count();
        $data['recordsTotal']=$total;
        $data['recordsFiltered']=$total;

    } else if($banyak<0) {
        $item=group_survey::select('id','nama_group','keterangan')
                              ->orderBy('id','desc')->get();
        $data['recordsTotal']=count($item);
        $data['recordsFiltered']=count($item);
    } else {
        $item=group_survey::select('id','nama_group','keterangan')
                              ->skip($awal)->take($banyak)->get();
        $total=group_survey::select('id','nama_group','keterangan')
                              ->count();
        $data['recordsTotal']=$total;
        $data['recordsFiltered']=$total;
    }

    $gh_x=$awal+1;
    $item->each(function($item) use (&$gh_x) {
        $item->setAttribute('nomer',$gh_x++);
        $item->setAttribute('action','<a alt="Proses Request" href="'.url("hasil-survey").'/'.$item->id.'" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon green">
                  <i class="fa fa-check"> Hasil Survey</i>
              </a>
              ');
    });

    $data['draw']=$echo;
    $data['data']=$item;
    return $data;
  }

  public function nilai_survey($idsurvey){
    $data['halaman']='buka-survey';
    $id_karyawan = Auth::id();
    $group = group_survey::find($idsurvey);
    $survey = group_survey::select('group_surveys.id','group_surveys.nama_group','soals.id as id_soal','soals.pertanyaan','soals.option')
              ->leftjoin('soals','soals.group_id','group_surveys.id')
              ->where('group_surveys.id',$idsurvey)
              ->get();
    $data['survey']=$survey;
    $data['group_id']=$idsurvey;
    $data['target']=target_survey::select('karyawans.id','karyawans.nama_karyawan')
                                    ->leftjoin('karyawans','target_surveys.karyawan_id','karyawans.id')
                                    ->where('status','1')
                                    ->where('group_id',$idsurvey)
                                    ->get();
    $data['nama_survey']=$group->nama_group;
    $data['hasilnya']=target_survey::select('karyawans.nama_karyawan')
                      ->addSelect(DB::raw('ROUND(count(responden)/2,0) as responder '))
                      ->addSelect(DB::raw('ROUND(SUM(IF (soal_id=1,jawaban,0)) / (count(responden)/2),2) as soal1 '))
                      ->addSelect(DB::raw('ROUND(SUM(IF (soal_id=2,jawaban,0)) / (count(responden)/2),2) as soal2 '))
                      ->addSelect(DB::raw('ROUND(AVG(jawabans.jawaban),2) as nilai_akhir'))
                      ->leftjoin('karyawans','target_surveys.karyawan_id','karyawans.id')
                      ->leftjoin('soals','target_surveys.group_id','soals.group_id')
                      ->leftjoin('jawabans', function ($join) {
                          $join->on('jawabans.soal_id', '=', 'soals.id')
                          ->On('jawabans.target','=','karyawans.id');
                      })
                      ->where('target_surveys.group_id',$idsurvey)
                      // ->groupBy('soals.group_id')
                      // ->groupBy('soals.id')
                      ->groupBy('target_surveys.karyawan_id')
                      ->orderBy('nilai_akhir','DESC')

                      ->get();
    return view('survey/nilai-survey',$data);
  }

  public function simpan_survey(){
    $data['halaman']='simpan-survey';
  }

  public function list_survey(Request $request)
  {
      $data['halaman']='list-survey';
      $id = Auth::id();
      $divisi_id = Auth::user()->divisi_id;
      $kantor_id = Auth::user()->kantor_id;
      $jabatan = Auth::user()->jabatan_id;
      $divisi=divisi::find($divisi_id);
      $kantor=kantor::find($kantor_id);
      $data['divisi']=$divisi->nama_divisi;
      $data['kantor']=$kantor->nama_kantor;


      //list table

      $awal=$request->start;
      $banyak=$request->length;
      $banyak_colom=$request->iColumns;
      $kata_kunci_global=$request->sSearch;
      $echo=$request->draw;
      $kata_kunci=$request->search['value'];
      if( ($banyak<0) AND ($kata_kunci != "") ){
          $keyword='%'.$kata_kunci.'%';
          $item=group_survey::select('id','nama_group','keterangan')
                                ->where('status','=','1')
                                ->where(function($query)  {
                                      $query->where('nama_group','like',$keyword)
                                            ->orWhere('keterangan','like',$keyword);
                                  })
                                ->orderBy('id','desc')->get();
          $data['recordsTotal']=count($item);
          $data['recordsFiltered']=count($item);
      }else if($kata_kunci != "" ) {
          $keyword='%'.$kata_kunci.'%';
          $item=group_survey::select('id','nama_group','keterangan')
                                ->where('status','=','1')
                                ->where(function($query)  {
                                      $query->where('nama_group','like',$keyword)
                                            ->orWhere('keterangan','like',$keyword);
                                  })
                                ->skip($awal)
                                ->take($banyak)
                                ->orderBy('id','desc')
                                ->get();
          $total=group_survey::select('id','nama_group','keterangan')
                                ->where('status','=','1')
                                ->where(function($query)  {
                                      $query->where('nama_group','like',$keyword)
                                            ->orWhere('keterangan','like',$keyword);
                                  })
                                ->skip($awal)
                                ->take($banyak)
                                ->orderBy('id','desc')
                                ->count();
          $data['recordsTotal']=$total;
          $data['recordsFiltered']=$total;

      } else if($banyak<0) {
          $item=group_survey::select('id','nama_group','keterangan')
                                ->where('status','=','1')
                                ->orderBy('id','desc')->get();
          $data['recordsTotal']=count($item);
          $data['recordsFiltered']=count($item);
      } else {
          $item=group_survey::select('id','nama_group','keterangan')
                                ->where('status','=','1')->skip($awal)->take($banyak)->get();
          $total=group_survey::select('id','nama_group','keterangan')
                                ->where('status','=','1')->count();
          $data['recordsTotal']=$total;
          $data['recordsFiltered']=$total;
      }

      $gh_x=$awal+1;
      $item->each(function($item) use (&$gh_x) {
          $item->setAttribute('nomer',$gh_x++);
          $item->setAttribute('action','<a alt="Proses Request" href="'.url("buka-survey").'/'.$item->id.'" data-target="#full" data-toggle="modal" class="btn btn-md btn-icon green">
                    <i class="fa fa-check"> isi Survey</i>
                </a>
                ');
      });

      $data['draw']=$echo;
      $data['data']=$item;
      return $data;
  }
}
