<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\karyawan;
use App\data_cuti;
use App\ambilcuti;
use App\jabatan;
use App\leave;
use App\data_ijin;
use App\data_nasabah;
use App\data_progress;
use DB;
use App\Mail\mailijin;
use Mail;


class approveijin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['halaman']='approve-ijin';
        return view('administrasi/approve-ijin/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=="list-all"){
          $role=Auth::user()->role;
          $karyawan_id= Auth::id();
          $divisi_id = Auth::user()->divisi_id;
          $kantor_id = Auth::user()->kantor_id;
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' ){
                $item=DB::table('leaves as ijin')
                        ->select('ijin.*','karyawan.nama_karyawan','data_ijins.nama_ijin')
                        ->where(function($query) use ($keyword){
                          $query->where('ijin.alasan','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('data_ijins.nama_ijin','like',$keyword);

                        })
                        ->where(function($query) {
                          $query->where('ijin.statusatasan','pending');
                        })
                        ->leftjoin('karyawans as karyawan','ijin.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_ijins','ijin.data_ijin_id','=','data_ijins.id')
                        ->groupBy('ijin.id')
                        ->orderBy('ijin.id','desc')
                        ->get();
              }else{
                $item=DB::table('leaves as ijin')
                        ->select('ijin.*','karyawan.nama_karyawan','data_ijins.nama_ijin')
                        ->where(function($query) use ($keyword){
                          $query->where('ijin.alasan','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('data_ijins.nama_ijin','like',$keyword);

                        })
                        ->where('ijin.atasan',$karyawan_id)
                        ->where(function($query) {
                          $query->where('ijin.statusatasan','pending');
                        })
                        ->leftjoin('karyawans as karyawan','ijin.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_ijins','ijin.data_ijin_id','=','data_ijins.id')
                        ->groupBy('ijin.id')
                        ->orderBy('ijin.id','desc')
                        ->get();
              }

              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin'){
                $item=DB::table('leaves as ijin')
                        ->select('ijin.*','karyawan.nama_karyawan','data_ijins.nama_ijin')
                        ->where(function($query) use ($keyword){
                          $query->where('ijin.alasan','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('data_ijins.nama_ijin','like',$keyword);

                        })
                        ->where(function($query) {
                          $query->where('ijin.statusatasan','pending');
                        })
                        ->leftjoin('karyawans as karyawan','ijin.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_ijins','ijin.data_ijin_id','=','data_ijins.id')
                        ->groupBy('ijin.id')
                        ->orderBy('ijin.id','desc')
                        ->skip($awal)
                        ->take($banyak)
                        ->get();
                $total=DB::table('leaves as ijin')
                        ->select('ijin.*','karyawan.nama_karyawan','data_ijins.nama_ijin')
                        ->where(function($query) use ($keyword){
                          $query->where('ijin.alasan','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('data_ijins.nama_ijin','like',$keyword);

                        })
                        ->where(function($query) {
                          $query->where('ijin.statusatasan','pending');
                        })
                        ->leftjoin('karyawans as karyawan','ijin.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_ijins','ijin.data_ijin_id','=','data_ijins.id')
                        ->groupBy('ijin.id')
                        ->orderBy('ijin.id','desc')
                        ->count();
              }else{
                $item=DB::table('leaves as ijin')
                        ->select('ijin.*','karyawan.nama_karyawan','data_ijins.nama_ijin')
                        ->where(function($query) use ($keyword){
                          $query->where('ijin.alasan','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('data_ijins.nama_ijin','like',$keyword);

                        })
                        ->where(function($query) {
                          $query->where('ijin.statusatasan','pending');
                        })
                        ->where('ijin.atasan',$karyawan_id)
                        ->leftjoin('karyawans as karyawan','ijin.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_ijins','ijin.data_ijin_id','=','data_ijins.id')
                        ->groupBy('ijin.id')
                        ->orderBy('ijin.id','desc')
                        ->skip($awal)
                        ->take($banyak)
                        ->get();
                $total=DB::table('leaves as ijin')
                        ->select('ijin.*','karyawan.nama_karyawan','data_ijins.nama_ijin')
                        ->where(function($query) use ($keyword){
                          $query->where('ijin.alasan','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('data_ijins.nama_ijin','like',$keyword);

                        })
                        ->where(function($query) {
                          $query->where('ijin.statusatasan','pending');
                        })
                        ->where('ijin.atasan',$karyawan_id)
                        ->leftjoin('karyawans as karyawan','ijin.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_ijins','ijin.data_ijin_id','=','data_ijins.id')
                        ->groupBy('ijin.id')
                        ->orderBy('ijin.id','desc')
                        ->get();
              }

              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);

          } else if($banyak<0) {
            if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin'){
              $item=DB::table('leaves as ijin')
                      ->select('ijin.*','karyawan.nama_karyawan','data_ijins.nama_ijin')
                      ->where(function($query) {
                        $query->where('ijin.statusatasan','pending');
                      })
                      ->leftjoin('karyawans as karyawan','ijin.karyawan_id','=','karyawan.id')
                      ->leftjoin('data_ijins','ijin.data_ijin_id','=','data_ijins.id')
                      ->groupBy('ijin.id')
                      ->orderBy('ijin.id','desc')
                      ->get();
            }else{
              $item=DB::table('leaves as ijin')
                      ->select('ijin.*','karyawan.nama_karyawan','data_ijins.nama_ijin')
                      ->where(function($query) {
                        $query->where('ijin.statusatasan','pending');
                      })
                      ->leftjoin('karyawans as karyawan','ijin.karyawan_id','=','karyawan.id')
                      ->leftjoin('data_ijins','ijin.data_ijin_id','=','data_ijins.id')
                      ->where('ijin.atasan',$karyawan_id)
                      ->groupBy('ijin.id')
                      ->orderBy('ijin.id','desc')
                      ->get();
              }
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
              if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin'){
                $item=DB::table('leaves as ijin')
                        ->select('ijin.*','karyawan.nama_karyawan','data_ijins.nama_ijin')
                        ->where(function($query) {
                          $query->where('ijin.statusatasan','pending');
                        })
                        ->leftjoin('karyawans as karyawan','ijin.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_ijins','ijin.data_ijin_id','=','data_ijins.id')
                        ->groupBy('ijin.id')
                        ->orderBy('ijin.id','desc')
                        ->skip($awal)
                        ->take($banyak)
                        ->get();

                $total=DB::table('leaves as ijin')
                        ->select('ijin.*','karyawan.nama_karyawan','data_ijins.nama_ijin')
                        ->where(function($query) {
                          $query->where('ijin.statusatasan','pending');
                        })
                        ->leftjoin('karyawans as karyawan','ijin.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_ijins','ijin.data_ijin_id','=','data_ijins.id')
                        ->groupBy('ijin.id')
                        ->orderBy('ijin.id','desc')
                        ->get();
              }else{
                $item=DB::table('leaves as ijin')
                        ->select('ijin.*','karyawan.nama_karyawan','data_ijins.nama_ijin')
                        ->where(function($query) {
                          $query->where('ijin.statusatasan','pending');
                        })
                        ->leftjoin('karyawans as karyawan','ijin.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_ijins','ijin.data_ijin_id','=','data_ijins.id')
                        ->where('ijin.atasan',$karyawan_id)
                        ->groupBy('ijin.id')
                        ->orderBy('ijin.id','desc')
                        ->skip($awal)
                        ->take($banyak)
                        ->get();

                $total=DB::table('leaves as ijin')
                        ->select('ijin.*','karyawan.nama_karyawan','data_ijins.nama_ijin')
                        ->where(function($query) {
                          $query->where('ijin.statusatasan','pending');
                        })
                        ->leftjoin('karyawans as karyawan','ijin.karyawan_id','=','karyawan.id')
                        ->leftjoin('data_ijins','ijin.data_ijin_id','=','data_ijins.id')
                        ->where('ijin.atasan',$karyawan_id)
                        ->groupBy('ijin.id')
                        ->orderBy('ijin.id','desc')
                        ->get();
              }

              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x,$karyawan_id) {
              //$item->setAttribute('nomer',$gh_x++);
              $item->nomer=$gh_x++;
              $item->action='';
              $item->checkbox='';
                if($item->atasan==$karyawan_id){
                  if($item->statusatasan=='pending'){
                    $item->action.='<a href="'.url("approve-ijin").'/'.$item->id.'?aksi=terima&status=atasan" data-target="#ajax" data-toggle="modal" class="btn btn-md purple">
                        <i class="fa fa-check"></i> Atasan
                    </a>';
                    $item->checkbox.='<input type="checkbox" name="bulk" value="'.$item->id.'" />';
                  }
                }
                /*
                if(Auth::user()->role=='admin'){
                  if($item->statushrd=='pending'){
                    $item->action.='<a href="'.url("approve-ijin").'/'.$item->id.'?aksi=terima&status=hrd" data-target="#ajax" data-toggle="modal" class="btn btn-md yellow">
                        <i class="fa fa-check"></i> HRD
                    </a>';
                  }
                }
                */
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }

      if($request->aksi='terima'){
        $status=$request->status;
        $data['status']=$status;
        $data['item']=DB::table('leaves as ijin')
                      ->select('ijin.*','data_ijins.hanya_marketing','karyawan.nama_karyawan as nama_karyawan','atasan.nama_karyawan as nama_atasan','data_ijins.nama_ijin','nasabah.nama_nasabah','progress.nama_progress')
                      ->where('ijin.id',$id)
                      ->leftjoin('data_ijins','ijin.data_ijin_id','=','data_ijins.id')
                      ->leftjoin('karyawans as karyawan','ijin.karyawan_id','=','karyawan.id')
                      ->leftjoin('karyawans as atasan','ijin.atasan','=','atasan.id')
                      ->leftjoin('data_nasabahs as nasabah','ijin.nasabah_id','=','nasabah.id')
                      ->leftjoin('data_progresses as progress','ijin.progress_id','=','progress.id')
                      ->first();
        return view('administrasi/approve-ijin/terima',$data);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->aksi=='terima'){
          $ambilijin=leave::find($id);
          $status=$request->status;
          $id_pemutus=Auth::user()->id;
          $keputusan=$request->keputusan;
          $alasan=$request->alasan;
          $ijinnya=data_ijin::find($ambilijin->data_ijin_id);
          $karyawan=karyawan::find($ambilijin->karyawan_id);

          //akhir tentukan periode_awal
        if($status=='atasan'){
            if($id_pemutus==$ambilijin->atasan){
              //update
              DB::table('leaves')
                  ->where('id', $ambilijin->id)
                  ->update(['statusatasan' => $keputusan,'alasanatasan'=> $alasan]);
              $pelaksana=karyawan::find($id_pemutus);
              $email = $karyawan->email;
              $nama_pelaksana = $pelaksana->nama_karyawan;
              // kirim email notif kalo pic bersedia
              $content = [
                    'title' => 'Status Permohonan Izin '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Pengajuan izin Anda'.$karyawan->nama_karyawan.' '.$keputusan.' Oleh Atasan dengan alasan '.$alasan,
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $ambilijin->tanggal_ijin,
                    'alasan' => $ambilijin->alasan,
                    'jenis_ijin' => $ijinnya->nama_ijin,
                    'hanya_marketing' => $ijinnya->hanya_marketing
                    ];
              $content_lap = [
                    'title' => 'Status Permohonan Izin '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Pengajuan izin '.$karyawan->nama_karyawan.' '.$keputusan.' Oleh Atasan dengan alasan '.$alasan,
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $ambilijin->tanggal_ijin,
                    'alasan' => $ambilijin->alasan,
                    'jenis_ijin' => $ijinnya->nama_ijin,
                    'hanya_marketing' => $ijinnya->hanya_marketing
                    ];
              if($ijinnya->hanya_marketing=="Ya"){
                $content['nominal'] = $ambilijin->nominal;
                $content['alamat'] = $ambilijin->alamat;
                $nasabah=data_nasabah::find($ambilijin->nasabah_id);
                $progress=data_progress::find($ambilijin->progress_id);
                $content['nasabah'] = $nasabah->nama_nasabah;
                $content['progress'] = $progress->nama_progress;
                $content['nominal'] = $ambilijin->nominal;
                $content['alasan_progress'] = $ambilijin->alasan_progress;
                $content['tanggal_progress'] = $ambilijin->tanggal_progress;
                //laporan ke hrd
                $content_lap['nominal'] = $ambilijin->nominal;
                $content_lap['alamat'] = $ambilijin->alamat;
                $content_lap['nasabah'] = $nasabah->nama_nasabah;
                $content_lap['progress'] = $progress->nama_progress;
                $content_lap['nominal'] = $ambilijin->nominal;
                $content_lap['alasan_progress'] = $ambilijin->alasan_progress;
                $content_lap['tanggal_progress'] = $ambilijin->tanggal_progress;

              }
              Mail::to($email)->send(new mailijin($content,"",$id));
              Mail::to('hris.absensi@hpam.co.id')->send(new mailijin($content_lap,"",$id));
              return 'ok';
            }else{
              return 'atasan-salah';
            }

          }else if($status=='hrd'){
            $karyawan2=karyawan::find($id_pemutus);
            if($karyawan2->role=='admin' || $karyawan2->role=='admin'){
              //update
              DB::table('leaves')
                  ->where('id', $ambilijin->id)
                  ->update(['statushrd' => $keputusan,'alasanhrd'=> $alasan]);
              $pelaksana=karyawan::find($id_pemutus);
              $email = $karyawan->email;
              $nama_pelaksana = $pelaksana->nama_karyawan;
              // kirim email notif kalo pic bersedi
              $content_atasan = [
                    'title' => 'Status Permohonan Izin '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Pengajuan Izin anda  '.$keputusan.' Oleh HRD dengan alasan :  '.$alasan,
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $ambilijin->tanggal_ijin,
                    'alasan' => $ambilijin->alasan,
                    'jenis_ijin' => $ijinnya->nama_ijin,
                    'hanya_marketing' => $ijinnya->hanya_marketing
                    ];
                    if($ijinnya->hanya_marketing=="Ya"){
                      $content_atasan['nominal'] = $ambilijin->nominal;
                      $content_atasan['alamat'] = $ambilijin->alamat;
                      $nasabah=data_nasabah::find($ambilijin->nasabah_id);
                      $progress=data_progress::find($ambilijin->progress_id);
                      $content_atasan['nasabah'] = $nasabah->nama_nasabah;
                      $content_atasan['progress'] = $progress->nama_progress;
                      $content_atasan['nominal'] = $ambilijin->nominal;
                      $content_atasan['alasan_progress'] = $ambilijin->alasan_progress;
                      $content_atasan['tanggal_progress'] = $ambilijin->tanggal_progress;
                    }
              Mail::to($email)->send(new mailijin($content_atasan));
              return 'ok';
            }else{
              return 'hrd-salah';
            }
          }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
