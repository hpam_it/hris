<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\target_survey;
use App\group_survey;
use App\karyawan;

class targetSurvey extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['halaman']='data-target';
      $data['karyawan']=karyawan::all();
      $data['group']=group_survey::all();
      return view('survey/target/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->aksi=='simpan_item'){
          $cek_item=target_survey::where('karyawan_id',$request->karyawan_id)->Where('group_id',$request->group_id)->count();
          if($cek_item>0){
              return 0;
          } else {
              target_survey::create($request->all());
              return 1;
          }

      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if($request->aksi=='edit'){

            $item=target_survey::where('karyawan_id',$id)->first();
            $kr=karyawan::find($id);
            $data['nama_target']=$kr->nama_karyawan;
            $data['item']=$item;
            //$data['group']=group_survey::where('status','1')->get();
            return view('survey/target/edit_list',$data);
        }

        if($request->aksi=="list-all"){
            $awal=$request->start;
            $banyak=$request->length;
            $banyak_colom=$request->iColumns;
            $kata_kunci_global=$request->sSearch;
            $echo=$request->draw;
            $kata_kunci=$request->search['value'];
            if( ($banyak<0) AND ($kata_kunci != "") ){
                $keyword='%'.$kata_kunci.'%';
                $item=target_survey::select('target_surveys.id as id_target','karyawans.id','nama_karyawan','nama_group','target_surveys.status')
                            ->leftjoin('karyawans','target_surveys.karyawan_id','karyawans.id')
                            ->leftjoin('group_surveys','target_surveys.group_id','group_surveys.id')
                            ->where('nama_karyawan','like',$keyword)
                            ->orWhere('nama_group','like',$keyword)
                            ->orderBy('karyawans.id','desc')
                            ->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            }else if($kata_kunci != "" ) {
                $keyword='%'.$kata_kunci.'%';
                $item=target_survey::select('target_surveys.id as id_target','karyawans.id','nama_karyawan','nama_group','target_surveys.status')
                            ->leftjoin('group_surveys','target_surveys.group_id','group_surveys.id')
                            ->leftjoin('karyawans','target_surveys.karyawan_id','karyawans.id')
                            ->where('nama_karyawan','like',$keyword)
                            ->orWhere('nama_group','like',$keyword)
                            ->orderBy('karyawans.id','desc')
                              ->take($banyak)->orderBy('id','desc')->get();
                $total=target_survey::select('target_surveys.id as id_target','karyawans.id','nama_karyawan','nama_group','target_surveys.status')
                            ->leftjoin('group_surveys','target_surveys.group_id','group_surveys.id')
                            ->leftjoin('karyawans','target_surveys.karyawan_id','karyawans.id')
                            ->where('nama_karyawan','like',$keyword)
                            ->orWhere('nama_group','like',$keyword)
                            ->orderBy('karyawans.id','desc')
                              ->count();
                $data['recordsTotal']=$total;
                $data['recordsFiltered']=$total;

            } else if($banyak<0) {
                $item=target_survey::select('target_surveys.id as id_target','karyawans.id','nama_karyawan','nama_group','target_surveys.status')
                            ->leftjoin('group_surveys','target_surveys.group_id','group_surveys.id')
                            ->leftjoin('karyawans','target_surveys.karyawan_id','karyawans.id')
                ->orderBy('karyawans.id','desc')->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            } else {
                $item=target_survey::select('target_surveys.id as id_target','karyawans.id','nama_karyawan','nama_group','target_surveys.status')
                            ->leftjoin('group_surveys','target_surveys.group_id','group_surveys.id')
                            ->leftjoin('karyawans','target_surveys.karyawan_id','karyawans.id')
                ->skip($awal)->take($banyak)->orderBy('karyawans.id','desc')->get();
                $total=target_survey::select('target_surveys.id as id_target','nama_karyawan','nama_group','target_surveys.status')
                            ->leftjoin('group_surveys','target_surveys.group_id','group_surveys.id')
                            ->leftjoin('karyawans','target_surveys.karyawan_id','karyawans.id')
                ->count();
                $data['recordsTotal']=$total;
                $data['recordsFiltered']=$total;
            }

            $gh_x=$awal+1;
            $item->each(function($item) use (&$gh_x) {
                $item->setAttribute('nomer',$gh_x++);
                if($item->status == '1'){
                  $item->setAttribute('status_target','Aktif');
                }else{
                  $item->setAttribute('status_target','Off');
                }

                $item->setAttribute('action','<a href="'.url("target-survey").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                            <i class="fa fa-edit"></i>
                        </a>
                        <button onclick="hapus_input('."'".url('target-survey')."/".$item->id_target."'".');" class="btn btn-md btn-icon-only red">
                            <i class="fa fa-trash"></i>
                        </button>');
            });

            $data['draw']=$echo;
            $data['data']=$item;
            return $data;
            /*return json_encode($data);*/
        }

        if($request->aksi=='hapus_input'){
            target_survey::find($id)->delete();
            return 1;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->aksi=='update_item'){
        $barang=target_survey::find($id)->update($request->all());
        return 1;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
