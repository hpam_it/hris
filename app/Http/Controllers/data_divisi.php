<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\divisi;

class data_divisi extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['halaman']='data-divisi';
      return view('data/divisi/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->aksi=='simpan_item'){
          $cek_item=divisi::where('nama_divisi',$request->nama_divisi)->orWhere('kode_divisi',$request->kode_divisi)->count();
          if($cek_item>0){
              return 0;
          } else {
              divisi::create($request->all());
              return 1;
          }

      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=='edit'){

        $data['item']=divisi::find($id);
        return view('data/divisi/edit_list',$data);
    }

    if($request->aksi=="list-all"){
        $awal=$request->start;
        $banyak=$request->length;
        $banyak_colom=$request->iColumns;
        $kata_kunci_global=$request->sSearch;
        $echo=$request->draw;
        $kata_kunci=$request->search['value'];
        if( ($banyak<0) AND ($kata_kunci != "") ){
            $keyword='%'.$kata_kunci.'%';
            $item=divisi::select('id','nama_divisi','kode_divisi')->where('nama_divisi','like',$keyword)->orWhere('kode_divisi','like',$keyword)->orderBy('id','desc')->get();
            $data['recordsTotal']=count($item);
            $data['recordsFiltered']=count($item);
        }else if($kata_kunci != "" ) {
            $keyword='%'.$kata_kunci.'%';
            $item=divisi::select('id','nama_divisi','kode_divisi')->skip($awal)->where('kode_divisi','like',$keyword)->orWhere('nama_divisi','like',$keyword)->take($banyak)->orderBy('id','desc')->get();
            $total=divisi::select('id','nama_divisi','kode_divisi')->where('kode_divisi','like',$keyword)->orWhere('nama_divisi','like',$keyword)->count();
            $data['recordsTotal']=$total;
            $data['recordsFiltered']=$total;

        } else if($banyak<0) {
            $item=divisi::select('id','nama_divisi','kode_divisi')->orderBy('id','desc')->get();
            $data['recordsTotal']=count($item);
            $data['recordsFiltered']=count($item);
        } else {
            $item=divisi::select('id','nama_divisi','kode_divisi')->skip($awal)->take($banyak)->orderBy('id','desc')->get();
            $total=divisi::select('id','nama_divisi','kode_divisi')->count();
            $data['recordsTotal']=$total;
            $data['recordsFiltered']=$total;
        }

        $gh_x=$awal+1;
        $item->each(function($item) use (&$gh_x) {
            $item->setAttribute('nomer',$gh_x++);
            $item->setAttribute('action','<a href="'.url("data-divisi").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                        <i class="fa fa-edit"></i>
                    </a>
                    <button onclick="hapus_input('."'".url('data-divisi')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
                        <i class="fa fa-trash"></i>
                    </button>');
        });

        $data['draw']=$echo;
        $data['data']=$item;
        return $data;
        /*return json_encode($data);*/
    }

    if($request->aksi=='hapus_input'){
        divisi::find($id)->delete();
        return 1;
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->aksi=='update_item'){
        $barang=divisi::find($id)->update($request->all());
        return 1;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
