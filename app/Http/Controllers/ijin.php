<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\karyawan;
use App\data_cuti;
use App\ambilcuti;
use App\jabatan;
use App\leave;
use DB;
use App\Mail\mailijin;
use Mail;
use App\struktur;
use App\data_ijin;
use App\data_nasabah;
use App\data_progress;
class ijin extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lihat_file($id_ijin){
      $namafile=leave::find($id_ijin);
      return response(Storage::get('dokument_ijin/'.$namafile->file_pendukung))
            ->withHeaders([
                'Content-Type' => 'image/jpeg'
            ]);
    }

    public function index()
    {
      $id = Auth::id();
      $divisi_id = Auth::user()->divisi_id;
      $kantor_id = Auth::user()->kantor_id;
      $data['halaman']='ijin';
      $data['karyawan']=karyawan::where('status_kerja','aktif')->where('id','=',$id)->get();
      $data['ijin']=leave::where('karyawan_id',$id)->get();
      $data['nasabah']=data_nasabah::where('karyawan_id',$id)->where('status','aktif')->get();
      $data['progress']=data_progress::where('progress_visit','Ya')->get();
      $data['jenis_ijin']=data_ijin::all();
      return view('ijin/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->aksi=='simpan_item'){
        $karyawan=karyawan::find($request->karyawan_id);
        $jabatan=jabatan::find($karyawan->jabatan_id);
        $struktur=struktur::where('jabatan_id',$karyawan->jabatan_id)
                  ->where('divisi_id',$karyawan->divisi_id)
                  ->where('kantor_id',$karyawan->kantor_id)
                  ->first();
        $data_ijin=data_ijin::find($request->data_ijin_id);
        $struktur_atasan=struktur::find($struktur->atasan);
        $atasan=DB::table('karyawans as karyawan')
                ->select('id','email')
                ->where('karyawan.divisi_id',$struktur_atasan->divisi_id)
                ->where('karyawan.jabatan_id',$struktur_atasan->jabatan_id)
                ->where('karyawan.kantor_id',$struktur_atasan->kantor_id)
                ->where('karyawan.status_kerja','aktif')
                ->first();
          $cek_item=leave::where('tanggal_ijin',$request->tanggal_ijin)
                            ->where('jam_ijin',$request->jam_ijin)
                            ->where('karyawan_id',$request->karyawan_id)
                            ->where('statusatasan','!=','Ditolak')
                            ->count();
          $cek_cuti=ambilcuti::where('karyawan_id',$request->karyawan_id)
                    ->where('tanggal_cuti','=',$request->tanggal_ijin)
                    ->where('statusatasan','=','Diterima')
                    ->where('statushrd','=','Diterima')
                    ->count();
          $jamkerja=DB::table('jam_kerjas')
                    ->where('tgl_mulai','<=',$request->tanggal_ijin)
                    ->where('tgl_selesai','>=',$request->tanggal_ijin)
                    ->orderBy('tgl_mulai','DESC')
                    ->first();
          if($cek_item>0 OR $cek_cuti > 0){
              return 0;
          } else {
              //upload dokument pendukung
              $gambar = 0;
              $ukuran = 0;
              $file_pendukung = "";
              if( $request->hasFile('file_ijin') ){
                    $gambar = 0;
                    $ukuran = 0;
                    $extensions = ['jpg','jpeg','png','pdf'];
                    $extensi = $request->file('file_ijin')->getClientOriginalExtension();
                    if(in_array($extensi , $extensions)){
                      $gambar = 1;
                    }
                      // mendapatkan ukuran berkas
                    $size = $request->file('file_ijin')->getClientSize()/1024/1024;
                    if( $size <= 10 ){
                      $ukuran = 1;
                    }
                    if($ukuran==1 AND $gambar==1){
                      //$path = Storage::putFile('logo_bank', $request->file('file_logo'));
                      $namafile = $karyawan->npp."_".time().'.'.$extensi;
                      $path=$request->file('file_ijin')->storeAs(
                              'dokument_ijin',$namafile
                          );
                      //$file_nama = explode('/',$path);
                      $request->merge(['file_pendukung'=>$namafile]);
                      $file_pendukung = $namafile;
                    }
                    if($gambar==0){
                      $data['sukses']=0;
                      $data['error']="File yang diijinkan hanya .jpg,.png";
                      return $data;
                    }

                    if($ukuran==0){
                      $data['sukses']=0;
                      $data['error']="Ukuran file maksimal 10Mb";
                      return $data;
                    }
                  }

                //simpan db dokument
                //tentukan file_type

              //end upload dokument pendukung
              $simpan = new leave();
              if($data_ijin->fullday=='1'){
                $simpan->jam_ijin=$jamkerja->masuk;
                $simpan->sampai_dengan=$jamkerja->pulang;
              }else{
                $simpan->jam_ijin=$request->jam_ijin;
                $simpan->sampai_dengan=$request->sampai_dengan;
              }
              $simpan->karyawan_id=$request->karyawan_id;
              $simpan->data_ijin_id=$request->data_ijin_id;
              $simpan->tanggal_ijin=$request->tanggal_ijin;

              $simpan->jumlah_hari=$request->jumlah_hari;
              $simpan->alasan=$request->alasan;
              $simpan->keterangan=$request->keterangan;
              $simpan->atasan=$atasan->id;
              $simpan->statusatasan='pending';
              $simpan->alasanatasan=$request->alasanatasan;
              $simpan->statushrd='pending';
              $simpan->alasanhrd=$request->alasanhrd;
              $simpan->alamat=$request->alamat;
              $simpan->nasabah_id=$request->nasabah_id;
              $simpan->progress_id=$request->progress_id;
              $simpan->nominal=$request->nominal;
              $simpan->tanggal_progress=$request->tanggal_progress;
              $simpan->alasan_progress=$request->alasan_progress;
              $simpan->file_pendukung=$file_pendukung;
              $simpan->save();
              $data_ijin=data_ijin::find($request->data_ijin_id);
              //kirim notif
              $content_atasan = [
                    'title' => 'Permohonan Izin '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Permohonan Izin berikut ini membutuhkan persetujuan anda :',
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $request->tanggal_ijin,
                    'alasan' => $request->alasan,
                    'jenis_ijin' => $data_ijin->nama_ijin,
                    'hanya_marketing' => $data_ijin->hanya_marketing
                    ];
              $content_hrd = [
                    'title' => 'Permohonan Izin Baru '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Permohonan Izin Baru',
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $request->tanggal_ijin,
                    'alasan' => $request->alasan,
                    'jenis_ijin' => $data_ijin->nama_ijin,
                    'hanya_marketing' => $data_ijin->hanya_marketing
                    ];
              if($data_ijin->hanya_marketing=="Ya"){
                            $nasabah=data_nasabah::find($request->nasabah_id);
                            $progress=data_progress::find($request->progress_id);
                            $content_atasan['nominal'] = $data_ijin->nominal;
                            $content_atasan['alamat'] = $data_ijin->alamat;
                            $content_atasan['nasabah'] = $nasabah->nama_nasabah;
                            $content_atasan['progress'] = $progress->nama_progress;
                            $content_atasan['nominal'] = $request->nominal;
                            $content_atasan['alasan_progress'] = $request->alasan_progress;
                            $content_atasan['tanggal_progress'] = $request->tanggal_progress;
                            //hrd
                            $content_hrd['nominal'] = $request->nominal;
                            $content_hrd['alamat'] = $request->alamat;
                            $content_hrd['nasabah'] = $nasabah->nama_nasabah;
                            $content_hrd['progress'] = $progress->nama_progress;
                            $content_hrd['nominal'] = $request->nominal;
                            $content_hrd['alasan_progress'] = $request->alasan_progress;
                            $content_hrd['tanggal_progress'] = $request->tanggal_progress;
                          }

             Mail::to($atasan->email)->send(new mailijin($content_atasan,$file_pendukung,$simpan->id));
             Mail::to('hris.absensi@hpam.co.id')->send(new mailijin($content_hrd,$file_pendukung,$simpan->id));
        //     Mail::to('agusfahrurroji@gmail.com')->send(new mailijin($content_atasan,$file_pendukung,$simpan->id));
             $data['sukses']=1;
             $data['pesan']="Data izin berhasil disimpan";
             return $data;
          }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=="list_all"){
          $karyawan_id= Auth::id();
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          $status=$request->status;
          $where_status=DB::table('leaves');
          $where_status2=DB::table('leaves');
          switch ($status) {
            case 'pending':
                $where_status=$where_status->where(function($query)  {
                    $query->where('statusatasan','pending');
                });
                $where_status2=$where_status2->where(function($query)  {
                    $query->where('statusatasan','pending');
                });
              break;
            case 'oke':
                $where_status=$where_status->where(function($query) {
                    $query->where('statusatasan','Diterima');
                });
                $where_status2=$where_status2->where(function($query) {
                    $query->where('statusatasan','Diterima');
                });
              break;
            case 'batal':
                $where_status=$where_status->where(function($query) {
                    $query->where('statusatasan','Ditolak')
                          ->orWhere('statushrd','Ditolak')
                          ->orWhere('statusatasan','batal')
                          ->orWhere('statushrd','batal');
                });
                $where_status2=$where_status2->where(function($query) {
                    $query->where('statusatasan','Ditolak')
                          ->orWhere('statushrd','Ditolak')
                          ->orWhere('statusatasan','batal')
                          ->orWhere('statushrd','batal');
                });
              break;
            default:
                $where_status=$where_status->where(function($query) {
                    $query->where('statusatasan','pending')
                          ->orWhere('statushrd','pending');
                });
                $where_status2=$where_status2->where(function($query) {
                    $query->where('statusatasan','pending')
                          ->orWhere('statushrd','pending');
                });
              break;
          }
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              $item=$where_status->select('leaves.*','data_ijins.nama_ijin','data_ijins.hitung')
                            ->where('karyawan_id',$karyawan_id)
                            ->leftjoin('data_ijins','data_ijin_id','=','data_ijins.id')
                            ->where(function($query) use ($keyword) {
                                $query->where('leaves.tanggal_ijin','like',$keyword)
                                      ->orWhere('leaves.alasan','like',$keyword)
                                      ->orWhere('data_ijins.nama_ijin','like',$keyword);
                              });
              $item=$item->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              $item=$where_status->select('leaves.*','data_ijins.nama_ijin','data_ijins.hitung')
                            ->where('karyawan_id',$karyawan_id)
                            ->leftjoin('data_ijins','data_ijin_id','=','data_ijins.id')
                            ->where(function($query) use ($keyword) {
                                $query->where('leaves.tanggal_ijin','like',$keyword)
                                      ->orWhere('leaves.alasan','like',$keyword)
                                      ->orWhere('data_ijins.nama_ijin','like',$keyword);
                              });
              $item=$item->skip($awal)->take($banyak)->orderBy('id','desc')->get();
              $total=$where_status2->select('leaves.*','data_ijins.nama_ijin','data_ijins.hitung')
                            ->where('karyawan_id',$karyawan_id)
                            ->leftjoin('data_ijins','data_ijin_id','=','data_ijins.id')
                            ->where(function($query) use ($keyword) {
                                $query->where('leaves.tanggal_ijin','like',$keyword)
                                      ->orWhere('leaves.alasan','like',$keyword)
                                      ->orWhere('data_ijins.nama_ijin','like',$keyword);
                              });
              $total=$total->get();
              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);

          } else if($banyak<0) {
              $item=$where_status->select('leaves.*','data_ijins.nama_ijin','data_ijins.hitung')
                            ->where('karyawan_id',$karyawan_id)
                            ->leftjoin('data_ijins','data_ijin_id','=','data_ijins.id')
                            ->orderBy('id','desc');
              $item=$item->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
              $item=$where_status->select('leaves.*','data_ijins.nama_ijin','data_ijins.hitung')
                            ->where('karyawan_id',$karyawan_id)
                            ->leftjoin('data_ijins','data_ijin_id','=','data_ijins.id')
                            ->skip($awal)->take($banyak)->orderBy('id','desc');
              $item=$item->get();
              $total=$where_status2->select('leaves.*','data_ijins.nama_ijin','data_ijins.hitung')
                            ->where('karyawan_id',$karyawan_id)
                            ->leftjoin('data_ijins','data_ijin_id','=','data_ijins.id');
              $total=$total->get();
              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x) {
              $item->nomer=$gh_x++;
              // $item->setAttribute('action','<a href="'.url("data-divisi").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
              //             <i class="fa fa-edit"></i>
              //         </a>
              //         <button onclick="hapus_input('."'".url('data-divisi')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
              //             <i class="fa fa-trash"></i>
              //         </button>');
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
