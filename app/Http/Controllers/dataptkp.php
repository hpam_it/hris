<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\data_ptkp;
use DB;

class dataptkp extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['halaman']='data-ptkp';
      return view('pajak/data-ptkp/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->aksi=='simpan_item'){
          $cek_item=data_ptkp::where('nama',$request->nama)->count();
          if($cek_item>0){
              return 0;
          } else {
              data_ptkp::create($request->all());
              return 1;
          }

      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=='edit'){
        $data['item']=data_ptkp::find($id);
        return view('pajak/data-ptkp/edit_list',$data);
        }

        if($request->aksi=="list-all"){
            $awal=$request->start;
            $banyak=$request->length;
            $banyak_colom=$request->iColumns;
            $kata_kunci_global=$request->sSearch;
            $echo=$request->draw;
            $kata_kunci=$request->search['value'];
            if( ($banyak<0) AND ($kata_kunci != "") ){
                $keyword='%'.$kata_kunci.'%';
                $item=data_ptkp::select('id','nama','nilai')->where('nama','like',$keyword)->orderBy('id','desc')->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            }else if($kata_kunci != "" ) {
                $keyword='%'.$kata_kunci.'%';
                $item=data_ptkp::select('id','nama','nilai')->skip($awal)->where('nama','like',$keyword)->take($banyak)->orderBy('id','desc')->get();
                $total=data_ptkp::select('id','nama','nilai')->where('nama','like',$keyword)->count();
                $data['recordsTotal']=$total;
                $data['recordsFiltered']=$total;

            } else if($banyak<0) {
                $item=data_ptkp::select('id','nama','nilai')->orderBy('id','desc')->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            } else {
                $item=data_ptkp::select('id','nama','nilai')->skip($awal)->take($banyak)->orderBy('id','desc')->get();
                $total=data_ptkp::select('id','nama','nilai')->count();
                $data['recordsTotal']=$total;
                $data['recordsFiltered']=$total;
            }

            $gh_x=$awal+1;
            $item->each(function($item) use (&$gh_x) {
                $item->setAttribute('nomer',$gh_x++);
                $item->setAttribute('action','<a href="'.url("data-ptkp").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                            <i class="fa fa-edit"></i>
                        </a>
                        <button data-title="Hapus Data Ini ?" data-toggle="confirmation" data-placement="left" data-url="'.url("data-ptkp")."/".$item->id.'" class="konfirmasi hapus-input btn btn-md btn-icon-only red" >
                            <i class="fa fa-trash"></i>
                        </button>');
            });

            $data['draw']=$echo;
            $data['data']=$item;
            return $data;
            /*return json_encode($data);*/
        }

        if($request->aksi=='hapus_input'){
            data_ptkp::find($id)->delete();
            return 1;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
