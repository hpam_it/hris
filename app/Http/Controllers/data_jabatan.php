<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\jabatan;
class data_jabatan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['halaman']='data-jabatan';
      $data['jabatan']=jabatan::all();
      return view('data/jabatan/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->aksi=='simpan_item'){
          $cek_item=jabatan::where('nama_jabatan',$request->nama_jabatan)
                    ->orWhere('kode_jabatan',$request->kode_jabatan)
                    ->count();
          if($cek_item>0){
              return 0;
          } else {
              jabatan::create($request->all());
              return 1;
          }

      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=='edit'){

          $data['item']=jabatan::find($id);
          $data['jabatan']=jabatan::all();
          return view('data/jabatan/edit_list',$data);
      }

      if($request->aksi=="list-all"){
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              $item=jabatan::select('id','nama_jabatan','kode_jabatan','atasan','tanggung_jawab','wewenang')
                    ->where('nama_jabatan','like',$keyword)
                    ->orWhere('kode_jabatan','like',$keyword)
                    ->orWhere('wewenang','like',$keyword)
                    ->orWhere('tanggung_jawab','like',$keyword)
                    ->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              $item=jabatan::select('id','nama_jabatan','kode_jabatan','atasan','tanggung_jawab','wewenang')->skip($awal)
                    ->where('kode_jabatan','like',$keyword)
                    ->orWhere('nama_jabatan','like',$keyword)
                    ->orWhere('wewenang','like',$keyword)
                    ->orWhere('tanggung_jawab','like',$keyword)
                    ->take($banyak)->orderBy('id','desc')->get();
              $total=jabatan::select('id','nama_jabatan','kode_jabatan','atasan','tanggung_jawab','wewenang')
                    ->where('kode_jabatan','like',$keyword)
                    ->orWhere('nama_jabatan','like',$keyword)
                    ->orWhere('wewenang','like',$keyword)
                    ->orWhere('tanggung_jawab','like',$keyword)
                    ->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;

          } else if($banyak<0) {
              $item=jabatan::select('id','nama_jabatan','kode_jabatan','atasan','tanggung_jawab','wewenang')->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
              $item=jabatan::select('id','nama_jabatan','kode_jabatan','atasan','tanggung_jawab','wewenang')->skip($awal)->take($banyak)->orderBy('id','desc')->get();
              $total=jabatan::select('id','nama_jabatan','kode_jabatan','atasan','tanggung_jawab','wewenang')->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x) {
              $item->setAttribute('nomer',$gh_x++);
              if($item->atasan > 0 ){
                $atasan=jabatan::find($item->atasan);
                $hitung=count($atasan);
                if($hitung>0){
                  $item->setAttribute('atasannya',$atasan->nama_jabatan);
                }else{
                  $item->setAttribute('atasannya','Pilih Atasan Baru');
                }

              }else{
                $item->setAttribute('atasannya','');
              }

              $item->setAttribute('action','<a href="'.url("data-jabatan").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                          <i class="fa fa-edit"></i>
                      </a>
                    
                      <button onclick="hapus_input('."'".url('data-jabatan')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
                          <i class="fa fa-trash"></i>
                      </button>');
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }

      if($request->aksi=='hapus_input'){
          jabatan::find($id)->delete();
          return 1;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->aksi=='update_item'){
        $barang=jabatan::find($id)->update($request->all());
        return 1;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
