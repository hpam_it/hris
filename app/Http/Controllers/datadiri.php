<?php

namespace App\Http\Controllers;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


use App\karyawan;
use App\divisi;
use App\jabatan;
use App\pangkat;
use App\status_karyawan;
use App\kantor;
use App\kelengkapan_document;
use App\data_asuransi;
use App\data_keluarga;
use App\data_pajak;
use Hash;
use Auth;


class datadiri extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $id=Auth::user()->id;
      $karyawan=karyawan::find($id);
      $data['karyawan']=$karyawan;
      $data['jabatan']=jabatan::find($karyawan->jabatan_id);
      $data['pangkat']=pangkat::find($karyawan->pangkat_id);
      $data['divisi']=divisi::find($karyawan->divisi_id);
      $data['kantor']=kantor::find($karyawan->kantor_id);
      $data['dokument']=kelengkapan_document::where('karyawan_id',$id)->orderBy('id','desc')->get();
      $data['halaman']='data-diri';
      return view('data/datadiri/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->aksi="update_item"){
          if($request->passwordnya != null){
            $request->merge(array('password' => bcrypt($request->passwordnya) ));
            $id=Auth::user()->id;
            $kyw=karyawan::find($id);
            $simpan=$kyw->update($request->all());
          }
          return 1;

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=="list-all"){
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              $item=data_ijin::select('id','nama_ijin','hitung')->where('nama_ijin','like',$keyword)->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              $item=data_ijin::select('id','nama_ijin','hitung')->skip($awal)->where('nama_ijin','like',$keyword)->take($banyak)->orderBy('id','desc')->get();
              $total=data_ijin::select('id','nama_ijin','hitung')->where('nama_ijin','like',$keyword)->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;

          } else if($banyak<0) {
              $item=data_ijin::select('id','nama_ijin','hitung')->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
              $item=data_ijin::select('id','nama_ijin','hitung')->skip($awal)->take($banyak)->orderBy('id','desc')->get();
              $total=data_ijin::select('id','nama_ijin','hitung')->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x) {
              $item->setAttribute('nomer',$gh_x++);
              $item->setAttribute('action','<a href="'.url("data-ijin").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                          <i class="fa fa-edit"></i>
                      </a>
                      <button onclick="hapus_input('."'".url('data-ijin')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
                          <i class="fa fa-trash"></i>
                      </button>');
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
