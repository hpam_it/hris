<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\jam_kerja;
use App\divisi;
use App\absen;
use App\ambilcuti;
use App\hari_libur;
use App\leave;
use App\data_ijin;
class absensi extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['halaman']='absensi';
        $data['divisi']=divisi::all();
        return view('absensi/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //import log {sampe sini ya, renaca mau bikin import log dengan progressbar ajax}
        if($request->aksi=='import-log'){
          $awal=$request->awal;
          $akhir=$request->akhir;
          $saiki=explode('/',$request->posisi);
          $posisi=$saiki[2]."-".$saiki[0]."-".$saiki[1]." 00:00:00" ;
          $posisi2=$saiki[2]."-".$saiki[0]."-".$saiki[1]." 23:59:59";
          //  $posisi=date_format($saiki, 'Y-m-d');
          $db_username = ''; //username
          $db_password = ''; //password
          //path to database file
          $database_path = "D:/public/public/att2000.mdb";
          $database = new \PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$database_path; Uid=$db_username; Pwd=$db_password;");
          $baca  = $database->prepare("SELECT COUNT(*) as [totalnya] FROM CHECKINOUT A LEFT JOIN USERINFO B ON A.USERID = B.USERID WHERE A.CHECKTIME > :awal AND A.CHECKTIME < :akhir");
          $baca->execute(array(':awal' => $posisi, ':akhir' => $posisi2));
          //$hitung = $database->query($sql1);
          $jumlah = $baca->fetch();
          $total = $jumlah['totalnya'];
          if($total==0){
            return 0;
          }else{
          $database2 = new \PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$database_path; Uid=$db_username; Pwd=$db_password;");
          $baca2 =$database2->prepare("SELECT A.*,B.Badgenumber  FROM CHECKINOUT A LEFT JOIN USERINFO B ON A.USERID = B.USERID WHERE A.CHECKTIME > :posisi AND A.CHECKTIME < :posisi2 "); //cari cara rownum
          $baca2->execute(array(':posisi' => $posisi,':posisi2'=>$posisi2));
          //return json_encode($baca2->fetch());
          while ($log = $baca2->fetch()) {
                $cek=absen::where('userid',$log['USERID'])->where('checktime',$log['CHECKTIME'])->count();
                if($cek==0){
                  $insert=new absen;
                  $insert['userid']=$log['USERID'];
                  $insert['checktime']=$log['CHECKTIME'];
                  $insert->save();
                }
             }
          return $total;
          }

        }
        //simpan jadwal
        if($request->aksi=='simpan-jadwal'){
          $simpan=jam_kerja::create($request->all());
          if($simpan){
            return '1';
          }else{
            return '0';
          }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=='list-absen'){
        //data-absensi
        $awal=$request->start;
        $banyak=$request->length;
        $banyak_colom=$request->iColumns;
        $kata_kunci_global=$request->sSearch;
        $echo=$request->draw;
        $tanggal_mulai=$request->startdate;
        $tanggal_selesai=$request->enddate;
        $divisi=$request->divisi;
        $kata_kunci=$request->search['value'];
        $karya=DB::table('karyawans')->select('id')->where('karyawans.nama_karyawan','like','%'.$kata_kunci.'%');
        if($divisi!='all'){
                  $karya=$karya->where('karyawans.divisi_id',$divisi);
          }
        $karya=$karya->get();
        if( ($banyak<0) AND ($kata_kunci != "") ){
            $keyword='%'.$kata_kunci.'%';
            //loop sesuai range tanggal
            if($tanggal_mulai!='' AND $tanggal_selesai!=''){
              $ayo=$tanggal_mulai;
              $absen=[];
              while (strtotime($ayo) <= strtotime($tanggal_selesai)) {
                foreach ($karya as $key) {
                  $cek=DB::table('karyawans')
                          ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                          ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                          ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                          ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                          ->leftjoin('absens','karyawans.userid','=','absens.userid')
                          ->addSelect('karyawans.jamkerjaaktif')
                          ->whereRaw("DATE(absens.checktime) = '$ayo' ")
                          ->where('karyawans.id',$key->id)
                          ->orderBy('absens.checktime','DESC')
                          ->groupBy('karyawans.userid')
                          ->groupBy('karyawans.nama_karyawan')
                          ->groupBy('karyawans.npp')
                          ->groupBy('karyawans.jamkerjaaktif')
                          ->groupBy('tanggal')
                          ->first();
                  $ada=count($cek);
                  if($ada>0){
                        array_push($absen,$cek);
                  }else{
                    $karyawan=DB::table('karyawans')
                            ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                            ->where('karyawans.id',$key->id);
                            if($divisi!='all'){
                                  $karyawan=$karyawan->where('karyawans.divisi_id',$divisi);
                              }
                            $karyawan=$karyawan->first();
                      $karyawan->cek_in='00:00:00';
                      $karyawan->cek_out='00:00:00';
                      $karyawan->tanggal=$ayo;
                      array_push($absen,$karyawan);
                  }
                }

                $ayo = date ("Y-m-d", strtotime("+1 day", strtotime($ayo)));
               }
           }else{
             $absen=DB::table('karyawans')
                     ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                     ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                     ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                     ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                     ->addSelect('karyawans.jamkerjaaktif')
                     ->leftjoin('absens','karyawans.userid','=','absens.userid')
                     ->where('karyawans.nama_karyawan','like',$keyword)
                     ->orderBy('absens.checktime','DESC')
                     ->groupBy('karyawans.userid')
                     ->groupBy('karyawans.nama_karyawan')
                     ->groupBy('karyawans.npp')
                     ->groupBy('karyawans.jamkerjaaktif')
                     ->groupBy('tanggal');
                     if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                         $absen=$absen->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                     }else if($tanggal_mulai!=''){
                         $absen=$absen->where('absens.checktime','>=',$tanggal_mulai);
                     }else if($tanggal_selesai!=''){
                         $absen=$absen->where('absens.checktime','<=',$tanggal_selesai);
                     }

                     $absen=$absen->get();
           }


          $data['recordsTotal']=count($absen);
          $data['recordsFiltered']=count($absen);
        }else if($kata_kunci != "" ) {
            $keyword='%'.$kata_kunci.'%';
            $absen=DB::table('karyawans')
                    ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                    ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                    ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                    ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                    ->addSelect('karyawans.jamkerjaaktif')
                    ->leftjoin('absens','karyawans.userid','=','absens.userid')
                    ->where('karyawans.nama_karyawan','like',$keyword)
                    ->skip($awal)
                    ->take($banyak)
                    ->orderBy('absens.checktime','DESC')
                    ->groupBy('karyawans.userid')
                    ->groupBy('karyawans.nama_karyawan')
                    ->groupBy('karyawans.npp')
                    ->groupBy('tanggal')
                    ->orderBy('karyawans.userid')
                    ->orderBy('tanggal','ASC');
                    if($divisi!='all'){
                            $absen=$absen->where('karyawans.divisi_id',$divisi);
                    }

                    if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                        $absen=$absen->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                    }else if($tanggal_mulai!=''){
                        $absen=$absen->where('absens.checktime','>=',$tanggal_mulai);
                    }else if($tanggal_selesai!=''){
                        $absen=$absen->where('absens.checktime','<=',$tanggal_selesai);
                    }

                    $absen=$absen->get();
            $total=DB::table('karyawans')
                    ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                    ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                    ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                    ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                    ->addSelect('karyawans.jamkerjaaktif')
                    ->leftjoin('absens','karyawans.userid','=','absens.userid')
                    ->where('karyawans.nama_karyawan','like',$keyword)
                    ->orderBy('absens.checktime','DESC')
                    ->groupBy('karyawans.userid')
                    ->groupBy('karyawans.nama_karyawan')
                    ->groupBy('karyawans.npp')
                    ->groupBy('tanggal')
                    ->orderBy('karyawans.userid')
                    ->orderBy('tanggal','ASC');
                    if($divisi!='all'){
                            $total=$total->where('karyawans.divisi_id',$divisi);
                    }

                    if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                        $total=$total->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                    }else if($tanggal_mulai!=''){
                        $total=$total->where('absens.checktime','>=',$tanggal_mulai);
                    }else if($tanggal_selesai!=''){
                        $total=$total->where('absens.checktime','<=',$tanggal_selesai);
                    }
            $total=$total->get();

            //$absen=$absen->get();
            $data['recordsTotal']=count($total);
            $data['recordsFiltered']=count($total);

        } else if($banyak<0) {
          //loop sesuai range tanggal
          if($tanggal_mulai!='' AND $tanggal_selesai!=''){
            $ayo=$tanggal_mulai;
            $absen=[];
            while (strtotime($ayo) <= strtotime($tanggal_selesai)) {
              foreach ($karya as $key) {
                $cek=DB::table('karyawans')
                        ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                        ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                        ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                        ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                        ->leftjoin('absens','karyawans.userid','=','absens.userid')
                        ->addSelect('karyawans.jamkerjaaktif')
                        ->whereRaw("DATE(absens.checktime) = '$ayo' ")
                        ->where('karyawans.id',$key->id)
                        ->orderBy('absens.checktime','DESC')
                        ->groupBy('karyawans.userid')
                        ->groupBy('karyawans.nama_karyawan')
                        ->groupBy('karyawans.npp')
                        ->groupBy('karyawans.jamkerjaaktif')
                        ->groupBy('tanggal')
                        ->first();
                $ada=count($cek);
                if($ada>0){
                      array_push($absen,$cek);
                }else{
                  $karyawan=DB::table('karyawans')
                          ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                          ->where('karyawans.id',$key->id);
                          if($divisi!='all'){
                                $karyawan=$karyawan->where('karyawans.divisi_id',$divisi);
                            }
                          $karyawan=$karyawan->first();
                    $karyawan->cek_in='00:00:00';
                    $karyawan->cek_out='00:00:00';
                    $karyawan->tanggal=$ayo;
                    array_push($absen,$karyawan);
                }
              }

              $ayo = date ("Y-m-d", strtotime("+1 day", strtotime($ayo)));
             }
         }else{
           $absen=DB::table('karyawans')
                   ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                   ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                   ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                   ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                   ->addSelect('karyawans.jamkerjaaktif')
                   ->leftjoin('absens','karyawans.userid','=','absens.userid')
                   ->orderBy('absens.checktime','DESC')
                   ->groupBy('karyawans.userid')
                   ->groupBy('karyawans.nama_karyawan')
                   ->groupBy('karyawans.npp')
                   ->groupBy('karyawans.jamkerjaaktif')
                   ->groupBy('tanggal');
                   if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                       $absen=$absen->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                   }else if($tanggal_mulai!=''){
                       $absen=$absen->where('absens.checktime','>=',$tanggal_mulai);
                   }else if($tanggal_selesai!=''){
                       $absen=$absen->where('absens.checktime','<=',$tanggal_selesai);
                   }

                   $absen=$absen->get();
         }


        $data['recordsTotal']=count($absen);
        $data['recordsFiltered']=count($absen);
        } else {
          $absen=DB::table('karyawans')
                  ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                  ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                  ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                  ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                  ->addSelect('karyawans.jamkerjaaktif')
                  ->leftjoin('absens','karyawans.userid','=','absens.userid')
                  ->orderBy('absens.checktime','DESC')
                  ->groupBy('karyawans.userid')
                  ->groupBy('karyawans.nama_karyawan')
                  ->groupBy('karyawans.npp')
                  ->groupBy('tanggal')
                  ->orderBy('karyawans.userid','ASC')
                  ->skip($awal)
                  ->take($banyak);
                  //->orderBy('tanggal','ASC');
                  if($divisi!='all'){
                          $absen=$absen->where('karyawans.divisi_id',$divisi);
                  }

                  if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                      $absen=$absen->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                  }else if($tanggal_mulai!=''){
                      $absen=$absen->where('absens.checktime','>=',$tanggal_mulai);
                  }else if($tanggal_selesai!=''){
                      $absen=$absen->where('absens.checktime','<=',$tanggal_selesai);
                  }

                  $absen=$absen->get();

            $total=DB::table('karyawans')
                    ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                    ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                    ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                    ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                    ->addSelect('karyawans.jamkerjaaktif')
                    ->leftjoin('absens','karyawans.userid','=','absens.userid')
                    ->orderBy('absens.checktime','DESC')
                    ->groupBy('karyawans.userid')
                    ->groupBy('karyawans.nama_karyawan')
                    ->groupBy('karyawans.npp')
                    ->groupBy('tanggal')
                    ->orderBy('karyawans.userid');
                    //->orderBy('tanggal','ASC');
                    if($divisi!='all'){
                            $total=$total->where('karyawans.divisi_id',$divisi);
                    }

                    if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                        $total=$total->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                    }else if($tanggal_mulai!=''){
                        $total=$total->where('absens.checktime','>=',$tanggal_mulai);
                    }else if($tanggal_selesai!=''){
                        $total=$total->where('absens.checktime','<=',$tanggal_selesai);
                    }
                    $totale=$total->get();
              $data['recordsTotal']=count($totale);
              $data['recordsFiltered']=count($totale);
        }
        if(count($absen) > 0){
        foreach ($absen as $key) {
          $libur=hari_libur::where('tanggal',$key->tanggal)->first();
          //disini cari hari kerjanya, apakah minggu atau hari libur
          $timestamp = strtotime($key->tanggal);
          $day = date('w', $timestamp);
          $hari='';
          switch ($day) {
            case '0':$hari='Minggu';
              break;
            case '1':$hari='Senin';
              break;
            case '2':$hari='Selasa';
              break;
            case '3':$hari='Rabu';
              break;
            case '4':$hari='Kamis';
              break;
            case '5':$hari='Jumat';
              break;
            case '6':$hari='Sabtu';
              break;

          }
          if( $key->cek_in=='00:00:00' AND $key->cek_out=='00:00:00' ){
            //cek apakah ada cuti di tanggal ini
            $cek_cuti=ambilcuti::where('karyawan_id',$key->id)
                      ->where('tanggal_cuti','like','%'.$key->tanggal.'%')
                      ->where('statusatasan','=','Diterima')
                      ->where('statushrd','=','Diterima')
                      ->first();
            $cek_ijin=leave::where('karyawan_id',$key->id)
                      ->where('tanggal_ijin',$key->tanggal)
                      ->first();
            $iscuti=count($cek_cuti);
            $isijin=count($cek_ijin);

            if($iscuti>0){
              $key->jam_kerja='cuti';
            }else if($isijin>0){
              $key->jam_kerja='ijin';
            }else{
              if(count($libur) > 0){
                $key->jam_kerja='libur';
              }else{
                if($hari=='Minggu' || $hari=='Sabtu'){
                  $key->jam_kerja='weekend';
                }else{
                  $key->jam_kerja='mangkir';
                }

              }
            }


            $key->cek_in='';
            $key->cek_out='';
            $key->masuk='';
            $key->pulang='';
            $key->istirahat='';
            $key->jamnormal='';
            $key->toleransi='';
            $key->maxpulang='';
            $key->hari=$hari;
          }elseif($key->tanggal!=null){
          $jams=$key->jamkerjaaktif;
          // $jamkerja=DB::table('jam_kerjas')->whereIn('id',[$jams])
          //                     ->where('tgl_mulai','<',"$key->tanggal")
          //                     ->where('tgl_selesai','>',"'$key->tanggal'")
          //                     ->orderBy('tgl_mulai','DESC')
          //                     ->first();
          $jamkerja=DB::table('jam_kerjas')
                              ->where('tgl_mulai','<=',$key->tanggal)
                              ->where('tgl_selesai','>=',$key->tanggal)
                              ->orderBy('tgl_mulai','DESC')
                              ->first();
          $key->masuk=$jamkerja->masuk;
          $key->pulang=$jamkerja->pulang;
          $key->istirahat=$jamkerja->istirahat;
          $key->jamnormal=$jamkerja->jam_normal;
          $key->toleransi=$jamkerja->toleransi;
          $key->maxpulang=$jamkerja->maxpulang;
          $timestamp = strtotime($key->tanggal);
          $day = date('w', $timestamp);
          $hari='';
          switch ($day) {
            case '0':$hari='Minggu';
              break;
            case '1':$hari='Senin';
              break;
            case '2':$hari='Selasa';
              break;
            case '3':$hari='Rabu';
              break;
            case '4':$hari='Kamis';
              break;
            case '5':$hari='Jumat';
              break;
            case '6':$hari='Sabtu';
              break;

          }
          $key->hari=$hari;
          $masuk = new \DateTime($key->tanggal.' '.$key->masuk);
          $pulang = new \DateTime($key->tanggal.' '.$key->pulang);
          $toleransi = new \DateTime($key->tanggal.' '.$key->toleransi);
          $maxpulang = new \DateTime($key->tanggal.' '.$key->maxpulang);
          $cek_in = new \Datetime($key->cek_in);
          $cek_out = new \Datetime($key->cek_out);
          //convert istirahat ke detik
          $break=explode(':',$key->istirahat);
          $istirahat = ($break[0]*3600) + ($break[1]*60) + $break[2];
          $norm=explode(':',$key->jamnormal);
          $normal = ($norm[0]*3600) + ($norm[1]*60) + $norm[2];
          if( ($cek_in > $masuk) AND ($cek_in < $toleransi) ){
            $selisih=$cek_in->diff($cek_out);
            $kerja=$selisih->format('%H:%I:%S');
          }else if($cek_in <= $masuk ){
            $selisih=$cek_in->diff($cek_out);
            $kerja=$selisih->format('%H:%I:%S');
          }else if($cek_in > $toleransi AND $cek_out > $maxpulang ){
            $selisih=$cek_in->diff($maxpulang);
            $kerja=$selisih->format('%H:%I:%S');
          }else if($cek_in > $toleransi){
            $selisih=$cek_in->diff($cek_out);
            $kerja=$selisih->format('%H:%I:%S');
          }
          $krj=explode(':',$kerja);
          $detikkerja=($krj[0]*3600) + ($krj[1]*60) + $krj[2];
          $totalkerja=$detikkerja - $istirahat;
          if($totalkerja >= $normal ){
            $kerjanya=$normal;
          }elseif($totalkerja < 0){
            $kerjanya=$detikkerja;
          }else{
            $kerjanya=$totalkerja;
          }
          $key->jam_kerja=gmdate("H:i:s", $kerjanya);
          $key->cek_in=$cek_in->format('H:i:s');
          $key->cek_out=$cek_out->format('H:i:s');
        }else{
          $key->jam_kerja='';
          $key->cek_in='';
          $key->cek_out='';
          $key->masuk='';
          $key->pulang='';
          $key->istirahat='';
          $key->jamnormal='';
          $key->toleransi='';
          $key->maxpulang='';
        }
        //cek kalender libur nasional
        if(count($libur) > 0){
          $key->keterangan=$libur->keterangan;
        }else{
          if($hari=='Minggu' || $hari=='Sabtu'){
            $key->keterangan='Week End';
          }else{
            $key->keterangan='Normal';
          }

        }
      }//end foreach


      }
        $data['data']=$absen;
        $data['draw']=$echo;
        return json_encode($data);

      }

      //list jadwal
      if($request->aksi=='list-all'){
        $awal=$request->start;
        $banyak=$request->length;
        $banyak_colom=$request->iColumns;
        $kata_kunci_global=$request->sSearch;
        $echo=$request->draw;
        $kata_kunci=$request->search['value'];
        if( ($banyak<0) AND ($kata_kunci != "") ){
            $keyword='%'.$kata_kunci.'%';
            $item=jam_kerja::where('nama_jamkerja','like',$keyword)->orderBy('id','desc')->get();
            $data['recordsTotal']=count($item);
            $data['recordsFiltered']=count($item);
        }else if($kata_kunci != "" ) {
            $keyword='%'.$kata_kunci.'%';
            $item=jam_kerja::where('nama_jamkerja','like',$keyword)->skip($awal)->take($banyak)->orderBy('id','desc')->get();
            $total=jam_kerja::where('nama_jamkerja','like',$keyword)->count();
            $data['recordsTotal']=$total;
            $data['recordsFiltered']=$total;

        } else if($banyak<0) {
            $item=jam_kerja::orderBy('id','desc')->get();
            $data['recordsTotal']=count($item);
            $data['recordsFiltered']=count($item);
        } else {
            $item=jam_kerja::skip($awal)->take($banyak)->orderBy('id','desc')->get();
            $total=jam_kerja::count();
            $data['recordsTotal']=$total;
            $data['recordsFiltered']=$total;
        }

        $gh_x=$awal+1;
        $item->each(function($item) use (&$gh_x) {
            $item->setAttribute('nomer',$gh_x++);
            $item->setAttribute('action','<a href="'.url("absensi").'/'.$item->id.'/edit/?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                        <i class="fa fa-edit"></i>
                    </a>
                    ');
        });

        $data['draw']=$echo;
        $data['data']=$item;
        return $data;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        if($request->aksi=='edit'){
          $data['jadwal']=jam_kerja::find($id);
          return view('absensi/edit_jadwal',$data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->aksi=='update_jadwal'){
          $update=jam_kerja::find($id)->update($request->all());
          return 1;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
