<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\karyawan;
use App\purchase_request;
use App\jabatan;
use App\kantor;
use App\divisi;
use DB;
use App\Mail\purchase;
use Mail;
use App\struktur;


class purchaseRequest extends Controller
{
  public function index()
  {
      $data['halaman']='purchase-request';
      $id = Auth::id();
      $divisi_id = Auth::user()->divisi_id;
      $kantor_id = Auth::user()->kantor_id;
      $divisi=divisi::find($divisi_id);
      $kantor=kantor::find($kantor_id);
      $data['divisi']=$divisi->nama_divisi;
      $data['kantor']=$kantor->nama_kantor;
      return view('purchase-request/purchase-request',$data);
  }

  public function daftar_request(Request $request)
  {
      $data['halaman']='list-request';
      return view('purchase-request/list-request',$data);
  }

  public function approval(){
    $data['halaman'] = 'approval-request';
    $id = Auth::id();
    $item = purchase_request::select('purchase_requests.id','nama_divisi','nama_kantor','nama_karyawan','tanggal_request','kategori','alasan','keterangan','status')
                          ->leftjoin('divisis','purchase_requests.divisi_id','divisis.id')
                          ->leftjoin('kantors','purchase_requests.kantor_id','kantors.id')
                          ->leftjoin('karyawans','purchase_requests.karyawan_id','karyawans.id')
                          ->where('atasan',$id)
                          ->where('status','Pending Atasan')
                          ->orderBy('purchase_requests.id','desc')
                          ->get();
    $item->each(function($item) {
      $item->action='';
      $item->action.='<a href="'.url("approve-request").'/'.$item->id.'" data-target="#ajax" data-toggle="modal" class="btn btn-md blue">
          <i class="fa fa-check"></i> Atasan
      </a>';
    });

    $data['item'] = $item;
    return view('purchase-request/approval-request',$data);
  }

  public function approve($id){
    $data['halaman'] = 'approval-request';
    $id_user = Auth::id();
    $item = purchase_request::select('purchase_requests.id','nama_divisi','nama_kantor','nama_karyawan','tanggal_request','kategori','alasan','keterangan','status')
                          ->leftjoin('divisis','purchase_requests.divisi_id','divisis.id')
                          ->leftjoin('kantors','purchase_requests.kantor_id','kantors.id')
                          ->leftjoin('karyawans','purchase_requests.karyawan_id','karyawans.id')
                          ->where('purchase_request.id',$id)
                          ->orderBy('purchase_requests.id','desc')
                          ->first();
    $data['item'] = $item;
    return view('purchase-request/approve-request',$data);
  }

  public function list_request(Request $request){

    $id = Auth::id();
    $divisi_id = Auth::user()->divisi_id;
    $kantor_id = Auth::user()->kantor_id;
    $awal=$request->start;
    $banyak=$request->length;
    $banyak_colom=$request->iColumns;
    $kata_kunci_global=$request->sSearch;
    $echo=$request->draw;
    $kata_kunci=$request->search['value'];
    $karyawan = karyawan::find($id);
    if($karyawan->role == 'admin' || $karyawan->role = 'superadmin' ){
      if( ($banyak<0) AND ($kata_kunci != "") ){
          $keyword='%'.$kata_kunci.'%';
          $item=purchase_request::select('purchase_requests.id','nama_divisi','nama_kantor','nama_karyawan','tanggal_request','kategori','alasan','keterangan','status')
                                ->leftjoin('divisis','purchase_requests.divisi_id','divisis.id')
                                ->leftjoin('kantors','purchase_requests.kantor_id','kantors.id')
                                ->leftjoin('karyawans','purchase_requests.karyawan_id','karyawans.id')
                                ->where(function($query)  {
                                      $query->where('alasan','like',$keyword)
                                            ->orWhere('keterangan','like',$keyword)
                                            ->orWhere('tanggal_request','like',$keyword);
                                  })
                                ->orderBy('purchase_requests.id','desc')->get();
          $data['recordsTotal']=count($item);
          $data['recordsFiltered']=count($item);
      }else if($kata_kunci != "" ) {
          $keyword='%'.$kata_kunci.'%';
          $item=purchase_request::select('purchase_requests.id','nama_divisi','nama_kantor','nama_karyawan','tanggal_request','kategori','alasan','keterangan','status')
                                ->leftjoin('divisis','purchase_requests.divisi_id','divisis.id')
                                ->leftjoin('kantors','purchase_requests.kantor_id','kantors.id')
                                ->leftjoin('karyawans','purchase_requests.karyawan_id','karyawans.id')
                                ->where(function($query)  {
                                      $query->where('alasan','like',$keyword)
                                            ->orWhere('keterangan','like',$keyword)
                                            ->orWhere('tanggal_request','like',$keyword);
                                  })
                                ->skip($awal)
                                ->take($banyak)
                                ->orderBy('purchase_requests.id','desc')
                                ->get();
          $total=purchase_request::select('purchase_requests.id','nama_divisi','nama_kantor','nama_karyawan','tanggal_request','kategori','alasan','keterangan','status')
                                ->leftjoin('divisis','purchase_requests.divisi_id','divisis.id')
                                ->leftjoin('kantors','purchase_requests.kantor_id','kantors.id')
                                ->leftjoin('karyawans','purchase_requests.karyawan_id','karyawans.id')
                                ->where(function($query)  {
                                      $query->where('alasan','like',$keyword)
                                            ->orWhere('keterangan','like',$keyword)
                                            ->orWhere('tanggal_request','like',$keyword);
                                  })
                                ->skip($awal)
                                ->take($banyak)
                                ->orderBy('purchase_requests.id','desc')
                                ->count();
          $data['recordsTotal']=$total;
          $data['recordsFiltered']=$total;

      } else if($banyak<0) {
          $item=purchase_request::select('purchase_requests.id','nama_divisi','nama_kantor','nama_karyawan','tanggal_request','kategori','alasan','keterangan','status')
                                ->leftjoin('divisis','purchase_requests.divisi_id','divisis.id')
                                ->leftjoin('kantors','purchase_requests.kantor_id','kantors.id')
                                ->leftjoin('karyawans','purchase_requests.karyawan_id','karyawans.id')
                                ->orderBy('purchase_requests.id','desc')->get();
          $data['recordsTotal']=count($item);
          $data['recordsFiltered']=count($item);
      } else {
          $item=purchase_request::select('purchase_requests.id','nama_divisi','nama_kantor','nama_karyawan','tanggal_request','kategori','alasan','keterangan','status')
                                ->leftjoin('divisis','purchase_requests.divisi_id','divisis.id')
                                ->leftjoin('kantors','purchase_requests.kantor_id','kantors.id')
                                ->leftjoin('karyawans','purchase_requests.karyawan_id','karyawans.id')
                                ->skip($awal)
                                ->take($banyak)
                                ->orderBy('purchase_requests.id','desc')
                                ->get();
          $total=purchase_request::select('purchase_requests.id','nama_divisi','nama_kantor','nama_karyawan','tanggal_request','kategori','alasan','keterangan','status')
                                  ->leftjoin('divisis','purchase_requests.divisi_id','divisis.id')
                                  ->leftjoin('kantors','purchase_requests.kantor_id','kantors.id')
                                  ->leftjoin('karyawans','purchase_requests.karyawan_id','karyawans.id')
                                ->count();
          $data['recordsTotal']=$total;
          $data['recordsFiltered']=$total;
      }

      $gh_x=$awal+1;
      $item->each(function($item) use (&$gh_x) {
          $item->setAttribute('nomer',$gh_x++);
          if($item->status == 'Pending Atasan'){
              $item->setAttribute('action','Pending Atasan');
          }else{
            $item->setAttribute('action','<a alt="Proses Request" href="'.url("purchase-request/request").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                      <i class="fa fa-play"></i>
                  </a>
                  <a alt="Proses Request" href="'.url("purchase-request/request").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only yellow">
                            <i class="fa fa-pause"></i>
                        </a>
                  <button alt="Reject Request" onclick="reject_request('."'".url('purchase-request/reject').$item->id."'".');" class="btn btn-md btn-icon-only red">
                      <i class="fa fa-stop"></i>
                  </button>');
          }

      });
    }else{
      $item=[];
      $data['recordsTotal']=0;
      $data['recordsFiltered']=0;
    }


    $data['draw']=$echo;
    $data['data']=$item;
    return $data;


  }

  public function list_table(Request $request){
    $id = Auth::id();
    $divisi_id = Auth::user()->divisi_id;
    $kantor_id = Auth::user()->kantor_id;

    $awal=$request->start;
    $banyak=$request->length;
    $banyak_colom=$request->iColumns;
    $kata_kunci_global=$request->sSearch;
    $echo=$request->draw;
    $kata_kunci=$request->search['value'];
    if( ($banyak<0) AND ($kata_kunci != "") ){
        $keyword='%'.$kata_kunci.'%';
        $item=purchase_request::select('id','tanggal_request','kategori','alasan','keterangan','status')
                              ->where('karyawan_id','=',$id)
                              ->where(function($query)  {
                                    $query->where('alasan','like',$keyword)
                                          ->orWhere('keterangan','like',$keyword)
                                          ->orWhere('tanggal_request','like',$keyword);
                                })
                              ->orderBy('id','desc')->get();
        $data['recordsTotal']=count($item);
        $data['recordsFiltered']=count($item);
    }else if($kata_kunci != "" ) {
        $keyword='%'.$kata_kunci.'%';
        $item=purchase_request::select('id','tanggal_request','kategori','alasan','keterangan','status')
                              ->where('karyawan_id','=',$id)
                              ->where(function($query)  {
                                    $query->where('alasan','like',$keyword)
                                          ->orWhere('keterangan','like',$keyword)
                                          ->orWhere('tanggal_request','like',$keyword);
                                })
                              ->skip($awal)
                              ->take($banyak)
                              ->orderBy('id','desc')
                              ->get();
        $total=purchase_request::select('id','tanggal_request','kategori','alasan','keterangan','status')
                              ->where('karyawan_id','=',$id)
                              ->where(function($query)  {
                                    $query->where('alasan','like',$keyword)
                                          ->orWhere('keterangan','like',$keyword)
                                          ->orWhere('tanggal_request','like',$keyword);
                                })
                              ->skip($awal)
                              ->take($banyak)
                              ->orderBy('id','desc')
                              ->count();
        $data['recordsTotal']=$total;
        $data['recordsFiltered']=$total;

    } else if($banyak<0) {
        $item=purchase_request::select('id','tanggal_request','kategori','alasan','keterangan','status')
                              ->where('karyawan_id','=',$id)
                              ->orderBy('id','desc')->get();
        $data['recordsTotal']=count($item);
        $data['recordsFiltered']=count($item);
    } else {
        $item=purchase_request::select('id','tanggal_request','kategori','alasan','keterangan','status')
                              ->where('karyawan_id','=',$id)->skip($awal)->take($banyak)->get();
        $total=purchase_request::select('id','tanggal_request','kategori','alasan','keterangan','status')
                              ->where('karyawan_id','=',$id)->count();
        $data['recordsTotal']=$total;
        $data['recordsFiltered']=$total;
    }

    $gh_x=$awal+1;
    $item->each(function($item) use (&$gh_x) {
        $item->setAttribute('nomer',$gh_x++);
    });

    $data['draw']=$echo;
    $data['data']=$item;
    return $data;
  }

  public function simpan(request $request){
    $id = Auth::id();
    $divisi_id = Auth::user()->divisi_id;
    $kantor_id = Auth::user()->kantor_id;
    $jabatan_id = Auth::user()->jabatan_id;

    if($request->aksi=='simpan_item'){
        $cek_item=purchase_request::where('karyawan_id',$id)
                                    ->where('divisi_id',$divisi_id)
                                    ->where('kantor_id',$kantor_id)
                                    ->where('alasan',$request->alasan)
                                    ->where('keterangan',$request->keterangan)
                                    ->where('kategori',$request->kategori)
                                    ->where('tanggal_request',$request->tanggal_request)
                                    ->count();
        if($cek_item>0){
            return 0;
        } else {

            $karyawan = karyawan::find($id);
            // cari atasan
            $struktur=struktur::where('jabatan_id',$karyawan->jabatan_id)
                      ->where('divisi_id',$karyawan->divisi_id)
                      ->where('kantor_id',$karyawan->kantor_id)
                      ->where('atasan','>',0)
                      ->first();
            if(count($struktur) > 0){
              $struktur_atasan=struktur::find($struktur->atasan);
              $atasan=DB::table('karyawans as karyawan')
                      ->select('id','email')
                      ->where('karyawan.divisi_id',$struktur_atasan->divisi_id)
                      ->where('karyawan.jabatan_id',$struktur_atasan->jabatan_id)
                      ->where('karyawan.kantor_id',$struktur_atasan->kantor_id)
                      ->where('karyawan.status_kerja','aktif')
                      ->first();
              if($atasan->id < 0){
                $oke = 0;
              }else{
                $oke = 1;
              }

            }else{
              $oke = 0;
            }


            //jika oke artinya strukturnya tidak bermasalah
            if($oke == 1){
              $simpan = new purchase_request;
               if($jabatan_id == '11' || $jabatan_id == '15' || $jabatan_id == '25' || $divisi_id =='30'){
                 //tidak perlu approval atasan
                 $simpan->status = 'Pending GA';
                 $status = 'Pending GA';
                 $text_2 = 'Purchase request telah diteruskan ke GA untuk di proses lebih lanjut';
                 $email_atasan = $karyawan->email;
                 $simpan->atasan = $karyawan->id;
               }else{
                 //perlu approval atasan
                 $simpan->status = 'Pending Atasan';
                 $status = 'Pending Atasan';
                 $text_2 = 'Purchase request berikut membutuhkan persetujuan anda';
                 $email_atasan = $atasan->email;
                 $simpan->atasan = $atasan->id;
               }
              $simpan->divisi_id = $divisi_id;
              $simpan->kantor_id = $kantor_id;
              $simpan->karyawan_id = $id;
              $simpan->tanggal_request = date('Y-m-d');
              $simpan->alasan = $request->alasan;
              $simpan->kategori = $request->kategori;
              $simpan->keterangan = $request->keterangan;

              $berhasil = $simpan->save();
              if($berhasil){
                $divisi=divisi::find($divisi_id);
                $kantor=kantor::find($kantor_id);
                $content = [
                    'title' => 'Purchase Request Oleh '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Berikut detail purchase request : ',
                    'nama_karyawan' => $karyawan->nama_karyawan.' / '.$karyawan->npp,
                    'nama_kantor' => $kantor->nama_kantor,
                    'nama_divisi' => $divisi->nama_divisi,
                    'tanggal_request' => $request->tanggal_request,
                    'alasan' => $request->alasan,
                    'keterangan' => $request->keterangan,
                    'Status' => $status
                    ];
                $content_2 = [
                    'title' => 'Persetujuan Purchase Request Oleh '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => $text_2.' : ',
                    'nama_karyawan' => $karyawan->nama_karyawan.' / '.$karyawan->npp,
                    'nama_kantor' => $kantor->nama_kantor,
                    'nama_divisi' => $divisi->nama_divisi,
                    'tanggal_request' => $request->tanggal_request,
                    'alasan' => $request->alasan,
                    'keterangan' => $request->keterangan,
                    'Status' => $status
                    ];
                $content_3 =  [
                    'title' => 'Purchase Request Oleh '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Mohon untuk diproses purchase request berikut : ',
                    'nama_karyawan' => $karyawan->nama_karyawan.' / '.$karyawan->npp,
                    'nama_kantor' => $kantor->nama_kantor,
                    'nama_divisi' => $divisi->nama_divisi,
                    'tanggal_request' => $request->tanggal_request,
                    'alasan' => $request->alasan,
                    'keterangan' => $request->keterangan,
                    'Status' => $status
                  ];
            $email='hris.absensi@hpam.co.id';
            $email2=$karyawan->email;
                //Mail::to($email)->send(new purchase($content));
                Mail::to($email2)->send(new purchase($content));
                Mail::to($email_atasan)->send( new purchase($content_2) );
                if($status == 'Pending GA'){
                  Mail::to($email)->send( new purchase($content_2) );
                }
                return 1;
              }else{
                return 0;
              }
            }else{
              return 0;
            }


        }

    }
  }
}
