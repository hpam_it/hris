<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\karyawan;
use App\data_cuti;
use App\ambilcuti;
use App\jabatan;
use DB;
use App\Mail\approval;
use Mail;
use App\leave;
use Excel;

class daftarcuti extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function export_excel(){
         $daftar_cuti=ambilcuti::select('ambilcutis.*','data_cutis.nama_cuti','karyawans.nama_karyawan')
                     ->leftjoin('data_cutis','ambilcutis.data_cuti_id','=','data_cutis.id')
                     ->leftjoin('karyawans','ambilcutis.karyawan_id','=','karyawans.id')
                     ->where('ambilcutis.statuspic','Diterima')
                     ->where('ambilcutis.statushrd','Diterima')
                     ->get()
                     ->toArray();
        $data_cuti=data_cuti::all()->toArray();;
        $karyawan=karyawan::select('id','npp','nama_karyawan')->get()->toArray();;

         Excel::create('data_cuti', function($excel) use($daftar_cuti,$data_cuti,$karyawan) {
                 //mulai insert row
                 $excel->sheet('Pengajuan', function($sheet) use($daftar_cuti) {
                   $sheet->fromArray($daftar_cuti);

                 });
                 $excel->sheet('data_cuti', function($sheet) use($data_cuti) {
                   $sheet->fromArray($data_cuti);
                 });
                 $excel->sheet('karyawan', function($sheet) use($karyawan) {
                   $sheet->fromArray($karyawan);
                 });

         })->export('xls');
     }

    public function index()
    {
      $data['halaman']='daftar-cuti';
      return view('administrasi/daftar-cuti/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     
    public function show(Request $request,$id)
    {
      if($request->aksi=="list-all"){
          $role=Auth::user()->role;
          $divisi_id = Auth::user()->divisi_id;
          $kantor_id = Auth::user()->kantor_id;
          $jabatan_id = Auth::user()->jabatan_id;
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              //if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' ){
                $item=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) use ($keyword){
                          $query->where('ambilcuti.keperluan','like',$keyword)
                          ->orWhere('ambilcuti.alamat_cuti','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('pic.nama_karyawan','like',$keyword);
                        })
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','Diterima')
                                ->Where('ambilcuti.statushrd','Diterima');
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id');
                        if($jabatan_id==15 AND ($role=='user' OR $role=='marketing' OR $role=='' )){
                              $item=$item->where('karyawan.divisi_id',$divisi_id);
                          }
                        if($jabatan_id==11 AND ($role=='user' OR $role=='marketing' OR $role=='' )){
                              $item=$item->where('karyawan.kantor_id',$kantor_id);
                          }
                        $item=$item->groupBy('ambilcuti.nomor_pengajuan')
                        ->orderBy('ambilcuti.id','desc')
                        ->get();
            //}

              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              //if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' ){
                $item=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) use ($keyword) {
                          $query->where('ambilcuti.keperluan','like',$keyword)
                          ->orWhere('ambilcuti.alamat_cuti','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('pic.nama_karyawan','like',$keyword);
                        })
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','Diterima')
                                ->Where('ambilcuti.statushrd','Diterima');
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->skip($awal)
                        ->take($banyak);
                        if($jabatan_id==15 AND ($role=='user' OR $role=='marketing' OR $role=='' )){
                              $item=$item->where('karyawan.divisi_id',$divisi_id);
                          }
                        if($jabatan_id==11 AND ($role=='user' OR $role=='marketing' OR $role=='' )){
                              $item=$item->where('karyawan.kantor_id',$kantor_id);
                          }
                        $item=$item->groupBy('ambilcuti.nomor_pengajuan')
                        ->orderBy('ambilcuti.id','desc')
                        ->get();
                $total=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) use ($keyword) {
                          $query->where('ambilcuti.keperluan','like',$keyword)
                          ->orWhere('ambilcuti.alamat_cuti','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('pic.nama_karyawan','like',$keyword);
                        })
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','Diterima')
                                ->Where('ambilcuti.statushrd','Diterima');
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id');
                        if($jabatan_id==15 AND ($role=='user' OR $role=='marketing' OR $role=='' )){
                              $total=$total->where('karyawan.divisi_id',$divisi_id);
                          }
                        if($jabatan_id==11 AND ($role=='user' OR $role=='marketing' OR $role=='' )){
                              $total=$total->where('karyawan.kantor_id',$kantor_id);
                          }
                        $total=$total->groupBy('ambilcuti.nomor_pengajuan')
                        ->get();
            //  }

              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);

          } else if($banyak<0) {
            //if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' ){
              $item=DB::table('ambilcutis as ambilcuti')
                      ->select('ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                      ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                      ->where(function($query) {
                        $query->where('ambilcuti.statusatasan','Diterima')
                              ->Where('ambilcuti.statushrd','Diterima');
                      })
                      ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                      ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id');
                      if($jabatan_id==15 AND ($role=='user' OR $role=='marketing' OR $role=='' )){
                            $item=$item->where('karyawan.divisi_id',$divisi_id);
                        }
                      if($jabatan_id==11 AND ($role=='user' OR $role=='marketing' OR $role=='' )){
                            $item=$item->where('karyawan.kantor_id',$kantor_id);
                        }
                      $item=$item->orderBy('ambilcuti.id','desc')
                      ->groupBy('ambilcuti.nomor_pengajuan')
                      ->get();
            //}
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
              //if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' ){
                $item=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','Diterima')
                                ->Where('ambilcuti.statushrd','Diterima');
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id');
                        if($jabatan_id==15 AND ($role=='user' OR $role=='marketing' OR $role=='' )){
                              $item=$item->where('karyawan.divisi_id',$divisi_id);
                          }
                        if($jabatan_id==11 AND ($role=='user' OR $role=='marketing' OR $role=='' )){
                              $item=$item->where('karyawan.kantor_id',$kantor_id);
                          }
                        $item=$item->orderBy('ambilcuti.id','desc')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->skip($awal)
                        ->take($banyak)
                        ->get();

                $total=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','Diterima')
                                ->Where('ambilcuti.statushrd','Diterima');
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id');
                        if($jabatan_id==15 AND ($role=='user' OR $role=='marketing' OR $role=='' )){
                              $total=$total->where('karyawan.divisi_id',$divisi_id);
                          }
                        if($jabatan_id==11 AND ($role=='user' OR $role=='marketing' OR $role=='' )){
                              $total=$total->where('karyawan.kantor_id',$kantor_id);
                          }
                        $total=$total->groupBy('ambilcuti.nomor_pengajuan')
                        ->get();
            //  }

              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x) {
              //$item->setAttribute('nomer',$gh_x++);
              setlocale(LC_TIME,'id_ID.UTF8', 'id_ID.UTF-8','id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID');
              if($item->tanggal_tanggal !=""){
              $tgl_cuti=explode(',',$item->tanggal_tanggal);
              $tanggal=[];
              foreach ($tgl_cuti as $k) {
                $tanggal[]=strftime("%A %d %b %y", strtotime($k));
              }
              $item->tanggal_tanggal=implode(', ',$tanggal);
            }else{
              $item->tanggal_tanggal=strftime("%A %d %b %y", strtotime($item->tanggal_cuti));
            }


              $item->nomer=$gh_x++;
              $item->action='<a href="'.url("daftar-cuti").'/'.$item->id.'?aksi=cancel" data-target="#ajax" data-toggle="modal" class="btn btn-md blue">
                  <i class="fa fa-check"></i> Cancel
              </a>';
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }

      if($request->aksi='cancel'){
        $status=$request->status;
        $ambil=ambilcuti::find($id);
        $data['status']=$status;
        $data['item']=DB::table('ambilcutis as ambilcuti')
                      ->select('ambilcuti.*','data_cuti.nama_cuti','karyawan.nama_karyawan as nama_karyawan','pengganti.nama_karyawan as nama_pengganti','atasan.nama_karyawan as nama_atasan')
                      ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                      ->where('ambilcuti.nomor_pengajuan',$ambil->nomor_pengajuan)
                      ->leftjoin('data_cutis as data_cuti','ambilcuti.data_cuti_id','=','data_cuti.id')
                      ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                      ->leftjoin('karyawans as pengganti','ambilcuti.pic_pengganti','=','pengganti.id')
                      ->leftjoin('karyawans as atasan','ambilcuti.atasan','=','atasan.id')
                      ->first();
        return view('administrasi/daftar-cuti/cancel',$data);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->aksi=='cancel'){
        $ambilcuti=ambilcuti::find($id);
        $tglcuti=explode(',',$ambilcuti->tanggal_tanggal);
        $status=$request->status;
        $id_pemutus=Auth::user()->id;
        $keputusan=$request->keputusan;
        $alasan=$request->alasan;
        $hitung_cuti=$ambilcuti->hitung;
        $cutinya=data_cuti::find($ambilcuti->data_cuti_id);
        $karyawan=karyawan::find($ambilcuti->karyawan_id);
        //tentukan periode untuk bikin sisa cuti baru
        $awal_masuk=explode('-',$karyawan->tgl_gabung);
        $tahun_ini=date('Y').'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
        $depan=intval(date('Y')+1);
        $tahun_depan=$depan.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
        $kemarin=intval(date('Y')-1);
        $tahun_kemarin=$kemarin.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
        $kemarinnya=intval(date('Y')-2);
        $tahun_kemarinnya=$kemarinnya.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
        //akhir tentukan periode_awal
         if(Auth::user()->role=='admin'){
          $karyawan=karyawan::find($id_pemutus);
          if($karyawan->role=='admin'){
            //update
            DB::table('ambilcutis')
                ->where('nomor_pengajuan', $ambilcuti->nomor_pengajuan)
                ->update(['statushrd' => $keputusan,'alasan_hrd'=> $alasan]);
                  //cek apakah tanggal cuti sudah masuk di periode tersebut
                  //jika masuk periode, maka sisa cuti akan di kurangi, jika belum ada maka bikin periode
                  //dan pengurangan 1x
                  foreach ($tglcuti as $key) {
                    if($hitung_cuti==1){
                    $cek=DB::table('sisa_cutis')
                            ->where('data_cuti_id',$ambilcuti->data_cuti_id)
                            ->where('karyawan_id',$ambilcuti->karyawan_id)
                            ->whereRaw($key.' BETWEEN `periode_awal` AND `periode_tambahan`')
                            ->orderBy('periode_awal','DESC')
                            ->get();
                    $berapa=count($cek);
                    if($berapa>1){
                      //berarti sudah masuk periode baru
                      $ambils=DB::table('sisa_cutis')
                              ->where('data_cuti_id',$ambilcuti->data_cuti_id)
                              ->where('karyawan_id',$ambilcuti->karyawan_id)
                              ->whereRaw($key.' BETWEEN `periode_awal` AND `periode_tambahan`')
                              ->orderBy('periode_awal','DESC')
                              ->first();
                      $sisaambil=$ambil->sisa + 1;
                      //kurangi satu cutinya
                      DB::table('sisa_cutis')
                          ->where('id', $ambils->id)
                          ->update(['sisa' => $sisaambil]);

                    }else{
                      //masih di periode kemaren
                      //cek apakah periode kemaren masih ada sisa
                      $ambils=DB::table('sisa_cutis')
                              ->where('data_cuti_id',$ambilcuti->data_cuti_id)
                              ->where('karyawan_id',$ambilcuti->karyawan_id)
                              ->whereRaw($key.' BETWEEN `periode_awal` AND `periode_tambahan`')
                              ->orderBy('periode_awal','DESC')
                              ->first();
                      if(count($ambils) > 0){
                                $sisaambil=$ambils->sisa;
                        }else{
                                $sisaambil=0;
                        }
                      if($sisaambil<=0){
                        //habis atau minus maka bikin sisa cuti Baru
                        $datacuti=data_cuti::find($ambilcuti->data_cuti_id);
                        $sisa=$datacuti->jumlah + 1;
                        if(date('Y-m-d') > $tahun_ini){
                          //$data['cuti_tahunan_oke']='oke';
                          $periode_awal=$tahun_ini;
                          $periode_kemarin=$tahun_kemarin;
                          $periode_akhir=$tahun_depan;
                          $periode_tambahan=date('Y-m-d', strtotime("+6 month",strtotime($periode_akhir)));

                        }else{
                          //$data['cuti_tahunan_oke']='belum';
                          $periode_awal=$tahun_kemarin;
                          $periode_kemarin=$tahun_kemarinnya;
                          $periode_akhir=$tahun_ini;
                          $periode_tambahan=date('Y-m-d', strtotime("+6 month",strtotime($periode_akhir)));
                        }
                        DB::table('sisa_cutis')->insert(
                            [
                              'karyawan_id' => $ambilcuti->karyawan_id,
                              'data_cuti_id' => $ambilcuti->data_cuti_id,
                              'periode_awal' => $periode_awal,
                              'periode_akhir' => $periode_akhir,
                              'periode_tambahan' => $periode_tambahan,
                              'sisa'=>$sisa
                            ]
                          );
                      }else{
                        //kurangi satu cutinya
                        $sisaambil=$ambil->sisa + 1;
                        DB::table('sisa_cutis')
                            ->where('id', $ambils->id)
                            ->update(['sisa' => $sisaambil]);
                      }
                    }
                    }
                  }
            $pelaksana=karyawan::find($id_pemutus);
            $pengganti=karyawan::find($ambilcuti->pic_pengganti);
            $email = $karyawan->email;
            $nama_pelaksana = $pelaksana->nama_karyawan;
            // kirim email notif kalo pic bersedi
            $content = [
                  'title' => 'Status Pengganti '.$karyawan->nama_karyawan,
                  'button' => 'Go To Apps',
                  'text' => 'Admin / HRD '.$nama_pelaksana.' menyatakan '.$keputusan.' dengan alasan '.$alasan,
                  'nama' => $karyawan->nama_karyawan,
                  'tanggal' => $ambilcuti->tanggal_tanggal,
                  'keperluan' => $ambilcuti->keperluan,
                  'pengganti' => $pengganti->nama_karyawan,
                  'cuti' => $cutinya->nama_cuti,
                  'alamat_cuti' => $ambilcuti->alamat_cuti,
                  'kontak_cuti' => $ambilcuti->notelpon
                  ];
            //Mail::to($email)->send(new approval($content));
            return 'ok';
          }else{
            return 'hrd-salah';
          }
        }

      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
