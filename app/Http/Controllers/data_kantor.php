<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kantor;

class data_kantor extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['halaman']='data-kantor';
      return view('data/kantor/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->aksi=='simpan_item'){
          $cek_item=kantor::where('nama_kantor',$request->nama_kantor)->orWhere('kode_kantor',$request->kode_kantor)->count();
          if($cek_item>0){
              return 0;
          } else {
              kantor::create($request->all());
              return 1;
          }

      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        if($request->aksi=='edit'){

            $data['item']=kantor::find($id);
            return view('data/kantor/edit_list',$data);
        }

        if($request->aksi=="list-all"){
            $awal=$request->start;
            $banyak=$request->length;
            $banyak_colom=$request->iColumns;
            $kata_kunci_global=$request->sSearch;
            $echo=$request->draw;
            $kata_kunci=$request->search['value'];
            if( ($banyak<0) AND ($kata_kunci != "") ){
                $keyword='%'.$kata_kunci.'%';
                $item=kantor::select('id','nama_kantor','kode_kantor','alamat_kantor','nama_kota','telpon_kantor')
                            ->where('nama_kantor','like',$keyword)
                            ->orWhere('kode_kantor','like',$keyword)
                            ->orWhere('nama_kota','like',$keyword)
                            ->orWhere('alamat_kantor','like',$keyword)
                            ->orWhere('telpon_kantor','like',$keyword)->orderBy('id','desc')->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            }else if($kata_kunci != "" ) {
                $keyword='%'.$kata_kunci.'%';
                $item=kantor::select('id','nama_kantor','kode_kantor','alamat_kantor','nama_kota','telpon_kantor')
                              ->skip($awal)
                              ->where('kode_kantor','like',$keyword)
                              ->orWhere('nama_kantor','like',$keyword)
                              ->orWhere('nama_kota','like',$keyword)
                              ->orWhere('alamat_kantor','like',$keyword)
                              ->orWhere('telpon_kantor','like',$keyword)
                              ->take($banyak)->orderBy('id','desc')->get();
                $total=kantor::select('id','nama_kantor','kode_kantor','alamat_kantor','nama_kota','telpon_kantor')
                              ->where('kode_kantor','like',$keyword)
                              ->orWhere('nama_kantor','like',$keyword)
                              ->orWhere('nama_kota','like',$keyword)
                              ->orWhere('alamat_kantor','like',$keyword)
                              ->orWhere('telpon_kantor','like',$keyword)
                              ->count();
                $data['recordsTotal']=$total;
                $data['recordsFiltered']=$total;

            } else if($banyak<0) {
                $item=kantor::select('id','nama_kantor','kode_kantor','alamat_kantor','nama_kota','telpon_kantor')
                ->orderBy('id','desc')->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            } else {
                $item=kantor::select('id','nama_kantor','kode_kantor','alamat_kantor','nama_kota','telpon_kantor')
                ->skip($awal)->take($banyak)->orderBy('id','desc')->get();
                $total=kantor::select('id','nama_kantor','kode_kantor','alamat_kantor','nama_kota','telpon_kantor')
                ->count();
                $data['recordsTotal']=$total;
                $data['recordsFiltered']=$total;
            }

            $gh_x=$awal+1;
            $item->each(function($item) use (&$gh_x) {
                $item->setAttribute('nomer',$gh_x++);
                $item->setAttribute('action','<a href="'.url("data-kantor").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                            <i class="fa fa-edit"></i>
                        </a>
                        <button onclick="hapus_input('."'".url('data-kantor')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
                            <i class="fa fa-trash"></i>
                        </button>');
            });

            $data['draw']=$echo;
            $data['data']=$item;
            return $data;
            /*return json_encode($data);*/
        }

        if($request->aksi=='hapus_input'){
            kantor::find($id)->delete();
            return 1;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->aksi=='update_item'){
        $barang=kantor::find($id)->update($request->all());
        return 1;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
