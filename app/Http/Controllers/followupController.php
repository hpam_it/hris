<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\karyawan;
use App\data_cuti;
use App\ambilcuti;
use App\jabatan;
use App\leave;
use DB;
use App\Mail\mailfollowup;
use Mail;
use App\struktur;
use App\data_ijin;
use App\data_nasabah;
use App\data_progress;
use App\followup;
class followupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $id = Auth::id();
      $divisi_id = Auth::user()->divisi_id;
      $kantor_id = Auth::user()->kantor_id;
      $data['halaman']='followup';
      $data['karyawan']=karyawan::where('status_kerja','aktif')->where('id','=',$id)->get();
      $data['nasabah']=data_nasabah::where('karyawan_id',$id)->where('status','aktif')->get();
      $data['progress']=data_progress::where('progress_followup','Ya')->get();
      $data['hasil_progress']=data_progress::where('hasil_progress_followup','Ya')->get();
      return view('followup/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->aksi=='simpan_item'){
        $karyawan=karyawan::find($request->karyawan_id);
        $jabatan=jabatan::find($karyawan->jabatan_id);
        $struktur=struktur::where('jabatan_id',$karyawan->jabatan_id)
                  ->where('divisi_id',$karyawan->divisi_id)
                  ->where('kantor_id',$karyawan->kantor_id)
                  ->first();
        $struktur_atasan=struktur::find($struktur->atasan);
        $atasan=DB::table('karyawans as karyawan')
                ->select('id','email')
                ->where('karyawan.divisi_id',$struktur_atasan->divisi_id)
                ->where('karyawan.jabatan_id',$struktur_atasan->jabatan_id)
                ->where('karyawan.kantor_id',$struktur_atasan->kantor_id)
                ->where('karyawan.status_kerja','aktif')
                ->first();

              //kirim notif
              $content_atasan = [
                    'title' => 'Progress Followup '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Progress berikut ini membutuhkan persetujuan anda :',
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $request->tanggal_progress,
                    'keterangan' => $request->keterangan,
                    'hanya_marketing' => 'Ya'
                    ];
              $content_hrd = [
                    'title' => 'Progress Followup '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Progress Followup Baru',
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $request->tanggal_progress,
                    'keterangan' => $request->keterangan,
                    'hanya_marketing' => 'Ya'
                    ];

                            $nasabah=data_nasabah::find($request->nasabah_id);
                            $progress=data_progress::find($request->progress_id);
                            $hasil_progress=data_progress::find($request->hasil_progress_id);
                            $content_atasan['nominal'] = $request->nominal_progress;
                            $content_atasan['nasabah'] = $nasabah->nama_nasabah;
                            $content_atasan['progress'] = $progress->nama_progress;
                            $content_atasan['hasil_progress'] = $progress->hasil_progress;
                            $content_atasan['nominal'] = $request->nominal;
                            $content_atasan['keterangan'] = $request->keterangan_progress;
                            $content_atasan['tanggal_progress'] = $request->tanggal_progress;
                            $content_atasan['tanggal_progress_berikutnya'] = $request->tanggal_progress_berikutnya;
                            //hrd
                            $content_hrd['nominal'] = $request->nominal;
                            $content_hrd['nasabah'] = $nasabah->nama_nasabah;
                            $content_hrd['progress'] = $progress->nama_progress;
                            $content_hrd['hasil_progress'] = $progress->hasil_progress;
                            $content_hrd['nominal'] = $request->nominal;
                            $content_hrd['keterangan'] = $request->keterangan_progress;
                            $content_hrd['tanggal_progress'] = $request->tanggal_progress;
                            $content_hrd['tanggal_progress_berikutnya'] = $request->tanggal_progress_berikutnya;


             Mail::to($atasan->email)->send(new mailfollowup($content_atasan));
             Mail::to('hris.absensi@hpam.co.id')->send(new mailfollowup($content_hrd));
             // Mail::to('zuliantoe@gmail.com')->send(new mailfollowup($content_hrd));
             // Mail::to('zuliantoe@gmail.com')->send(new mailfollowup($content_atasan));

             $simpan = new followup();
             $simpan->karyawan_id=$request->karyawan_id;
             $simpan->nasabah_id=$request->nasabah_id;
             $simpan->progress_id=$request->progress_id;
             $simpan->hasil_progress_id=$request->hasil_progress_id;
             $simpan->keterangan_progress=$request->keterangan_progress;
             $simpan->tanggal_progress=$request->tanggal_progress;
             $simpan->tanggal_progress_berikutnya=$request->tanggal_progress_berikutnya;
             $simpan->nominal_progress=$request->nominal_progress;
             $simpan->statusatasan='pending';
             $simpan->atasan=$atasan->id;
             $simpan->save();

              return 1;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=="list_all"){
          $karyawan_id= Auth::id();
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          $status=$request->status;
          $where_status=DB::table('followups');
          $where_status2=DB::table('followups');
          switch ($status) {
            case 'pending':
                $where_status=$where_status->where(function($query)  {
                    $query->where('statusatasan','pending');
                });
                $where_status2=$where_status2->where(function($query)  {
                    $query->where('statusatasan','pending');
                });
              break;
            case 'oke':
                $where_status=$where_status->where(function($query) {
                    $query->where('statusatasan','Diterima');
                });
                $where_status2=$where_status2->where(function($query) {
                    $query->where('statusatasan','Diterima');
                });
              break;
            case 'batal':
                $where_status=$where_status->where(function($query) {
                    $query->where('statusatasan','Ditolak');
                });
                $where_status2=$where_status2->where(function($query) {
                    $query->where('statusatasan','Ditolak');
                });
              break;
            default:
                $where_status=$where_status->where(function($query) {
                    $query->where('statusatasan','pending');
                });
                $where_status2=$where_status2->where(function($query) {
                    $query->where('statusatasan','pending');
                });
              break;
          }
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              $item=$where_status->select('followups.*','data_nasabahs.nama_nasabah','data_progresses.nama_progress','hasil_progress.nama_progress as hasil_progress')
                            ->where('followups.karyawan_id',$karyawan_id)
                            ->leftjoin('data_progresses','followups.progresses_id','=','data_progresses.id')
                            ->leftjoin('data_progresses as hasil_progress','followups.hasil_progress_id','=','hasil_progress.id')
                            ->leftjoin('data_nasabahs','data_nasabahs.id','=','followups.nasabah_id')
                            ->where(function($query) use ($keyword) {
                                $query->where('followups.tanggal_progress','like',$keyword)
                                      ->orWhere('followups.keterangan','like',$keyword)
                                      ->orWhere('data_progresses.nama_progress','like',$keyword)
                                      ->orWhere('hasil_progress.nama_progress','like',$keyword)
                                      ->orWhere('data_nasabahs.nama_nasabah','like',$keyword);
                              });
              $item=$item->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              $item=$where_status->select('followups.*','data_nasabahs.nama_nasabah','data_progresses.nama_progress','hasil_progress.nama_progress as hasil_progress')
                            ->where('followups.karyawan_id',$karyawan_id)
                            ->leftjoin('data_progresses','followups.progress_id','=','data_progresses.id')
                            ->leftjoin('data_progresses as hasil_progress','followups.hasil_progress_id','=','hasil_progress.id')
                            ->leftjoin('data_nasabahs','data_nasabahs.id','=','followups.nasabah_id')
                            ->where(function($query) use ($keyword) {
                                $query->where('followups.tanggal_progress','like',$keyword)
                                      ->orWhere('followups.keterangan','like',$keyword)
                                      ->orWhere('data_progresses.nama_progress','like',$keyword)
                                      ->orWhere('hasil_progress.nama_progress','like',$keyword)
                                      ->orWhere('data_nasabahs.nama_nasabah','like',$keyword);
                              });
              $item=$item->skip($awal)->take($banyak)->orderBy('id','desc')->get();
              $total=$where_status2->select('followups.*','data_nasabahs.nama_nasabah','data_progresses.nama_progress','hasil_progress.nama_progress as hasil_progress')
                            ->where('followups.karyawan_id',$karyawan_id)
                            ->leftjoin('data_progresses','followups.progress_id','=','data_progresses.id')
                            ->leftjoin('data_progresses as hasil_progress','followups.hasil_progress_id','=','hasil_progress.id')
                            ->leftjoin('data_nasabahs','data_nasabahs.id','=','followups.nasabah_id')
                            ->where(function($query) use ($keyword) {
                                $query->where('followups.tanggal_progress','like',$keyword)
                                      ->orWhere('followups.keterangan','like',$keyword)
                                      ->orWhere('data_progresses.nama_progress','like',$keyword)
                                      ->orWhere('hasil_progress.nama_progress','like',$keyword)
                                      ->orWhere('data_nasabahs.nama_nasabah','like',$keyword);
                              });
              $total=$total->get();
              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);

          } else if($banyak<0) {
              $item=$where_status->select('followups.*','data_nasabahs.nama_nasabah','data_progresses.nama_progress','hasil_progress.nama_progress as hasil_progress')
                            ->where('followups.karyawan_id',$karyawan_id)
                            ->leftjoin('data_progresses','followups.progress_id','=','data_progresses.id')
                            ->leftjoin('data_progresses as hasil_progress','followups.hasil_progress_id','=','hasil_progress.id')
                            ->leftjoin('data_nasabahs','data_nasabahs.id','=','followups.nasabah_id')
                            ->orderBy('id','desc');
              $item=$item->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
              $item=$where_status->select('followups.*','data_nasabahs.nama_nasabah','data_progresses.nama_progress','hasil_progress.nama_progress as hasil_progress')
                            ->where('followups.karyawan_id',$karyawan_id)
                            ->leftjoin('data_progresses','followups.progress_id','=','data_progresses.id')
                            ->leftjoin('data_progresses as hasil_progress','followups.hasil_progress_id','=','hasil_progress.id')
                            ->leftjoin('data_nasabahs','data_nasabahs.id','=','followups.nasabah_id')
                            ->skip($awal)->take($banyak)->orderBy('id','desc');
              $item=$item->get();
              $total=$where_status2->select('followups.*','data_nasabahs.nama_nasabah','data_progresses.nama_progress','hasil_progress.nama_progress as hasil_progress')
                            ->where('followups.karyawan_id',$karyawan_id)
                            ->leftjoin('data_progresses','followups.progress_id','=','data_progresses.id')
                            ->leftjoin('data_progresses as hasil_progress','followups.hasil_progress_id','=','hasil_progress.id')
                            ->leftjoin('data_nasabahs','data_nasabahs.id','=','followups.nasabah_id');
              $total=$total->get();
              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x) {
              $item->nomer=$gh_x++;
              // $item->setAttribute('action','<a href="'.url("data-divisi").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
              //             <i class="fa fa-edit"></i>
              //         </a>
              //         <button onclick="hapus_input('."'".url('data-divisi')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
              //             <i class="fa fa-trash"></i>
              //         </button>');
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
