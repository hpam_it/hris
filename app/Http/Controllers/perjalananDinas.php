<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\karyawan;
use App\jabatan;
use App\akomodasi_dinas;
use App\aktivitas_dinas;
use App\perjalanan_dinas;
use App\transportasi_dinas;
use DB;
use Mail;
use Elibyy\TCPDF\Facades\TCPDF;
use App\struktur;
use App\elly\helper;

class perjalananDinas extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['halaman']='perjalanan_dinas';
      return view('perjalanan_dinas/index',$data);
    }

    public function approval_dinas()
    {
      $data['halaman']='approval_dinas';
      return view('perjalanan_dinas/approval',$data);
    }

    public function approvaldinas(Request $request){
      if($request->aksi=="sebelum"){
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              $item=perjalanan_dinas::where('tujuan_dinas','like',$keyword);
              $item=$item->where(function($query){
                    $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                          ->orWhere('before_atasan',2)
                          ->orWhere('before_direksi',2);
              })
              ->where(function($query){
                  $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                      ->where('before_atasan','!=',0)
                      ->where('before_direksi','!=',0)
                      ->where('after_hrd','!=',0)
                      ->where('after_atasan','!=',0)
                      ->where('after_direksi','!=',0);
              });

                $item=$item->orderBy('id','desc')
                ->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              $item=perjalanan_dinas::where('tujuan_dinas','like',$keyword);
              $item=$item->where(function($query){
                    $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                          ->orWhere('before_atasan',2)
                          ->orWhere('before_direksi',2);
              })
              ->where(function($query){
                  $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                      ->where('before_atasan','!=',0)
                      ->where('before_direksi','!=',0)
                      ->where('after_hrd','!=',0)
                      ->where('after_atasan','!=',0)
                      ->where('after_direksi','!=',0);
              });
              $item=$item->skip($awal)->take($banyak)->orderBy('id','desc')->get();
              $total=perjalanan_dinas::where('tujuan_dinas','like',$keyword);
              $total=$total->where(function($query){
                    $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                          ->orWhere('before_atasan',2)
                          ->orWhere('before_direksi',2);
              })
              ->where(function($query){
                  $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                      ->where('before_atasan','!=',0)
                      ->where('before_direksi','!=',0)
                      ->where('after_hrd','!=',0)
                      ->where('after_atasan','!=',0)
                      ->where('after_direksi','!=',0);
              });
              $total=$total->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;

          } else if($banyak<0) {
            $item=perjalanan_dinas::select('*');
            $item=$item->where(function($query){
                  $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                        ->orWhere('before_atasan',2)
                        ->orWhere('before_direksi',2);
            })
            ->where(function($query){
                $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                    ->where('before_atasan','!=',0)
                    ->where('before_direksi','!=',0)
                    ->where('after_hrd','!=',0)
                    ->where('after_atasan','!=',0)
                    ->where('after_direksi','!=',0);
            });
            $item=$item->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
            $item=perjalanan_dinas::select('*');
            $item=$item->where(function($query){
                  $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                        ->orWhere('before_atasan',2)
                        ->orWhere('before_direksi',2);
            })
            ->where(function($query){
                $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                    ->where('before_atasan','!=',0)
                    ->where('before_direksi','!=',0)
                    ->where('after_hrd','!=',0)
                    ->where('after_atasan','!=',0)
                    ->where('after_direksi','!=',0);
            });
            $item=$item->skip($awal)->take($banyak)->orderBy('id','desc')->get();
            $total=perjalanan_dinas::select('*');
            $total=$total->where(function($query){
                  $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                        ->orWhere('before_atasan',2)
                        ->orWhere('before_direksi',2);
            })
            ->where(function($query){
                $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                    ->where('before_atasan','!=',0)
                    ->where('before_direksi','!=',0)
                    ->where('after_hrd','!=',0)
                    ->where('after_atasan','!=',0)
                    ->where('after_direksi','!=',0);
            });
            $total=$total->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x) {
              $item->setAttribute('nomer',$gh_x++);
              $item->setAttribute('action','<a href="'.url("perjalanan-dinas/terima").'/'.$item->id.'" data-target="#full" data-toggle="modal" class="btn btn-md btn-icon-only green">
                          <i class="fa fa-check"></i>
                      </a>');
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }
    }

    public function print_pdf($id_dinas){
      $perjalanan_dinas=perjalanan_dinas::select('karyawan.nama_karyawan','perjalanan_dinas.*','atasan.nama_karyawan as nama_atasan','direksi.nama_karyawan as nama_direksi')
            ->leftjoin('karyawans as atasan','perjalanan_dinas.atasan','atasan.id')
            ->leftjoin('karyawans as direksi','perjalanan_dinas.direksi','direksi.id')
            ->leftjoin('karyawans as karyawan','perjalanan_dinas.karyawan_id','karyawan.id')
            ->where('perjalanan_dinas.id',$id_dinas)
            ->first();
      $aktifitas=aktivitas_dinas::select('aktivitas_dinas.*','karyawans.nama_karyawan')
                          ->where('perjalanan_dinas_id',$id_dinas)
                          ->leftjoin('karyawans','aktivitas_dinas.pic_dinas','karyawans.id')
                          ->orderBy('aktivitas_dinas.tanggal_aktifitas','ASC')
                          ->get();
      $penginapan=akomodasi_dinas::where('perjalanan_dinas_id',$id_dinas)->where('jenis_akomodasi','Penginapan')->get();
      $uangsaku=akomodasi_dinas::where('perjalanan_dinas_id',$id_dinas)->where('jenis_akomodasi','Uang Saku')->get();
      $transportasi=transportasi_dinas::where('perjalanan_dinas_id',$id_dinas)->get();

      $pdf = new TCPDF();

    $pdf::setHeaderCallback(function($pdf){
      $image_file = public_path().'/storage/images/logo_hpam_kecil.png';
      $pdf->Image($image_file, 10, 8, 40, '', 'png', '', 'T', false, 300, '', false, false, 0, false, false, false);
      $pdf->SetXY(70, 8);
      $pdf->SetFont('helvetica', 'B', 12);
      $pdf->Cell(0, 0, 'PT. HENAN PUTIHRAI ASSET MANAGEMENT', 0, 1, 'L', 0, '', 0);
      $pdf->SetX(70);
      $pdf->Cell(0, 0, 'FORM PERJALANAN DINAS', 0, 1, 'L', 0, '', 0);
      $pdf->SetFont('helvetica', '', 11);
      $pdf->SetLineStyle(array('width' => 0.3, 'cap' => 'butt', 'join' => 'round', 'dash' => 0, 'color' => array(0, 0, 0)));
      $pdf->SetX(10);
      $pdf->Cell(0, 1, '', 'B', 1, 'L', 0, '', 1);
    });

    $nama_nama='
    <table>
      <tr>
        <td width="150"><b>Nama Karyawan</b></td>
        <td width="25">:</td>
        <td width="300">'.$perjalanan_dinas->nama_karyawan.'</td>
      </tr>
      <tr>
        <td width="150"><b>Periode Dinas</b></td>
        <td width="25">:</td>
        <td width="300">'.Helper::balik_tgl($perjalanan_dinas->mulai_dinas,'/').' - '.Helper::balik_tgl($perjalanan_dinas->selesai_dinas,'/').'</td>
      </tr>
      <tr>
        <td width="150"><b>Destinasi Dinas</b></td>
        <td width="25">:</td>
        <td width="300">'.$perjalanan_dinas->tujuan_dinas.'</td>
      </tr>
    </table>';
    $estimasi_biaya='
    <table border="1" padding="3">
      <tr>
        <td width="538" colspan="6" style="text-align:center;background-color:#cccccc"><b>Estimasi Tunjangan / Biaya Perjalanan Dinas</b></td>
      </tr>
    </table>
    ';
    $estimasi_transportasi='
    <table padding="3" cellpadding="2" style="font-size:11px">
      <tr>
        <td width="430" colspan="5" style="text-align:left"><b>Transportasi</b></td>
        <td width="108" style="text-align:center;" border="1px"><b>Keterangan</b></td>
      </tr>';
      foreach ($transportasi as $key) {
        $estimasi_transportasi.='
          <tr>
            <td width="210" >'.$key->jenis_transportasi.'</td>
            <td width="20">:</td>
            <td width="80" style="text-align:right;">'.number_format($key->biaya,2,",",".").'</td>
            <td width="20">/</td>
            <td width="100">'.$key->maskapai.'</td>
            <td width="108" border="1px">'.$key->keterangan.'</td>
          </tr>';
      }
      $estimasi_transportasi.='
      <tr>
        <td width="538" colspan="6" style="text-align:left"></td>
      </tr>
      <tr>
        <td width="538" colspan="6" style="text-align:left"><b>Akomodasi dan Uang Saku</b></td>
      </tr>
      <tr>
      <td width="180" border="1px" style="text-align:center" ><b>Nama Tempat Penginapan</b></td>
      <td width="60" border="1px" colspan="2" style="text-align:center"><b>Total Hari</b></td>
      <td width="90" border="1px" style="text-align:center" ><b>Biaya/Hari</b></td>
      <td width="100" border="1px" style="text-align:center" ><b>Total Biaya</b></td>
      <td width="108" border="1px" style="text-align:center" ><b>Keterangan</b></td>
      </tr>';
      foreach ($penginapan as $key) {
        $estimasi_transportasi.='
        <tr>
        <td width="180" border="1px" style="text-align:center" >'.$key->nama_akomodasi.'</td>
        <td width="60" border="1px" colspan="2" style="text-align:center">'.$key->total_hari.'</td>
        <td width="90" border="1px" style="text-align:right" >'.number_format($key->biaya_perhari,2,",",".").'</td>
        <td width="100" border="1px" style="text-align:right" >'.number_format($key->total_biaya,2,",",".").'</td>
        <td width="108" border="1px" style="text-align:center" >'.$key->keterangan.'</td>
        </tr>
        ';
      }
      $estimasi_transportasi.='
      <tr>
        <td width="538" colspan="6" style="text-align:left"></td>
      </tr>
      <tr>
      <td width="180" border="0px" style="text-align:center" ></td>
      <td width="60" border="1px" colspan="2" style="text-align:center"><b>Total Hari</b></td>
      <td width="120" border="1px" style="text-align:center" ><b>Tunjangan/Hari</b></td>
      <td width="130" border="1px" style="text-align:center" ><b>Total Tunjangan</b></td>
      <td width="48" border="0px" style="text-align:center" ></td>
      </tr>
      ';
      foreach ($uangsaku as $key) {
        $estimasi_transportasi.='
        <tr>
        <td width="180" border="1px" style="text-align:center" ><b>'.$key->jenis_akomodasi.'</b></td>
        <td width="60" border="1px" colspan="2" style="text-align:center">'.$key->total_hari.'</td>
        <td width="120" border="1px" style="text-align:right" >'.number_format($key->biaya_perhari,2,",",".").'</td>
        <td width="130" border="1px" style="text-align:right" >'.number_format($key->total_biaya,2,",",".").'</td>
        <td width="48" border="0px" style="text-align:center" ></td>
        </tr>
        ';
      }
    $estimasi_transportasi.='
    </table>
    ';

    $aktifitas_perjalanan='
    <table padding="3" style="font-size:11px">
      <tr>
        <td colspan="4" width="538" border="0" style="text-align:center;" ></td>
      </tr>
      <tr>
        <td colspan="4" width="538" border="1" style="text-align:center;background-color:#cccccc;font-size:12px" ><b>Aktivitas Perjalanan Dinas</b></td>
      </tr>
      <tr>
        <td width="190" style="text-align:center;">Tanggal dan Waktu Kedatangan :</td>
        <td width="130"  style="text-align:center;border-bottom:1px solid #000000;">'.Helper::balik_tgl_jam($perjalanan_dinas->kedatangan,'/').'</td>
        <td width="88"  style="text-align:center;">Kepulangan:</td>
        <td width="130"  style="text-align:center;border-bottom:1px solid #000000;">'.Helper::balik_tgl_jam($perjalanan_dinas->kepulangan,'/').'</td>
      </tr>
      <tr>
        <td width="180" ></td>
        <td width="140"  style="text-align:center;">DD/MM/YY HH:MM</td>
        <td width="78"  ></td>
        <td width="140"  style="text-align:center;">DD/MM/YY HH:MM</td>
      </tr>
      <tr>
        <td colspan="4" width="538" border="0" style="text-align:center;" ></td>
      </tr>
      <tr>
        <td width="100" border="1" style="text-align:center;" ><b>Tanggal</b></td>
        <td width="278" border="1" style="text-align:center;" ><b>Aktivitas</b></td>
        <td width="80" border="1" style="text-align:center;" ><b>Paraf PIC</b></td>
        <td width="80" border="1" style="text-align:center;" ><b>Paraf Atasan</b></td>
      </tr>';
      foreach ($aktifitas as $key) {
        $aktifitas_perjalanan.='
        <tr>
          <td width="100" border="1" style="text-align:center;" >'.Helper::tgl_bulan($key->tanggal_aktifitas,'-').'</td>
          <td width="278" border="1" style="text-align:left;" > '.$key->aktivitas.'</td>
          <td width="80" border="1" style="text-align:center;" > '.$key->status_pic_dinas.'</td>
          <td width="80" border="1" style="text-align:center;" > '.$key->status_atasan.'</td>
        </tr>
        ';
      }
      $aktifitas_perjalanan.='
    </table>
    ';
    $persetujuan1='
    <table style="border:1px solid #000000;font-size:11px" cellpadding="2" >
      <tr>
        <td colspan="7" width="538"><b>Ditandatangani <u>Sebelum</u> Pelaksanaan</b></td>
      </tr>
      <tr>
        <td width="120" style="text-align:center;">Dibuat Oleh,</td>
        <td width="14.5"></td>
        <td width="120" style="text-align:center;">Diperiksa Oleh,</td>
        <td width="14.5"></td>
        <td width="120" style="text-align:center;">Disetujui Oleh,</td>
        <td width="14.5"></td>
        <td width="120" style="text-align:center;"></td>
      </tr>
      <tr>
        <td width="130" height="40"></td>
        <td width="14.5"></td>
        <td width="90" height="40"></td>
        <td width="14.5"></td>
        <td width="130" height="40"></td>
        <td width="14.5"></td>
        <td width="130" height="40"></td>
      </tr>
      <tr>
        <td width="130" style="text-align:center;border-bottom:1px solid #000000;"><b>'.$perjalanan_dinas->nama_karyawan.'</b></td>
        <td width="14.5"></td>
        <td width="90" style="text-align:center;border-bottom:1px solid #000000;"></td>
        <td width="14.5"></td>
        <td width="130" style="text-align:center;border-bottom:1px solid #000000;"><b>'.$perjalanan_dinas->nama_atasan.'</b></td>
        <td width="14.5"></td>
        <td width="130" style="text-align:center;border-bottom:1px solid #000000;"><b>'.$perjalanan_dinas->nama_direksi.'</b></td>
      </tr>
      <tr>
        <td width="130" style="text-align:center">Pemohon</td>
        <td width="14.5"></td>
        <td width="90" style="text-align:center">Staff GA/TAD</td>
        <td width="14.5"></td>
        <td width="130" style="text-align:center">Atasan</td>
        <td width="14.5"></td>
        <td width="130" style="text-align:center">Direktur</td>
      </tr>
    </table>';
    $persetujuan2='
    <table style="border:1px solid #000000;font-size:11px;" cellpadding="2" >
      <tr>
        <td colspan="7" width="538"><b>Ditandatangani <u>Setelah</u> Pelaksanaan</b></td>
      </tr>
      <tr>
        <td width="120" style="text-align:center;">Dibuat Oleh,</td>
        <td width="14.5"></td>
        <td width="120" style="text-align:center;">Diperiksa Oleh,</td>
        <td width="14.5"></td>
        <td width="120" style="text-align:center;">Disetujui Oleh,</td>
        <td width="14.5"></td>
        <td width="120" style="text-align:center;"></td>
      </tr>
      <tr>
        <td width="130" height="40"></td>
        <td width="14.5"></td>
        <td width="90" height="40"></td>
        <td width="14.5"></td>
        <td width="130" height="40"></td>
        <td width="14.5"></td>
        <td width="130" height="40"></td>
      </tr>
      <tr>
        <td width="130" style="text-align:center;border-bottom:1px solid #000000;"><b>'.$perjalanan_dinas->nama_karyawan.'</b></td>
        <td width="14.5"></td>
        <td width="90" style="text-align:center;border-bottom:1px solid #000000;"></td>
        <td width="14.5"></td>
        <td width="130" style="text-align:center;border-bottom:1px solid #000000;"><b>'.$perjalanan_dinas->nama_atasan.'</b></td>
        <td width="14.5"></td>
        <td width="130" style="text-align:center;border-bottom:1px solid #000000;"><b>'.$perjalanan_dinas->nama_direksi.'</b></td>
      </tr>
      <tr>
        <td width="130" style="text-align:center">Pemohon</td>
        <td width="14.5"></td>
        <td width="90" style="text-align:center">Staff GA/TAD</td>
        <td width="14.5"></td>
        <td width="130" style="text-align:center">Atasan</td>
        <td width="14.5"></td>
        <td width="130" style="text-align:center">Direktur</td>
      </tr>
    </table>
    ';
      $pdf::SetTitle('Form Perjalanan Dinas '.$perjalanan_dinas->nama_karyawan);
      $pdf::AddPage();
      $pdf::SetMargins(10, 8, 10);
      $pdf::SetAutoPageBreak(TRUE, 5);
      $pdf::setXY(10,29);
      $pdf::writeHtml($nama_nama);
      $pdf::ln(5);
      $pdf::writeHtml($estimasi_biaya.$estimasi_transportasi.$aktifitas_perjalanan);
      $pdf::setXY(10,-100);
      $pdf::writeHtml($persetujuan1);
      $pdf::ln(5);
      $pdf::writeHtml($persetujuan2);
      $pdf::Output('form-perjalanan-dinas'.$perjalanan_dinas->nama_karyawan.'.pdf');

    }

    public function akomodasi(){
      $data['halaman']='perjalanan_dinas_akomodasi';
      $data['dinas']=perjalanan_dinas::select('perjalanan_dinas.*','karyawans.nama_karyawan')
                ->leftjoin('karyawans','perjalanan_dinas.karyawan_id','karyawans.id')
                ->where('before_atasan',1)
                ->where(function($query){
                  $query->where('before_hrd',2)
                  ->orWhere('before_direksi',2);
                })
                ->where(function($query){
                  $query->where('before_hrd','!=',0)
                  ->where('before_atasan','!=',0)
                  ->where('before_direksi','!=',0);
                })
                ->whereNotExists(function ($query) {
               $query->select(DB::raw(1))
                     ->from('akomodasi_dinas')
                     ->whereRaw('akomodasi_dinas.perjalanan_dinas_id = perjalanan_dinas.id');
                })
                ->get();
      return view('perjalanan_dinas/akomodasi',$data);
    }

    public function pilih_dinas(Request $request){
      $id=$request->id;
      $data['item']=perjalanan_dinas::select('perjalanan_dinas.*','atasan.nama_karyawan as nama_atasan','direksi.nama_karyawan as nama_direksi')
            ->leftjoin('karyawans as atasan','perjalanan_dinas.atasan','atasan.id')
            ->leftjoin('karyawans as direksi','perjalanan_dinas.direksi','direksi.id')
            ->where('perjalanan_dinas.id',$id)
            ->first();
      $data['aktifitas']=aktivitas_dinas::select('aktivitas_dinas.*','karyawans.nama_karyawan')
                          ->where('perjalanan_dinas_id',$id)
                          ->leftjoin('karyawans','aktivitas_dinas.pic_dinas','karyawans.id')
                          ->orderBy('aktivitas_dinas.tanggal_aktifitas','ASC')
                          ->get();
      $data['akomodasi']=akomodasi_dinas::where('perjalanan_dinas_id',$id)->get();
      $data['transportasi']=transportasi_dinas::where('perjalanan_dinas_id',$id)->get();
      return view('perjalanan_dinas/detail_dinas',$data);
    }

    public function simpan_akomodasi(Request $request){
      DB::beginTransaction();
       try {
          $id_dinas = $request->perjalanan_dinas_id;
          //update dinas untuk kedatangan dan kepulangan
          $kedatangan = $request->tgl_kedatangan.' '.$request->jam_kedatangan;
          $kepulangan = $request->tgl_kepulangan.' '.$request->jam_kepulangan;
          DB::table('perjalanan_dinas')
              ->where('id',$id_dinas)
              ->update([
                'kedatangan'=>$kedatangan,
                'kepulangan'=>$kepulangan
              ]);
          //insert aktifitas
          $akomodasi=$request->list_akomodasi;
          foreach($akomodasi as $key) {
            //insert ke table aktivitas
            DB::table('akomodasi_dinas')->insert(
              [
                'perjalanan_dinas_id'=>$id_dinas,
                'jenis_akomodasi'=>$key['jenis_akomodasi'],
                'nama_akomodasi'=>$key['nama_akomodasi'],
                'total_hari'=>$key['total_hari'],
                'biaya_perhari'=>$key['biaya_perhari'],
                'total_biaya'=>$key['total_biaya'],
                'keterangan'=>$key['keterangan']
              ]
            );
          }

          //insert transportasi
          $transportasi=$request->list_transportasi;
          foreach ($transportasi as $key) {
            //insert ke table transportasi_dinas
            DB::table('transportasi_dinas')->insert(
              [
                'perjalanan_dinas_id'=>$id_dinas,
                'jenis_transportasi'=>$key['jenis_transportasi'],
                'biaya'=>$key['biaya'],
                'maskapai'=>$key['maskapai'],
                'keterangan'=>$key['keterangan']
              ]
            );
          }

            DB::commit();
            return 1;
        } catch (\Exception $e) {
            DB::rollback();
            return 0;
        }
    }

    public function list_akomodasi(){

    }

    public function get_dinas($id){
      $data=perjalanan_dinas::select('perjalanan_dinas.*','atasan.nama_karyawan as nama_atasan','direksi.nama_karyawan as nama_direksi')
                    ->where('perjalanan_dinas.id',$id)
                    ->first();
      return $data;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->aksi=='simpan_item'){
          $id = Auth::id();
          $atasan = Helper::cariatasan($id);
          $direksi = Helper::caridireksi($id);
          if($atasan=='struktur kosong' || $atasan=='atasan kosong'){
            //kalo ada masalah dengan struktur balikin dulu
            return $atasan;
          }else{
            //simpan2
            DB::beginTransaction();
             try {
                $id_perjalanan = DB::table('perjalanan_dinas')->insertGetId(
                          [
                            'karyawan_id' => $id,
                            'mulai_dinas' => $request->mulai_dinas,
                            'selesai_dinas' => $request->selesai_dinas,
                            'tujuan_dinas' => $request->tujuan_dinas,
                            'atasan' => $atasan,
                            'direksi' => $direksi,
                            'keterangan'=>$request->keterangan
                          ]
                      );
                //insert aktifitas
                $aktifitas=$request->list_aktivitas;
                $xx=0;
                foreach($aktifitas as $key) {
                  //insert ke table aktivitas
                  DB::table('aktivitas_dinas')->insert(
                    [
                      'perjalanan_dinas_id'=>$id_perjalanan,
                      'tanggal_aktifitas'=>$key['tanggal_aktifitas'],
                      'aktivitas'=>$key['aktivitas']
                    ]
                  );
                }
                  DB::commit();
                  return 1;
              } catch (\Exception $e) {
                  DB::rollback();
                  return 0;
              }
          }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=='detail'){
        $data['item']=perjalanan_dinas::select('perjalanan_dinas.*','atasan.nama_karyawan as nama_atasan','direksi.nama_karyawan as nama_direksi')
              ->leftjoin('karyawans as atasan','perjalanan_dinas.atasan','atasan.id')
              ->leftjoin('karyawans as direksi','perjalanan_dinas.direksi','direksi.id')
              ->where('perjalanan_dinas.id',$id)
              ->first();
        $data['aktifitas']=aktivitas_dinas::select('aktivitas_dinas.*','karyawans.nama_karyawan')
                            ->where('perjalanan_dinas_id',$id)
                            ->leftjoin('karyawans','aktivitas_dinas.pic_dinas','karyawans.id')
                            ->orderBy('aktivitas_dinas.tanggal_aktifitas','ASC')
                            ->get();
        $data['akomodasi']=akomodasi_dinas::where('perjalanan_dinas_id',$id)->get();
        $data['transportasi']=transportasi_dinas::where('perjalanan_dinas_id',$id)->get();
        return view('perjalanan_dinas/detail',$data);
      }

      if($request->aksi=="list_all"){
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              $item=perjalanan_dinas::where('tujuan_dinas','like',$keyword);
              if($request->status=='selesai'){
                $item=$item->where('before_hrd',1)//2 itu pending,0 itu reject,1 itu diterima
                          ->where('before_atasan',1)
                          ->where('before_direksi',1)
                          ->where('after_hrd',1)
                          ->where('after_atasan',1)
                          ->where('after_direksi',1);
              }else if($request->status=='proggress'){
                $item=$item->where(function($query){
                      $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                            ->orWhere('before_atasan',2)
                            ->orWhere('before_direksi',2)
                            ->orWhere('after_hrd',2)
                            ->orWhere('after_atasan',2)
                            ->orWhere('after_direksi',2);
                })
                ->where(function($query){
                  $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                        ->where('before_atasan','!=',0)
                        ->where('before_direksi','!=',0)
                        ->where('after_hrd','!=',0)
                        ->where('after_atasan','!=',0)
                        ->where('after_direksi','!=',0);
                });
              }else if($request->status=='akomodasi_selesai'){
                $item=$item->where('before_hrd',1)//2 itu pending,0 itu reject,1 itu diterima
                          ->where('before_atasan',1)
                          ->where('before_direksi',1)
                          ->where('after_hrd',1)
                          ->where('after_atasan',1)
                          ->where('after_direksi',1)
                          ->whereExists(function ($query) {
                              $query->select(DB::raw(1))
                                    ->from('akomodasi_dinas')
                                    ->whereRaw('akomodasi_dinas.perjalanan_dinas_id = perjalanan_dinas.id');
                          });
              }else if($request->status=='akomodasi_progress'){
                $item=$item->where(function($query){
                      $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                            ->orWhere('before_atasan',2)
                            ->orWhere('before_direksi',2)
                            ->orWhere('after_hrd',2)
                            ->orWhere('after_atasan',2)
                            ->orWhere('after_direksi',2);
                })
                ->where(function($query){
                    $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                        ->where('before_atasan','!=',0)
                        ->where('before_direksi','!=',0)
                        ->where('after_hrd','!=',0)
                        ->where('after_atasan','!=',0)
                        ->where('after_direksi','!=',0);
                })
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                          ->from('akomodasi_dinas')
                          ->whereRaw('akomodasi_dinas.perjalanan_dinas_id = perjalanan_dinas.id');
                });
              }

                $item=$item->orderBy('id','desc')
                ->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              $item=perjalanan_dinas::where('tujuan_dinas','like',$keyword);
              if($request->status=='selesai'){
                $item=$item->where('before_hrd',1)//2 itu pending,0 itu reject,1 itu diterima
                          ->where('before_atasan',1)
                          ->where('before_direksi',1)
                          ->where('after_hrd',1)
                          ->where('after_atasan',1)
                          ->where('after_direksi',1);
              }else if($request->status=='proggress'){
                $item=$item->where(function($query){
                      $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                            ->orWhere('before_atasan',2)
                            ->orWhere('before_direksi',2)
                            ->orWhere('after_hrd',2)
                            ->orWhere('after_atasan',2)
                            ->orWhere('after_direksi',2);
                })
                ->where(function($query){
                  $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                        ->where('before_atasan','!=',0)
                        ->where('before_direksi','!=',0)
                        ->where('after_hrd','!=',0)
                        ->where('after_atasan','!=',0)
                        ->where('after_direksi','!=',0);
                });
              }else if($request->status=='akomodasi_selesai'){
                $item=$item->where('before_hrd',1)//2 itu pending,0 itu reject,1 itu diterima
                          ->where('before_atasan',1)
                          ->where('before_direksi',1)
                          ->where('after_hrd',1)
                          ->where('after_atasan',1)
                          ->where('after_direksi',1)
                          ->whereExists(function ($query) {
                              $query->select(DB::raw(1))
                                    ->from('akomodasi_dinas')
                                    ->whereRaw('akomodasi_dinas.perjalanan_dinas_id = perjalanan_dinas.id');
                          });
              }else if($request->status=='akomodasi_progress'){
                $item=$item->where(function($query){
                      $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                            ->orWhere('before_atasan',2)
                            ->orWhere('before_direksi',2)
                            ->orWhere('after_hrd',2)
                            ->orWhere('after_atasan',2)
                            ->orWhere('after_direksi',2);
                })
                ->where(function($query){
                    $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                        ->where('before_atasan','!=',0)
                        ->where('before_direksi','!=',0)
                        ->where('after_hrd','!=',0)
                        ->where('after_atasan','!=',0)
                        ->where('after_direksi','!=',0);
                })
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                          ->from('akomodasi_dinas')
                          ->whereRaw('akomodasi_dinas.perjalanan_dinas_id = perjalanan_dinas.id');
                });
              }
              $item=$item->skip($awal)->take($banyak)->orderBy('id','desc')->get();
              $total=perjalanan_dinas::where('tujuan_dinas','like',$keyword);
              if($request->status=='selesai'){
                $total=$total->where('before_hrd',1)//2 itu pending,0 itu reject,1 itu diterima
                          ->where('before_atasan',1)
                          ->where('before_direksi',1)
                          ->where('after_hrd',1)
                          ->where('after_atasan',1)
                          ->where('after_direksi',1);
              }else if($request->status=='proggress'){
                $total=$total->where(function($query){
                      $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                            ->orWhere('before_atasan',2)
                            ->orWhere('before_direksi',2)
                            ->orWhere('after_hrd',2)
                            ->orWhere('after_atasan',2)
                            ->orWhere('after_direksi',2);
                })
                ->where(function($query){
                  $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                        ->where('before_atasan','!=',0)
                        ->where('before_direksi','!=',0)
                        ->where('after_hrd','!=',0)
                        ->where('after_atasan','!=',0)
                        ->where('after_direksi','!=',0);
                });
              }else if($request->status=='akomodasi_selesai'){
                $total=$total->where('before_hrd',1)//2 itu pending,0 itu reject,1 itu diterima
                          ->where('before_atasan',1)
                          ->where('before_direksi',1)
                          ->where('after_hrd',1)
                          ->where('after_atasan',1)
                          ->where('after_direksi',1)
                          ->whereExists(function ($query) {
                              $query->select(DB::raw(1))
                                    ->from('akomodasi_dinas')
                                    ->whereRaw('akomodasi_dinas.perjalanan_dinas_id = perjalanan_dinas.id');
                          });
              }else if($request->status=='akomodasi_progress'){
                $total=$total->where(function($query){
                      $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                            ->orWhere('before_atasan',2)
                            ->orWhere('before_direksi',2)
                            ->orWhere('after_hrd',2)
                            ->orWhere('after_atasan',2)
                            ->orWhere('after_direksi',2);
                })
                ->where(function($query){
                    $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                        ->where('before_atasan','!=',0)
                        ->where('before_direksi','!=',0)
                        ->where('after_hrd','!=',0)
                        ->where('after_atasan','!=',0)
                        ->where('after_direksi','!=',0);
                })
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                          ->from('akomodasi_dinas')
                          ->whereRaw('akomodasi_dinas.perjalanan_dinas_id = perjalanan_dinas.id');
                });
              }
              $total=$total->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;

          } else if($banyak<0) {
            $item=perjalanan_dinas::select('*');
            if($request->status=='selesai'){
              $item=$item->where('before_hrd',1)//2 itu pending,0 itu reject,1 itu diterima
                        ->where('before_atasan',1)
                        ->where('before_direksi',1)
                        ->where('after_hrd',1)
                        ->where('after_atasan',1)
                        ->where('after_direksi',1);
            }else if($request->status=='proggress'){
              $item=$item->where(function($query){
                    $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                          ->orWhere('before_atasan',2)
                          ->orWhere('before_direksi',2)
                          ->orWhere('after_hrd',2)
                          ->orWhere('after_atasan',2)
                          ->orWhere('after_direksi',2);
              })
              ->where(function($query){
                $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                      ->where('before_atasan','!=',0)
                      ->where('before_direksi','!=',0)
                      ->where('after_hrd','!=',0)
                      ->where('after_atasan','!=',0)
                      ->where('after_direksi','!=',0);
              });
            }else if($request->status=='akomodasi_selesai'){
              $item=$item->where('before_hrd',1)//2 itu pending,0 itu reject,1 itu diterima
                        ->where('before_atasan',1)
                        ->where('before_direksi',1)
                        ->where('after_hrd',1)
                        ->where('after_atasan',1)
                        ->where('after_direksi',1)
                        ->whereExists(function ($query) {
                            $query->select(DB::raw(1))
                                  ->from('akomodasi_dinas')
                                  ->whereRaw('akomodasi_dinas.perjalanan_dinas_id = perjalanan_dinas.id');
                        });
            }else if($request->status=='akomodasi_progress'){
              $item=$item->where(function($query){
                    $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                          ->orWhere('before_atasan',2)
                          ->orWhere('before_direksi',2)
                          ->orWhere('after_hrd',2)
                          ->orWhere('after_atasan',2)
                          ->orWhere('after_direksi',2);
              })
              ->where(function($query){
                  $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                      ->where('before_atasan','!=',0)
                      ->where('before_direksi','!=',0)
                      ->where('after_hrd','!=',0)
                      ->where('after_atasan','!=',0)
                      ->where('after_direksi','!=',0);
              })
              ->whereExists(function ($query) {
                  $query->select(DB::raw(1))
                        ->from('akomodasi_dinas')
                        ->whereRaw('akomodasi_dinas.perjalanan_dinas_id = perjalanan_dinas.id');
              });
            }
            $item=$item->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
            $item=perjalanan_dinas::select('*');
            if($request->status=='selesai'){
              $item=$item->where('before_hrd',1)//2 itu pending,0 itu reject,1 itu diterima
                        ->where('before_atasan',1)
                        ->where('before_direksi',1)
                        ->where('after_hrd',1)
                        ->where('after_atasan',1)
                        ->where('after_direksi',1);
            }else if($request->status=='proggress'){
              $item=$item->where(function($query){
                    $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                          ->orWhere('before_atasan',2)
                          ->orWhere('before_direksi',2)
                          ->orWhere('after_hrd',2)
                          ->orWhere('after_atasan',2)
                          ->orWhere('after_direksi',2);
              })
              ->where(function($query){
                $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                      ->where('before_atasan','!=',0)
                      ->where('before_direksi','!=',0)
                      ->where('after_hrd','!=',0)
                      ->where('after_atasan','!=',0)
                      ->where('after_direksi','!=',0);
              });
            }else if($request->status=='akomodasi_selesai'){
              $item=$item->where('before_hrd',1)//2 itu pending,0 itu reject,1 itu diterima
                        ->where('before_atasan',1)
                        ->where('before_direksi',1)
                        ->where('after_hrd',1)
                        ->where('after_atasan',1)
                        ->where('after_direksi',1)
                        ->whereExists(function ($query) {
                            $query->select(DB::raw(1))
                                  ->from('akomodasi_dinas')
                                  ->whereRaw('akomodasi_dinas.perjalanan_dinas_id = perjalanan_dinas.id');
                        });
            }else if($request->status=='akomodasi_progress'){
              $item=$item->where(function($query){
                    $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                          ->orWhere('before_atasan',2)
                          ->orWhere('before_direksi',2)
                          ->orWhere('after_hrd',2)
                          ->orWhere('after_atasan',2)
                          ->orWhere('after_direksi',2);
              })
              ->where(function($query){
                  $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                      ->where('before_atasan','!=',0)
                      ->where('before_direksi','!=',0)
                      ->where('after_hrd','!=',0)
                      ->where('after_atasan','!=',0)
                      ->where('after_direksi','!=',0);
              })
              ->whereExists(function ($query) {
                  $query->select(DB::raw(1))
                        ->from('akomodasi_dinas')
                        ->whereRaw('akomodasi_dinas.perjalanan_dinas_id = perjalanan_dinas.id');
              });
            }
            $item=$item->skip($awal)->take($banyak)->orderBy('id','desc')->get();
            $total=perjalanan_dinas::select('*');
            if($request->status=='selesai'){
              $total=$total->where('before_hrd',1)//2 itu pending,0 itu reject,1 itu diterima
                        ->where('before_atasan',1)
                        ->where('before_direksi',1)
                        ->where('after_hrd',1)
                        ->where('after_atasan',1)
                        ->where('after_direksi',1);
            }else if($request->status=='proggress'){
              $total=$total->where(function($query){
                    $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                          ->orWhere('before_atasan',2)
                          ->orWhere('before_direksi',2)
                          ->orWhere('after_hrd',2)
                          ->orWhere('after_atasan',2)
                          ->orWhere('after_direksi',2);
              })
              ->where(function($query){
                $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                      ->where('before_atasan','!=',0)
                      ->where('before_direksi','!=',0)
                      ->where('after_hrd','!=',0)
                      ->where('after_atasan','!=',0)
                      ->where('after_direksi','!=',0);
              });
            }else if($request->status=='akomodasi_selesai'){
              $total=$total->where('before_hrd',1)//2 itu pending,0 itu reject,1 itu diterima
                        ->where('before_atasan',1)
                        ->where('before_direksi',1)
                        ->where('after_hrd',1)
                        ->where('after_atasan',1)
                        ->where('after_direksi',1)
                        ->whereExists(function ($query) {
                            $query->select(DB::raw(1))
                                  ->from('akomodasi_dinas')
                                  ->whereRaw('akomodasi_dinas.perjalanan_dinas_id = perjalanan_dinas.id');
                        });
            }else if($request->status=='akomodasi_progress'){
              $total=$total->where(function($query){
                    $query->where('before_hrd',2)//2 itu pending,0 itu reject,1 itu diterima
                          ->orWhere('before_atasan',2)
                          ->orWhere('before_direksi',2)
                          ->orWhere('after_hrd',2)
                          ->orWhere('after_atasan',2)
                          ->orWhere('after_direksi',2);
              })
              ->where(function($query){
                  $query->where('before_hrd','!=',0)//2 itu pending,0 itu reject,1 itu diterima
                      ->where('before_atasan','!=',0)
                      ->where('before_direksi','!=',0)
                      ->where('after_hrd','!=',0)
                      ->where('after_atasan','!=',0)
                      ->where('after_direksi','!=',0);
              })
              ->whereExists(function ($query) {
                  $query->select(DB::raw(1))
                        ->from('akomodasi_dinas')
                        ->whereRaw('akomodasi_dinas.perjalanan_dinas_id = perjalanan_dinas.id');
              });
            }
            $total=$total->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x) {
              $item->setAttribute('nomer',$gh_x++);
              $item->setAttribute('action','<a href="'.url("perjalanan-dinas").'/'.$item->id.'?aksi=detail" data-target="#full" data-toggle="modal" class="btn btn-md btn-icon-only green">
                          <i class="fa fa-eye"></i>
                      </a>&nbsp;<a href="'.url("perjalanan-dinas/print").'/'.$item->id.'" target="_blank" class="btn btn-md btn-icon-only green">
                                  <i class="fa fa-print"></i>
                              </a>');
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
