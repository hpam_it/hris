<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;


use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\karyawan;
use App\divisi;
use App\jabatan;
use App\pangkat;
use App\status_karyawan;
use App\kantor;
use App\kelengkapan_document;
use App\data_asuransi;
use App\data_keluarga;
use App\data_pajak;
use App\userinfo;
use App\jam_kerja;
use Hash;
use Excel;

class data_karyawan extends Controller
{
  use RegistersUsers;

  public function export_excel(){
      $karyawan=karyawan::select('karyawans.*','divisis.nama_divisi','divisis.kode_divisi','jabatans.nama_jabatan','jabatans.kode_jabatan','pangkats.kode_pangkat','pangkats.nama_pangkat','kantors.nama_kantor','kantors.kode_kantor')
                  ->leftjoin('divisis','karyawans.divisi_id','=','divisis.id')
                  ->leftjoin('jabatans','karyawans.jabatan_id','=','jabatans.id')
                  ->leftjoin('pangkats','karyawans.pangkat_id','=','pangkats.id')
                  ->leftjoin('kantors','karyawans.kantor_id','=','kantors.id')
                  ->get()
                  ->toArray();
    $data['jabatan']=jabatan::all()->toArray();
    $data['pangkat']=pangkat::all()->toArray();
    $data['divisi']=divisi::all()->toArray();
    $data['kantor']=kantor::all()->toArray();
    $data['pajak']=data_pajak::all()->toArray();
    $data['status']=status_karyawan::all()->toArray();

      Excel::create('data_karyawan', function($excel) use($karyawan,$data) {
              //mulai insert row
              $excel->sheet('karyawan', function($sheet) use($karyawan) {
                $sheet->fromArray($karyawan);

              });
              $excel->sheet('jabatan', function($sheet) use($data) {
                $sheet->fromArray($data['jabatan']);
              });
              $excel->sheet('pangkat', function($sheet) use($data) {
                $sheet->fromArray($data['pangkat']);
              });
              $excel->sheet('divisi', function($sheet) use($data) {
                $sheet->fromArray($data['divisi']);
              });
              $excel->sheet('kantor', function($sheet) use($data) {
                $sheet->fromArray($data['kantor']);
              });
              $excel->sheet('pajak', function($sheet) use($data) {
                $sheet->fromArray($data['pajak']);
              });
              $excel->sheet('status', function($sheet) use($data) {
                $sheet->fromArray($data['status']);
              });


      })->export('xls');
  }

  public function index()
  {
    $data['halaman']='data-karyawan';
    $data['jabatan']=jabatan::all();
    $data['pangkat']=pangkat::all();
    $data['divisi']=divisi::all();
    $data['kantor']=kantor::all();
    $data['pajak']=data_pajak::all();
    $data['status']=status_karyawan::all();
    $data['userinfo']=userinfo::all();
    return view('data/karyawan/index',$data);
  }

  public function store(Request $request)
  {
      if($request->aksi=='simpan_item'){
        //cek tanggal gabung kapan ?
        $sekarang = date('Y-m-d');
        if($request->tgl_gabung > $sekarang){
          if($request->status_kerja == 'Aktif'){
            return 'gabung';
          }
        }
        //cek nik dan npp
        $nik=karyawan::where('nik',$request->nik)->count();
        $npp=karyawan::where('npp',$request->npp)->count();
        $password=bcrypt($request->passwordnya);
        if($nik>0){
          return 'nik';
        }else if($npp>0){
          return 'npp';
        }else{
          $request->merge(array('password' => bcrypt($request->passwordnya) ));
          $simpan=karyawan::create($request->all());

          if($simpan){
            return 'ok';
            //update passwordnya
            // $update=karyawan::where('nik', $request->nik)
            //                 ->where('npp', $request->npp)
            //                 ->update(['password' => $password]);
            // if($update){
            //   return 'ok';
            // }else{
            //   return 'gagal';
            // }
          }else{
            return 'gagal';
          }
        }

      }

      //update karyawan
      if($request->aksi=='update_item'){
        //cek nik dan npp
        $nik=karyawan::where('nik',$request->nik)->count();
        $npp=karyawan::where('npp',$request->npp)->count();
        $password=bcrypt($request->passwordnya);
        $jam_aktif=implode(",",$request->jamkerja);
        $request->merge(array('jamkerjaaktif' => $jam_aktif ));
        if($nik>1){
          return 'nik';
        }else if($npp>1){
          return 'npp';
        }else{
          if($request->passwordnya != null){
            $request->merge(array('password' => bcrypt($request->passwordnya) ));
          }
          $id_karyawan=$request->id_karyawan;
          $kyw=karyawan::find($id_karyawan);
          $simpan=$kyw->update($request->all());

          if($simpan){
            return 'ok';
            //update passwordnya
            // $update=karyawan::where('nik', $request->nik)
            //                 ->where('npp', $request->npp)
            //                 ->update(['password' => $password]);
            // if($update){
            //   return 'ok';
            // }else{
            //   return 'gagal';
            // }
          }else{
            return 'gagal';
          }
        }

      }

      if($request->aksi=='simpan_keluarga'){
        //cek apakah nik sudah ada di keluarga itu
        $nik=$request->nik;
        $karyawan_id=$request->karyawan_id;
        $cek=data_keluarga::where('karyawan_id',$karyawan_id)->where('nik',$nik)->count();
        if($cek>0){
          return 'ada';
        }else{
          $insert=data_keluarga::create($request->all());
          if($insert){
            return 'ok';
          }else{
            return 'gagal';
          }
        }
      }

      if($request->aksi=='simpan_asuransi'){
        $nomor=$request->nomor_asuransi;
        $karyawan_id=$request->karyawan_id;
        $cek=data_asuransi::where('karyawan_id',$karyawan_id)->where('nomor_asuransi',$nomor)->count();
        if($cek>0){
          return 'ada';
        }else{
          $nama_asuransi = $request->nama_asuransi;
          $path = $request->scan_kartu->store('public/asuransi');
            if ($request->file('scan_kartu')->isValid()) {
              $dokumen = new data_asuransi;
              $dokumen -> karyawan_id = $karyawan_id;
              $dokumen -> nama_asuransi = $nama_asuransi;
              $dokumen -> nomor_asuransi = $nomor;
              $dokumen -> scan_kartu = $path;
              $simpan = $dokumen->save();
              if($simpan){
                $data['hasil']='ok';
                return json_encode($data);
              }else{
                $data['hasil']='gagal';
                return json_encode($data);
              }
            }else{
              $data['hasil']='gagal';
              return json_encode($data);
            }
        }
      }

      if($request->aksi=='simpan_dokumen'){
              $karyawan_id = $request->karyawan_id;
              $nama_document = $request->nama_dokumen;
              $path = $request->scan_dokumen->store('public/dokumen');
                if ($request->file('scan_dokumen')->isValid()) {
                  $dokumen = new kelengkapan_document;
                  $dokumen -> karyawan_id = $karyawan_id;
                  $dokumen -> nama_dokumen = $nama_document;
                  $dokumen -> nomor_dokumen = $request->nomor_dokumen;
                  $dokumen -> scan_dokumen = $path;
                  $dokumen -> tanggal_berakhir = $request->tanggal_berakhir;
                  $simpan = $dokumen->save();
                  if($simpan){
                    $data['hasil']='ok';
                    return json_encode($data);
                  }else{
                    $data['hasil']='gagal';
                    return json_encode($data);
                  }
                }else{
                  $data['hasil']='gagal';
                  return json_encode($data);
                }

      }
  }

  public function show(Request $request,$id)
  {
    if($request->aksi=='edit'){

        $data['karyawan']=karyawan::find($id);
        $data['jabatan']=jabatan::all();
        $data['pangkat']=pangkat::all();
        $data['divisi']=divisi::all();
        $data['kantor']=kantor::all();
        $data['pajak']=data_pajak::all();
        $data['status']=status_karyawan::all();
        $data['userinfo']=userinfo::all();
        $kar=karyawan::find($id);
        $jam_aktif=explode(",",$kar->jamkerjaaktif);
        $jamkerja=jam_kerja::all();
        $jamkerja->each(function($item) use (&$jam_aktif) {
          if(in_array($item->id,$jam_aktif) ){
            //pakai
            $item->selected='ya';
          }else{
            //tidak pakai
            $item->selected='tidak';
          }
        });
        $data['jamkerja']=$jamkerja;
        return view('data/karyawan/edit_list',$data);
    }

    if($request->aksi=='detail'){

        $data['karyawan']=karyawan::find($id);
        $data['jabatan']=jabatan::all();
        $data['pangkat']=pangkat::all();
        $data['divisi']=divisi::all();
        $data['kantor']=kantor::all();
        $data['pajak']=data_pajak::all();
        $data['jamkerja']=jam_kerja::all();
        $data['status']=status_karyawan::all();
        return view('data/karyawan/detail',$data);
    }

    if($request->aksi=="list-all"){
        $awal=$request->start;
        $banyak=$request->length;
        $banyak_colom=$request->iColumns;
        $kata_kunci_global=$request->sSearch;
        $echo=$request->draw;
        $kata_kunci=$request->search['value'];
        if( ($banyak<0) AND ($kata_kunci != "") ){
            $keyword='%'.$kata_kunci.'%';
            $item=karyawan::select('id','nama_karyawan','npp','nomor_ponsel')
                  ->where('nama_karyawan','like',$keyword)
                  ->orWhere('npp','like',$keyword)
                  ->orWhere('nomor_ponsel','like',$keyword)
                  ->orderBy('id','desc')->get();
            $data['recordsTotal']=count($item);
            $data['recordsFiltered']=count($item);
        }else if($kata_kunci != "" ) {
            $keyword='%'.$kata_kunci.'%';
            $item=karyawan::select('id','nama_karyawan','npp','nomor_ponsel')
                  ->where('nama_karyawan','like',$keyword)
                  ->orWhere('npp','like',$keyword)
                  ->orWhere('nomor_ponsel','like',$keyword)
                  ->take($banyak)->orderBy('id','desc')->get();
                  $item2=karyawan::select('id','nama_karyawan','npp','nomor_ponsel')
                        ->where('nama_karyawan','like',$keyword)
                        ->orWhere('npp','like',$keyword)
                        ->orWhere('nomor_ponsel','like',$keyword)
                  ->count();
            $data['recordsTotal']=$item2;
            $data['recordsFiltered']=$item2;

        } else if($banyak<0) {
            $item=karyawan::select('id','nama_karyawan','npp','nomor_ponsel')->orderBy('id','desc')->get();
            $data['recordsTotal']=count($item);
            $data['recordsFiltered']=count($item);
        } else {
            $item=karyawan::select('id','nama_karyawan','npp','nomor_ponsel')->skip($awal)->take($banyak)->orderBy('id','desc')->get();
            $total=karyawan::select('id','nama_karyawan','npp','nomor_ponsel')->count();
            $data['recordsTotal']=$total;
            $data['recordsFiltered']=$total;
        }

        $gh_x=$awal+1;
        $item->each(function($item) use (&$gh_x) {
            $item->setAttribute('nomer',$gh_x++);


            $item->setAttribute('action','
                    <a href="'.url("data-karyawan").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                                                <i class="fa fa-edit"></i>
                                            </a>
                    <button  data-title="Hapus Pesan ?" data-toggle="confirmation" data-placement="left" data-url="'.url("data-karyawan").'/'.$item->id.'?aksi=hapus_karyawan" class="konfirmasi hapus-input btn btn-md btn-icon-only red">
                        <i class="fa fa-trash"></i>
                    </button>');
              $item->setAttribute('tambahan','
                            <a href="'.url("data-karyawan").'/'.$item->id.'?aksi=keluarga" data-target="#ajax" data-toggle="modal" class="btn btn-md purple">
                              <i class="fa fa-users"></i>Keluarga
                            </a>
                            <a href="'.url("data-karyawan").'/'.$item->id.'?aksi=dokumen" data-target="#ajax" data-toggle="modal" class="btn btn-md blue">
                              <i class="fa fa-file-picture-o"></i>Doc
                            </a>
                            <a href="'.url("data-karyawan").'/'.$item->id.'?aksi=asuransi" data-target="#ajax" data-toggle="modal" class="btn btn-md  yellow">
                              <i class="fa fa-heart-o"></i>Insurance
                            </a>
                            <a href="'.url("data-karyawan").'/'.$item->id.'?aksi=detail" data-target="#ajax" data-toggle="modal" class="btn btn-md  yellow">
                               <i class="fa fa-server"></i>Detail
                            </a>
                          ');
        });

        $data['draw']=$echo;
        $data['data']=$item;
        return $data;
        /*return json_encode($data);*/
    }

    if($request->aksi=='hapus_input'){
        karyawan::find($id)->delete();
        return 1;
    }

    if($request->aksi=='hapus_keluarga'){
      data_keluarga::find($id)->delete();
      return 1;
    }

    if($request->aksi=='hapus_asuransi'){
      $data=data_asuransi::find($id);
      $doc=$data->scan_kartu;
      Storage::delete($doc);
      $data->delete();
      return 1;
    }

    if($request->aksi=='hapus_dokumen'){
      $data=kelengkapan_document::find($id);
      $doc=$data->dokumen;
      Storage::delete($doc);
      $data->delete();
      return 1;
    }

    if($request->aksi=='dokumen'){
      $id_karyawan=$id;
      $data['id_karyawan']=$id_karyawan;
      $data['dokument']=kelengkapan_document::where('karyawan_id',$id)->orderBy('id','desc')->get();
      return view('data/karyawan/document',$data);

    }

    if($request->aksi=='keluarga'){
      $id_karyawan=$id;
      $data['id_karyawan']=$id_karyawan;
      $data['keluarga']=data_keluarga::where('karyawan_id',$id)->orderBy('id','desc')->get();
      return view('data/karyawan/keluarga',$data);

    }

    if($request->aksi=='asuransi'){
      $id_karyawan=$id;
      $data['id_karyawan']=$id_karyawan;
      $data['dokument']=data_asuransi::where('karyawan_id',$id)->orderBy('id','desc')->get();
      return view('data/karyawan/asuransi',$data);

    }
  }

  public function update(Request $request, $id)
  {
    // if($request->aksi=='update_item'){
    //   $barang=karyawan::find($id)->update($request->all());
    //   return 1;
    // }
  }



}
