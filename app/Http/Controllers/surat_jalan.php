<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Fpdf;
use App\suratjalan;
use App\TableText;

class surat_jalan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $sj=suratjalan::orderBy('id','DESC')->get();
       $data['sj']=$sj;
       $data['halaman']='data-jabatan';
       return view('surat/suratjalan/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->aksi=='simpan_item'){
              suratjalan::create($request->all());
              return 1;
          }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cetak1($id){
      $sj=suratjalan::find($id);
      //$pdf=new Fpdf('L','mm',array(212,161));
      Fpdf::AddPage('L',array(212,161));
      Fpdf::SetMargins(5,5,5,5);
      Fpdf::SetAutoPageBreak(false,10);
      Fpdf::SetTitle('Surat Jalan');
      Fpdf::setXY(135,20);
      Fpdf::SetFont('helvetica','i',11);
      Fpdf::MultiCell(75,4,$sj->tujuan,0,'L');
      Fpdf::SetFont('helvetica','',11);
      Fpdf::setXY(47,57);
      Fpdf::cell(162,5,$sj->dari,0,0,'L');
      switch ($sj->tipe) {
        case 'SURAT':
          Fpdf::Text(11,70,'X');
          break;
        case 'INVOICE':
          Fpdf::Text(40,70,'X');
          break;
        case 'KUITANSI':
          Fpdf::Text(73,70,'X');
          break;
        case 'CEK':
          Fpdf::Text(109,70,'X');
          break;
        case 'GIRO':
          Fpdf::Text(132,70,'X');
          break;
        case 'BARANG':
          Fpdf::Text(156,70,'X');
          break;
        default :
          Fpdf::Text(185,70,'X');
          Fpdf::Text(192,70,$sj->tipe);
          break;

      }

      Fpdf::setXY(36,75);
      Fpdf::MultiCell(173,5,$sj->keterangan,0,'L');
      Fpdf::Text(138,107,$sj->kota);
      $tgl=explode('-',$sj->tanggal);
      $bulan='';
      switch ($tgl[1]) {
        case '01':
          $bulan = 'Januari';
        break;
        case '02':
          $bulan = 'Februari';
        break;
        case '03':
          $bulan = 'Maret';
        break;
        case '04':
          $bulan = 'April';
        break;
        case '05':
          $bulan = 'Mei';
        break;
        case '06':
          $bulan = 'Juni';
        break;
        case '07':
          $bulan = 'Juli';
        break;
        case '08':
          $bulan = 'Agustus';
        break;
        case '09':
          $bulan = 'September';
        break;
        case '10':
          $bulan = 'Oktober';
        break;
        case '11':
          $bulan = 'Nopember';
        break;
        case '12':
          $bulan = 'Desember';
        break;

      }
      $tgl_baru=$tgl[2].' '.$bulan.' '.$tgl[0];
      Fpdf::Text(172,107,$tgl_baru);
      Fpdf::setXY(15,135);
      Fpdf::cell(70,5,$sj->dikirim,0,0,'C');
      Fpdf::setX(134);
      Fpdf::cell(70,5,$sj->diterima,0,0,'C');
      Fpdf::Output('D',$sj->dari.".pdf");

    }
    public function show(Request $request,$id)
    {
        if($request->aksi=='print'){
          $data=$this->cetak1($id);
        }
        if($request->aksi=='PDF'){
              $sj=suratjalan::find($id);
              //$pdf=new Fpdf('P','mm',array())
              $tgl=explode('-',$sj->tanggal);
              $bulan='';
              switch ($tgl[1]) {
                case '01':
                  $bulan = 'Januari';
                break;
                case '02':
                  $bulan = 'Februari';
                break;
                case '03':
                  $bulan = 'Maret';
                break;
                case '04':
                  $bulan = 'April';
                break;
                case '05':
                  $bulan = 'Mei';
                break;
                case '06':
                  $bulan = 'Juni';
                break;
                case '07':
                  $bulan = 'Juli';
                break;
                case '08':
                  $bulan = 'Agustus';
                break;
                case '09':
                  $bulan = 'September';
                break;
                case '10':
                  $bulan = 'Oktober';
                break;
                case '11':
                  $bulan = 'Nopember';
                break;
                case '12':
                  $bulan = 'Desember';
                break;

              }
              $tgl_baru=$tgl[2].' '.$bulan.' '.$tgl[0];

              //
              Fpdf::AddPage('L',array(217,140));
              Fpdf::SetMargins(5,5,5,5);
              Fpdf::SetAutoPageBreak(true,5);
              Fpdf::SetTitle('Surat Jalan');
              Fpdf::rect(5,5,207,130);
              Fpdf::image("./img/logo_hpam_kecil.png",10,10,30);
              Fpdf::setXY(130,10);
              Fpdf::SetFont('helvetica','i',12);
              Fpdf::MultiCell(10,5,'To :',0,'L');
              Fpdf::SetFont('helvetica','',11);
              Fpdf::setXY(140,10);
              Fpdf::MultiCell(60,5,$sj->tujuan,0,'L');
              Fpdf::SetFont('helvetica','B',15);
              Fpdf::ln(4);
              Fpdf::setX(90);
              Fpdf::SetTextColor(255,255,255);
              Fpdf::cell(30,8,'RECEIPT',0,1,'C',1);
              Fpdf::SetTextColor(0,0,0);
              Fpdf::ln(4);
              Fpdf::setX(10);
              Fpdf::SetFont('helvetica','i',12);
              Fpdf::cell(30,5,'Diterima dari : ',0,0,'L');
              Fpdf::setX(40);
              Fpdf::SetFont('helvetica','',12);
              Fpdf::cell(50,5,$sj->dari,0,1,'L');
              Fpdf::ln(3);
              Fpdf::setX(10);
              Fpdf::SetFont('helvetica','i',12);
              Fpdf::cell(15,5,'Tipe : ',0,0,'L');
              Fpdf::setX(25);
              Fpdf::SetFont('helvetica','',12);
              Fpdf::cell(140,5,$sj->tipe,0,1,'L');
              Fpdf::ln(5);
              Fpdf::setX(10);
              Fpdf::SetFont('helvetica','i',12);
              Fpdf::cell(40,5,'Keterangan : ',0,0,'L');
              Fpdf::setX(50);
              Fpdf::SetFont('helvetica','',12);
              Fpdf::MultiCell(150,5,$sj->keterangan,0,'L');
              Fpdf::setXY(150,85);
              Fpdf::cell(40,5,$sj->kota.',  '.$tgl_baru,0,0,'L');
              Fpdf::setXY(10,93);
              Fpdf::SetFont('helvetica','i',12);
              Fpdf::cell(40,5,'Diterima Oleh, ',0,0,'L');
              Fpdf::SetX(160);
              Fpdf::cell(40,5,'Dikirim Oleh, ',0,0,'L');
              Fpdf::setXY(10,110);
              if($sj->diterima==NULL){
                $penerima="......................";
              }else{
                $penerima=$sj->diterima;
              }
              Fpdf::cell(40,2,$penerima,0,0,'L');
              Fpdf::SetX(160);
              if($sj->dikirim==NULL){
                $pengirim="......................";
              }else{
                $pengirim=$sj->dikirim;
              }
              Fpdf::cell(40,2,$pengirim,0,0,'L');
              Fpdf::setXY(7,116);
              Fpdf::SetFont('helvetica','',10);
              //Fpdf::SetFillColor(130, 130, 130);
              Fpdf::rect(5,115,207,20);
              //Fpdf::SetTextColor(255, 255, 255);
              Fpdf::MultiCell(204,5,"HP ASSET MANAGEMENT \n Tamara Center Lt.7 - Jln Jendral Sudirman 24, Jakarta 1290, Indonesia -Telp : (+6221) 520 6699, Fax : (+6221) 520 6700 \n WWW.HPAM.CO.ID",0,'C');
              Fpdf::Output();
        }

        if($request->aksi=='edit'){

            $data['item']=suratjalan::find($id);
            return view('surat/suratjalan/edit_list',$data);
        }

        if($request->aksi=="list-all"){
            $awal=$request->start;
            $banyak=$request->length;
            $banyak_colom=$request->iColumns;
            $kata_kunci_global=$request->sSearch;
            $echo=$request->draw;
            $kata_kunci=$request->search['value'];
            if( ($banyak<0) AND ($kata_kunci != "") ){
                $keyword='%'.$kata_kunci.'%';
                $item=suratjalan::select('id','dari','tujuan','tanggal','keterangan','diterima','dikirim','kota')
                      ->where('tujuan','like',$keyword)
                      ->orWhere('tanggal','like',$keyword)
                      ->orWhere('keterangan','like',$keyword)
                      ->orWhere('diterima','like',$keyword)
                      ->orWhere('dari','like',$keyword)
                      ->orWhere('dikirim','like',$keyword)
                      ->orderBy('id','desc')->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            }else if($kata_kunci != "" ) {
                $keyword='%'.$kata_kunci.'%';
              $item=suratjalan::select('id','dari','tujuan','tanggal','keterangan','diterima','dikirim','kota')
                        ->where('tujuan','like',$keyword)
                        ->orWhere('tanggal','like',$keyword)
                        ->orWhere('keterangan','like',$keyword)
                        ->orWhere('diterima','like',$keyword)
                        ->orWhere('dari','like',$keyword)
                        ->orWhere('dikirim','like',$keyword)
                      ->take($banyak)->orderBy('id','desc')->get();
              $total=suratjalan::select('id','dari','tujuan','tanggal','keterangan','diterima','dikirim','kota')
                        ->where('tujuan','like',$keyword)
                        ->orWhere('tanggal','like',$keyword)
                        ->orWhere('keterangan','like',$keyword)
                        ->orWhere('diterima','like',$keyword)
                        ->orWhere('dari','like',$keyword)
                        ->orWhere('dikirim','like',$keyword)
                      ->count();
                $data['recordsTotal']=$total;
                $data['recordsFiltered']=$total;

            } else if($banyak<0) {
                $item=suratjalan::select('id','dari','tujuan','tanggal','keterangan','diterima','dikirim','kota')->orderBy('id','desc')->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            } else {
                $item=suratjalan::select('id','dari','tujuan','tanggal','keterangan','diterima','dikirim','kota')->skip($awal)->take($banyak)->orderBy('id','desc')->get();
                $total=suratjalan::select('id','dari','tujuan','tanggal','keterangan','diterima','dikirim','kota')->count();
                $data['recordsTotal']=$total;
                $data['recordsFiltered']=$total;
            }

            $gh_x=$awal+1;
            $item->each(function($item) use (&$gh_x) {
                $item->setAttribute('nomer',$gh_x++);
                $item->setAttribute('action','<a alt="Edit Surat"  href="'.url("surat-jalan").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                            <i class="fa fa-edit"></i>
                        </a>

                        <a alt="Generate PDF File" target="_blank" href="'.url("surat-jalan").'/'.$item->id.'?aksi=PDF" class="btn btn-md btn-icon-only red">
                            <i class="fa fa-file-pdf-o"></i>
                        </a>

                        <button alt="Print Surat" onclick="print_surat('."'".url("surat-jalan").'/'.$item->id."?aksi=print"."'".');"  href="'.url("surat-jalan").'/'.$item->id.'?aksi=pdf" class="btn btn-md btn-icon-only yellow">
                            <i class="fa fa-print"></i>
                        </a>
                        ');
            });

            $data['draw']=$echo;
            $data['data']=$item;
            return $data;
            /*return json_encode($data);*/
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->aksi=='update_item'){
        $barang=suratjalan::find($id)->update($request->all());
        return 1;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
