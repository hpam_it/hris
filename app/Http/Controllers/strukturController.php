<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\divisi;
use App\jabatan;
use App\kantor;
use App\struktur;
use DB;

class strukturController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['halaman']='data-struktur';
      $data['divisi']=divisi::all();
      $data['jabatan']=jabatan::all();
      $data['kantor']=kantor::all();

      return view('data/struktur/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->aksi=='simpan_item'){
          $item=struktur::where('divisi_id',$request->divisi_id)
                    ->where('jabatan_id',$request->jabatan_id)
                    ->where('kantor_id',$request->kantor_id)
                    ->first();
          $jabatan=jabatan::find($request->jabatan_id);
          $divisi=divisi::find($request->divisi_id);
          $kantor=kantor::find($request->kantor_id);
          //kode struktur
          $kode = $jabatan->kode_jabatan.$divisi->kode_divisi.$kantor->kode_kantor;


          $atasan=struktur::where('divisi_id',$request->divisi_atasan)
                    ->where('jabatan_id',$request->jabatan_atasan)
                    ->where('kantor_id',$request->kantor_atasan)
                    ->first();

          $jabatan1=jabatan::find($request->jabatan_atasan);
          $divisi1=divisi::find($request->divisi_atasan);
          $kantor1=kantor::find($request->kantor_atasan);
          //kode struktur atasan
          $kode1 = $jabatan1->kode_jabatan.$divisi1->kode_divisi.$kantor1->kode_kantor;


          $cek_item=count($item);
          $ada=count($atasan);
          if($cek_item>0){
            //update aja
            //jika atasan sudah ada ambil aja id nya, kalo belum ada bikinin dulu
            if($ada>0){
              //update langsung dengan id yang ada
              $struktur=struktur::find($item->id);
              $struktur->atasan=$atasan->id;
              $struktur->save();
            }else{
              //insert atasan baru
              $struktur_baru=new struktur;
              $struktur_baru->jabatan_id=$request->jabatan_atasan;
              $struktur_baru->divisi_id=$request->divisi_atasan;
              $struktur_baru->kantor_id=$request->kantor_atasan;
              $struktur_baru->atasan=0;
              $struktur_baru->kode_struktur=$kode1;
              $struktur_baru->save();
              //cari atasannya
              $atasan2=struktur::where('divisi_id',$request->divisi_atasan)
                      ->where('jabatan_id',$request->jabatan_atasan)
                      ->where('kantor_id',$request->kantor_atasan)
                      ->first();
              //update jabatan
              $struktur=struktur::find($item->id);
              $struktur->atasan=$atasan2->id;
              $struktur->save();
            }

              return 1;
          } else {
              //cari atasan
              //jika atasan sudah ada ambil aja id nya, kalo belum ada
              if($ada>0){
                //insert langsung dengan id yang ada
                $struktur=new struktur;
                $struktur->jabatan_id=$request->jabatan_id;
                $struktur->divisi_id=$request->divisi_id;
                $struktur->kantor_id=$request->kantor_id;
                $struktur->atasan=$atasan->id;
                $struktur->kode_struktur=$kode;
                $struktur->save();
              }else{
                //insert atasan baru
                $struktur_baru=new struktur;
                $struktur_baru->jabatan_id=$request->jabatan_atasan;
                $struktur_baru->divisi_id=$request->divisi_atasan;
                $struktur_baru->kantor_id=$request->kantor_atasan;
                $struktur_baru->atasan=0;
                $struktur_baru->kode_struktur=$kode1;
                $struktur_baru->save();
                //cari atasannya
                $atasan2=struktur::where('divisi_id',$request->divisi_atasan)
                        ->where('jabatan_id',$request->jabatan_atasan)
                        ->where('kantor_id',$request->kantor_atasan)
                        ->first();
                // insert ajax
                $struktur=new struktur;
                $struktur->jabatan_id=$request->jabatan_id;
                $struktur->divisi_id=$request->divisi_id;
                $struktur->kantor_id=$request->kantor_id;
                $struktur->atasan=$atasan2->id;
                $struktur->kode_struktur=$kode;
                $struktur->save();
              }
              return 1;
          }

      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=="list-all"){
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              $item=DB::table('strukturs AS struktur')
                    ->select('struktur.id','struktur.kode_struktur','divisiA.nama_divisi','jabatanA.nama_jabatan','kantorA.nama_kantor')
                    ->addSelect('atasan.id AS id_atasan','atasan.kode_struktur AS kode_atasan','divisiB.nama_divisi AS divisi_atasan','jabatanB.nama_jabatan AS jabatan_atasan','kantorB.nama_kantor AS kantor_atasan')
                    ->leftjoin('divisis AS divisiA','struktur.divisi_id','=','divisiA.id')
                    ->leftjoin('jabatans AS jabatanA','struktur.jabatan_id','=','jabatanA.id')
                    ->leftjoin('kantors AS kantorA','struktur.kantor_id','=','kantorA.id')
                    ->leftjoin('strukturs AS atasan','struktur.atasan','=','atasan.id')
                    ->leftjoin('divisis AS divisiB','atasan.divisi_id','=','divisiB.id')
                    ->leftjoin('jabatans AS jabatanB','atasan.jabatan_id','=','jabatanB.id')
                    ->leftjoin('kantors AS kantorB','atasan.kantor_id','=','kantorB.id')
                    ->where('divisiA.nama_divisi','like',$keyword)
                    ->orWhere('jabatanA.nama_jabatan','like',$keyword)
                    ->orWhere('kantorA.nama_kantor','like',$keyword)
                    ->orderBy('id','desc')
                    ->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              $item=DB::table('strukturs AS struktur')
                    ->select('struktur.id','struktur.kode_struktur','divisiA.nama_divisi','jabatanA.nama_jabatan','kantorA.nama_kantor')
                    ->addSelect('atasan.id AS id_atasan','atasan.kode_struktur AS kode_atasan','divisiB.nama_divisi AS divisi_atasan','jabatanB.nama_jabatan AS jabatan_atasan','kantorB.nama_kantor AS kantor_atasan')
                    ->leftjoin('divisis AS divisiA','struktur.divisi_id','=','divisiA.id')
                    ->leftjoin('jabatans AS jabatanA','struktur.jabatan_id','=','jabatanA.id')
                    ->leftjoin('kantors AS kantorA','struktur.kantor_id','=','kantorA.id')
                    ->leftjoin('strukturs AS atasan','struktur.atasan','=','atasan.id')
                    ->leftjoin('divisis AS divisiB','atasan.divisi_id','=','divisiB.id')
                    ->leftjoin('jabatans AS jabatanB','atasan.jabatan_id','=','jabatanB.id')
                    ->leftjoin('kantors AS kantorB','atasan.kantor_id','=','kantorB.id')
                    ->Where('divisiA.nama_divisi','like',$keyword)
                    ->orWhere('jabatanA.nama_jabatan','like',$keyword)
                    ->orWhere('kantorA.nama_kantor','like',$keyword)
                    ->take($banyak)
                    ->orderBy('id','desc')
                    ->get();
              $total=DB::table('strukturs AS struktur')
              ->select('struktur.id','struktur.kode_struktur','divisiA.nama_divisi','jabatanA.nama_jabatan','kantorA.nama_kantor')
              ->addSelect('atasan.id AS id_atasan','atasan.kode_struktur AS kode_atasan','divisiB.nama_divisi AS divisi_atasan','jabatanB.nama_jabatan AS jabatan_atasan','kantorB.nama_kantor AS kantor_atasan')
              ->leftjoin('divisis AS divisiA','struktur.divisi_id','=','divisiA.id')
              ->leftjoin('jabatans AS jabatanA','struktur.jabatan_id','=','jabatanA.id')
              ->leftjoin('kantors AS kantorA','struktur.kantor_id','=','kantorA.id')
              ->leftjoin('strukturs AS atasan','struktur.atasan','=','atasan.id')
              ->leftjoin('divisis AS divisiB','atasan.divisi_id','=','divisiB.id')
              ->leftjoin('jabatans AS jabatanB','atasan.jabatan_id','=','jabatanB.id')
              ->leftjoin('kantors AS kantorB','atasan.kantor_id','=','kantorB.id')
              ->Where('divisiA.nama_divisi','like',$keyword)
              ->orWhere('jabatanA.nama_jabatan','like',$keyword)
              ->orWhere('kantorA.nama_kantor','like',$keyword)
              ->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;

          } else if($banyak<0) {
              $item=DB::table('strukturs AS struktur')
              ->select('struktur.id','struktur.kode_struktur','divisiA.nama_divisi','jabatanA.nama_jabatan','kantorA.nama_kantor')
              ->addSelect('atasan.id AS id_atasan','atasan.kode_struktur AS kode_atasan','divisiB.nama_divisi AS divisi_atasan','jabatanB.nama_jabatan AS jabatan_atasan','kantorB.nama_kantor AS kantor_atasan')
              ->leftjoin('divisis AS divisiA','struktur.divisi_id','=','divisiA.id')
              ->leftjoin('jabatans AS jabatanA','struktur.jabatan_id','=','jabatanA.id')
              ->leftjoin('kantors AS kantorA','struktur.kantor_id','=','kantorA.id')
              ->leftjoin('strukturs AS atasan','struktur.atasan','=','atasan.id')
              ->leftjoin('divisis AS divisiB','atasan.divisi_id','=','divisiB.id')
              ->leftjoin('jabatans AS jabatanB','atasan.jabatan_id','=','jabatanB.id')
              ->leftjoin('kantors AS kantorB','atasan.kantor_id','=','kantorB.id')
                    ->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
              $item=DB::table('strukturs AS struktur')
              ->select('struktur.id','struktur.kode_struktur','divisiA.nama_divisi','jabatanA.nama_jabatan','kantorA.nama_kantor')
              ->addSelect('atasan.id AS id_atasan','atasan.kode_struktur AS kode_atasan','divisiB.nama_divisi AS divisi_atasan','jabatanB.nama_jabatan AS jabatan_atasan','kantorB.nama_kantor AS kantor_atasan')
              ->leftjoin('divisis AS divisiA','struktur.divisi_id','=','divisiA.id')
              ->leftjoin('jabatans AS jabatanA','struktur.jabatan_id','=','jabatanA.id')
              ->leftjoin('kantors AS kantorA','struktur.kantor_id','=','kantorA.id')
              ->leftjoin('strukturs AS atasan','struktur.atasan','=','atasan.id')
              ->leftjoin('divisis AS divisiB','atasan.divisi_id','=','divisiB.id')
              ->leftjoin('jabatans AS jabatanB','atasan.jabatan_id','=','jabatanB.id')
              ->leftjoin('kantors AS kantorB','atasan.kantor_id','=','kantorB.id')
                    ->skip($awal)->take($banyak)->orderBy('id','desc')->get();
              $total=DB::table('strukturs AS struktur')
              ->select('struktur.id','struktur.kode_struktur','divisiA.nama_divisi','jabatanA.nama_jabatan','kantorA.nama_kantor')
              ->addSelect('atasan.id AS id_atasan','atasan.kode_struktur AS kode_atasan','divisiB.nama_divisi AS divisi_atasan','jabatanB.nama_jabatan AS jabatan_atasan','kantorB.nama_kantor AS kantor_atasan')
              ->leftjoin('divisis AS divisiA','struktur.divisi_id','=','divisiA.id')
              ->leftjoin('jabatans AS jabatanA','struktur.jabatan_id','=','jabatanA.id')
              ->leftjoin('kantors AS kantorA','struktur.kantor_id','=','kantorA.id')
              ->leftjoin('strukturs AS atasan','struktur.atasan','=','atasan.id')
              ->leftjoin('divisis AS divisiB','atasan.divisi_id','=','divisiB.id')
              ->leftjoin('jabatans AS jabatanB','atasan.jabatan_id','=','jabatanB.id')
              ->leftjoin('kantors AS kantorB','atasan.kantor_id','=','kantorB.id')
                    ->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x) {
              $item->nomer=$gh_x;
              $item->action='<a href="'.url("data-struktur").'/'.$item->id.'/edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                          <i class="fa fa-edit"></i>
                      </a>
                      <button data-url="'.url("data-struktur").'/'.$item->id.'" class=" hapus-input konfirmasi btn btn-md btn-icon-only red">
                          <i class="fa fa-trash"></i>
                      </button>';
              $gh_x++;

          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }

      if($request->aksi=='hapus_input'){
        $hapus=struktur::where('atasan',$id)->get();
        if(count($hapus) > 0){
          return 0;
        }else{
          $st=struktur::find($id);
          $st->delete();
          return 1;
        }
      }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['struktur']=DB::table('strukturs AS struktur')
            ->select('struktur.id','struktur.jabatan_id','struktur.divisi_id','struktur.kantor_id')
            ->addSelect('atasan.id AS id_atasan','atasan.jabatan_id AS jabatan_atasan','atasan.divisi_id AS divisi_atasan','atasan.kantor_id as kantor_atasan')
            ->leftjoin('strukturs AS atasan','struktur.atasan','=','atasan.id')
            ->Where('struktur.id','=',$id)
            ->first();
            $data['divisi']=divisi::all();
            $data['jabatan']=jabatan::all();
            $data['kantor']=kantor::all();
      return view('data/struktur/edit_list',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->aksi=='simpan_item'){
          $item=struktur::where('divisi_id',$request->divisi_id)
                    ->where('jabatan_id',$request->jabatan_id)
                    ->where('kantor_id',$request->kantor_id)
                    ->first();
          $jabatan=jabatan::find($request->jabatan_id);
          $divisi=divisi::find($request->divisi_id);
          $kantor=kantor::find($request->kantor_id);
          //kode struktur
          $kode = $jabatan->kode_jabatan.$divisi->kode_divisi.$kantor->kode_kantor;


          $atasan=struktur::where('divisi_id',$request->divisi_atasan)
                    ->where('jabatan_id',$request->jabatan_atasan)
                    ->where('kantor_id',$request->kantor_atasan)
                    ->first();

          $jabatan1=jabatan::find($request->jabatan_atasan);
          $divisi1=divisi::find($request->divisi_atasan);
          $kantor1=kantor::find($request->kantor_atasan);
          //kode struktur atasan
          $kode1 = $jabatan1->kode_jabatan.$divisi1->kode_divisi.$kantor1->kode_kantor;


          $cek_item=count($item);
          $ada=count($atasan);
          if($cek_item>0){
            //update aja
            //jika atasan sudah ada ambil aja id nya, kalo belum ada bikinin dulu
            if($ada>0){
              //update langsung dengan id yang ada
              $struktur=struktur::find($item->id);
              $struktur->atasan=$atasan->id;
              $struktur->save();
            }else{
              //insert atasan baru
              $struktur_baru=new struktur;
              $struktur_baru->jabatan_id=$request->jabatan_atasan;
              $struktur_baru->divisi_id=$request->divisi_atasan;
              $struktur_baru->kantor_id=$request->kantor_atasan;
              $struktur_baru->atasan=0;
              $struktur_baru->kode_struktur=$kode1;
              $struktur_baru->save();
              //cari atasannya
              $atasan2=struktur::where('divisi_id',$request->divisi_atasan)
                      ->where('jabatan_id',$request->jabatan_atasan)
                      ->where('kantor_id',$request->kantor_atasan)
                      ->first();
              //update jabatan
              $struktur=struktur::find($item->id);
              $struktur->atasan=$atasan2->id;
              $struktur->save();
            }

              return 1;
          } else {
              //cari atasan
              //jika atasan sudah ada ambil aja id nya, kalo belum ada
              if($ada>0){
                //insert langsung dengan id yang ada
                $struktur=new struktur;
                $struktur->jabatan_id=$request->jabatan_id;
                $struktur->divisi_id=$request->divisi_id;
                $struktur->kantor_id=$request->kantor_id;
                $struktur->atasan=$atasan->id;
                $struktur->kode_struktur=$kode;
                $struktur->save();
              }else{
                //insert atasan baru
                $struktur_baru=new struktur;
                $struktur_baru->jabatan_id=$request->jabatan_atasan;
                $struktur_baru->divisi_id=$request->divisi_atasan;
                $struktur_baru->kantor_id=$request->kantor_atasan;
                $struktur_baru->atasan=0;
                $struktur_baru->kode_struktur=$kode1;
                $struktur_baru->save();
                //cari atasannya
                $atasan2=struktur::where('divisi_id',$request->divisi_atasan)
                        ->where('jabatan_id',$request->jabatan_atasan)
                        ->where('kantor_id',$request->kantor_atasan)
                        ->first();
                // insert ajax
                $struktur=new struktur;
                $struktur->jabatan_id=$request->jabatan_id;
                $struktur->divisi_id=$request->divisi_id;
                $struktur->kantor_id=$request->kantor_id;
                $struktur->atasan=$atasan2->id;
                $struktur->kode_struktur=$kode;
                $struktur->save();
              }
              return 1;
          }

      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
