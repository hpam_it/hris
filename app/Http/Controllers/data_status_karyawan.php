<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\status_karyawan;

class data_status_karyawan extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['halaman']='data-status-karyawan';
      return view('data/status-karyawan/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->aksi=='simpan_item'){
          $cek_item=status_karyawan::where('status_karyawan',$request->status_karyawan)->count();
          if($cek_item>0){
              return 0;
          } else {
              status_karyawan::create($request->all());
              return 1;
          }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=='edit'){

        $data['item']=status_karyawan::find($id);
        return view('data/status-karyawan/edit_list',$data);
    }

    if($request->aksi=="list-all"){
        $awal=$request->start;
        $banyak=$request->length;
        $banyak_colom=$request->iColumns;
        $kata_kunci_global=$request->sSearch;
        $echo=$request->draw;
        $kata_kunci=$request->search['value'];
        if( ($banyak<0) AND ($kata_kunci != "") ){
            $keyword='%'.$kata_kunci.'%';
            $item=status_karyawan::select('id','status_karyawan')->where('status_karyawan','like',$keyword)->orderBy('id','desc')->get();
            $data['recordsTotal']=count($item);
            $data['recordsFiltered']=count($item);
        }else if($kata_kunci != "" ) {
            $keyword='%'.$kata_kunci.'%';
            $item=status_karyawan::select('id','status_karyawan')->skip($awal)->orWhere('status_karyawan','like',$keyword)->take($banyak)->orderBy('id','desc')->get();
            $total=status_karyawan::select('id','status_karyawan')->orWhere('status_karyawan','like',$keyword)->count();
            $data['recordsTotal']=$total;
            $data['recordsFiltered']=$total;

        } else if($banyak<0) {
            $item=status_karyawan::select('id','status_karyawan')->orderBy('id','desc')->get();
            $data['recordsTotal']=count($item);
            $data['recordsFiltered']=count($item);
        } else {
            $item=status_karyawan::select('id','status_karyawan')->skip($awal)->take($banyak)->orderBy('id','desc')->get();
            $total=status_karyawan::select('id','status_karyawan')->count();
            $data['recordsTotal']=$total;
            $data['recordsFiltered']=$total;
        }

        $gh_x=$awal+1;
        $item->each(function($item) use (&$gh_x) {
            $item->setAttribute('nomer',$gh_x++);
            $item->setAttribute('action','
                    <button onclick="hapus_input('."'".url('data-status-karyawan')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
                        <i class="fa fa-trash"></i>
                    </button>');
        });

        $data['draw']=$echo;
        $data['data']=$item;
        return $data;
        /*return json_encode($data);*/
    }

    if($request->aksi=='hapus_input'){
        status_karyawan::find($id)->delete();
        return 1;
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->aksi=='update_item'){
        $barang=status_karyawan::find($id)->update($request->all());
        return 1;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
