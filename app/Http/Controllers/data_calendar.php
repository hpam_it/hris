<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\hari_libur;

class data_calendar extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['halaman']='data-calendar';
      return view('data/calendar/index',$data);
    }

    public function list(Request $request){

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->aksi=='simpan_item'){
          $cek_item=hari_libur::where('tanggal',$request->tanggal)->count();
          if($cek_item>0){
              return 0;
          } else {
              hari_libur::create($request->all());
              return 1;
          }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=='json'){
          $start=$request->start;
          $end=$request->end;
          $libur=hari_libur::whereBetween('tanggal', [$start, $end])->get();
          $item=[];
          $x=0;
          foreach ($libur as $libur) {
            $item[$x]['title']=$libur->keterangan;
            $item[$x]['start']=$libur->tanggal;
            $item[$x]['backgroundColor']='#870e0e';
            $x++;
            }
          return json_encode($item);
      }

      if($request->aksi=='edit'){
        $data['item']=hari_libur::find($id);
        return view('data/calendar/edit_list',$data);
      }


      if($request->aksi=="list-all"){
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $order=$request->order;
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              $item=hari_libur::where('keterangan','like',$keyword);
              foreach ($order as $order) {
                switch ($order['column']) {
                  case '0':
                      $item=$item->orderBy('id',$order['dir']);
                      break;
                  case '1':
                      $item=$item->orderBy('tanggal',$order['dir']);
                      break;
                  case '2':
                      $item=$item->orderBy('keterangan',$order['dir']);
                      break;
                  default:
                      $item=$item->orderBy('id',$order['dir']);
                      break;
                }
              }
              $item=$item->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              $item=hari_libur::skip($awal)->where('keterangan','like',$keyword)->take($banyak);
              foreach ($order as $order) {
                switch ($order['column']) {
                  case '0':
                      $item=$item->orderBy('id',$order['dir']);
                      break;
                  case '1':
                      $item=$item->orderBy('tanggal',$order['dir']);
                      break;
                  case '2':
                      $item=$item->orderBy('keterangan',$order['dir']);
                      break;
                  default:
                      $item=$item->orderBy('id',$order['dir']);
                      break;
                }
              }
              $item=$item->get();
              $total=hari_libur::where('keterangan','like',$keyword)->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;

          } else if($banyak<0) {
              $item=hari_libur::select('hari_liburs.*');
              foreach ($order as $order) {
                switch ($order['column']) {
                  case '0':
                      $item=$item->orderBy('id',$order['dir']);
                      break;
                  case '1':
                      $item=$item->orderBy('tanggal',$order['dir']);
                      break;
                  case '2':
                      $item=$item->orderBy('keterangan',$order['dir']);
                      break;
                  default:
                      $item=$item->orderBy('id',$order['dir']);
                      break;
                }
              }
              $item=$item->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
              $item=hari_libur::skip($awal)->take($banyak);
              foreach ($order as $order) {
                switch ($order['column']) {
                  case '0':
                      $item=$item->orderBy('id',$order['dir']);
                      break;
                  case '1':
                      $item=$item->orderBy('tanggal',$order['dir']);
                      break;
                  case '2':
                      $item=$item->orderBy('keterangan',$order['dir']);
                      break;
                  default:
                      $item=$item->orderBy('id',$order['dir']);
                      break;
                }
              }
              $item=$item->get();
              $total=hari_libur::count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x) {
              $item->setAttribute('nomer',$gh_x++);
              $item->setAttribute('action','<a href="'.url("data-calendar").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                          <i class="fa fa-edit"></i>
                      </a>
                      <button onclick="hapus_input('."'".url('data-calendar')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
                          <i class="fa fa-trash"></i>
                      </button>');
          });
          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }

      if($request->aksi=='hapus_input'){
          hari_libur::find($id)->delete();
          return 1;
      }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if($request->aksi=='update_item'){
        $barang=hari_libur::find($id)->update($request->all());
        return 1;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
