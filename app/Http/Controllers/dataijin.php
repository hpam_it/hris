<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\data_ijin;

class dataijin extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $data['halaman']='data-ijin';
    return view('data/ijin/index',$data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if($request->aksi=='simpan_item'){
        $cek_item=data_ijin::where('nama_ijin',$request->nama_ijin)->count();
        if($cek_item>0){
            return 0;
        } else {
            data_ijin::create($request->all());
            return 1;
        }

    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request,$id)
  {
    if($request->aksi=='edit'){
      $data['item']=data_ijin::find($id);
      return view('data/ijin/edit_list',$data);
      }

      if($request->aksi=="list-all"){
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              $item=data_ijin::select('id','nama_ijin','hitung','fullday','hanya_marketing')->where('nama_ijin','like',$keyword)->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              $item=data_ijin::select('id','nama_ijin','hitung','fullday','hanya_marketing')->skip($awal)->where('nama_ijin','like',$keyword)->take($banyak)->orderBy('id','desc')->get();
              $total=data_ijin::select('id','nama_ijin','hitung','fullday','hanya_marketing')->where('nama_ijin','like',$keyword)->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;

          } else if($banyak<0) {
              $item=data_ijin::select('id','nama_ijin','hitung','fullday','hanya_marketing')->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
              $item=data_ijin::select('id','nama_ijin','hitung','fullday','hanya_marketing')->skip($awal)->take($banyak)->orderBy('id','desc')->get();
              $total=data_ijin::select('id','nama_ijin','hitung','fullday','hanya_marketing')->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x) {
              $item->setAttribute('nomer',$gh_x++);
              if($item->hitung=='1'){
                $item->setAttribute('hitung_kerja','YA');
              }else{
                $item->setAttribute('hitung_kerja','TIDAK');
              }
              if($item->fullday=='1'){
                $item->setAttribute('sehari_penuh','YA');
              }else{
                $item->setAttribute('sehari_penuh','TIDAK');
              }
              $item->setAttribute('action','<a href="'.url("data-ijin").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                          <i class="fa fa-edit"></i>
                      </a>
                      <button onclick="hapus_input('."'".url('data-ijin')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
                          <i class="fa fa-trash"></i>
                      </button>');
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }

      if($request->aksi=='hapus_input'){
          data_ijin::find($id)->delete();
          return 1;
      }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if($request->aksi=='update_item'){
      $barang=data_ijin::find($id)->update($request->all());
      return 1;
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }
}
