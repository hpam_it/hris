<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\karyawan;
use App\data_ptkp;
use App\penghasilan;
use App\pengeluaran;
use App\nilai_ptkp;
use App\pajak;
use App\data_lapisan;
use DB;

class inputpajak extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['halaman']='input-pajak';
      $data['karyawan']=karyawan::orderBy('nama_karyawan','ASC')->get();
      $data['ptkp']=data_ptkp::all();
      return view('pajak/input-pajak/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->aksi=='simpan_item'){
        //simpan penghasilan
        //cek apakah sudah di input
        $bulan_pajak=explode('-',$request->bulan_pajak);
        $cek_penghasilan=penghasilan::where('karyawan_id',$request->karyawan_id)
                                      ->whereYear('bulan_pajak',$bulan_pajak['0'])
                                      ->whereMonth('bulan_pajak',$bulan_pajak['1'])
                                      ->count();
        $cek_pengeluaran=pengeluaran::where('karyawan_id',$request->karyawan_id)
                                      ->whereYear('bulan_pajak',$bulan_pajak['0'])
                                      ->whereMonth('bulan_pajak',$bulan_pajak['1'])
                                      ->count();
        $cek_pajak=pajak::where('karyawan_id',$request->karyawan_id)
                                      ->whereYear('bulan_pajak',$bulan_pajak['0'])
                                      ->whereMonth('bulan_pajak',$bulan_pajak['1'])
                                      ->count();
        $cek_ptkp=nilai_ptkp::where('karyawan_id',$request->karyawan_id)
                                      ->whereYear('bulan_pajak',$bulan_pajak['0'])
                                      ->whereMonth('bulan_pajak',$bulan_pajak['1'])
                                      ->count();
        if($cek_penghasilan > 0 OR $cek_pengeluaran > 0 OR $cek_pajak > 0 OR $cek_ptkp > 0){
          return 0;
        }else {
          $penghasilan_input=penghasilan::create($request->all());
          $pengurang_input=new pengeluaran;
          $pengurang_input['karyawan_id']=$request->karyawan_id;
          $pengurang_input['biaya_jabatan']=$request->biaya_jabatan;
          $pengurang_input['iuran_jht']=$request->iuran_jht;
          $pengurang_input['iuran_pensiun']=$request->iuran_pensiun;
          $pengurang_input['bulan_pajak']=$request->bulan_pajak;
          $pengurang_input->save();

          $data_ptkp=$request->data_ptkp;
          foreach ($data_ptkp as $key=>$value) {
            $data_ptkp=data_ptkp::find($key);
            $ptkp=new nilai_ptkp;
            $ptkp['karyawan_id']=$request->karyawan_id;
            $ptkp['data_ptkp_id']=$key;
            $ptkp['qty']=$value;
            $ptkp['nilai']=$data_ptkp->nilai;
            $ptkp['total']=$data_ptkp->nilai*$value;
            $ptkp['bulan_pajak']=$request->bulan_pajak;
            $ptkp->save();
          }

          //hitung total pendapatan
          $peng=DB::table('penghasilans')
                      ->addSelect(DB::raw('IFNULL(SUM(penghasilans.adjustmen_gaji),0) as total_adjustmen_gaji'))
                      ->addSelect(DB::raw('IFNULL(SUM(penghasilans.gaji_pokok),0) as total_gaji'))
                      ->addSelect(DB::raw('IFNULL(SUM(penghasilans.tunjangan_makan),0) as total_tunjangan_makan'))
                      ->addSelect(DB::raw('IFNULL(SUM(penghasilans.insentif),0) as total_insentif'))
                      ->addSelect(DB::raw('IFNULL(SUM(penghasilans.bonus),0) as total_bonus'))
                      ->addSelect(DB::raw('IFNULL(SUM(penghasilans.lembur),0) as total_lembur'))
                      ->addSelect(DB::raw('IFNULL(SUM(penghasilans.uang_dinas),0) as total_uang_dinas'))
                      ->addSelect(DB::raw('IFNULL(SUM(penghasilans.thr),0) as total_thr'))
                      ->addSelect(DB::raw('IFNULL(SUM(penghasilans.jkk),0) as total_jkk'))
                      ->addSelect(DB::raw('IFNULL(SUM(penghasilans.jk),0) as total_jk'))
                      ->where('karyawan_id',$request->karyawan_id)
                      ->whereYear('bulan_pajak',$bulan_pajak[0])
                      ->first();
          $bulan_kedepan=12-$bulan_pajak['1'];//total gaji tahun sebelumnya ditambah tahun berjalan
          $total_gaji=$peng->total_gaji+($request->gaji_pokok*$bulan_kedepan);
          $penghasilan_kotor=$total_gaji+$peng->total_jkk+($bulan_kedepan*$request->jkk)+$peng->total_jk+($bulan_kedepan*$request->jk)+$peng->total_adjustmen_gaji+$peng->total_tunjangan_makan+$peng->total_insentif+$peng->total_bonus+$peng->total_lembur+$peng->total_uang_dinas+$peng->total_thr;
          //hitung biaya jabatan
          $pengurang=pengeluaran::where('karyawan_id',$request->karyawan_id)
                                  ->addSelect(DB::raw('IFNULL(SUM(pengeluarans.biaya_jabatan),0) as total_biaya_jabatan'))
                                  ->addSelect(DB::raw('IFNULL(SUM(pengeluarans.iuran_jht),0) as total_jht'))
                                  ->addSelect(DB::raw('IFNULL(SUM(pengeluarans.iuran_pensiun),0) as total_pensiun'))
                                  ->whereYear('bulan_pajak',$bulan_pajak[0])
                                  ->first();
          $total_pengurang=$pengurang->total_biaya_jabatan+($request->biaya_jabatan*$bulan_kedepan)+$pengurang->total_jht+($request->iuran_jht*$bulan_kedepan)+$pengurang->total_pensiun+($request->iuran_pensiun*$bulan_kedepan);
          $penghasilan_neto=$penghasilan_kotor-$total_pengurang;
          $ptkp=nilai_ptkp::select(DB::raw('IFNULL(SUM(total),0) as total_ptkp'))
                            ->where('karyawan_id',$request->karyawan_id)
                            ->whereYear('bulan_pajak',$bulan_pajak[0])
                            ->whereMonth('bulan_pajak',$bulan_pajak[1])
                            ->first();
          $total_ptkp=$ptkp->total_ptkp;
          $total_pkp=$penghasilan_neto-$total_ptkp;
          //hitung pajak penghasilan
          $lapisan=data_lapisan::orderBy('urutan','ASC')->get();
          $pajak_setahun=0;//total pph setahun
          $hitung_pajak=$total_pkp;
          $banyak_lapisan=count($lapisan);
          $lapisan_count=1;
          $nilai_sebelum=0;
          //hitung berdasarkan lapisan
          $nilai_pajak=[];
          foreach ($lapisan as $key) {
            if($lapisan_count<$banyak_lapisan){
              if( ($hitung_pajak<=$key->lapisan) AND ($hitung_pajak > $nilai_sebelum) ){ //lebih kecil dari nilai setelahnya dan lebih besar daari sebelumnya
                  $pajak_setahun+=($hitung_pajak-$nilai_sebelum)*($key->tarif/100);
                  $nilai_pajak[$lapisan_count]=($hitung_pajak-$nilai_sebelum)*($key->tarif/100); // nilai pkp - nilai sebelumnya
              }else if( ($hitung_pajak > $nilai_sebelum) AND ($hitung_pajak > $key->lapisan) ) {// lebih besar dari sebelumnya dan lebih besar dari nilai sekarang
                  $pajak_setahun+=($key->lapisan-$nilai_sebelum)*($key->tarif/100);//nilai sekarang - nilai sebelum
                  $nilai_pajak[$lapisan_count]=($key->lapisan-$nilai_sebelum)*($key->tarif/100);
              }

            }else{
              if($hitung_pajak >= $key->lapisan){//lapisan terakhir jika pkp lebih besar dari lapisan terakhir
                $pajak_setahun+=($hitung_pajak-$nilai_sebelum)*($key->tarif/100);//nilai pkp dikurangi nilai teratas
                $nilai_pajak[$lapisan_count]=($hitung_pajak-$nilai_sebelum)*($key->tarif/100);
              }
            }
            $nilai_sebelum=$key->lapisan;
            $lapisan_count++;
          }

          $pajak_sebelumnya=pajak::select(DB::raw('IFNULL(SUM(pph_bulanini),0) as total_pph'))
                                  ->whereYear('bulan_pajak',$bulan_pajak[0])
                                  ->where('karyawan_id',$request->karyawan_id)
                                  ->first();
          $pph_bulansebelum=$pajak_sebelumnya->total_pph;
          $pph_belumterbayar=$pajak_setahun-$pph_bulansebelum;
          $pph_bulanini=$pph_belumterbayar/(13-$bulan_pajak[1]);
          //simpan data pajak baru
          $pajak_sekarang=new pajak;
          $pajak_sekarang['karyawan_id']=$request->karyawan_id;
          $pajak_sekarang['bulan_pajak']=$request->bulan_pajak;
          $pajak_sekarang['total_penghasilan']=$penghasilan_kotor;
          $pajak_sekarang['total_pengurang']=$total_pengurang;
          $pajak_sekarang['penghasilan_netto']=$penghasilan_neto;
          $pajak_sekarang['total_ptkp']=$total_ptkp;
          $pajak_sekarang['pph_bulansebelum']=$pph_bulansebelum;
          $pajak_sekarang['pkp']=$total_pkp;
          $pajak_sekarang['pph_total']=$pajak_setahun;
          $pajak_sekarang['pph_bulanini']=$pph_bulanini;
          $pajak_sekarang->save();
          return 1;
        }

      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=='pilih_karyawan'){
        $karyawan=DB::table('karyawans')->select('npwp','nik','status_pajak as status_nikah','tanggungan','jabatans.nama_jabatan as jabatan','penghasilan.gaji_pokok','penghasilan.bulan_pajak')
                    ->leftjoin('jabatans','karyawans.jabatan_id','=','jabatans.id')
                    ->leftjoin(DB::raw("(select gaji_pokok,bulan_pajak,karyawan_id FROM penghasilans WHERE karyawan_id = ".$id." ORDER BY bulan_pajak DESC LIMIT 0,1) penghasilan"), function ($join) {
                          $join->on('karyawans.id', '=', 'penghasilan.karyawan_id');
                      })
                    ->where('karyawans.id','=',$id)
                    ->first();
        $sekarang="";
        if($karyawan->bulan_pajak==NULL){
          $bulan=date('Y-m');
          $sekarang=$bulan.'-01';
        }else{
          $sekarang=date("Y-m-d", strtotime("+1 month", strtotime($karyawan->bulan_pajak)));
        }
        $karyawan->bulan_pajak=$sekarang;
        return json_encode($karyawan);
      }

      if($request->aksi=='pilih_ptkp'){
        $bulan_pajak=explode('-',$request->bulan_pajak);
        $bulan=$bulan_pajak[1];
        $tahun=$bulan_pajak[0];
        if($bulan==1){
          $tahun=$tahun-1;
          $bulan=12;
        }else{
          $bulan=$bulan-1;
        }

        $ptkp=DB::table('data_ptkps')
                      ->select('data_ptkps.*','nilai.qty')
                      ->leftjoin(DB::raw("(SELECT qty,data_ptkp_id FROM nilai_ptkps WHERE YEAR(bulan_pajak)='".$tahun."' AND MONTH(bulan_pajak)='".$bulan."' AND karyawan_id = ".$request->karyawan_id.") nilai"), function ($join) {
                            $join->on('data_ptkps.id', '=', 'nilai.data_ptkp_id');
                        })
                      ->get();
      foreach ($ptkp as $key) {
        $data['ptkp'.$key->id]=$key->qty;
      }

      return json_encode($data);

      }

      if($request->aksi=='hitung_biaya'){
            $tahun=explode('-',$request->bulan_pajak);
            $penghasilan=DB::table('penghasilans')
                        ->addSelect(DB::raw('IFNULL(SUM(penghasilans.gaji_pokok),0) as total_gaji'))
                        ->addSelect(DB::raw('IFNULL(SUM(penghasilans.adjustmen_gaji),0) as total_adjustmen_gaji'))
                        ->addSelect(DB::raw('IFNULL(SUM(penghasilans.tunjangan_makan),0) as total_tunjangan_makan'))
                        ->addSelect(DB::raw('IFNULL(SUM(penghasilans.insentif),0) as total_insentif'))
                        ->addSelect(DB::raw('IFNULL(SUM(penghasilans.bonus),0) as total_bonus'))
                        ->addSelect(DB::raw('IFNULL(SUM(penghasilans.lembur),0) as total_lembur'))
                        ->addSelect(DB::raw('IFNULL(SUM(penghasilans.uang_dinas),0) as total_uang_dinas'))
                        ->addSelect(DB::raw('IFNULL(SUM(penghasilans.thr),0) as total_thr'))
                        ->addSelect(DB::raw('IFNULL(SUM(penghasilans.jkk),0) as total_jkk'))
                        ->addSelect(DB::raw('IFNULL(SUM(penghasilans.jk),0) as total_jk'))
                        ->where('karyawan_id',$id)
                        ->whereYear('bulan_pajak',$tahun[0])
                        ->first();
          $bulan_kedepan=12-$tahun['1'];//total gaji tahun sebelumnya ditambah tahun berjalan
          $total_penghasilan=$penghasilan->total_gaji+($request->gaji_pokok*$bulan_kedepan);
          $total_penghasilan+=$penghasilan->total_adjustmen_gaji+$request->adjustmen_gaji;
          $total_penghasilan+=$penghasilan->total_tunjangan_makan+$request->tunjangan_makan;
          $total_penghasilan+=$penghasilan->total_insentif+$request->insentif;
          $total_penghasilan+=$penghasilan->total_bonus+$request->bonus;
          $total_penghasilan+=$penghasilan->total_lembur+$request->lembur;
          $total_penghasilan+=$penghasilan->total_uang_dinas+$request->uang_dinas;
          $total_penghasilan+=$penghasilan->total_thr+$request->thr;
          $total_penghasilan+=$penghasilan->total_jkk+$penghasilan->total_jk+$request->jkk+$request->jk;
          //jika 5% total penghasilan pertahun kurang dari 6juta maka biayanya 5% kalo lebih besar maksimal 6jt

          $persen5=$request->gaji_pokok*0.05;
          $biaya_jabatan=0;
          if($persen5 > 500000 ){
            $biaya_jabatan=500000;
          }else{
            $biaya_jabatan=$persen5;
          }

          $data['biaya_jabatan']=$biaya_jabatan;
          $data['total_penghasilan']=$total_penghasilan;
          return json_encode($data);
        }

        //list alll

        if($request->aksi=="list-all"){
            $awal=$request->start;
            $banyak=$request->length;
            $banyak_colom=$request->iColumns;
            $kata_kunci_global=$request->sSearch;
            $echo=$request->draw;
            $kata_kunci=$request->search['value'];
            $tahun=$request->tahun;

            if( ($banyak<0) AND ($kata_kunci != "") ){
                $keyword='%'.$kata_kunci.'%';
                $item=pajak::select('pajaks.*','karyawans.nama_karyawan')
                      ->leftjoin('karyawans','pajaks.karyawan_id','=','karyawans.id')
                      ->where('karyawans.nama_karyawan','like',$keyword);
                      if($tahun!=''){
                          $item=$item->whereYear('pajaks.bulan_pajak', $tahun);
                      }
                $item=$item->orderBy('id','desc')->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            }else if($kata_kunci != "" ) {
                $keyword='%'.$kata_kunci.'%';
                $item=pajak::select('pajaks.*','karyawans.nama_karyawan')
                      ->leftjoin('karyawans','pajaks.karyawan_id','=','karyawans.id')
                      ->where('karyawans.nama_karyawan','like',$keyword);
                      if($tahun!=''){
                          $item=$item->whereYear('pajaks.bulan_pajak', $tahun);
                      }
                $item=$item->orderBy('id','desc')->take($banyak)->skip($awal)->get();
                $total=pajak::select('pajaks.*','karyawans.nama_karyawan')
                      ->leftjoin('karyawans','pajaks.karyawan_id','=','karyawans.id')
                      ->where('karyawans.nama_karyawan','like',$keyword);
                      if($tahun!=''){
                          $item=$item->whereYear('pajaks.bulan_pajak', $tahun);
                      }
                $total=$total->get();
                $data['recordsTotal']=count($total);
                $data['recordsFiltered']=count($total);

            } else if($banyak<0) {
                $item=pajak::select('pajaks.*','karyawans.nama_karyawan')
                      ->leftjoin('karyawans','pajaks.karyawan_id','=','karyawans.id');
                      if($tahun!=''){
                          $item=$item->whereYear('pajaks.bulan_pajak', $tahun);
                      }
                $item=$item->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            } else {
              $item=pajak::select('pajaks.*','karyawans.nama_karyawan')
                    ->leftjoin('karyawans','pajaks.karyawan_id','=','karyawans.id');
                    if($tahun!=''){
                        $item=$item->whereYear('pajaks.bulan_pajak', $tahun);
                    }
                $item=$item->skip($awal)->take($banyak)->orderBy('id','desc')->get();
                $total=pajak::select('pajaks.*','karyawans.nama_karyawan')
                      ->leftjoin('karyawans','pajaks.karyawan_id','=','karyawans.id');
                      if($tahun!=''){
                          $item=$item->whereYear('pajaks.bulan_pajak', $tahun);
                      }
                $total=$total->get();
                $data['recordsTotal']=count($total);
                $data['recordsFiltered']=count($total);
            }

            $gh_x=$awal+1;
            $item->each(function($item) use (&$gh_x) {
                $item->setAttribute('nomer',$gh_x++);
                $item->setAttribute('action','<button onclick="hapus_input('."'".url('input-pajak')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
                            <i class="fa fa-trash"></i>
                        </button>');
            });

            $data['draw']=$echo;
            $data['data']=$item;
            return $data;
            /*return json_encode($data);*/
        }



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
