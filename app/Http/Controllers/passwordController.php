<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\karyawan;
use Hash;
use App\Mail\reset;
use Mail;

class passwordController extends Controller
{
    public function reset(Request $request){
      $email=$request->email;
      $cek=karyawan::where('email',$email)->first();
      if(count($cek) > 0){
        $sekarang=date('Y-m-d H:i:s');
        $waktu_reset=date('Y-m-d H:i:s', strtotime($sekarang . ' +1 day'));
        $token_reset=str_random(100);
        //insert token
        $karyawan=karyawan::find($cek->id);
        $karyawan->reset_token=$token_reset;
        $karyawan->waktu_reset=$waktu_reset;
        $karyawan->save();
        //kirim email berisi link reset
        $content = [
              'title' => 'Permohonan reset password '.$karyawan->nama_karyawan,
              'button' => 'Go TO APPS',
              'link'=> '/password/show/'.$token_reset,
              'text' => 'Petunjuk reset password',
              ];
        Mail::to($karyawan->email)->send(new reset($content));
        return 'oke';

      }else{
        return 'kosong';
      }
    }

    public function show($token){
      $data['token']=$token;
      return view('auth/passwords/reset',$data);
    }

    public function ganti(Request $request){
      $email=$request->email;
      $token=$request->token;
      $sekarang=date('Y-m-d H:i:s');
      $password=bcrypt($request->password);
      //cek apakah email,token dan belum expoired ada
      $cek=karyawan::where('email',$email)->where('reset_token',$token)->where('waktu_reset','>',$sekarang)->first();
      if(count($cek) == 0 ){
        return 'kosong';
      }else{
        //update pass
        $user=karyawan::find($cek->id);
        $user->password=$password;
        $user->waktu_reset=date('Y-m-d H:i:s');
        $user->save();
        return 'oke';
      }

    }
}
