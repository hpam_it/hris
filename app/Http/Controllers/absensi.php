<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\jam_kerja;
use App\divisi;
use App\absen;
use App\ambilcuti;
use App\hari_libur;
use App\leave;
use App\data_ijin;
use App\status_karyawan;
use App\elly\rekapabsensi;
use App\karyawan;
class absensi extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['halaman']='absensi';
        $data['divisi']=divisi::all();
        $data['divisi2']=divisi::all();
        $data['status_kerja']=status_karyawan::orderBy('status_karyawan','ASC')->get();
        return view('absensi/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //import log {sampe sini ya, renaca mau bikin import log dengan progressbar ajax}
        if($request->aksi=='import-log'){
          $awal=$request->awal;
          $akhir=$request->akhir;
          $saiki=explode('/',$request->posisi);
          $posisi=$saiki[2]."-".$saiki[0]."-".$saiki[1]." 00:00:00" ;
          $posisi2=$saiki[2]."-".$saiki[0]."-".$saiki[1]." 23:59:59";
          //  $posisi=date_format($saiki, 'Y-m-d');
          $db_username = ''; //username
          $db_password = ''; //password
          //path to database file
          $database_path = $_SERVER['DOCUMENT_ROOT']."/../public/att2000.mdb";
          $database = new \PDO("odbc:DRIVER=MDBTools; DBQ=$database_path; Uid=$db_username; Pwd=$db_password;");
          $baca  = $database->prepare("SELECT COUNT(*) as [totalnya] FROM CHECKINOUT A LEFT JOIN USERINFO B ON A.USERID = B.USERID WHERE A.CHECKTIME > :awal AND A.CHECKTIME < :akhir");
          $baca->execute(array(':awal' => $posisi, ':akhir' => $posisi2));
          //$hitung = $database->query($sql1);
          $jumlah = $baca->fetch();
          $total = $jumlah['totalnya'];
          if($total==0){
            return 0;
          }else{
          $database2 = new \PDO("odbc:DRIVER=MDBTools; DBQ=$database_path; Uid=$db_username; Pwd=$db_password;");
          $baca2 =$database2->prepare("SELECT A.*,B.Badgenumber  FROM CHECKINOUT A LEFT JOIN USERINFO B ON A.USERID = B.USERID WHERE A.CHECKTIME > :posisi AND A.CHECKTIME < :posisi2 "); //cari cara rownum
          $baca2->execute(array(':posisi' => $posisi,':posisi2'=>$posisi2));
          //return json_encode($baca2->fetch());
          while ($log = $baca2->fetch()) {
                $cek=absen::where('userid',$log['USERID'])->where('checktime',$log['CHECKTIME'])->count();
                if($cek==0){
                  $insert=new absen;
                  $insert['userid']=$log['USERID'];
                  $insert['checktime']=$log['CHECKTIME'];
                  $insert->save();
                }
             }
          return $total;
          }

        }
        //simpan jadwal
        if($request->aksi=='simpan-jadwal'){
          $simpan=jam_kerja::create($request->all());
          if($simpan){
            return '1';
          }else{
            return '0';
          }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function rasio_karyawan(Request $request){
      $filter=$request->filter;
      $tahun_sekarang=date('Y');
      $bulan_sekarang=date('m');
      $hari_sekarang=date('d');
      $tanggal_mulai=$request->startdate;
      $tanggal_selesai=$request->enddate;
      IF($tanggal_selesai==NULL){
        $tanggal_selesai=date('Y-m-d');
      }
      $tanggal_awal=$tahun_sekarang.'-01-01';
      $tgl_akhir=$tahun_sekarang.'-'.$bulan_sekarang.'-'.$hari_sekarang;
      $awal=$request->start;
      $banyak=$request->length;
      $banyak_colom=$request->iColumns;
      $kata_kunci_global=$request->sSearch;
      $echo=$request->draw;
      $divisi=$request->divisi;
      $kata_kunci=$request->search['value'];
      $order=$request->order;

      $karyawan1=karyawan::select('karyawans.id','karyawans.tgl_gabung','karyawans.nama_karyawan','karyawans.status_kerja','karyawans.tgl_gabung','karyawans.npp','divisis.nama_divisi','kantors.nama_kantor','jabatans.nama_jabatan')
                          ->leftjoin('divisis','karyawans.divisi_id','=','divisis.id')
                          ->leftjoin('kantors','karyawans.kantor_id','=','kantors.id')
                          ->leftjoin('jabatans','karyawans.jabatan_id','=','jabatans.id')
                          ->where('status_kerja',$filter)
                          ->where('karyawans.tgl_gabung','<=',$tanggal_selesai);

      $karyawan2=karyawan::select('karyawans.id','karyawans.tgl_gabung','karyawans.nama_karyawan','karyawans.status_kerja','karyawans.tgl_gabung','karyawans.npp','divisis.nama_divisi','kantors.nama_kantor','jabatans.nama_jabatan')
                          ->leftjoin('divisis','karyawans.divisi_id','=','divisis.id')
                          ->leftjoin('kantors','karyawans.kantor_id','=','kantors.id')
                          ->leftjoin('jabatans','karyawans.jabatan_id','=','jabatans.id')
                          ->where('status_kerja',$filter)
                          ->where('karyawans.tgl_gabung','<=',$tanggal_selesai);
      if($divisi!='all'){
        $karyawan1=$karyawan1->where('karyawans.divisi_id',$divisi);
        $karyawan2=$karyawan2->where('karyawans.divisi_id',$divisi);
      }
      if( ($banyak<0) AND ($kata_kunci != "") ){
            $keyword='%'.$kata_kunci.'%';
            $karyawan=$karyawan1->where(function($query) use ($keyword) {
                    $query->where('nama_karyawan','like',$keyword)
                          ->orWhere('npp','like',$keyword);
                    })->orderBy('nama_karyawan','asc')->get();
            $data['recordsTotal']=count($karyawan);
            $data['recordsFiltered']=count($karyawan);
      }else if($kata_kunci != "" ) {
            $keyword='%'.$kata_kunci.'%';
            $karyawan=$karyawan1->where(function($query) use ($keyword) {
                    $query->where('nama_karyawan','like',$keyword)
                          ->orWhere('npp','like',$keyword);
                    })
                    ->orderBy('nama_karyawan','asc')
                    ->skip($awal)
                    ->take($banyak)
                    ->get();
            $total=$karyawan2->where(function($query) use ($keyword) {
                    $query->where('nama_karyawan','like',$keyword)
                          ->orWhere('npp','like',$keyword);
                    })->get();
            $data['recordsTotal']=count($total);
            $data['recordsFiltered']=count($total);

      }else if($banyak<0) {
            $karyawan=$karyawan1->get();
            $data['recordsTotal']=count($karyawan);
            $data['recordsFiltered']=count($karyawan);
      }else {
            $karyawan=$karyawan1->skip($awal)->take($banyak)->orderBy('nama_karyawan','asc')->get();
            $total=$karyawan2->get();
            $data['recordsTotal']=count($total);
            $data['recordsFiltered']=count($total);
      }


      $hasil=[];

      //loop setiap karyawan
      if(count($karyawan) > 0) {
        foreach ($karyawan as $key) {
          if($tanggal_mulai=='' AND $tanggal_selesai==''){
            if($tanggal_awal<$key->tgl_gabung){
                $tgl_awal=$key->tgl_gabung;
              }else{
                $tgl_awal=$tanggal_awal;
              }
            $abs=new rekapabsensi;
            $absensi=$abs->rasio_absen($key->id,$tgl_awal,$tgl_akhir, 'absensi');
          }else{
            if($tanggal_mulai<$key->tgl_gabung){
                $tgl_awal=$key->tgl_gabung;
              }else{
                $tgl_awal=$tanggal_mulai;
              }
            $abs=new rekapabsensi;
            $absensi=$abs->rasio_absen($key->id,$tgl_awal,$tanggal_selesai, 'absensi');
          }

          $absensi['nama_karyawan']=$key->nama_karyawan;
          $absensi['npp']=$key->npp;
          $absensi['nama_kantor']=$key->nama_kantor;
          $absensi['nama_divisi']=$key->nama_divisi;
          $absensi['nama_jabatan']=$key->nama_jabatan;
          array_push($hasil,$absensi);
        }

        foreach ($order as $order) {
          switch ($order['column']) {
            case '0':
                //sort nama
                foreach ($hasil as $key => $row) {
                    $nama_karyawan[$key]  = $row['nama_karyawan'];
                }
                if($order['dir']=='asc'){
                  array_multisort($nama_karyawan,SORT_ASC, $hasil);
                }else{
                  array_multisort($nama_karyawan,SORT_DESC, $hasil);
                }

                break;
            case '1':
                //sort cuti
                foreach ($hasil as $key => $row) {
                    $total_cuti[$key]  = $row['total_cuti'];
                }
                if($order['dir']=='asc'){
                  array_multisort($total_cuti,SORT_ASC, $hasil);
                }else{
                  array_multisort($total_cuti,SORT_DESC, $hasil);
                }
                break;
            case '2':
                //sort telat
                usort($hasil, function($a, $b) {
                  return $a['hari_cepat'] <=> $b['hari_cepat'];
                });
                break;
            case '3':
                //mangkir
                usort($hasil, function($a, $b) {
                  return $a['total_mangkir'] <=> $b['total_mangkir'];
                });
                break;
            case '4':
                //rasio kerja
                usort($hasil, function($a, $b) {
                  return $a['persentase_kerja_hari'] <=> $b['persentase_kerja_hari'];
                });
                break;
            case '5':
                //rasio hari
                usort($hasil, function($a, $b) {
                  return $a['persentase_kerja'] <=> $b['persentase_kerja'];
                });
                break;
            default:
                usort($hasil, function($a, $b) {
                  return $a['nama_karyawan'] <=> $b['nama_karyawan'];
                });
                break;
          }
        }

      }

        $data['draw']=$echo;
        $data['data']=$hasil;
        return $data;
    }

    public function show(Request $request,$id)
    {

      if($request->aksi=='list-absen'){
        $awal=$request->start;
        $banyak=$request->length;
        $banyak_colom=$request->iColumns;
        $kata_kunci_global=$request->sSearch;
        $echo=$request->draw;
        $tanggal_mulai=$request->startdate;
        $tanggal_selesai=$request->enddate;
        $tahun_sekarang=date('Y');
        $bulan_sekarang=date('m');
        $hari_sekarang=date('d')+1;
        $tgl_awal=$tahun_sekarang.'-'.$bulan_sekarang.'-01';
        $tgl_akhir=$tahun_sekarang.'-'.$bulan_sekarang.'-'.$hari_sekarang;

        if($tanggal_mulai=='' OR $tanggal_selesai==''){
          $tanggal_mulai=$tgl_awal;
          $tanggal_selesai=$tgl_akhir;
        }
        $divisi=$request->divisi;
        $kata_kunci=$request->search['value'];
        $karya=DB::table('karyawans')->select('id')->where('karyawans.nama_karyawan','like','%'.$kata_kunci.'%')->where('status_kerja','Aktif');
        if($divisi!='all'){
                  $karya=$karya->where('karyawans.divisi_id',$divisi);
          }
        $karya=$karya->get();
        if( ($banyak<0) AND ($kata_kunci != "") ){
            $keyword='%'.$kata_kunci.'%';
            //loop sesuai range tanggal
            if($tanggal_mulai!='' AND $tanggal_selesai!=''){
              $ayo=$tanggal_mulai;
              $absen=[];
              while (strtotime($ayo) <= strtotime($tanggal_selesai)) {
                foreach ($karya as $key) {
                  $cek=DB::table('karyawans')
                          ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                          ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                          ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                          ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                          ->leftjoin('absens','karyawans.userid','=','absens.userid')
                          ->addSelect('karyawans.jamkerjaaktif')
                          ->whereRaw("DATE(absens.checktime) = '$ayo' ")
                          ->where('karyawans.id',$key->id)
			  ->where('karyawans.status_kerja','Aktif')
                          ->orderBy('absens.checktime','DESC')
                          ->groupBy('karyawans.userid')
                          ->groupBy('karyawans.nama_karyawan')
                          ->groupBy('karyawans.npp')
                          ->groupBy('karyawans.jamkerjaaktif')
                          ->groupBy('tanggal')
                          ->first();
                  $ada=count($cek);
                  if($ada>0){
                        array_push($absen,$cek);
                  }else{
                    $karyawan=DB::table('karyawans')
                            ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                            ->where('karyawans.id',$key->id)->where('karyawans.status_kerja','Aktif');
                            if($divisi!='all'){
                                  $karyawan=$karyawan->where('karyawans.divisi_id',$divisi);
                              }
                            $karyawan=$karyawan->first();
                      $karyawan->cek_in='00:00:00';
                      $karyawan->cek_out='00:00:00';
                      $karyawan->tanggal=$ayo;
                      array_push($absen,$karyawan);
                  }
                }

                $ayo = date ("Y-m-d", strtotime("+1 day", strtotime($ayo)));
               }
           }else{
             $absen=DB::table('karyawans')
                     ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                     ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                     ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                     ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                     ->addSelect('karyawans.jamkerjaaktif')
                     ->leftjoin('absens','karyawans.userid','=','absens.userid')
                     ->where('karyawans.nama_karyawan','like',$keyword)
		     ->where('karyawans.status_kerja','Aktif')
                     ->orderBy('absens.checktime','DESC')
                     ->groupBy('karyawans.userid')
                     ->groupBy('karyawans.nama_karyawan')
                     ->groupBy('karyawans.npp')
                     ->groupBy('karyawans.jamkerjaaktif')
                     ->groupBy('tanggal');
                     if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                         $absen=$absen->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                     }else if($tanggal_mulai!=''){
                         $absen=$absen->where('absens.checktime','>=',$tanggal_mulai);
                     }else if($tanggal_selesai!=''){
                         $absen=$absen->where('absens.checktime','<=',$tanggal_selesai);
                     }

                     $absen=$absen->get();
           }


          $data['recordsTotal']=count($absen);
          $data['recordsFiltered']=count($absen);
        }else if($kata_kunci != "" ) {
            $keyword='%'.$kata_kunci.'%';
            $absen=DB::table('karyawans')
                    ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                    ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                    ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                    ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                    ->addSelect('karyawans.jamkerjaaktif')
                    ->leftjoin('absens','karyawans.userid','=','absens.userid')
                    ->where('karyawans.nama_karyawan','like',$keyword)
		    ->where('karyawans.status_kerja','Aktif')
                    ->skip($awal)
                    ->take($banyak)
                    ->orderBy('absens.checktime','DESC')
                    ->groupBy('karyawans.userid')
                    ->groupBy('karyawans.nama_karyawan')
                    ->groupBy('karyawans.npp')
                    ->groupBy('tanggal')
                    ->orderBy('karyawans.userid')
                    ->orderBy('tanggal','ASC');
                    if($divisi!='all'){
                            $absen=$absen->where('karyawans.divisi_id',$divisi);
                    }

                    if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                        $absen=$absen->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                    }else if($tanggal_mulai!=''){
                        $absen=$absen->where('absens.checktime','>=',$tanggal_mulai);
                    }else if($tanggal_selesai!=''){
                        $absen=$absen->where('absens.checktime','<=',$tanggal_selesai);
                    }

                    $absen=$absen->get();
            $total=DB::table('karyawans')
                    ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                    ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                    ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                    ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                    ->addSelect('karyawans.jamkerjaaktif')
                    ->leftjoin('absens','karyawans.userid','=','absens.userid')
                    ->where('karyawans.nama_karyawan','like',$keyword)
		    ->where('karyawans.status_kerja','Aktif')
                    ->orderBy('absens.checktime','DESC')
                    ->groupBy('karyawans.userid')
                    ->groupBy('karyawans.nama_karyawan')
                    ->groupBy('karyawans.npp')
                    ->groupBy('tanggal')
                    ->orderBy('karyawans.userid')
                    ->orderBy('tanggal','ASC');
                    if($divisi!='all'){
                            $total=$total->where('karyawans.divisi_id',$divisi);
                    }

                    if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                        $total=$total->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                    }else if($tanggal_mulai!=''){
                        $total=$total->where('absens.checktime','>=',$tanggal_mulai);
                    }else if($tanggal_selesai!=''){
                        $total=$total->where('absens.checktime','<=',$tanggal_selesai);
                    }
            $total=$total->get();

            //$absen=$absen->get();
            $data['recordsTotal']=count($total);
            $data['recordsFiltered']=count($total);

        } else if($banyak<0) {
          //loop sesuai range tanggal
          if($tanggal_mulai!='' AND $tanggal_selesai!=''){
            $ayo=$tanggal_mulai;
            $absen=[];
            while (strtotime($ayo) <= strtotime($tanggal_selesai)) {
              foreach ($karya as $key) {
                $cek=DB::table('karyawans')
                        ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                        ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                        ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                        ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                        ->leftjoin('absens','karyawans.userid','=','absens.userid')
                        ->addSelect('karyawans.jamkerjaaktif')
                        ->whereRaw("DATE(absens.checktime) = '$ayo' ")
                        ->where('karyawans.id',$key->id)
			->where('karyawans.status_kerja','Aktif')
                        ->orderBy('absens.checktime','DESC')
                        ->groupBy('karyawans.userid')
                        ->groupBy('karyawans.nama_karyawan')
                        ->groupBy('karyawans.npp')
                        ->groupBy('karyawans.jamkerjaaktif')
                        ->groupBy('tanggal')
                        ->first();
                $ada=count($cek);
                if($ada>0){
                      array_push($absen,$cek);
                }else{
                  $karyawan=DB::table('karyawans')
                          ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                          ->where('karyawans.id',$key->id);
                          if($divisi!='all'){
                                $karyawan=$karyawan->where('karyawans.divisi_id',$divisi);
                            }
                          $karyawan=$karyawan->first();
                    $karyawan->cek_in='00:00:00';
                    $karyawan->cek_out='00:00:00';
                    $karyawan->tanggal=$ayo;
                    array_push($absen,$karyawan);
                }
              }

              $ayo = date ("Y-m-d", strtotime("+1 day", strtotime($ayo)));
             }
         }else{
           $absen=DB::table('karyawans')
                   ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                   ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                   ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                   ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                   ->addSelect('karyawans.jamkerjaaktif')
                   ->leftjoin('absens','karyawans.userid','=','absens.userid')
		   ->where('karyawans.status_kerja','Aktif')
                   ->orderBy('absens.checktime','DESC')
                   ->groupBy('karyawans.userid')
                   ->groupBy('karyawans.nama_karyawan')
                   ->groupBy('karyawans.npp')
                   ->groupBy('karyawans.jamkerjaaktif')
                   ->groupBy('tanggal');
                   if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                       $absen=$absen->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                   }else if($tanggal_mulai!=''){
                       $absen=$absen->where('absens.checktime','>=',$tanggal_mulai);
                   }else if($tanggal_selesai!=''){
                       $absen=$absen->where('absens.checktime','<=',$tanggal_selesai);
                   }

                   $absen=$absen->get();
         }


        $data['recordsTotal']=count($absen);
        $data['recordsFiltered']=count($absen);

        } else {
          $absen=DB::table('karyawans')
                  ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                  ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                  ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                  ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                  ->addSelect('karyawans.jamkerjaaktif')
                  ->leftjoin('absens','karyawans.userid','=','absens.userid')
		  ->where('karyawans.status_kerja','Aktif')
                  ->orderBy('absens.checktime','DESC')
                  ->groupBy('karyawans.userid')
                  ->groupBy('karyawans.nama_karyawan')
                  ->groupBy('karyawans.npp')
                  ->groupBy('tanggal')
                  ->orderBy('karyawans.userid','ASC')
                  ->skip($awal)
                  ->take($banyak);
                  //->orderBy('tanggal','ASC');
                  if($divisi!='all'){
                          $absen=$absen->where('karyawans.divisi_id',$divisi);
                  }

                  if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                      $absen=$absen->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                  }else if($tanggal_mulai!=''){
                      $absen=$absen->where('absens.checktime','>=',$tanggal_mulai);
                  }else if($tanggal_selesai!=''){
                      $absen=$absen->where('absens.checktime','<=',$tanggal_selesai);
                  }

                  $absen=$absen->get();

            $total=DB::table('karyawans')
                    ->select('karyawans.nama_karyawan','karyawans.npp','karyawans.id')
                    ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                    ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                    ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                    ->addSelect('karyawans.jamkerjaaktif')
                    ->leftjoin('absens','karyawans.userid','=','absens.userid')
		    ->where('karyawans.status_kerja','Aktif')
                    ->orderBy('absens.checktime','DESC')
                    ->groupBy('karyawans.userid')
                    ->groupBy('karyawans.nama_karyawan')
                    ->groupBy('karyawans.npp')
                    ->groupBy('tanggal')
                    ->orderBy('karyawans.userid');
                    //->orderBy('tanggal','ASC');
                    if($divisi!='all'){
                            $total=$total->where('karyawans.divisi_id',$divisi);
                    }

                    if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                        $total=$total->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                    }else if($tanggal_mulai!=''){
                        $total=$total->where('absens.checktime','>=',$tanggal_mulai);
                    }else if($tanggal_selesai!=''){
                        $total=$total->where('absens.checktime','<=',$tanggal_selesai);
                    }
                    $total=$total->get();
              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);
        }
        if(count($absen) > 0){
        foreach ($absen as $key) {
          $libur=hari_libur::where('tanggal',$key->tanggal)->first();
          //disini cari hari kerjanya, apakah minggu atau hari libur
          $timestamp = strtotime($key->tanggal);
          //cek apakah ada cuti di tanggal ini
          $cek_cuti=ambilcuti::where('karyawan_id',$key->id)
                    ->where('tanggal_cuti','like','%'.$key->tanggal.'%')
                    ->where('statusatasan','=','Diterima')
                    ->where('statushrd','=','Diterima')
                    ->where(function($query){
                      $query->where('status_direksi','NOT LIKE','%pending%')
                      ->where('status_direksi','NOT LIKE','%Ditolak%')
                      ->orWhereNull('status_direksi');
                    })
                    ->first();
          $cek_ijin=leave::select('leaves.*','data_ijins.nama_ijin','data_ijins.hitung')
                    ->where('leaves.karyawan_id',$key->id)
                    ->where('leaves.tanggal_ijin',$key->tanggal)
                    ->where('leaves.statusatasan','Diterima')
                    ->leftjoin('data_ijins','leaves.data_ijin_id','=','data_ijins.id')
                    ->get();
          $dt_kar=DB::table('karyawans')->where('id',$key->id)->first();
          $jam_aktif=$dt_kar->jamkerjaaktif;
          if($jam_aktif!=''){
            //ada
            $jam_aktifnya = array_map('intval', explode(',', $jam_aktif));
            $jamkerja=DB::table('jam_kerjas')
                     ->where('tgl_mulai','<=',$key->tanggal)
                     ->where('tgl_selesai','>=',$key->tanggal)
                     ->whereIn('id', $jam_aktifnya)
                     ->orderBy('tgl_mulai','DESC')
                     ->first();

          }else{
            $jamkerja=DB::table('jam_kerjas')
                      ->where('tgl_mulai','<=',$key->tanggal)
                      ->where('tgl_selesai','>=',$key->tanggal)
                      ->orderBy('tgl_mulai','DESC')
                      ->first();
          }


          $iscuti=count($cek_cuti);
          $isijin=count($cek_ijin);
          $ijin_mulai;
          $ijin_selesai;
          $ijinn=[];
          if($isijin>0){
            $se=0;
            foreach ($cek_ijin as $ijine) {
              $ijin_mulai=$ijine['jam_ijin'];
              $ijin_selesai=$ijine['sampai_dengan'];
              $ijinn[$se]=$ijine['nama_ijin'].':'.$ijine['alasan'].':'.$ijin_mulai.' - '.$ijin_selesai;

              //hitung durasi ijin
              $c_in = new \Datetime($key->cek_in);
              $c_out = new \Datetime($key->cek_out);
              $c_mulai = new \Datetime($key->tanggal.' '.$ijin_mulai);
              $c_selesai = new \Datetime($key->tanggal.' '.$ijin_selesai);

              $j_istirahat = new \DateTime($key->tanggal.' '.$jamkerja->jam_istirahat);
              $m_pulang = new \DateTime($key->tanggal.' '.$jamkerja->maxpulang);
              $b_reak=explode(':',$jamkerja->istirahat);
              $_istirahat = ($b_reak[0]*3600) + ($b_reak[1]*60) + $b_reak[2];
              $_cek_in=new \DateTime($key->cek_in);
              $_masuk = new \DateTime($key->tanggal.' '.$jamkerja->masuk);
              $_pulang = new \DateTime($key->tanggal.' '.$jamkerja->pulang);
              $_toleransi = new \DateTime($key->tanggal.' '.$jamkerja->toleransi);
              $_norm=explode(':',$jamkerja->jam_normal);
              $_normal = ($_norm[0]*3600) + ($_norm[1]*60) + $_norm[2];
              if( ($c_mulai > $_masuk) AND ($c_mulai < $_toleransi) ){
                $dinas=$c_mulai->diff($c_selesai);
              }else if($c_mulai <= $_masuk ){
                $dinas=$_masuk->diff($c_selesai);
              }else if($c_mulai > $_toleransi AND $c_selesai > $m_pulang ){
                $dinas=$c_mulai->diff($m_pulang);
              }else if($c_mulai > $_toleransi){
                $dinas=$c_mulai->diff($c_selesai);
              }


              //jika ijinnya termasuk hitung jam kerja
              if($ijine['hitung']==1){
                if($c_mulai < $c_in){
                    $key->cek_in=$key->tanggal." ".$ijin_mulai;
                  }
                if($c_selesai > $c_out){
                    $key->cek_out=$key->tanggal." ".$ijin_selesai;
                  }
              }else{
              }
              $se++;
            }
          }
          $day = date('w', $timestamp);
          $hari='';
          switch ($day) {
            case '0':$hari='Minggu';
              break;
            case '1':$hari='Senin';
              break;
            case '2':$hari='Selasa';
              break;
            case '3':$hari='Rabu';
              break;
            case '4':$hari='Kamis';
              break;
            case '5':$hari='Jumat';
              break;
            case '6':$hari='Sabtu';
              break;

          }
          if( $key->cek_in=='00:00:00' AND $key->cek_out=='00:00:00' ){
            $jumlah_cuti=0;

            if($iscuti>0){
              $key->jam_kerja='00:00:00';
            }else if($isijin>0){
              $key->jam_kerja='00:00:00';
            }else{
              if(count($libur) > 0){
                $key->jam_kerja='00:00:00';
              }else{
                if($hari=='Minggu' || $hari=='Sabtu'){
                  $key->jam_kerja='00:00:00';
                }else{
                  $key->jam_kerja='00:00:00';
                }

              }
            }


            $key->cek_in='';
            $key->cek_out='';
            $key->masuk='';
            $key->pulang='';
            $key->istirahat='';
            $key->jamnormal='';
            $key->toleransi='';
            $key->maxpulang='';
            $key->hari=$hari;
          }elseif($key->tanggal!=null){
        //  $jams=$key->jamkerjaaktif;
          // $jamkerja=DB::table('jam_kerjas')->whereIn('id',[$jams])
          //                     ->whereRaw("`tgl_mulai` <= '$key->tanggal' and `tgl_selesai` >= '$key->tanggal'")
          //                     ->orderBy('tgl_mulai','DESC')
          //                     ->first();

          $key->masuk=$jamkerja->masuk;
          $key->pulang=$jamkerja->pulang;
          $key->istirahat=$jamkerja->istirahat;
          $key->jamnormal=$jamkerja->jam_normal;
          $key->toleransi=$jamkerja->toleransi;
          $key->maxpulang=$jamkerja->maxpulang;
          $timestamp = strtotime($key->tanggal);
          $key->hari=$hari;
          $masuk = new \DateTime($key->tanggal.' '.$key->masuk);
          $pulang = new \DateTime($key->tanggal.' '.$key->pulang);
          $jam_istirahat = new \DateTime($key->tanggal.' '.$jamkerja->jam_istirahat);
          $toleransi = new \DateTime($key->tanggal.' '.$key->toleransi);
          $maxpulang = new \DateTime($key->tanggal.' '.$key->maxpulang);
          $cek_in = new \Datetime($key->cek_in);
          $cek_out = new \Datetime($key->cek_out);
          //convert istirahat ke detik
          $break=explode(':',$key->istirahat);
          $istirahat = ($break[0]*3600) + ($break[1]*60) + $break[2];
          $norm=explode(':',$key->jamnormal);
          $normal = ($norm[0]*3600) + ($norm[1]*60) + $norm[2];
          if( ($cek_in > $masuk) AND ($cek_in <= $toleransi) ){
            $selisih=$cek_in->diff($cek_out);
            $kerja=$selisih->format('%H:%I:%S');
          }else if($cek_in <= $masuk AND $cek_out >= $masuk ){
            $selisih=$masuk->diff($cek_out);
            $kerja=$selisih->format('%H:%I:%S');
          }else if($cek_in > $toleransi AND $cek_out > $maxpulang ){
            $selisih=$cek_in->diff($maxpulang);
            $kerja=$selisih->format('%H:%I:%S');
          }else if($cek_in > $toleransi){
            $selisih=$cek_in->diff($cek_out);
            $kerja=$selisih->format('%H:%I:%S');
          }else{
            $kerja='00:00:00';
          }
          $krj=explode(':',$kerja);
          $detikkerja=($krj[0]*3600) + ($krj[1]*60) + $krj[2];
          if(($cek_in > $jam_istirahat) OR ($cek_out < $jam_istirahat)){
            $totalkerja=$detikkerja;
          }else{
            $totalkerja=$detikkerja - $istirahat;
          }

          if($totalkerja >= $normal ){
            $kerjanya=$normal;
          }elseif($totalkerja < 0){
            $kerjanya=$detikkerja;
          }else{
            $kerjanya=$totalkerja;
          }
          $key->jam_kerja=gmdate("H:i:s", $kerjanya);
          if($hari=='Minggu' || $hari=='Sabtu'){
            $key->jam_kerja='00:00:00';
          }else{

          }

          $key->cek_in=$cek_in->format('H:i:s');
          $key->cek_out=$cek_out->format('H:i:s');
        }else{
          $key->jam_kerja='';
          $key->cek_in='';
          $key->cek_out='';
          $key->masuk='';
          $key->pulang='';
          $key->istirahat='';
          $key->jamnormal='';
          $key->toleransi='';
          $key->maxpulang='';
        }
        //cek kalender libur nasional
        if(count($libur) > 0){
          $key->keterangan=$libur->keterangan;
        }else{
          if($hari=='Minggu' || $hari=='Sabtu'){
            $key->keterangan='Week End';
          }else{
            if($isijin>0){
              $key->keterangan=implode(',',$ijinn);
            }else if($iscuti>0){
              $key->keterangan='Cuti';
            }else{
              $key->keterangan='Normal';
            }

          }

        }
      }//end foreach


      }
        $data['data']=$absen;
        $data['draw']=$echo;
        return json_encode($data);

      }

      //list jadwal
      if($request->aksi=='list-all'){
        $awal=$request->start;
        $banyak=$request->length;
        $banyak_colom=$request->iColumns;
        $kata_kunci_global=$request->sSearch;
        $echo=$request->draw;
        $kata_kunci=$request->search['value'];
        if( ($banyak<0) AND ($kata_kunci != "") ){
            $keyword='%'.$kata_kunci.'%';
            $item=jam_kerja::where('nama_jamkerja','like',$keyword)->orderBy('id','desc')->get();
            $data['recordsTotal']=count($item);
            $data['recordsFiltered']=count($item);
        }else if($kata_kunci != "" ) {
            $keyword='%'.$kata_kunci.'%';
            $item=jam_kerja::where('nama_jamkerja','like',$keyword)->skip($awal)->take($banyak)->orderBy('id','desc')->get();
            $total=jam_kerja::where('nama_jamkerja','like',$keyword)->count();
            $data['recordsTotal']=$total;
            $data['recordsFiltered']=$total;

        } else if($banyak<0) {
            $item=jam_kerja::orderBy('id','desc')->get();
            $data['recordsTotal']=count($item);
            $data['recordsFiltered']=count($item);
        } else {
            $item=jam_kerja::skip($awal)->take($banyak)->orderBy('id','desc')->get();
            $total=jam_kerja::count();
            $data['recordsTotal']=$total;
            $data['recordsFiltered']=$total;
        }

        $gh_x=$awal+1;
        $item->each(function($item) use (&$gh_x) {
            $item->setAttribute('nomer',$gh_x++);
            $item->setAttribute('action','<a href="'.url("absensi").'/'.$item->id.'/edit/?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                        <i class="fa fa-edit"></i>
                    </a>
                    ');
        });

        $data['draw']=$echo;
        $data['data']=$item;
        return $data;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        if($request->aksi=='edit'){
          $data['jadwal']=jam_kerja::find($id);
          return view('absensi/edit_jadwal',$data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->aksi=='update_jadwal'){
          $update=jam_kerja::find($id)->update($request->all());
          return 1;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
