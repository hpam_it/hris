<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\jam_kerja;
use App\divisi;
use App\absen;
use App\ambilcuti;
use App\hari_libur;
use App\leave;
use App\data_ijin;
use Auth;
class logabsen extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['halaman']='logabsen';
      $id_karyawan=Auth::user()->id;
      $karyawan=DB::table('karyawans')
              ->select('karyawans.nama_karyawan','karyawans.npp')
              ->where('id',$id_karyawan)
              ->first();
      $data['nama_karyawan']=$karyawan->nama_karyawan;
      $data['npp']=$karyawan->npp;
      return view('log_absen/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=='json'){
        $tanggal_mulai=$request->start;
        $tanggal_selesai=$request->end;
        $ayo=$tanggal_mulai;
        $id_karyawan=Auth::user()->id;
        $absen=[];
        while (strtotime($ayo) <= strtotime($tanggal_selesai)) {
          $cek=DB::table('karyawans')
                  ->select('karyawans.nama_karyawan','karyawans.npp')
                  ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                  ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                  ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                  ->leftjoin('absens','karyawans.userid','=','absens.userid')
                  ->addSelect('karyawans.jamkerjaaktif')
                  ->where('karyawans.id',$id_karyawan)
                  ->whereRaw("DATE(absens.checktime) = '$ayo' ")
                  ->orderBy('absens.checktime','DESC')
                  ->groupBy('karyawans.userid')
                  ->groupBy('karyawans.nama_karyawan')
                  ->groupBy('karyawans.npp')
                  ->groupBy('karyawans.jamkerjaaktif')
                  ->groupBy('tanggal')
                  ->first();
          $ada=count($cek);
          if($ada>0){
              array_push($absen,$cek);
          }else{
            $karyawan=DB::table('karyawans')
                    ->select('karyawans.nama_karyawan','karyawans.npp')
                    ->where('karyawans.id',$id_karyawan)
                    ->first();
            $karyawan->cek_in='00:00:00';
            $karyawan->cek_out='00:00:00';
            $karyawan->tanggal=$ayo;
            array_push($absen,$karyawan);
          }

          $ayo = date ("Y-m-d", strtotime("+1 day", strtotime($ayo)));
         }

        if(count($absen) > 0){
	$dinasdanvisit = [4,10];
        foreach ($absen as $key) {
          //cek apakah ada cuti di tanggal ini
          $cek_cuti=ambilcuti::select('c.nama_cuti','ambilcutis.*')->where('karyawan_id',$id_karyawan)
		    ->leftJoin('data_cutis as c','c.id','ambilcutis.data_cuti_id')
                    ->where('tanggal_cuti','like','%'.$key->tanggal.'%')
                    ->where('statusatasan','=','Diterima')
                    ->where('statushrd','=','Diterima')
                    ->where(function($query){
                      $query->where('status_direksi','NOT LIKE','%pending%')
                      ->where('status_direksi','NOT LIKE','%Ditolak%')
                      ->orWhereNull('status_direksi');
                    })

                    ->first();
          $isnot_dinas_visit=leave::where('karyawan_id',$id_karyawan)
                    ->select('leaves.*','data_ijins.nama_ijin','data_ijins.hitung')
                    ->leftjoin('data_ijins','leaves.data_ijin_id','=','data_ijins.id')
                    ->where('tanggal_ijin',$key->tanggal)
                    ->where('statusatasan','Diterima')
                    ->whereNotIn('leaves.data_ijin_id',$dinasdanvisit)
                    //->where('statushrd','Diterima')
                    ->count();

	  $cek_ijin=leave::where('karyawan_id',$id_karyawan)
                    ->select('leaves.*','data_ijins.nama_ijin','data_ijins.hitung')
                    ->leftjoin('data_ijins','leaves.data_ijin_id','=','data_ijins.id')
                    ->where('tanggal_ijin',$key->tanggal)
                    ->where('statusatasan','Diterima')
		   // ->whereNotIn('leaves.data_ijin_id',$dinasdanvisit)
                    //->where('statushrd','Diterima')
                    ->get();

          $iscuti=count($cek_cuti);
          $isijin=count($cek_ijin);
          if( $key->cek_in=='00:00:00' AND $key->cek_out=='00:00:00' ){
            if($iscuti>0){
              $key->jam_kerja='cuti';
              $key->title="Cuti \n".$cek_cuti->nama_cuti;
              $key->start=$key->tanggal;
             // $key->backgroundColor='#fcdf07'; 
	      $key->backgroundColor='#f7c0c4'; //cuti tanpa absen abu abu
            }else if($isijin>0){
              $key->jam_kerja='ijin';
              $key->title='Izin :';

                foreach ($cek_ijin as $cijin) {
                  $ijinmulai=new \Datetime($cijin->jam_ijin);
                  $ijinselesai=new \Datetime($cijin->sampai_dengan);
                  $key->title.="\n".$cijin->nama_ijin."\n".$ijinmulai->format('H:i')." - ".$ijinselesai->format('H:i');
                  if($cijin->hitung=='1'){

                      $cek_in = $ijinmulai;
                      $cek_out = $ijinselesai;
                  }
                }

              $key->start=$key->tanggal;
              //$key->backgroundColor='#e5bf37';
	      if($isnot_dinas_visit > 0){
	       	$key->backgroundColor='#e6e629'; //izin tanpa absen kuning
	      }else{
		$key->backgroundColor='#2dd613';
	      }
	    }else{
              $key->jam_kerja='';
              if(date('Y-m-d') >= $key->tanggal){
                $key->title='mangkir';
                $key->start=$key->tanggal;
                $key->backgroundColor='#e20f0f'; //mangkir merah
              }else{
                $key->title='';
                $key->start=$key->tanggal;
              }
            }

            //disini cari hari kerjanya, apakah minggu atau hari libur
            $timestamp = strtotime($key->tanggal);
            $day = date('w', $timestamp);
            $hari='';
            switch ($day) {
              case '0':$hari='Minggu';
                break;
              case '1':$hari='Senin';
                break;
              case '2':$hari='Selasa';
                break;
              case '3':$hari='Rabu';
                break;
              case '4':$hari='Kamis';
                break;
              case '5':$hari='Jumat';
                break;
              case '6':$hari='Sabtu';
                break;

            }
            $key->cek_in='';
            $key->cek_out='';
            $key->masuk='';
            $key->pulang='';
            $key->istirahat='';
            $key->jamnormal='';
            $key->toleransi='';
            $key->maxpulang='';
            $key->hari=$hari;
          }elseif($key->tanggal!=null){
          $jams=$key->jamkerjaaktif;
          $tg=$key->tanggal;
          // $jamkerja=DB::table('jam_kerjas')->whereIn('id',[$jams])
          //                     ->where('tgl_mulai','<=',"$key->tanggal")
          //                     ->where('tgl_selesai','>=',"'$key->tanggal'")
          //                     ->orderBy('tgl_mulai','DESC')
          //                     ->first();
          if($jams!=''){
            //ada
            $jam_aktifnya = array_map('intval', explode(',', $jams));
            $jamkerja=DB::table('jam_kerjas')
                      ->where('tgl_mulai','<=',"$key->tanggal")
                      ->where('tgl_selesai','>=',"'$key->tanggal'")
                     ->whereIn('id', $jam_aktifnya)
                     ->orderBy('tgl_mulai','DESC')
                     ->first();

          }else{
            //gak ada
            $jamkerja=DB::table('jam_kerjas')
            ->where('tgl_mulai','<=',"$key->tanggal")
            ->where('tgl_selesai','>=',"'$key->tanggal'")
                     ->orderBy('tgl_mulai','DESC')
                     ->first();
          }


          // $jamkerja=DB::table('jam_kerjas')
          //                     ->where('tgl_mulai','<=',"$key->tanggal")
          //                     ->where('tgl_selesai','>=',"'$key->tanggal'")
          //                     ->orderBy('tgl_mulai','DESC')
          //                     ->first();
          if($jamkerja==null){
            $jamkerja=DB::table('jam_kerjas')->whereIn('id',[$jams])
                                ->orderBy('tgl_mulai','DESC')
                                ->first();
            $key->masuk=$jamkerja->masuk;
            $key->pulang=$jamkerja->pulang;
            $key->istirahat=$jamkerja->istirahat;
            $key->jamnormal=$jamkerja->jam_normal;
            $key->toleransi=$jamkerja->toleransi;
            $key->maxpulang=$jamkerja->maxpulang;
          }else{
            $key->masuk=$jamkerja->masuk;
            $key->pulang=$jamkerja->pulang;
            $key->istirahat=$jamkerja->istirahat;
            $key->jamnormal=$jamkerja->jam_normal;
            $key->toleransi=$jamkerja->toleransi;
            $key->maxpulang=$jamkerja->maxpulang;
          }

          $key->masuk=$jamkerja->masuk;
          $key->pulang=$jamkerja->pulang;
          $key->istirahat=$jamkerja->istirahat;
          $key->jamnormal=$jamkerja->jam_normal;
          $key->toleransi=$jamkerja->toleransi;
          $key->maxpulang=$jamkerja->maxpulang;
          $timestamp = strtotime($key->tanggal);
          $day = date('w', $timestamp);
          $hari='';
          switch ($day) {
            case '0':$hari='Minggu';
              break;
            case '1':$hari='Senin';
              break;
            case '2':$hari='Selasa';
              break;
            case '3':$hari='Rabu';
              break;
            case '4':$hari='Kamis';
              break;
            case '5':$hari='Jumat';
              break;
            case '6':$hari='Sabtu';
              break;

          }
	  $dinas_visit = 0;
          $key->hari=$hari;
          $masuk = new \DateTime($key->tanggal.' '.$key->masuk);
          $pulang = new \DateTime($key->tanggal.' '.$key->pulang);
          $toleransi = new \DateTime($key->tanggal.' '.$key->toleransi);
          $maxpulang = new \DateTime($key->tanggal.' '.$key->maxpulang);
          $cek_in = new \Datetime($key->cek_in);
          $cek_out = new \Datetime($key->cek_out);
          $title="";
          $key->title=$cek_in->format('H:i').' - '.$cek_out->format('H:i');
          /*tambahkan object ijin*/
        $key->title=$cek_in->format('H:i').' - '.$cek_out->format('H:i');
        if($isijin>0){
          foreach ($cek_ijin as $cijin) {
            $ijinmulai=new \Datetime($cijin->jam_ijin);
            $ijinselesai=new \Datetime($cijin->sampai_dengan);
            if($cijin->hitung=='1'){
              if($ijinmulai < $cek_in){
                $cek_in = $ijin_mulai;
              }
              if($ijinselesai > $cek_out){
                $cek_out = $ijinselesai;
              }
            }
	    if($cijin->data_ijin_id == 4 || $cijin->data_ijin_id == 10){
		$dinas_visit = 1;
	    }else{
		$dinas_visit = 0;
	    }
            $key->title.="\n".$cijin->nama_ijin."\n".$ijinmulai->format('H:i')." - ".$ijinselesai->format('H:i');
          }
        }
        /*end objec ijin*/
          //convert istirahat ke detik
          $break=explode(':',$key->istirahat);
          $istirahat = ($break[0]*3600) + ($break[1]*60) + $break[2];
          $norm=explode(':',$key->jamnormal);
          $normal = ($norm[0]*3600) + ($norm[1]*60) + $norm[2];
          if( ($cek_in > $masuk) AND ($cek_in < $toleransi) ){
            $selisih=$cek_in->diff($cek_out);
            $kerja=$selisih->format('%H:%I:%S');
          }else if($cek_in <= $masuk ){
            $selisih=$masuk->diff($cek_out);
            $kerja=$selisih->format('%H:%I:%S');
          }else if($cek_in > $toleransi ){
            $selisih=$cek_in->diff($maxpulang);
            $kerja=$selisih->format('%H:%I:%S');
          }
          $krj=explode(':',$kerja);
          $detikkerja=($krj[0]*3600) + ($krj[1]*60) + $krj[2];
          $totalkerja=$detikkerja - $istirahat;
          if($totalkerja >= $normal AND $cek_in < $toleransi ){
            $kerjanya=$normal;
          }elseif($totalkerja < 0){
            $kerjanya=$detikkerja;
          }else{
            $kerjanya=$totalkerja;
          }
          $key->jam_kerja=gmdate("H:i:s", $kerjanya);
          $key->cek_in=$cek_in->format('H:i:s');
          $key->cek_out=$cek_out->format('H:i:s');

          $key->start=$key->tanggal;
          /* if($key->jam_kerja != $key->jamnormal) {
            $key->backgroundColor='#7d5fff';
          }else{
            $key->backgroundColor='#2dd613';
          } */
	  if($key->jam_kerja < $key->jamnormal && $dinas_visit == 0) {
            $key->backgroundColor='#e6e629'; //jam kerja kurang dari jam normal kuning
          }else{
            $key->backgroundColor='#2dd613'; //jam kerja sesuai atau lebih jam normal hijau 
          }

        }else{
          $key->jam_kerja='';
          $key->cek_in='';
          $key->cek_out='';
          $key->masuk='';
          $key->pulang='';
          $key->istirahat='';
          $key->jamnormal='';
          $key->toleransi='';
          $key->maxpulang='';
          $key->title='Mangkir';
          $key->start=$key->tanggal;
          $key->backgroundColor='#e20f0f'; //mangkir merah
        }
        //cek kalender libur nasional
        $libur=hari_libur::where('tanggal',$key->tanggal)->first();
        if(count($libur) > 0){
          $key->keterangan=$libur->keterangan;
          $key->title=$libur->keterangan;
          $key->start=$key->tanggal;
          $key->backgroundColor='#f7c0c4'; //abu abu hari libur
        }else{
          if($hari=='Minggu' || $hari=='Sabtu'){
            $key->keterangan='Week End';
            $key->title='Week End';
            $key->start=$key->tanggal;
           // $key->backgroundColor='#8e8686';
	    $key->backgroundColor='#f7c0c4'; // abu abu hari libur week end
          }else{
            $key->keterangan='Normal';
          }

        }

      }//end foreach


      }
        $data['data']=$absen;
        return json_encode($absen);
      }

      //list absensi dalam table
      if($request->aksi=='list-absen'){
        //data-absensi
        $id_karyawan=Auth::user()->id;
        $awal=$request->start;
        $banyak=$request->length;
        $banyak_colom=$request->iColumns;
        $kata_kunci_global=$request->sSearch;
        $echo=$request->draw;
        $tanggal_mulai=$request->startdate;

        $tanggal_selesai=$request->enddate;
        $kata_kunci=$request->search['value'];
        if( ($banyak<0) AND ($kata_kunci != "") ){
            $keyword='%'.$kata_kunci.'%';
            $absen=DB::table('karyawans')
                    ->select('karyawans.nama_karyawan','karyawans.npp')
                    ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                    ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                    ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                    ->addSelect('karyawans.jamkerjaaktif')
                    ->leftjoin('absens','karyawans.userid','=','absens.userid')
                    ->where('tanggal','=',$keyword)
                    ->where('karyawans.id',$id_karyawan)
                    ->orderBy('absens.checktime','DESC')
                    ->groupBy('karyawans.userid')
                    ->groupBy('karyawans.nama_karyawan')
                    ->groupBy('karyawans.npp')
                    ->groupBy('karyawans.jamkerjaaktif')
                    ->groupBy('tanggal');
                    if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                        $absen=$absen->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                    }else if($tanggal_mulai!=''){
                        $absen=$absen->where('absens.checktime','>=',$tanggal_mulai);
                    }else if($tanggal_selesai!=''){
                        $absen=$absen->where('absens.checktime','<=',$tanggal_selesai);
                    }

                    $absen=$absen->get();
            $data['recordsTotal']=count($absen);
            $data['recordsFiltered']=count($absen);
        }else if($kata_kunci != "" ) {
            $keyword='%'.$kata_kunci.'%';
            $absen=DB::table('karyawans')
                    ->select('karyawans.nama_karyawan','karyawans.npp')
                    ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                    ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                    ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                    ->addSelect('karyawans.jamkerjaaktif')
                    ->leftjoin('absens','karyawans.userid','=','absens.userid')
                    ->whereRaw("DATE(absens.checktime) LIKE '$keyword'")
                    ->where('karyawans.id',$id_karyawan)
                    ->skip($awal)
                    ->take($banyak)
                    ->orderBy('absens.checktime','DESC')
                    ->groupBy('karyawans.userid')
                    ->groupBy('karyawans.nama_karyawan')
                    ->groupBy('karyawans.npp')
                    ->groupBy('karyawans.jamkerjaaktif')
                    ->groupBy('tanggal');
                    if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                        $absen=$absen->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                    }else if($tanggal_mulai!=''){
                        $absen=$absen->where('absens.checktime','>=',$tanggal_mulai);
                    }else if($tanggal_selesai!=''){
                        $absen=$absen->where('absens.checktime','<=',$tanggal_selesai);
                    }

                    $absen=$absen->get();
            $total=DB::table('karyawans')
                    ->select('karyawans.nama_karyawan','karyawans.npp')
                    ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                    ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                    ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                    ->leftjoin('absens','karyawans.userid','=','absens.userid')
                    ->addSelect('karyawans.jamkerjaaktif')
                    ->whereRaw("DATE(absens.checktime) LIKE '$keyword'")
                    ->where('karyawans.id',$id_karyawan)
                    ->orderBy('absens.checktime','DESC')
                    ->groupBy('karyawans.userid')
                    ->groupBy('karyawans.nama_karyawan')
                    ->groupBy('karyawans.npp')
                    ->groupBy('karyawans.jamkerjaaktif')
                    ->groupBy('tanggal');
                    if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                        $total=$total->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                    }else if($tanggal_mulai!=''){
                        $total=$total->where('absens.checktime','>=',$tanggal_mulai);
                    }else if($tanggal_selesai!=''){
                        $total=$total->where('absens.checktime','<=',$tanggal_selesai);
                    }

                    $total=$total->get();
            $data['recordsTotal']=count($total);
            $data['recordsFiltered']=count($total);

        } else if($banyak<0) {
              //loop sesuai range tanggal
              if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                $ayo=$tanggal_mulai;
                $absen=[];
                while (strtotime($ayo) <= strtotime($tanggal_selesai)) {
                  $cek=DB::table('karyawans')
                          ->select('karyawans.nama_karyawan','karyawans.npp')
                          ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                          ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                          ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                          ->leftjoin('absens','karyawans.userid','=','absens.userid')
                          ->addSelect('karyawans.jamkerjaaktif')
                          ->where('karyawans.id',$id_karyawan)
                          ->whereRaw("DATE(absens.checktime) = '$ayo' ")
                          ->orderBy('absens.checktime','DESC')
                          ->groupBy('karyawans.userid')
                          ->groupBy('karyawans.nama_karyawan')
                          ->groupBy('karyawans.npp')
                          ->groupBy('karyawans.jamkerjaaktif')
                          ->groupBy('tanggal')
                          ->first();
                  $ada=count($cek);
                  if($ada>0){
                      array_push($absen,$cek);
                  }else{
                    $karyawan=DB::table('karyawans')
                            ->select('karyawans.nama_karyawan','karyawans.npp')
                            ->where('karyawans.id',$id_karyawan)
                            ->first();
                    $karyawan->cek_in='00:00:00';
                    $karyawan->cek_out='00:00:00';
                    $karyawan->tanggal=$ayo;
                    array_push($absen,$karyawan);
                  }

                  $ayo = date ("Y-m-d", strtotime("+1 day", strtotime($ayo)));
	               }
             }else{
               $absen=DB::table('karyawans')
                       ->select('karyawans.nama_karyawan','karyawans.npp')
                       ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                       ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                       ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                       ->addSelect('karyawans.jamkerjaaktif')
                       ->leftjoin('absens','karyawans.userid','=','absens.userid')
                       ->where('karyawans.id',$id_karyawan)
                       ->orderBy('absens.checktime','DESC')
                       ->groupBy('karyawans.userid')
                       ->groupBy('karyawans.nama_karyawan')
                       ->groupBy('karyawans.npp')
                       ->groupBy('karyawans.jamkerjaaktif')
                       ->groupBy('tanggal');
                       if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                           $absen=$absen->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                       }else if($tanggal_mulai!=''){
                           $absen=$absen->where('absens.checktime','>=',$tanggal_mulai);
                       }else if($tanggal_selesai!=''){
                           $absen=$absen->where('absens.checktime','<=',$tanggal_selesai);
                       }

                       $absen=$absen->get();
             }


            $data['recordsTotal']=count($absen);
            $data['recordsFiltered']=count($absen);
        } else {
          $absen=DB::table('karyawans')
                  ->select('karyawans.nama_karyawan','karyawans.npp')
                  ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                  ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                  ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                  ->addSelect('karyawans.jamkerjaaktif')
                  ->leftjoin('absens','karyawans.userid','=','absens.userid')
                  ->skip($awal)
                  ->take($banyak)
                  ->where('karyawans.id',$id_karyawan)
                  ->orderBy('absens.checktime','DESC')
                  ->groupBy('karyawans.userid')
                  ->groupBy('karyawans.nama_karyawan')
                  ->groupBy('karyawans.npp')
                  ->groupBy('karyawans.jamkerjaaktif')
                  ->groupBy('tanggal');
                  if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                      $absen=$absen->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                  }else if($tanggal_mulai!=''){
                      $absen=$absen->where('absens.checktime','>=',$tanggal_mulai);
                  }else if($tanggal_selesai!=''){
                      $absen=$absen->where('absens.checktime','<=',$tanggal_selesai);
                  }

                  $absen=$absen->get();

            $total=DB::table('karyawans')
                    ->select('karyawans.nama_karyawan','karyawans.npp')
                    ->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                    ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                    ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                    ->addSelect('karyawans.jamkerjaaktif')
                    ->leftjoin('absens','karyawans.userid','=','absens.userid')
                    ->where('karyawans.id',$id_karyawan)
                    ->orderBy('absens.checktime','DESC')
                    ->groupBy('karyawans.userid')
                    ->groupBy('karyawans.nama_karyawan')
                    ->groupBy('karyawans.npp')
                    ->groupBy('karyawans.jamkerjaaktif')
                    ->groupBy('tanggal');
                    if($tanggal_mulai!='' AND $tanggal_selesai!=''){
                        $total=$total->whereBetween('absens.checktime', [$tanggal_mulai, $tanggal_selesai]);
                    }else if($tanggal_mulai!=''){
                        $total=$total->where('absens.checktime','>=',$tanggal_mulai);
                    }else if($tanggal_selesai!=''){
                        $total=$total->where('absens.checktime','<=',$tanggal_selesai);
                    }
                    $totale=$total->get();
              $data['recordsTotal']=count($totale);
              $data['recordsFiltered']=count($totale);
        }
        if(count($absen) > 0){
        foreach ($absen as $key) {
          if( $key->cek_in=='00:00:00' AND $key->cek_out=='00:00:00' ){
            //cek apakah ada cuti di tanggal ini
            $cek_cuti=ambilcuti::where('karyawan_id',$id_karyawan)
                      ->where('tanggal_cuti','like','%'.$key->tanggal.'%')
                      ->where('statusatasan','=','Diterima')
                      ->where('statushrd','=','Diterima')
                      ->where(function($query){
                        $query->where('status_direksi','NOT LIKE','%pending%')
                        ->where('status_direksi','NOT LIKE','%Ditolak%')
                        ->orWhereNull('status_direksi');
                      })
                      ->first();
            $cek_ijin=leave::where('karyawan_id',$id_karyawan)
                      ->where('tanggal_ijin',$key->tanggal)
                      ->first();
            $iscuti=count($cek_cuti);
            $isijin=count($cek_ijin);

            if($iscuti>0){
              $key->jam_kerja='cuti';
            }else if($isijin>0){
              $key->jam_kerja='ijin';
            }else{
              $key->jam_kerja='';
            }

            //disini cari hari kerjanya, apakah minggu atau hari libur
            $timestamp = strtotime($key->tanggal);
            $day = date('w', $timestamp);
            $hari='';
            switch ($day) {
              case '0':$hari='Minggu';
                break;
              case '1':$hari='Senin';
                break;
              case '2':$hari='Selasa';
                break;
              case '3':$hari='Rabu';
                break;
              case '4':$hari='Kamis';
                break;
              case '5':$hari='Jumat';
                break;
              case '6':$hari='Sabtu';
                break;

            }
            $key->cek_in='';
            $key->cek_out='';
            $key->masuk='';
            $key->pulang='';
            $key->istirahat='';
            $key->jamnormal='';
            $key->toleransi='';
            $key->maxpulang='';
            $key->hari=$hari;
          }elseif($key->tanggal!=null){
          $jams=$key->jamkerjaaktif;
          // $jamkerja=DB::table('jam_kerjas')->whereIn('id',[$jams])
          //                     ->where('tgl_mulai','<',"$key->tanggal")
          //                     ->where('tgl_selesai','>',"'$key->tanggal'")
          //                     ->orderBy('tgl_mulai','DESC')
          //                     ->first();
          $jamkerja=DB::table('jam_kerjas')
                              ->where('tgl_mulai','<=',$key->tanggal)
                              ->where('tgl_selesai','>=',$key->tanggal)
                              ->orderBy('tgl_mulai','DESC')
                              ->first();
          $key->masuk=$jamkerja->masuk;
          $key->pulang=$jamkerja->pulang;
          $key->istirahat=$jamkerja->istirahat;
          $key->jamnormal=$jamkerja->jam_normal;
          $key->toleransi=$jamkerja->toleransi;
          $key->maxpulang=$jamkerja->maxpulang;
          $timestamp = strtotime($key->tanggal);
          $day = date('w', $timestamp);
          $hari='';
          switch ($day) {
            case '0':$hari='Minggu';
              break;
            case '1':$hari='Senin';
              break;
            case '2':$hari='Selasa';
              break;
            case '3':$hari='Rabu';
              break;
            case '4':$hari='Kamis';
              break;
            case '5':$hari='Jumat';
              break;
            case '6':$hari='Sabtu';
              break;

          }
          $key->hari=$hari;
          $masuk = new \DateTime($key->tanggal.' '.$key->masuk);
          $pulang = new \DateTime($key->tanggal.' '.$key->pulang);
          $toleransi = new \DateTime($key->tanggal.' '.$key->toleransi);
          $maxpulang = new \DateTime($key->tanggal.' '.$key->maxpulang);
          $cek_in = new \Datetime($key->cek_in);
          $cek_out = new \Datetime($key->cek_out);
          //convert istirahat ke detik
          $break=explode(':',$key->istirahat);
          $istirahat = ($break[0]*3600) + ($break[1]*60) + $break[2];
          $norm=explode(':',$key->jamnormal);
          $normal = ($norm[0]*3600) + ($norm[1]*60) + $norm[2];
          if( ($cek_in > $masuk) AND ($cek_in < $toleransi) ){
            $selisih=$cek_in->diff($cek_out);
            $kerja=$selisih->format('%H:%I:%S');
          }else if($cek_in <= $masuk ){
            $selisih=$cek_in->diff($cek_out);
            $kerja=$selisih->format('%H:%I:%S');
          }else if($cek_in > $toleransi AND $cek_out > $maxpulang ){
            $selisih=$cek_in->diff($maxpulang);
            $kerja=$selisih->format('%H:%I:%S');
          }else if($cek_in > $toleransi){
            $selisih=$cek_in->diff($cek_out);
            $kerja=$selisih->format('%H:%I:%S');
          }
          $krj=explode(':',$kerja);
          $detikkerja=($krj[0]*3600) + ($krj[1]*60) + $krj[2];
          $totalkerja=$detikkerja - $istirahat;
          if($totalkerja >= $normal ){
            $kerjanya=$normal;
          }elseif($totalkerja < 0){
            $kerjanya=$detikkerja;
          }else{
            $kerjanya=$totalkerja;
          }
          $key->jam_kerja=gmdate("H:i:s", $kerjanya);
          $key->cek_in=$cek_in->format('H:i:s');
          $key->cek_out=$cek_out->format('H:i:s');
        }else{
          $key->jam_kerja='';
          $key->cek_in='';
          $key->cek_out='';
          $key->masuk='';
          $key->pulang='';
          $key->istirahat='';
          $key->jamnormal='';
          $key->toleransi='';
          $key->maxpulang='';
        }
        //cek kalender libur nasional
        $libur=hari_libur::where('tanggal',$key->tanggal)->first();
        $timestamp = strtotime($key->tanggal);
        $day = date('w', $timestamp);
        $hari='';
        switch ($day) {
          case '0':$hari='Minggu';
            break;
          case '1':$hari='Senin';
            break;
          case '2':$hari='Selasa';
            break;
          case '3':$hari='Rabu';
            break;
          case '4':$hari='Kamis';
            break;
          case '5':$hari='Jumat';
            break;
          case '6':$hari='Sabtu';
            break;

        }
        $key->hari=$hari;
        if(count($libur) > 0){
          $key->keterangan=$libur->keterangan;
        }else{
          if($hari=='Minggu' || $hari=='Sabtu'){
            $key->keterangan='Week End';
          }else{
            $key->keterangan='Normal';
          }

        }

      }//end foreach


      }
        $data['data']=$absen;
        $data['draw']=$echo;
        return json_encode($data);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
