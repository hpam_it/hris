<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\karyawan;
use App\data_cuti;
use App\ambilcuti;
use App\jabatan;
use DB;
use App\Mail\approval;
use App\elly\hitungcuti;
use Mail;


class approvalcuti extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['halaman']='approve-cuti';
        return view('administrasi/approve-cuti/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=="list-all"){
          $role=Auth::user()->role;
          $karyawan_id= Auth::id();
          $divisi_id = Auth::user()->divisi_id;
          $kantor_id = Auth::user()->kantor_id;
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin'){
                $item=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.persetujuan_direksi','ambilcuti.direksi','ambilcuti.status_direksi','ambilcuti.alasan_direksi','ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) use ($keyword){
                          $query->where('ambilcuti.keperluan','like',$keyword)
                          ->orWhere('ambilcuti.alamat_cuti','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('pic.nama_karyawan','like',$keyword);
                        })
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','pending')
                                ->orWhere('ambilcuti.statushrd','pending')
                                ->orWhere('ambilcuti.status_direksi','LIKE','%pending%')
                                ->orWhereNull('status_direksi');
                        })
                        ->where(function($query){
                          $query->where('ambilcuti.statusatasan','<>','Ditolak')
                                ->orWhere('ambilcuti.statushrd','<>','Ditolak')
                                ->orWhere('ambilcuti.status_direksi','NOT LIKE','%Ditolak%')
                                ->orWhereNull('status_direksi');
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->orderBy('ambilcuti.id','desc')
                        ->get();
              }else{
                $item=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.persetujuan_direksi','ambilcuti.direksi','ambilcuti.status_direksi','ambilcuti.alasan_direksi','ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) use ($karyawan_id) {
                          $query->Where('ambilcuti.atasan',$karyawan_id)
                          ->orWhere('ambilcuti.pic_pengganti',$karyawan_id)
                          ->orWhereRaw(' FIND_IN_SET('.$karyawan_id.',ambilcuti.direksi) ')
                          ->orWhere('ambilcuti.statushrd','pending');
                        })
                        ->where(function($query) use ($keyword) {
                          $query->where('ambilcuti.keperluan','like',$keyword)
                          ->orWhere('ambilcuti.alamat_cuti','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('pic.nama_karyawan','like',$keyword);
                        })
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','pending')
                                ->orWhere('ambilcuti.statushrd','pending')
                                ->orWhere('ambilcuti.status_direksi','LIKE','%pending%')
                                ->orWhereNull('status_direksi');
                        })
                        ->where(function($query){
                          $query->where('ambilcuti.statusatasan','<>','Ditolak')
                                ->orWhere('ambilcuti.statushrd','<>','Ditolak')
                                ->orWhere('ambilcuti.status_direksi','NOT LIKE','%Ditolak%')
                                ->orWhereNull('status_direksi');
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->orderBy('ambilcuti.id','desc')
                        ->get();
              }

              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' ){
                $item=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.persetujuan_direksi','ambilcuti.direksi','ambilcuti.status_direksi','ambilcuti.alasan_direksi','ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) use ($keyword) {
                          $query->where('ambilcuti.keperluan','like',$keyword)
                          ->orWhere('ambilcuti.alamat_cuti','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('pic.nama_karyawan','like',$keyword);
                        })
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','pending')
                                ->orWhere('ambilcuti.statushrd','pending')
                                ->orWhere('ambilcuti.status_direksi','LIKE','%pending%')
                                ->orWhereNull('status_direksi');
                        })
                        ->where(function($query){
                          $query->where('ambilcuti.statusatasan','<>','Ditolak')
                                ->where('ambilcuti.statushrd','<>','Ditolak')
                                ->where('ambilcuti.status_direksi','NOT LIKE','%Ditolak%')
                                ->orWhereNull('status_direksi');
                        })

                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->skip($awal)
                        ->take($banyak)
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->orderBy('ambilcuti.id','desc')
                        ->get();
                $total=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.persetujuan_direksi','ambilcuti.direksi','ambilcuti.status_direksi','ambilcuti.alasan_direksi','ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) use ($keyword) {
                          $query->where('ambilcuti.keperluan','like',$keyword)
                          ->orWhere('ambilcuti.alamat_cuti','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('pic.nama_karyawan','like',$keyword);
                        })
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','pending')
                                ->orWhere('ambilcuti.statushrd','pending')
                                ->orWhere('ambilcuti.status_direksi','LIKE','%pending%')
                                ->orWhereNull('status_direksi');
                        })
                        ->where(function($query){
                          $query->where('ambilcuti.statusatasan','<>','Ditolak')
                                ->where('ambilcuti.statushrd','<>','Ditolak')
                                ->where('ambilcuti.status_direksi','NOT LIKE','%Ditolak%')
                                ->orWhereNull('status_direksi');
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->count();
              }else{
                $item=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.persetujuan_direksi','ambilcuti.direksi','ambilcuti.status_direksi','ambilcuti.alasan_direksi','ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) use ($karyawan_id) {
                          $query->Where('ambilcuti.atasan',$karyawan_id)
                          ->orWhere('ambilcuti.pic_pengganti',$karyawan_id)
                          ->orWhereRaw(' FIND_IN_SET('.$karyawan_id.',ambilcuti.direksi) ');
                        })
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','pending')
                                ->orWhere('ambilcuti.statushrd','pending')
                                ->orWhere('ambilcuti.status_direksi','LIKE','%pending%')
                                ->orWhereNull('status_direksi');
                        })
                        ->where(function($query){
                          $query->where('ambilcuti.statusatasan','<>','Ditolak')
                                ->where('ambilcuti.statushrd','<>','Ditolak')
                                ->where('ambilcuti.status_direksi','NOT LIKE','%Ditolak%')
                                ->orWhereNull('status_direksi');
                        })
                        ->where(function($query) use ($keyword) {
                          $query->where('ambilcuti.keperluan','like',$keyword)
                          ->orWhere('ambilcuti.alamat_cuti','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('pic.nama_karyawan','like',$keyword);
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->skip($awal)
                        ->take($banyak)
                        ->orderBy('ambilcuti.id','desc')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->get();
                $total=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.persetujuan_direksi','ambilcuti.direksi','ambilcuti.status_direksi','ambilcuti.alasan_direksi','ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) use ($karyawan_id) {
                          $query->Where('ambilcuti.atasan',$karyawan_id)
                          ->orWhere('ambilcuti.pic_pengganti',$karyawan_id)
                          ->orWhere('ambilcuti.statushrd','pending');
                        })
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','pending')
                                ->orWhere('ambilcuti.statushrd','pending')
                                ->orWhere('ambilcuti.status_direksi','LIKE','%pending%')
                                ->orWhereNull('status_direksi');
                        })
                        ->where(function($query){
                          $query->where('ambilcuti.statusatasan','<>','Ditolak')
                                ->where('ambilcuti.statushrd','<>','Ditolak')
                                ->where('ambilcuti.status_direksi','NOT LIKE','%Ditolak%')
                                ->orWhereNull('status_direksi');
                        })
                        ->where(function($query) use ($keyword) {
                          $query->where('ambilcuti.keperluan','like',$keyword)
                          ->orWhere('ambilcuti.alamat_cuti','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('pic.nama_karyawan','like',$keyword);
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->get();
              }

              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);

          } else if($banyak<0) {
            if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' ){
              $item=DB::table('ambilcutis as ambilcuti')
                      ->select('ambilcuti.persetujuan_direksi','ambilcuti.direksi','ambilcuti.status_direksi','ambilcuti.alasan_direksi','ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                      ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                      ->where(function($query) {
                        $query->where('ambilcuti.statusatasan','pending')
                              ->orWhere('ambilcuti.statushrd','pending')
                              ->orWhere('ambilcuti.status_direksi','LIKE','%pending%')
                              ->orWhereNull('status_direksi');
                      })
                      ->where(function($query){
                        $query->where('ambilcuti.statusatasan','<>','Ditolak')
                              ->where('ambilcuti.statushrd','<>','Ditolak')
                              ->where('ambilcuti.status_direksi','NOT LIKE','%Ditolak%')
                              ->orWhereNull('status_direksi');
                      })
                      ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                      ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                      ->orderBy('ambilcuti.id','desc')
                      ->groupBy('ambilcuti.nomor_pengajuan')
                      ->get();
            }else{
              $item=DB::table('ambilcutis as ambilcuti')
                      ->select('ambilcuti.persetujuan_direksi','ambilcuti.direksi','ambilcuti.status_direksi','ambilcuti.alasan_direksi','ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_tanggal','ambilcuti.jumlah_hari','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                      ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                      ->where(function($query) use ($karyawan_id) {
                        $query->Where('ambilcuti.atasan',$karyawan_id)
                        ->orWhereRaw(' FIND_IN_SET('.$karyawan_id.',ambilcuti.direksi) ')
                        ->orWhere('ambilcuti.pic_pengganti',$karyawan_id);
                      })
                      ->where(function($query) {
                        $query->where('ambilcuti.statusatasan','pending')
                              ->orWhere('ambilcuti.statushrd','pending')
                              ->orWhere('ambilcuti.status_direksi','LIKE','%pending%')
                              ->orWhereNull('status_direksi');
                      })
                      ->where(function($query){
                        $query->where('ambilcuti.statusatasan','<>','Ditolak')
                              ->where('ambilcuti.statushrd','<>','Ditolak')
                              ->where('ambilcuti.status_direksi','NOT LIKE','%Ditolak%')
                              ->orWhereNull('status_direksi');
                      })
                      ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                      ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                      ->orderBy('ambilcuti.id','desc')
                      ->groupBy('ambilcuti.nomor_pengajuan')
                      ->get();
              }
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
              if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' ){
                $item=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.persetujuan_direksi','ambilcuti.direksi','ambilcuti.status_direksi','ambilcuti.alasan_direksi','ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','pending')
                                ->orWhere('ambilcuti.statushrd','pending')
                                ->orWhere('ambilcuti.status_direksi','LIKE','%pending%')
                                ->orWhereNull('status_direksi');
                        })
                        ->where(function($query){
                          $query->where('ambilcuti.statusatasan','<>','Ditolak')
                                ->where('ambilcuti.statushrd','<>','Ditolak')
                                ->where('ambilcuti.status_direksi','NOT LIKE','%Ditolak%')
                                ->orWhereNull('status_direksi');
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->orderBy('ambilcuti.id','desc')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->skip($awal)
                        ->take($banyak)
                        ->get();

                $total=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.persetujuan_direksi','ambilcuti.direksi','ambilcuti.status_direksi','ambilcuti.alasan_direksi','ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','pending')
                                ->orWhere('ambilcuti.statushrd','pending')
                                ->orWhere('ambilcuti.status_direksi','LIKE','%pending%')
                                ->orWhereNull('status_direksi');
                        })
                        ->where(function($query){
                          $query->where('ambilcuti.statusatasan','<>','Ditolak')
                                ->where('ambilcuti.statushrd','<>','Ditolak')
                                ->where('ambilcuti.status_direksi','NOT LIKE','%Ditolak%')
                                ->orWhereNull('status_direksi');
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->get();
              }else{
                $item=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.persetujuan_direksi','ambilcuti.direksi','ambilcuti.status_direksi','ambilcuti.alasan_direksi','ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where(function($query) use ($karyawan_id) {
                          $query->Where('ambilcuti.atasan',$karyawan_id)
                          ->orWhereRaw(' FIND_IN_SET('.$karyawan_id.',ambilcuti.direksi) ')
                          ->orWhere('ambilcuti.pic_pengganti',$karyawan_id);
                        })
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','pending')
                                ->orWhere('ambilcuti.statushrd','pending')
                                ->orWhere('ambilcuti.status_direksi','LIKE','%pending%')
                                ->orWhereNull('status_direksi');
                        })
                        ->where(function($query){
                          $query->where('ambilcuti.statusatasan','<>','Ditolak')
                                ->where('ambilcuti.statushrd','<>','Ditolak')
                                ->where('ambilcuti.status_direksi','NOT LIKE','%Ditolak%')
                                ->orWhereNull('status_direksi');
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->orderBy('ambilcuti.id','desc')
                        ->skip($awal)
                        ->take($banyak)
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->get();

                $total=DB::table('ambilcutis as ambilcuti')
                        ->select('ambilcuti.persetujuan_direksi','ambilcuti.direksi','ambilcuti.status_direksi','ambilcuti.alasan_direksi','ambilcuti.id','ambilcuti.pic_pengganti','ambilcuti.atasan','ambilcuti.statusatasan','ambilcuti.statuspic','ambilcuti.statushrd','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->where(function($query) use ($karyawan_id) {
                          $query->Where('ambilcuti.atasan',$karyawan_id)
                          ->orWhere('ambilcuti.pic_pengganti',$karyawan_id);
                        })
                        ->where(function($query) {
                          $query->where('ambilcuti.statusatasan','pending')
                                ->orWhere('ambilcuti.statushrd','pending')
                                ->orWhere('ambilcuti.status_direksi','LIKE','%pending%')
                                ->orWhereNull('status_direksi');
                        })
                        ->where(function($query){
                          $query->where('ambilcuti.statusatasan','<>','Ditolak')
                                ->where('ambilcuti.statushrd','<>','Ditolak')
                                ->where('ambilcuti.status_direksi','NOT LIKE','%Ditolak%')
                                ->orWhereNull('status_direksi');
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->get();
              }

              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x,$karyawan_id) {
              //$item->setAttribute('nomer',$gh_x++);
              $tgl_cuti=explode(',',$item->tanggal_tanggal);
              $tanggal=[];
                setlocale(LC_TIME,'id_ID.UTF8', 'id_ID.UTF-8','id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID');
              foreach ($tgl_cuti as $k) {
                $tanggal[]=strftime("%A %d %b %y", strtotime($k));
              }
              $item->tanggal_tanggal=implode(', ',$tanggal);
              $item->nomer=$gh_x++;
              $item->action='';
              //cek status direksi
              if($item->persetujuan_direksi == 1){
                $status_direksi = explode(';',$item->status_direksi);
                foreach ($status_direksi as $key) {
                  $stts=explode(':',$key);
                  //ini kalo key nya sama dengan idnya
                  if($stts[0]==$karyawan_id){
                      //cek apakah statusnya masih pending ? kalo masih pending munculin button
                      if($stts[1]=='pending'){
                        $item->action.='<a href="'.url("approve-cuti").'/'.$item->id.'?aksi=terima&status=direksi" data-target="#ajax" data-toggle="modal" class="btn btn-md red">
                            <i class="fa fa-check"></i> Direksi
                        </a>';
                      }
                  }
                }
              }
                if($item->pic_pengganti==$karyawan_id){
                  if($item->statuspic=='pending'){
                    $item->action.='<a href="'.url("approve-cuti").'/'.$item->id.'?aksi=terima&status=pic" data-target="#ajax" data-toggle="modal" class="btn btn-md blue">
                        <i class="fa fa-check"></i> PIC
                    </a>';
                  }
                }
                if($item->atasan==$karyawan_id){
                  if($item->statusatasan=='pending'){
                    $item->action.='<a href="'.url("approve-cuti").'/'.$item->id.'?aksi=terima&status=atasan" data-target="#ajax" data-toggle="modal" class="btn btn-md purple">
                        <i class="fa fa-check"></i> Atasan
                    </a>';
                  }
                }
                if(Auth::user()->role=='admin' || Auth::user()->role=='superadmin' ){
                  if($item->statushrd=='pending'){
                    $item->action.='<a href="'.url("approve-cuti").'/'.$item->id.'?aksi=terima&status=hrd" data-target="#ajax" data-toggle="modal" class="btn btn-md yellow">
                        <i class="fa fa-check"></i> HRD
                    </a>';
                  }
                }
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }

      if($request->aksi='terima'){
        $status=$request->status;
        $ambil=ambilcuti::find($id);
        $tgltgl=explode(",",$ambil->tanggal_tanggal);
        $datacuti=data_cuti::find($ambil->data_cuti_id);
        if($datacuti->hitung==1){
          $sisa_cuti=Hitungcuti::ambil_sisa($ambil->karyawan_id,$ambil->data_cuti_id,$tgltgl);
          $data['pesan']= 'Sisa Periode ini akan menjadi : '.$sisa_cuti['periode_sekarang'].' dan Sisa Periode yang akan datang akan menjadi '.$sisa_cuti['periode_depan'];
        }else{
          $data['pesan']='';
        }
        $data['status']=$status;
        $data['item']=DB::table('ambilcutis as ambilcuti')
                      ->select('ambilcuti.*','data_cuti.nama_cuti','karyawan.nama_karyawan as nama_karyawan','pengganti.nama_karyawan as nama_pengganti','atasan.nama_karyawan as nama_atasan')
                      ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                      ->where('ambilcuti.nomor_pengajuan',$ambil->nomor_pengajuan)
                      ->leftjoin('data_cutis as data_cuti','ambilcuti.data_cuti_id','=','data_cuti.id')
                      ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                      ->leftjoin('karyawans as pengganti','ambilcuti.pic_pengganti','=','pengganti.id')
                      ->leftjoin('karyawans as atasan','ambilcuti.atasan','=','atasan.id')
                      ->first();
        /* tampilin data cutinya */
        $karyawan=karyawan::find($ambil->karyawan_id);
        $tahun_ini=date('Y');
        $awal_masuk=explode('-',$karyawan->tgl_gabung);
        $tahun_ini=date('Y').'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
        $depan=intval(date('Y')+1);
        $tahun_depan=$depan.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
        $kemarin=intval(date('Y')-1);
        $tahun_kemarin=$kemarin.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
        $kemarinnya=intval(date('Y')-2);
        $tahun_kemarinnya=$kemarinnya.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];

        if(date('Y-m-d') >= $tahun_ini){
          //$data['cuti_tahunan_oke']='oke';
          $periode_awal=date('Y-m-d',strtotime($tahun_ini));
          $periode_kemarin=date('Y-m-d',strtotime($tahun_kemarin));
          //$periode_akhir=$tahun_depan;

          $periode_akhir=date('Y-m-d', strtotime('-1 day',strtotime($tahun_depan)));
          $periode_tambahan=date('Y-m-d', strtotime("+6 month",strtotime($periode_akhir)));
        }else{
          //$data['cuti_tahunan_oke']='belum';
          $periode_awal=date('Y-m-d',strtotime($tahun_kemarin));
          $periode_kemarin=date('Y-m-d',strtotime($tahun_kemarinnya));
          //$periode_akhir=$tahun_ini;

          $periode_akhir=date('Y-m-d', strtotime('-1 day',strtotime($tahun_ini)));
          $periode_tambahan=date('Y-m-d', strtotime("+6 month",strtotime($periode_akhir)));
        }
        $id_karyawan=$karyawan->id;

        $cutioke=data_cuti::select('data_cutis.nama_cuti','data_cutis.hitung','data_cutis.jumlah')
                  ->leftjoin('sisa_cutis as sekarang',function($join) use ($periode_awal,$id_karyawan){
                    $join->on('data_cutis.id', '=', 'sekarang.data_cuti_id')
                          ->where('sekarang.periode_awal',$periode_awal)
                          ->where('sekarang.karyawan_id',$id_karyawan);
                  })
                  ->leftjoin('sisa_cutis as kemarin',function($join) use ($periode_kemarin,$id_karyawan){
                    $join->on('data_cutis.id', '=', 'kemarin.data_cuti_id')
                          ->where('kemarin.periode_awal',$periode_kemarin)
                          ->where('kemarin.karyawan_id',$id_karyawan);;
                  })
                  ->leftjoin(DB::raw("(SELECT
                  IFNULL((SUM(ambilcutis.jumlah_hari)),0) as total_cuti,
                  data_cuti_id
                  FROM
                  `ambilcutis`
                  WHERE
                  `karyawan_id` = ".$id_karyawan." and
                  `ambilcutis`.`statusatasan` = 'Diterima' and
                  `ambilcutis`.`statushrd` = 'Diterima' and
                  `ambilcutis`.`tanggal_cuti` between '".$periode_awal."' and '".$periode_akhir."' GROUP BY data_cuti_id) rekap"),
                    function ($join) {
                        $join->on('data_cutis.id', '=', 'rekap.data_cuti_id');
                    })
                  ->addSelect('sekarang.jumlah_awal as sekarang_awal','sekarang.diambil as sekarang_ambil','sekarang.sisa as sekarang_sisa','sekarang.periode_tambahan as sekarang_tambahan')
                  ->addSelect('kemarin.jumlah_awal as kemarin_awal','kemarin.diambil as kemarin_ambil','kemarin.sisa as kemarin_sisa','kemarin.periode_tambahan as kemarin_tambahan')
                  ->addSelect('rekap.total_cuti');
                  $cutioke=$cutioke->where('data_cutis.id','=',$ambil->data_cuti_id);
                  $cutioke=$cutioke->groupBy('data_cutis.id')
                  ->get();
                  $data['cuti_oke']=$cutioke;


        return view('administrasi/approve-cuti/terima',$data);
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->aksi=='terima'){
          $ambilcuti=ambilcuti::find($id);
          $tglcuti=explode(',',$ambilcuti->tanggal_tanggal);
          $status=$request->status;
          $id_pemutus=Auth::user()->id;
          $keputusan=$request->keputusan;
          $alasan=$request->alasan;
          $cutinya=data_cuti::find($ambilcuti->data_cuti_id);
          $hitung_cuti=$cutinya->hitung;
          $karyawan=karyawan::find($ambilcuti->karyawan_id);

          //akhir tentukan periode_awal
          if($status=='pic'){
            if($id_pemutus==$ambilcuti->pic_pengganti){
              //update
              DB::table('ambilcutis')
                  ->where('nomor_pengajuan', $ambilcuti->nomor_pengajuan)
                  ->update(['statuspic' => $keputusan,'alasan_pic'=> $alasan]);
              $pelaksana=karyawan::find($id_pemutus);
              $pengganti=karyawan::find($ambilcuti->pic_pengganti);
              $email = $karyawan->email;
              $nama_pelaksana = $pelaksana->nama_karyawan;
              // kirim email notif kalo pic bersedia
              $content = [
                    'title' => 'Status Pengganti '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Pengajuan Cuti anda  '.$keputusan.' Oleh Pengganti Anda '.$nama_pelaksana.' dengan alasan '.$alasan,
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $ambilcuti->tanggal_tanggal,
                    'keperluan' => $ambilcuti->keperluan,
                    'pengganti' => $pengganti->nama_karyawan,
                    'cuti' => $cutinya->nama_cuti,
                    'alamat_cuti' => $ambilcuti->alamat_cuti,
                    'kontak_cuti' => $ambilcuti->notelpon
                    ];
             Mail::to($email)->send(new approval($content));
              return 'ok';
            }else{
              return 'pic-salah';
            }
          }else if($status=='atasan'){
            if($id_pemutus==$ambilcuti->atasan){
              //update
              DB::table('ambilcutis')
                  ->where('nomor_pengajuan', $ambilcuti->nomor_pengajuan)
                  ->update(['statusatasan' => $keputusan,'alasan_atasan'=> $alasan]);
              $hasil=Hitungcuti::cek_approval($id);
              if($hasil==1){

                //cek apakah tanggal cuti sudah masuk di periode tersebut
                //jika masuk periode, maka sisa cuti akan di kurangi, jika belum ada maka bikin periode
                //dan pengurangan 1x
                foreach ($tglcuti as $key) {
                  Hitungcuti::kurangi_cuti($key,$ambilcuti->karyawan_id,$ambilcuti->data_cuti_id);
                }//endforeach

              }
              $pelaksana=karyawan::find($id_pemutus);
              $pengganti=karyawan::find($ambilcuti->pic_pengganti);
              $email = $karyawan->email;
              $nama_pelaksana = $pelaksana->nama_karyawan;
              // kirim email notif kalo pic bersedi
              $content = [
                    'title' => 'Status Atasan '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Pengajuan Cuti anda '.$keputusan.' Oleh Atasan Anda '.$nama_pelaksana. 'dengan alasan '.$alasan,
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $ambilcuti->tanggal_tanggal,
                    'keperluan' => $ambilcuti->keperluan,
                    'pengganti' => $pengganti->nama_karyawan,
                    'cuti' => $cutinya->nama_cuti,
                    'alamat_cuti' => $ambilcuti->alamat_cuti,
                    'kontak_cuti' => $ambilcuti->notelpon
                    ];
                    $content_lap = [
                          'title' => 'Status cuti  '.$karyawan->nama_karyawan,
                          'button' => 'Go To Apps',
                          'text' => 'Pengajuan Cuti '.$karyawan->nama_karyawan.' '.$keputusan.' Oleh Atasan '.$nama_pelaksana. 'dengan alasan '.$alasan,
                          'nama' => $karyawan->nama_karyawan,
                          'tanggal' => $ambilcuti->tanggal_tanggal,
                          'keperluan' => $ambilcuti->keperluan,
                          'pengganti' => $pengganti->nama_karyawan,
                          'cuti' => $cutinya->nama_cuti,
                          'alamat_cuti' => $ambilcuti->alamat_cuti,
                          'kontak_cuti' => $ambilcuti->notelpon
                          ];
                Mail::to($email)->send(new approval($content));
                Mail::to('hris.absensi@hpam.co.id')->send(new approval($content_lap));
              return 'ok';
            }else{
              return 'atasan-salah';
            }

          }else if($status=='hrd'){
            $karyawan2=karyawan::find($id_pemutus);
            if($karyawan2->role=='admin' || $karyawan2->role=='superadmin'){
              //update
              DB::table('ambilcutis')
                  ->where('nomor_pengajuan', $ambilcuti->nomor_pengajuan)
                  ->update(['statushrd' => $keputusan,'alasan_hrd'=> $alasan]);
                  $hasil=Hitungcuti::cek_approval($id);
                  if($hasil==1){
                    //cek apakah tanggal cuti sudah masuk di periode tersebut
                    //jika masuk periode, maka sisa cuti akan di kurangi, jika belum ada maka bikin periode
                    //dan pengurangan 1x
                    foreach ($tglcuti as $key) {
                      Hitungcuti::kurangi_cuti($key,$ambilcuti->karyawan_id,$ambilcuti->data_cuti_id);
                    }

                  }
              $pelaksana=karyawan::find($id_pemutus);
              $pengganti=karyawan::find($ambilcuti->pic_pengganti);
              $email = $karyawan->email;
              $nama_pelaksana = $pelaksana->nama_karyawan;
              // kirim email notif kalo pic bersedi
              $content = [
                    'title' => 'Status HRD '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Admin / HRD '.$nama_pelaksana.' menyatakan '.$keputusan.' dengan alasan '.$alasan,
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $ambilcuti->tanggal_tanggal,
                    'keperluan' => $ambilcuti->keperluan,
                    'pengganti' => $pengganti->nama_karyawan,
                    'cuti' => $cutinya->nama_cuti,
                    'alamat_cuti' => $ambilcuti->alamat_cuti,
                    'kontak_cuti' => $ambilcuti->notelpon
                    ];
          Mail::to($email)->send(new approval($content));
              return 'ok';
            }else{
              return 'hrd-salah';
            }
          }else if($status=='direksi'){
            $karyawan3=karyawan::find($id_pemutus);
            if($karyawan3->role=='direktur'){
              //update
              $keputusan_direksi = explode(';',$ambilcuti->status_direksi);
              $alasan_direksi = explode(';',$ambilcuti->alasan_direksi);
              //cek dan update keputusan direksi
              $keputusan_baru=[];
              foreach ($keputusan_direksi as $kpt) {
                $putus=explode(':',$kpt);
                if($putus[0]==$id_pemutus){
                  $keputusan_baru[]=$putus[0].':'.$keputusan;
                }else{
                  $keputusan_baru[]=$kpt;
                }
              }
              $keputusan_direksi_baru=join(';',$keputusan_baru);
              //cek dan update alasan direksi
              $alasan_baru=[];
              foreach ($alasan_direksi as $als) {
                $alas=explode(':',$als);
                if($alas[0]==$id_pemutus){
                  $alasan_baru[]=$alas[0].':'.$alasan;
                }else{
                  $alasan_baru[]=$als;
                }
              }
              $alasan_direksi_baru=join(';',$alasan_baru);
              DB::table('ambilcutis')
                  ->where('nomor_pengajuan', $ambilcuti->nomor_pengajuan)
                  ->update(['status_direksi' => $keputusan_direksi_baru,'alasan_direksi'=> $alasan_direksi_baru]);
                  $hasil=Hitungcuti::cek_approval($id);
                  if($hasil==1){
                    //cek apakah tanggal cuti sudah masuk di periode tersebut
                    //jika masuk periode, maka sisa cuti akan di kurangi, jika belum ada maka bikin periode
                    //dan pengurangan 1x
                    foreach ($tglcuti as $key) {
                      Hitungcuti::kurangi_cuti($key,$ambilcuti->karyawan_id,$ambilcuti->data_cuti_id);
                    }

                  }
              $pelaksana=karyawan::find($id_pemutus);
              $pengganti=karyawan::find($ambilcuti->pic_pengganti);
              $email = $karyawan->email;
              $nama_pelaksana = $pelaksana->nama_karyawan;
              // kirim email notif kalo pic bersedi
              $content = [
                    'title' => 'Status Direksi '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Direksi '.$nama_pelaksana.' menyatakan '.$keputusan.' dengan alasan '.$alasan,
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $ambilcuti->tanggal_tanggal,
                    'keperluan' => $ambilcuti->keperluan,
                    'pengganti' => $pengganti->nama_karyawan,
                    'cuti' => $cutinya->nama_cuti,
                    'alamat_cuti' => $ambilcuti->alamat_cuti,
                    'kontak_cuti' => $ambilcuti->notelpon
                    ];
              $content_lap = [
                    'title' => 'Status cuti  '.$karyawan->nama_karyawan,
                    'button' => 'Go To Apps',
                    'text' => 'Pengajuan Cuti '.$karyawan->nama_karyawan.' '.$keputusan.' Oleh Direksi '.$nama_pelaksana. 'dengan alasan '.$alasan,
                    'nama' => $karyawan->nama_karyawan,
                    'tanggal' => $ambilcuti->tanggal_tanggal,
                    'keperluan' => $ambilcuti->keperluan,
                    'pengganti' => $pengganti->nama_karyawan,
                    'cuti' => $cutinya->nama_cuti,
                    'alamat_cuti' => $ambilcuti->alamat_cuti,
                    'kontak_cuti' => $ambilcuti->notelpon
                    ];
                 Mail::to($email)->send(new approval($content));
                 Mail::to('hris.absensi@hpam.co.id')->send(new approval($content_lap));
              return 'ok';
            }else{
              return 'direktur-salah';
            }
          }

        }
    }

/*
    public function putar_cuti2(Request $request){
      $tahun=2019;
      $tglcuti=ambilcuti::whereYear('tanggal_cuti',$tahun)
              ->leftjoin('data_cutis','ambilcutis.data_cuti_id','=','data_cutis.id')
              ->where('statushrd','Diterima')
              ->where('statusatasan','Diterima')
              ->where('data_cutis.hitung','1')
              ->orderBy('ambilcutis.tanggal_cuti','ASC')
              ->get();

      foreach ($tglcuti as $key) {
        Hitungcuti::kurangi_cuti($key->tanggal_cuti,$key->karyawan_id,$key->data_cuti_id);
      }
    }
*/
    public function rekap_cuti(Request $request){
      $awal=$request->start;
      $banyak=$request->length;
      $banyak_colom=$request->iColumns;
      $kata_kunci_global=$request->sSearch;
      $echo=$request->draw;
      $kata_kunci=$request->search['value'];
      if( ($banyak<0) AND ($kata_kunci != "") ){
          $keyword='%'.$kata_kunci.'%';
          $item=karyawan::select('karyawans.nama_karyawan','sisa2.diambil','sisa2.saldo_awal','data_cutis.nama_cuti','data_cutis.jumlah','rekap.ambil','sisa.sisa_periode_sebelum','sisa2.sisa_periode_sekarang','sisa2.jumlah_awal','data_cutis.hitung')
                          ->addSelect(DB::raw('IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) )
                          		)
                          	as periode_mulai'))
                          ->addSelect(DB::raw(' IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		DATE_ADD(DATE_ADD( IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ) ,INTERVAL 1 YEAR),INTERVAL -1 DAY),
                          		DATE_ADD(DATE_ADD( IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY)) , INTERVAL 1 YEAR),INTERVAL -1 DAY)
                          		)
                          	as periode_selesai '))
                          ->addSelect(DB::raw('IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-2,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-2,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) )
                          		)
                          	as periode_sebelum'))
                            ->leftjoin(DB::raw("( SELECT SUM(jumlah_hari) as ambil,karyawan_id,data_cuti_id FROM ambilcutis left join karyawan_periode ON ambilcutis.karyawan_id = karyawan_periode.id WHERE tanggal_cuti BETWEEN karyawan_periode.periode_mulai AND karyawan_periode.periode_selesai AND ambilcutis.statusatasan='Diterima' AND ambilcutis.statushrd='Diterima' group by karyawan_id,data_cuti_id ) as rekap"), function($join){
                              $join->on('karyawans.id', '=', 'rekap.karyawan_id');
                            })
                            ->leftjoin(DB::raw("( SELECT karyawan_id,diambil,saldo_awal,sisa_cutis.jumlah_awal,data_cuti_id,IFNULL(sum(sisa),0) as sisa_periode_sebelum FROM sisa_cutis left join karyawan_periode ON sisa_cutis.karyawan_id = karyawan_periode.id WHERE periode_awal = karyawan_periode.periode_sebelum group by sisa_cutis.karyawan_id,sisa_cutis.data_cuti_id ) as sisa"), function ($join) {
                                    $join->on('karyawans.id','=','sisa.karyawan_id');
                            })
                            ->leftjoin(DB::raw("( SELECT karyawan_id,diambil,saldo_awal,sisa_cutis.jumlah_awal,data_cuti_id,IFNULL(sum(sisa),0) as sisa_periode_sekarang FROM sisa_cutis left join karyawan_periode ON sisa_cutis.karyawan_id = karyawan_periode.id WHERE periode_awal = karyawan_periode.periode_mulai group by sisa_cutis.karyawan_id,sisa_cutis.data_cuti_id ) as sisa2"), function ($join) {
                                    $join->on('karyawans.id','=','sisa2.karyawan_id');
                            })
                            ->leftjoin('data_cutis', function ($join) {
                                 $join->on('rekap.data_cuti_id', '=', 'data_cutis.id')->orOn('sisa.data_cuti_id','=','data_cutis.id')->orOn('sisa2.data_cuti_id','=','data_cutis.id');
                             })
                          ->where(function($query) use ($keyword){
                            $query->where('karyawans.nama_karyawan','like',$keyword)
                            ->orWhere('data_cutis.nama_cuti','like',$keyword);
                          })
                          ->orderBy('karyawans.nama_karyawan','ASC')
                          ->groupBy('karyawans.id')
                          ->groupBy('data_cutis.id')
                          ->get();
          $data['recordsTotal']=count($item);
          $data['recordsFiltered']=count($item);
      }else if($kata_kunci != "" ) {
          $keyword='%'.$kata_kunci.'%';
          $item=karyawan::select('karyawans.nama_karyawan','sisa2.diambil','sisa2.saldo_awal','sisa2.jumlah_awal','data_cutis.nama_cuti','data_cutis.jumlah','rekap.ambil','sisa.sisa_periode_sebelum','sisa2.sisa_periode_sekarang','data_cutis.hitung')
                          ->addSelect(DB::raw('IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) )
                          		)
                          	as periode_mulai'))
                          ->addSelect(DB::raw(' IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		DATE_ADD(DATE_ADD( IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ) ,INTERVAL 1 YEAR),INTERVAL -1 DAY),
                          		DATE_ADD(DATE_ADD( IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY)) , INTERVAL 1 YEAR),INTERVAL -1 DAY)
                          		)
                          	as periode_selesai '))
                          ->addSelect(DB::raw('IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-2,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-2,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) )
                          		)
                          	as periode_sebelum'))
                            ->leftjoin(DB::raw("( SELECT SUM(jumlah_hari) as ambil,karyawan_id,data_cuti_id FROM ambilcutis left join karyawan_periode ON ambilcutis.karyawan_id = karyawan_periode.id WHERE tanggal_cuti BETWEEN karyawan_periode.periode_mulai AND karyawan_periode.periode_selesai AND ambilcutis.statusatasan='Diterima' AND ambilcutis.statushrd='Diterima' group by karyawan_id,data_cuti_id ) as rekap"), function($join){
                              $join->on('karyawans.id', '=', 'rekap.karyawan_id');
                            })
                            ->leftjoin(DB::raw("( SELECT karyawan_id,diambil,saldo_awal,sisa_cutis.jumlah_awal,data_cuti_id,IFNULL(sum(sisa),0) as sisa_periode_sebelum FROM sisa_cutis left join karyawan_periode ON sisa_cutis.karyawan_id = karyawan_periode.id WHERE periode_awal = karyawan_periode.periode_sebelum group by sisa_cutis.karyawan_id,sisa_cutis.data_cuti_id ) as sisa"), function ($join) {
                                    $join->on('karyawans.id','=','sisa.karyawan_id');
                            })
                            ->leftjoin(DB::raw("( SELECT karyawan_id,diambil,saldo_awal,sisa_cutis.jumlah_awal,data_cuti_id,IFNULL(sum(sisa),0) as sisa_periode_sekarang FROM sisa_cutis left join karyawan_periode ON sisa_cutis.karyawan_id = karyawan_periode.id WHERE periode_awal = karyawan_periode.periode_mulai group by sisa_cutis.karyawan_id,sisa_cutis.data_cuti_id ) as sisa2"), function ($join) {
                                    $join->on('karyawans.id','=','sisa2.karyawan_id');
                            })
                            ->leftjoin('data_cutis', function ($join) {
                                 $join->on('rekap.data_cuti_id', '=', 'data_cutis.id')->orOn('sisa.data_cuti_id','=','data_cutis.id')->orOn('sisa2.data_cuti_id','=','data_cutis.id');
                             })
                          ->where(function($query) use ($keyword){
                            $query->where('karyawans.nama_karyawan','like',$keyword)
                            ->orWhere('data_cutis.nama_cuti','like',$keyword);
                          })
                          ->skip($awal)
                          ->take($banyak)
                          ->orderBy('karyawans.nama_karyawan','ASC')
                          ->groupBy('karyawans.id')
                          ->groupBy('data_cutis.id')
                          ->get();
          $total=karyawan::select('karyawans.nama_karyawan','sisa2.diambil','sisa2.saldo_awal','sisa2.jumlah_awal','data_cutis.nama_cuti','data_cutis.jumlah','rekap.ambil','sisa.sisa_periode_sebelum','sisa2.sisa_periode_sekarang','data_cutis.hitung')
                          ->addSelect(DB::raw('IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) )
                          		)
                          	as periode_mulai'))
                          ->addSelect(DB::raw(' IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		DATE_ADD(DATE_ADD( IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ) ,INTERVAL 1 YEAR),INTERVAL -1 DAY),
                          		DATE_ADD(DATE_ADD( IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY)) , INTERVAL 1 YEAR),INTERVAL -1 DAY)
                          		)
                          	as periode_selesai '))
                          ->addSelect(DB::raw('IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-2,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-2,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) )
                          		)
                          	as periode_sebelum'))
                            ->leftjoin(DB::raw("( SELECT SUM(jumlah_hari) as ambil,karyawan_id,data_cuti_id FROM ambilcutis left join karyawan_periode ON ambilcutis.karyawan_id = karyawan_periode.id WHERE tanggal_cuti BETWEEN karyawan_periode.periode_mulai AND karyawan_periode.periode_selesai AND ambilcutis.statusatasan='Diterima' AND ambilcutis.statushrd='Diterima' group by karyawan_id,data_cuti_id ) as rekap"), function($join){
                              $join->on('karyawans.id', '=', 'rekap.karyawan_id');
                            })
                            ->leftjoin(DB::raw("( SELECT karyawan_id,diambil,saldo_awal,sisa_cutis.jumlah_awal,data_cuti_id,IFNULL(sum(sisa),0) as sisa_periode_sebelum FROM sisa_cutis left join karyawan_periode ON sisa_cutis.karyawan_id = karyawan_periode.id WHERE periode_awal = karyawan_periode.periode_sebelum group by sisa_cutis.karyawan_id,sisa_cutis.data_cuti_id ) as sisa"), function ($join) {
                                    $join->on('karyawans.id','=','sisa.karyawan_id');
                            })
                            ->leftjoin(DB::raw("( SELECT karyawan_id,diambil,saldo_awal,sisa_cutis.jumlah_awal,data_cuti_id,IFNULL(sum(sisa),0) as sisa_periode_sekarang FROM sisa_cutis left join karyawan_periode ON sisa_cutis.karyawan_id = karyawan_periode.id WHERE periode_awal = karyawan_periode.periode_mulai group by sisa_cutis.karyawan_id,sisa_cutis.data_cuti_id ) as sisa2"), function ($join) {
                                    $join->on('karyawans.id','=','sisa2.karyawan_id');
                            })
                            ->leftjoin('data_cutis', function ($join) {
                                 $join->on('rekap.data_cuti_id', '=', 'data_cutis.id')->orOn('sisa.data_cuti_id','=','data_cutis.id')->orOn('sisa2.data_cuti_id','=','data_cutis.id');
                             })
                          //  ->leftjoin('data_cutis','rekap.data_cuti_id','=','data_cutis.id')
                          ->where(function($query) use ($keyword){
                            $query->where('karyawans.nama_karyawan','like',$keyword)
                            ->orWhere('data_cutis.nama_cuti','like',$keyword);
                          })
                          ->groupBy('karyawans.id')
                          ->groupBy('data_cutis.id')
                          ->orderBy('karyawans.nama_karyawan','ASC')
                          ->count();
          $data['recordsTotal']=$total;
          $data['recordsFiltered']=$total;

      } else if($banyak<0) {
        $item=karyawan::select('karyawans.nama_karyawan','sisa2.diambil','sisa2.saldo_awal','sisa2.jumlah_awal','data_cutis.nama_cuti','data_cutis.jumlah','rekap.ambil','sisa.sisa_periode_sebelum','sisa2.sisa_periode_sekarang','data_cutis.hitung')
                        ->addSelect(DB::raw('IF(
                            CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                            IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                            IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) )
                            )
                          as periode_mulai'))
                        ->addSelect(DB::raw(' IF(
                            CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                            DATE_ADD(DATE_ADD( IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ) ,INTERVAL 1 YEAR),INTERVAL -1 DAY),
                            DATE_ADD(DATE_ADD( IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY)) , INTERVAL 1 YEAR),INTERVAL -1 DAY)
                            )
                          as periode_selesai '))
                        ->addSelect(DB::raw('IF(
                            CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                            IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                            IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-2,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-2,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) )
                            )
                          as periode_sebelum'))
                          ->leftjoin(DB::raw("( SELECT SUM(jumlah_hari) as ambil,karyawan_id,data_cuti_id FROM ambilcutis left join karyawan_periode ON ambilcutis.karyawan_id = karyawan_periode.id WHERE tanggal_cuti BETWEEN karyawan_periode.periode_mulai AND karyawan_periode.periode_selesai AND ambilcutis.statusatasan='Diterima' AND ambilcutis.statushrd='Diterima' group by karyawan_id,data_cuti_id ) as rekap"), function($join){
                            $join->on('karyawans.id', '=', 'rekap.karyawan_id');
                          })
                          ->leftjoin(DB::raw("( SELECT karyawan_id,diambil,saldo_awal,sisa_cutis.jumlah_awal,data_cuti_id,IFNULL(sum(sisa),0) as sisa_periode_sebelum FROM sisa_cutis left join karyawan_periode ON sisa_cutis.karyawan_id = karyawan_periode.id WHERE periode_awal = karyawan_periode.periode_sebelum group by sisa_cutis.karyawan_id,sisa_cutis.data_cuti_id ) as sisa"), function ($join) {
                                  $join->on('karyawans.id','=','sisa.karyawan_id');
                          })
                          ->leftjoin(DB::raw("( SELECT karyawan_id,diambil,saldo_awal,sisa_cutis.jumlah_awal,data_cuti_id,IFNULL(sum(sisa),0) as sisa_periode_sekarang FROM sisa_cutis left join karyawan_periode ON sisa_cutis.karyawan_id = karyawan_periode.id WHERE periode_awal = karyawan_periode.periode_mulai group by sisa_cutis.karyawan_id,sisa_cutis.data_cuti_id ) as sisa2"), function ($join) {
                                  $join->on('karyawans.id','=','sisa2.karyawan_id');
                          })
                          ->leftjoin('data_cutis', function ($join) {
                               $join->on('rekap.data_cuti_id', '=', 'data_cutis.id')->orOn('sisa.data_cuti_id','=','data_cutis.id')->orOn('sisa2.data_cuti_id','=','data_cutis.id');
                           })
                          ->orderBy('karyawans.nama_karyawan','ASC')
                          ->groupBy('karyawans.id')
                          ->groupBy('data_cutis.id')
                          ->get();
          $data['recordsTotal']=count($item);
          $data['recordsFiltered']=count($item);
      } else {
          $item=karyawan::select('karyawans.nama_karyawan','sisa2.diambil','sisa2.saldo_awal','sisa2.jumlah_awal','data_cutis.nama_cuti','data_cutis.jumlah','rekap.ambil','sisa.sisa_periode_sebelum','sisa2.sisa_periode_sekarang','data_cutis.hitung')
                          ->addSelect(DB::raw('IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) )
                          		)
                          	as periode_mulai'))
                          ->addSelect(DB::raw(' IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		DATE_ADD(DATE_ADD( IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ) ,INTERVAL 1 YEAR),INTERVAL -1 DAY),
                          		DATE_ADD(DATE_ADD( IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY)) , INTERVAL 1 YEAR),INTERVAL -1 DAY)
                          		)
                          	as periode_selesai '))
                          ->addSelect(DB::raw('IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-2,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-2,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) )
                          		)
                          	as periode_sebelum'))
                            ->leftjoin(DB::raw("( SELECT SUM(jumlah_hari) as ambil,karyawan_id,data_cuti_id FROM ambilcutis left join karyawan_periode ON ambilcutis.karyawan_id = karyawan_periode.id WHERE tanggal_cuti BETWEEN karyawan_periode.periode_mulai AND karyawan_periode.periode_selesai AND ambilcutis.statusatasan='Diterima' AND ambilcutis.statushrd='Diterima' group by karyawan_id,data_cuti_id ) as rekap"), function($join){
                              $join->on('karyawans.id', '=', 'rekap.karyawan_id');
                            })
                            ->leftjoin(DB::raw("( SELECT karyawan_id,diambil,saldo_awal,sisa_cutis.jumlah_awal,data_cuti_id,IFNULL(sum(sisa),0) as sisa_periode_sebelum FROM sisa_cutis left join karyawan_periode ON sisa_cutis.karyawan_id = karyawan_periode.id WHERE periode_awal = karyawan_periode.periode_sebelum group by sisa_cutis.karyawan_id,sisa_cutis.data_cuti_id ) as sisa"), function ($join) {
                                    $join->on('karyawans.id','=','sisa.karyawan_id');
                            })
                            ->leftjoin(DB::raw("( SELECT karyawan_id,diambil,saldo_awal,sisa_cutis.jumlah_awal,data_cuti_id,IFNULL(sum(sisa),0) as sisa_periode_sekarang FROM sisa_cutis left join karyawan_periode ON sisa_cutis.karyawan_id = karyawan_periode.id WHERE periode_awal = karyawan_periode.periode_mulai group by sisa_cutis.karyawan_id,sisa_cutis.data_cuti_id ) as sisa2"), function ($join) {
                                    $join->on('karyawans.id','=','sisa2.karyawan_id');
                            })
                            ->leftjoin('data_cutis', function ($join) {
                                 $join->on('rekap.data_cuti_id', '=', 'data_cutis.id')->orOn('sisa.data_cuti_id','=','data_cutis.id')->orOn('sisa2.data_cuti_id','=','data_cutis.id');
                             })
                          ->skip($awal)
                          ->take($banyak)
                          ->orderBy('karyawans.nama_karyawan','ASC')
                          ->groupBy('karyawans.id')
                          ->groupBy('data_cutis.id')
                          ->get();
          $total=karyawan::select('karyawans.nama_karyawan','sisa2.diambil','sisa2.saldo_awal','sisa2.jumlah_awal','data_cutis.nama_cuti','data_cutis.jumlah','rekap.ambil','sisa.sisa_periode_sebelum','sisa2.sisa_periode_sekarang','data_cutis.hitung')
                          ->addSelect(DB::raw('IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) )
                          		)
                          	as periode_mulai'))
                          ->addSelect(DB::raw(' IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		DATE_ADD(DATE_ADD( IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ) ,INTERVAL 1 YEAR),INTERVAL -1 DAY),
                          		DATE_ADD(DATE_ADD( IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY)) , INTERVAL 1 YEAR),INTERVAL -1 DAY)
                          		)
                          	as periode_selesai '))
                          ->addSelect(DB::raw('IF(
                          		CURRENT_DATE() >= IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE()),MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-1,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) ),
                          		IFNULL( DATE_ADD(CONCAT_WS("-",YEAR(CURRENT_DATE())-2,MONTH(tgl_gabung),DAY(tgl_gabung)),INTERVAL 0 DAY) , DATE_ADD( CONCAT_WS("-",YEAR(CURRENT_DATE())-2,MONTH(tgl_gabung),DAY(tgl_gabung)-1) , INTERVAL 1 DAY) )
                          		)
                          	as periode_sebelum'))
                            ->leftjoin(DB::raw("( SELECT SUM(jumlah_hari) as ambil,karyawan_id,data_cuti_id FROM ambilcutis left join karyawan_periode ON ambilcutis.karyawan_id = karyawan_periode.id WHERE tanggal_cuti BETWEEN karyawan_periode.periode_mulai AND karyawan_periode.periode_selesai AND ambilcutis.statusatasan='Diterima' AND ambilcutis.statushrd='Diterima' group by karyawan_id,data_cuti_id ) as rekap"), function($join){
                              $join->on('karyawans.id', '=', 'rekap.karyawan_id');
                            })
                            ->leftjoin(DB::raw("( SELECT karyawan_id,diambil,saldo_awal,sisa_cutis.jumlah_awal,data_cuti_id,IFNULL(sum(sisa),0) as sisa_periode_sebelum FROM sisa_cutis left join karyawan_periode ON sisa_cutis.karyawan_id = karyawan_periode.id WHERE periode_awal = karyawan_periode.periode_sebelum group by sisa_cutis.karyawan_id,sisa_cutis.data_cuti_id ) as sisa"), function ($join) {
                                    $join->on('karyawans.id','=','sisa.karyawan_id');
                            })
                            ->leftjoin(DB::raw("( SELECT karyawan_id,diambil,saldo_awal,sisa_cutis.jumlah_awal,data_cuti_id,IFNULL(sum(sisa),0) as sisa_periode_sekarang FROM sisa_cutis left join karyawan_periode ON sisa_cutis.karyawan_id = karyawan_periode.id WHERE periode_awal = karyawan_periode.periode_mulai group by sisa_cutis.karyawan_id,sisa_cutis.data_cuti_id ) as sisa2"), function ($join) {
                                    $join->on('karyawans.id','=','sisa2.karyawan_id');
                            })
                            ->leftjoin('data_cutis', function ($join) {
                                 $join->on('rekap.data_cuti_id', '=', 'data_cutis.id')->orOn('sisa.data_cuti_id','=','data_cutis.id')->orOn('sisa2.data_cuti_id','=','data_cutis.id');
                             })
                          ->orderBy('karyawans.nama_karyawan','ASC')
                          ->groupBy('karyawans.id')
                          ->groupBy('data_cutis.id')
                          ->count();
          $data['recordsTotal']=$total;
          $data['recordsFiltered']=$total;
      }

      $gh_x=$awal+1;
      $item->each(function($item) use (&$gh_x) {
        $saldo=0;
          if($item->hitung=='1'){
            $saldo=$item->sisa_periode_sekarang;
            $saldo_belum=$item->sisa_periode_sebelum;
            $item->setAttribute('saldo_cuti',$saldo);
          }else{
            $item->setAttribute('sisa_periode_sebelum',0);
            $item->setAttribute('saldo_cuti',0);
            $item->setAttribute('saldo_awal',0);
            $item->setAttribute('diambil',$item->ambil);
            $item->setAttribute('jumlah_awal',$item->jumlah);
          }
          $item->setAttribute('nomer',$gh_x++);
          $item->setAttribute('action','<a href="'.url("data-jabatan").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                      <i class="fa fa-edit"></i>
                  </a>

                  <button onclick="hapus_input('."'".url('data-jabatan')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
                      <i class="fa fa-trash"></i>
                  </button>');
      });

      $data['draw']=$echo;
      $data['data']=$item;
      return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
