<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use App\data_nasabah;

class nasabah extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $data['halaman']='data-nasabah';
    return view('data/nasabah/index',$data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if($request->aksi=='simpan_item'){
        $request['karyawan_id']=Auth::user()->id;
        //$cek_item=data_nasabah::where('telpon',$request->telpon)->where('status','aktif')->count();
        $cek_item=0;
        if($cek_item>0){
            return 0;
        } else {
            data_nasabah::create($request->all());
            return 1;
        }

    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request,$id)
  {
    if($request->aksi=='edit'){
      $karyawan_id=Auth::id();
      $nasabah=data_nasabah::find($id);
      if($karyawan_id==$nasabah->karyawan_id){
        $data['item']=$nasabah;
        return view('data/nasabah/edit_list',$data);
      }else{
        return 'nasabah_salah';
      }

      }
    if($request->aksi=='history'){
      $nasabah_id=$id;
      $ijin=DB::table('leaves')
            ->select('data_ijins.nama_ijin as nama_progress','leaves.tanggal_ijin as tanggal_progress','data_progresses.nama_progress as hasil_progress','leaves.alasan_progress as keterangan_progress','leaves.tanggal_progress as tanggal_progress_berikutnya','leaves.nominal as nominal_progress')
            ->leftjoin('data_ijins','leaves.data_ijin_id','=','data_ijins.id')
            ->leftjoin('data_progresses','leaves.progress_id','=','data_progresses.id')
            ->where('data_ijins.hanya_marketing','Ya')
            ->where('leaves.nasabah_id',$nasabah_id);
      $data['progress']=DB::table('followups')
            ->select('data_progresses.nama_progress','followups.tanggal_progress','hasil.nama_progress as hasil_progress','followups.keterangan_progress','followups.tanggal_progress_berikutnya','followups.nominal_progress')
            ->leftjoin('data_progresses','followups.progress_id','=','data_progresses.id')
            ->leftjoin('data_progresses as hasil','followups.hasil_progress_id','hasil.id')
            ->where('followups.nasabah_id',$nasabah_id)
            ->union($ijin)
            ->orderBy('tanggal_progress','ASC')
            ->get();
            $karyawan_id=Auth::id();
            $nasabah=data_nasabah::find($id);
            if($karyawan_id==$nasabah->karyawan_id){
              return view('data/nasabah/history',$data);
            }else{
              return 'nasabah_salah';
            }


    }

      if($request->aksi=="list-all"){
          $karyawan_id=Auth::user()->id;
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              $item=data_nasabah::where('nama_nasabah','like',$keyword)->where('karyawan_id',$karyawan_id)->where('status','aktif')->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              $item=data_nasabah::where('nama_nasabah','like',$keyword)->where('karyawan_id',$karyawan_id)->where('status','aktif')->take($banyak)->orderBy('id','desc')->get();
              $total=data_nasabah::where('nama_nasabah','like',$keyword)->where('karyawan_id',$karyawan_id)->where('status','aktif')->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;

          } else if($banyak<0) {
              $item=data_nasabah::where('karyawan_id',$karyawan_id)->where('status','aktif')->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
              $item=data_nasabah::where('karyawan_id',$karyawan_id)->where('status','aktif')->skip($awal)->take($banyak)->orderBy('id','desc')->get();
              $total=data_nasabah::where('karyawan_id',$karyawan_id)->where('status','aktif')->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x) {
              $item->setAttribute('nomer',$gh_x++);
              $item->setAttribute('action','<a href="'.url("data-nasabah").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                          <i class="fa fa-edit"></i>
                      </a>
                      <button data-url="'.url("data-nasabah")."/".$item->id.'" class="hapus-input konfirmasi btn btn-md btn-icon-only red">
                          <i class="fa fa-trash"></i>
                      </button>
                      <a href="'.url("data-nasabah")."/".$item->id.'?aksi=history" data-target="#ajax" data-toggle="modal" class=" btn btn-md btn-icon-only blue">
                          <i class="fa fa-history"></i>
                      </a>');
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }

      if($request->aksi=='hapus_input'){
          $karyawan_id=Auth::user()->id;
          $nasabah=data_nasabah::find($id);
          if($karyawan_id==$nasabah->karyawan_id){
            $nasabah['status']='hapus';
            $nasabah->save();
            return 1;
            //update jadi nonaktif
          }else{
            return 0;
            //tidak bisa,bukan miliknya
          }

      }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if($request->aksi=='update_item'){
      $karyawan_id=Auth::user()->id;
      $nasabah=data_nasabah::find($id);
      if($karyawan_id==$nasabah->karyawan_id){
        $nasabah->update($request->all());
        return 1;
      }else{

        return 0;
      }


    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }
}
