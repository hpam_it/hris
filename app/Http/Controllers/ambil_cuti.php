<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\karyawan;
use App\data_cuti;
use App\ambilcuti;
use App\jabatan;
use DB;
use App\Mail\approval;
use Mail;
use App\struktur;
use App\elly\hitungcuti;
class ambil_cuti extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $tahun_ini=date('Y');
      $id = Auth::id();
      $divisi_id = Auth::user()->divisi_id;
      $kantor_id = Auth::user()->kantor_id;
      $data['halaman']='ambilcuti';
      $karyawan=karyawan::where('status_kerja','aktif')->where('divisi_id',$divisi_id)->where('id','=',$id)->first();
      $data['karyawan2']=karyawan::where('status_kerja','aktif')->where('id',$id)->get();

      if($id=='3'){
        $data['karyawan']=karyawan::where('status_kerja','aktif')->where('id','=','69')->get();
      }else{
        if($divisi_id==28){
          $data['karyawan']=karyawan::where('status_kerja','aktif')->where('divisi_id',18)->where('id','<>',$id)->get();
        }else if($divisi_id==33){
          $data['karyawan']=karyawan::where('status_kerja','aktif')->where('divisi_id',19)->where('id','<>',$id)->get();
        }else if($divisi_id==35){
          $data['karyawan']=karyawan::where('status_kerja','aktif')->where('divisi_id',17)->where('id','<>',$id)->get();
        }else if($divisi_id==36){
          $data['karyawan']=karyawan::where('status_kerja','aktif')->where('divisi_id',17)->where('id','<>',$id)->get();
        }else{
          $data['karyawan']=karyawan::where('status_kerja','aktif')->where('divisi_id',$divisi_id)->where('kantor_id',$kantor_id)->where('id','<>',$id)->get();
        }
      }
      $data['data_cuti']=data_cuti::all();
      $awal_masuk=explode('-',$karyawan->tgl_gabung);
      $tahun_ini=date('Y').'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
      $depan=intval(date('Y')+1);
      $tahun_depan=$depan.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
      $kemarin=intval(date('Y')-1);
      $tahun_kemarin=$kemarin.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
      $kemarinnya=intval(date('Y')-2);
      $tahun_kemarinnya=$kemarinnya.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];

      if(date('Y-m-d') >= $tahun_ini){
        //$data['cuti_tahunan_oke']='oke';
        $periode_awal=date('Y-m-d',strtotime($tahun_ini));
        $periode_kemarin=date('Y-m-d',strtotime($tahun_kemarin));
        //$periode_akhir=$tahun_depan;

        $periode_akhir=date('Y-m-d', strtotime('-1 day',strtotime($tahun_depan)));
        $periode_tambahan=date('Y-m-d', strtotime("+6 month",strtotime($periode_akhir)));
      }else{
        //$data['cuti_tahunan_oke']='belum';
        $periode_awal=date('Y-m-d',strtotime($tahun_kemarin));
        $periode_kemarin=date('Y-m-d',strtotime($tahun_kemarinnya));
        //$periode_akhir=$tahun_ini;

        $periode_akhir=date('Y-m-d', strtotime('-1 day',strtotime($tahun_ini)));
        $periode_tambahan=date('Y-m-d', strtotime("+6 month",strtotime($periode_akhir)));
      }


      $cutioke=data_cuti::select('data_cutis.nama_cuti','data_cutis.hitung','data_cutis.jumlah')
                ->leftjoin('sisa_cutis as sekarang',function($join) use ($periode_awal,$id){
                  $join->on('data_cutis.id', '=', 'sekarang.data_cuti_id')
                        ->where('sekarang.periode_awal',$periode_awal)
                        ->where('sekarang.karyawan_id',$id);
                })
                ->leftjoin('sisa_cutis as kemarin',function($join) use ($periode_kemarin,$id){
                  $join->on('data_cutis.id', '=', 'kemarin.data_cuti_id')
                        ->where('kemarin.periode_awal',$periode_kemarin)
                        ->where('kemarin.karyawan_id',$id);;
                })
                ->leftjoin(DB::raw("(SELECT
                IFNULL((SUM(ambilcutis.jumlah_hari)),0) as total_cuti,
                data_cuti_id
                FROM
                `ambilcutis`
                WHERE
                `karyawan_id` = ".$id." and
                `ambilcutis`.`statusatasan` = 'Diterima' and
                `ambilcutis`.`statushrd` = 'Diterima' and
                `ambilcutis`.`tanggal_cuti` between '".$periode_awal."' and '".$periode_akhir."' GROUP BY data_cuti_id) rekap"),
                  function ($join) {
                      $join->on('data_cutis.id', '=', 'rekap.data_cuti_id');
                  })
                ->addSelect('sekarang.jumlah_awal as sekarang_awal','sekarang.diambil as sekarang_ambil','sekarang.sisa as sekarang_sisa','sekarang.periode_tambahan as sekarang_tambahan')
                ->addSelect('kemarin.jumlah_awal as kemarin_awal','kemarin.diambil as kemarin_ambil','kemarin.sisa as kemarin_sisa','kemarin.periode_tambahan as kemarin_tambahan')
                ->addSelect('rekap.total_cuti');
                if($karyawan->gender=='L'){
                  $cutioke=$cutioke->where('data_cutis.id','!=','7');
                }else{
                  $cutioke=$cutioke->where('data_cutis.id','!=','5');
                }
                $cutioke=$cutioke->groupBy('data_cutis.id')
                ->get();
        $data['cuti_oke']=$cutioke;

      return view('ambilcuti/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->aksi=='simpan_item'){
          // cek apakah masih ada pengajuan cuti yang pending ?
          // kalo pending jangan boleh ngajuian dulu
          $pesan_tambahan='';
          $status_pending = ambilcuti::where(function($query)  {
                                        $query->where('statusatasan','pending')
                                              ->orWhere('statushrd','pending')
                                              ->orWhere('status_direksi','like','%pending%');
                              })
                              ->where(function($query){
                                    $query->where('statusatasan','!=','Ditolak')
                                          ->orWhere('statushrd','!=','Ditolak')
                                          ->orWhere('status_direksi','like','%Ditolak%');
                              })
                              ->where('karyawan_id',$request->karyawan_id)
                              ->count();
      // apkaah sudah di tolak
      $status_tolak = ambilcuti::where(function($query){
                                $query->where('statusatasan','=','Ditolak')
                                      ->orWhere('statushrd','=','Ditolak')
                                      ->orWhere('status_direksi','like','%Ditolak%');
                          })
                          ->where('karyawan_id',$request->karyawan_id)
                          ->count();
        if($status_pending > 0 AND $status_tolak <=0 ){
          return 'pending';
        }else{
          // cek apakah cuti di bulan yang sama
          if($request->pilihan_tanggal=='tanggal'){
            $tglcuti=explode(',',$request->tanggal_cuti);
            $tanggal_ambil_cuti=$request->tanggal_cuti;
          }else if($request->pilihan_tanggal=='range'){
            $tgl_awal=$request->tgl_awal;
            $tgl_akhir=$request->tgl_akhir;
            $ayo=$tgl_awal;
            $tglcuti=[];
            $tanggal_ambil_cuti=$tgl_awal." - ".$tgl_akhir;
            while(strtotime($ayo) <= strtotime($tgl_akhir)){
              $timestamp = strtotime($ayo);
              $day = date('w', $timestamp);
              $libur=DB::table('hari_liburs')->where('tanggal',$ayo)->first();
              if( (count($libur) > 0) ||  $day=='0' || $day=='6' ){
                //doing nothing
              }else{
                $tglcuti[]=$ayo;
              }

              $ayo = date("Y-m-d", strtotime("+1 day", strtotime($ayo)));
            }
          }

          $beda=0;
          $r=0;
          $bulan_sebelum='';
          $bulan_sekarang='';
          $batas = 0;
          foreach ($tglcuti as $key) {
            //cek apakah lebih dari 2 bulan yang lalu atau 6 bulan akan datang
            $dua_bulan_lalu = date("Y-m-d", strtotime("-2 month", strtotime($key)));
            $enam_bulan_depan = date("Y-m-d", strtotime("+6 month", strtotime($key)));
            if($key < $dua_bulan_lalu OR $key > $enam_bulan_depan){
              $batas++;
            }
            $iki=explode('-',$key);
            if($r==0){
              $bulan_sebelum=$iki[0].'-'.$iki[1];
            }
              $bulan_sekarang=$iki[0].'-'.$iki[1];
              if($bulan_sekarang!=$bulan_sebelum){
                $beda++;
              }
              $bulan_sebelum=$iki[0].'-'.$iki[1];

          }
          if($beda > 0){
            return "bulan";
          }else if($batas > 0){
            return "batas";
          }else{
          //cek nomor pengajuan terakhir
          $terakhir=ambilcuti::orderBy('nomor_pengajuan','DESC')->first();
          $nomor_terakhir=$terakhir['nomor_pengajuan']+1;
          $karyawanid=$request->karyawan_id;
          $jumlah_hari=count($tglcuti);
          $karyawan=karyawan::find($karyawanid);
          $jabatan=jabatan::find($karyawan->jabatan_id);
          $struktur=struktur::where('jabatan_id',$karyawan->jabatan_id)
                    ->where('divisi_id',$karyawan->divisi_id)
                    ->where('kantor_id',$karyawan->kantor_id)
                    ->where('atasan','>',0)
                    ->first();

          if(count($struktur) > 0){
            $struktur_atasan=struktur::find($struktur->atasan);
            $atasan=DB::table('karyawans as karyawan')
                    ->select('id')
                    ->where('karyawan.divisi_id',$struktur_atasan->divisi_id)
                    ->where('karyawan.jabatan_id',$struktur_atasan->jabatan_id)
                    ->where('karyawan.kantor_id',$struktur_atasan->kantor_id)
                    ->where('karyawan.status_kerja','aktif')
                    ->first();
          }else{
            $atasan=DB::table('karyawans as karyawan')
                    ->select('id')
                    ->where('karyawan.id',$karyawanid)
                    ->first();
          }


          //pengecekan apakah cuti yang dihitung harus lapor direksi ?
          $cuti_data = data_cuti::find($request->data_cuti_id);
          $perlu_direksi = 0;
          if($cuti_data->hitung == 1){
            //ambil direktur
            $direktur=karyawan::select('id','email')->where('status_kerja','aktif')->where('role','direktur')->get();
            $pejabat_direktur=[];
            //ambil sisa cuti apabila cuti ini diambil sisa berapa ?
            $sisa_cuti=Hitungcuti::ambil_sisa($karyawanid,$request->data_cuti_id,$tglcuti);
            //return $sisa_cuti;
            //cek apakah masuk cuti yang dihitung
            foreach ($direktur as $key) {
              $pejabat_direktur[]=$key->id;
              $direktur_status[]=$key->id.':pending';
              $direktur_alasan[]=$key->id.':-';
              $direktur_email[]=$key->email;
            }
            $pejabat_direktur = join(',',$pejabat_direktur);
            //$email_direksi = join(',',$direktur_email);
            $direktur_status = join(';',$direktur_status);
            $direktur_alasan = join(';',$direktur_alasan);
            if($sisa_cuti['periode_depan'] < 0 OR $sisa_cuti['periode_sekarang'] < 0 ){
              //butuh persetujuan direktur
              $perlu_direksi = 1;
              $pesan_tambahan = 'Sisa Periode ini akan menjadi : '.$sisa_cuti['periode_sekarang'].' dan Sisa Periode yang akan datang akan menjadi '.$sisa_cuti['periode_depan'];
            }
          }
          $oke=0;
          $tgltgl=implode(',',$tglcuti);
          foreach ($tglcuti as $key) {
            $cuti=new ambilcuti;
            $cuti->karyawan_id=$karyawanid;
            $cuti->tanggal_cuti=$key;
            $cuti->tanggal_tanggal=$tgltgl;
            $cuti->nomor_pengajuan=$nomor_terakhir;
            $cuti->data_cuti_id=$request->data_cuti_id;
            $cuti->keperluan=$request->keperluan;
            $cuti->alamat_cuti=$request->alamat_cuti;
            $cuti->notelpon=$request->notelpon;
            $cuti->pic_pengganti=$request->pic_pengganti;
            $cuti->jumlah_hari=1;
            $cuti->statusatasan='pending';
            $cuti->statuspic='pending';
            $cuti->statushrd='pending';
            if($perlu_direksi == 1){
              $cuti->persetujuan_direksi = 1;
              $cuti->direksi = $pejabat_direktur;
              $cuti->status_direksi = $direktur_status;
              $cuti->alasan_direksi = $direktur_alasan;
            }else{
              $cuti->persetujuan_direksi = 0;
            }
            if(count($atasan) <=0 ){
              $cuti->atasan=$karyawanid;
              $id_atasan = $karyawanid;
            }else{
              $cuti->atasan=$atasan->id;
              $id_atasan = $atasan->id;
            }
            $simpan= $cuti->save();
            if($simpan){
              $oke=1;
            }

          }


          if($oke==1){
            $pengganti=karyawan::find($request->pic_pengganti);
            $atasan=karyawan::find($id_atasan);
            $cutinya=data_cuti::find($request->data_cuti_id);
            if($perlu_direksi==1){
              $pesan = ' dan membutuhkan persetujuan direksi karena '.$pesan_tambahan;
            }else{
              $pesan = '';
            }
            $content_atasan = [
    		          'title' => 'Approval Cuti Untuk '.$karyawan->nama_karyawan,
    		          'button' => 'Go To Apps',
                  'text' => 'Permohonan cuti berikut ini membutuhkan persetujuan anda '.$pesan,
                  'nama' => $karyawan->nama_karyawan,
                  'tanggal' => $tanggal_ambil_cuti,
                  'keperluan' => $request->keperluan,
                  'pengganti' => $pengganti->nama_karyawan,
                  'cuti' => $cutinya->nama_cuti,
                  'alamat_cuti' => $request->alamat_cuti,
                  'kontak_cuti' => $request->notelpon
    		          ];
            $content_pengajuan = [
                  'title' => 'Salinan Permohonan Cuti '.$karyawan->nama_karyawan,
                  'button' => 'Go To Apps',
                  'text' => 'Berikut adalah rincian pengajuan cuti '.$pesan,
                  'nama' => $karyawan->nama_karyawan,
                  'tanggal' => $tanggal_ambil_cuti,
                  'keperluan' => $request->keperluan,
                  'pengganti' => $pengganti->nama_karyawan,
                  'cuti' => $cutinya->nama_cuti,
                  'alamat_cuti' => $request->alamat_cuti,
                  'kontak_cuti' => $request->notelpon

                  ];
            $content_pengganti = [
                  'title' => 'Informasi Cuti '.$karyawan->nama_karyawan,
                  'button' => 'Go To Apps',
                  'text' => 'Anda ditunjuk sebagai pengganti selama yang bersangkutan cuti',
                  'nama' => $karyawan->nama_karyawan,
                  'tanggal' => $tanggal_ambil_cuti,
                  'keperluan' => "-",//$request->keperluan,
                  'pengganti' => $pengganti->nama_karyawan,
                  'cuti' => $cutinya->nama_cuti,
                  'alamat_cuti' => "-",//$request->alamat_cuti,
                  'kontak_cuti' => $request->notelpon
                  ];
            $content_hrd = [
                  'title' => 'Permohonan Cuti Baru '.$karyawan->nama_karyawan,
                  'button' => 'Go To Apps',
                  'text' => 'Berikut adalah rincian pengajuan cuti '.$pesan,
                  'nama' => $karyawan->nama_karyawan,
                  'tanggal' => $tanggal_ambil_cuti,
                  'keperluan' => $request->keperluan,
                  'pengganti' => $pengganti->nama_karyawan,
                  'cuti' => $cutinya->nama_cuti,
                  'alamat_cuti' => $request->alamat_cuti,
                  'kontak_cuti' => $request->notelpon
                  ];

    	      $emailpengaju = $karyawan->email;
            $emailatasan = $atasan->email;
            $emailpengganti = $pengganti->email;
            $hrd = karyawan::where('role','admin')->select('email')->get();
            $emailhrd=[];
                foreach ($hrd as $k) {
                  $emailhrd[]=$k->email;
                }
            $hrdemail=implode(",",$emailhrd);
            if($perlu_direksi==1){

              $content_direksi = [
                'title' => 'Approval Cuti Untuk '.$karyawan->nama_karyawan,
                'button' => 'Go To Apps',
                'text' => 'Permohonan cuti berikut ini membutuhkan persetujuan direksi karena '.$pesan_tambahan,
                'nama' => $karyawan->nama_karyawan,
                'tanggal' => $tanggal_ambil_cuti,
                'keperluan' => $request->keperluan,
                'pengganti' => $pengganti->nama_karyawan,
                'cuti' => $cutinya->nama_cuti,
                'alamat_cuti' => $request->alamat_cuti,
                'kontak_cuti' => $request->notelpon
                    ];

                      Mail::to($direktur_email)->send(new approval($content_direksi));


            }
            //atasan
    	     Mail::to($emailatasan)->send(new approval($content_atasan));
            //pengaju
           Mail::to($emailpengaju)->send(new approval($content_pengajuan));
            //pengganti
           Mail::to($emailpengganti)->send(new approval($content_pengganti));
            //hrd
           Mail::to('hris.absensi@hpam.co.id')->send(new approval($content_hrd));

            return 1;
          }else{
            return 0;
          }
        }
        }
      }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=='edit'){

        $data['item']=ambilcuti::find($id);
        $data['karyawan']=karyawan::where('status_kerja','aktif')->get();
        $data['karyawan2']=karyawan::where('status_kerja','aktif')->get();
        $data['data_cuti']=data_cuti::all();
              return view('ambilcuti/edit',$data);
        }

        if($request->aksi=="list_all"){
            $role=Auth::user()->role;
            $karyawan_id= Auth::id();
            $divisi_id = Auth::user()->divisi_id;
            $kantor_id = Auth::user()->kantor_id;
            $awal=$request->start;
            $banyak=$request->length;
            $banyak_colom=$request->iColumns;
            $kata_kunci_global=$request->sSearch;
            $echo=$request->draw;
            $kata_kunci=$request->search['value'];
            $status=$request->status;
            $where_status=DB::table('ambilcutis as ambilcuti');
            $where_status2=DB::table('ambilcutis as ambilcuti');
            switch ($status) {
              case 'pending':
                  $where_status=$where_status->where(function($query)  {
                      $query->where('statusatasan','pending')
                            ->orWhere('statushrd','pending');
                  });
                  $where_status2=$where_status2->where(function($query)  {
                      $query->where('statusatasan','pending')
                            ->orWhere('statushrd','pending');
                  });
                break;
              case 'oke':
                  $where_status=$where_status->where(function($query) {
                      $query->where('statusatasan','Diterima')
                            ->Where('statushrd','Diterima');
                  });
                  $where_status2=$where_status2->where(function($query) {
                      $query->where('statusatasan','Diterima')
                            ->Where('statushrd','Diterima');
                  });
                break;
              case 'batal':
                  $where_status=$where_status->where(function($query) {
                      $query->where('statusatasan','Ditolak')
                            ->orWhere('statushrd','Ditolak')
                            ->orWhere('statusatasan','Dibatalkan')
                            ->orWhere('statushrd','Dibatalkan');
                  });
                  $where_status2=$where_status2->where(function($query) {
                      $query->where('statusatasan','Ditolak')
                            ->orWhere('statushrd','Ditolak')
                            ->orWhere('statusatasan','Dibatalkan')
                            ->orWhere('statushrd','Dibatalkan');
                  });
                break;
              default:
                  $where_status=$where_status->where(function($query) {
                      $query->where('statusatasan','pending')
                            ->orWhere('statushrd','pending');
                  });
                  $where_status2=$where_status2->where(function($query) {
                      $query->where('statusatasan','pending')
                            ->orWhere('statushrd','pending');
                  });
                break;
            }
            if( ($banyak<0) AND ($kata_kunci != "") ){
                $keyword='%'.$kata_kunci.'%';
                $item=$where_status->select('ambilcuti.statusatasan','ambilcuti.statushrd','ambilcuti.id','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where('ambilcuti.karyawan_id',$karyawan_id)
                        ->where(function($query) use ($keyword){
                          $query->where('ambilcuti.keperluan','like',$keyword)
                          ->orWhere('ambilcuti.alamat_cuti','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('pic.nama_karyawan','like',$keyword);
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->orderBy('ambilcuti.id','desc')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            }else if($kata_kunci != "" ) {
                $keyword='%'.$kata_kunci.'%';
                $item=$where_status->select('ambilcuti.statusatasan','ambilcuti.statushrd','ambilcuti.id','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where('ambilcuti.karyawan_id',$karyawan_id)
                        ->where(function($query) use ($keyword) {
                          $query->where('ambilcuti.keperluan','like',$keyword)
                          ->orWhere('ambilcuti.alamat_cuti','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('pic.nama_karyawan','like',$keyword);
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->skip($awal)
                        ->take($banyak)
                        ->orderBy('ambilcuti.id','desc')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->get();
                $total=$where_status2->select('ambilcuti.statusatasan','ambilcuti.statushrd','ambilcuti.id','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where('ambilcuti.karyawan_id',$karyawan_id)
                        ->where(function($query) use ($keyword) {
                          $query->where('ambilcuti.keperluan','like',$keyword)
                          ->orWhere('ambilcuti.alamat_cuti','like',$keyword)
                          ->orWhere('karyawan.nama_karyawan','like',$keyword)
                          ->orWhere('pic.nama_karyawan','like',$keyword);
                        })
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->count();
                $data['recordsTotal']=$total;
                $data['recordsFiltered']=$total;

            } else if($banyak<0) {
                $item=$where_status->select('ambilcuti.statusatasan','ambilcuti.statushrd','ambilcuti.id','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where('ambilcuti.karyawan_id',$karyawan_id)
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->orderBy('ambilcuti.id','desc')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            } else {
                $item=$where_status->select('ambilcuti.statusatasan','ambilcuti.statushrd','ambilcuti.id','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where('ambilcuti.karyawan_id',$karyawan_id)
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->orderBy('ambilcuti.id','desc')
                        ->skip($awal)
                        ->take($banyak)
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->get();

                $total=$where_status2->select('ambilcuti.statusatasan','ambilcuti.statushrd','ambilcuti.id','ambilcuti.tanggal_cuti','ambilcuti.tanggal_tanggal','ambilcuti.keperluan','ambilcuti.alamat_cuti','ambilcuti.notelpon','karyawan.nama_karyawan','pic.nama_karyawan as pengganti')
                        ->addSelect(DB::raw('SUM(ambilcuti.jumlah_hari) as jumlah_hari'))
                        ->where('ambilcuti.karyawan_id',$karyawan_id)
                        ->leftjoin('karyawans as karyawan','ambilcuti.karyawan_id','=','karyawan.id')
                        ->leftjoin('karyawans as pic','ambilcuti.pic_pengganti','=','pic.id')
                        ->groupBy('ambilcuti.nomor_pengajuan')
                        ->get();
                $data['recordsTotal']=count($total);
                $data['recordsFiltered']=count($total);
            }

            $gh_x=$awal+1;
            $item->each(function($item) use (&$gh_x) {
                //$item->setAttribute('nomer',$gh_x++);
                $item->nomer=$gh_x++;
                //tanggal cuti
                setlocale(LC_TIME,'id_ID.UTF8', 'id_ID.UTF-8','id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID');
                if($item->tanggal_tanggal !=""){
                $tgl_cuti=explode(',',$item->tanggal_tanggal);
                $tanggal=[];
                foreach ($tgl_cuti as $k) {
                  $tanggal[]=strftime("%A %d %b %y", strtotime($k));
                }
                $item->tanggal_tanggal=implode(', ',$tanggal);
              }else{
                $item->tanggal_tanggal=strftime("%A %d %b %y", strtotime($item->tanggal_cuti));
              }
                /*if(Auth::user()->role=='admin'){
                  $item->action='<a href="'.url("ambilcuti").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                              <i class="fa fa-edit"></i>
                          </a>
                          <button onclick="hapus_input('."'".url('ambilcuti')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
                              <i class="fa fa-trash"></i>
                          </button>';
                }else{
                  $item->action='';
                }

                $item->setAttribute('action','<a href="'.url("data-divisi").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                            <i class="fa fa-edit"></i>
                        </a>
                        <button onclick="hapus_input('."'".url('data-divisi')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
                            <i class="fa fa-trash"></i>
                        </button>');*/
            });

            $data['draw']=$echo;
            $data['data']=$item;
            return $data;
            /*return json_encode($data);*/
        }

        if($request->aksi=='hapus_input'){
            ambilcuti::find($id)->delete();
            return 1;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
