<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\hari_libur;
use App\ambilcuti;
use App\karyawan;
use App\absen;
use App\leave;
use App\data_ijin;
use App\elly\rekapabsensi;
use DB;
class Dashboard extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['halaman']='dashboard';
      $data['karyawan']=Auth::user();
      $id=Auth::user()->id;
      $karyawan=karyawan::find($id);
      $tahun_sekarang=date('Y');
      $bulan_sekarang=date('m');
      $hari_sekarang=date('d');
      $tgl_awal=$tahun_sekarang.'-01-01';
      if($tgl_awal<$karyawan->tgl_gabung){
        $tgl_awal=$karyawan->tgl_gabung;
      }
      $tgl_akhir=$tahun_sekarang.'-'.$bulan_sekarang.'-'.$hari_sekarang;
      $rekap=new rekapabsensi;
      $absensi=$rekap->rasio_absen($id,$tgl_awal,$tgl_akhir, 'oldVersion');

      //getCurrentPeriod
      $previousMonth = date("Y-m");
      $today = date('d');
      if($today <= 15) {
        $previousMonth = date("Y-m",strtotime("-1 month"));
      }
      $explodeToday = explode("-", $today);
      $tgl_awal = $previousMonth.'-16';
      $explodeTanggalAwal = explode("-", $tgl_awal);
      $getCurrentPeriod = $rekap->rasio_absen($id, $tgl_awal,$previousMonth.'-'.$today, 'currentPeriod');
      $currentPeriod = $getCurrentPeriod['hari_kerja'];
      $currentMonth = date("Y-m");
      $todayDate = date('d');
      if($todayDate > 15) {
        $startDate = $currentMonth;
      } else {
        $startDate = $previousMonth;
      }
      $todayDate = date('Y-m-d');
      $exlodeTodayDate = explode('-', $todayDate);
      $startDate = $startDate.'-16';
      $explodeStartDate = explode('-', $startDate);
      $currentDatePeriod = $explodeStartDate[2].'/'.$explodeStartDate[1].'/'.$explodeStartDate[0].' s/d '.$exlodeTodayDate[2].'/'.$exlodeTodayDate[1].'/'.$exlodeTodayDate[0];

      // Get Previous Period
      $today = date('Y-m-d');
      $todayMonth = date('m');
      $yearDatePreviousPeriod = date('Y');
      $todayDate = date('d');
      if($todayMonth == 01) {
        if($todayDate > 15) {
          $endMonthPreviousPeriod = '01';
        } else {
          $endMonthPreviousPeriod = '12';
        }
        $yearDatePreviousPeriod = date('Y') - 1;
        $endDatePreviousPeriod = $yearDatePreviousPeriod.'-'.$endMonthPreviousPeriod.'-'.'15';
        $startDatePreviousPeriod = $yearDatePreviousPeriod.'-'.($endMonthPreviousPeriod - 1).'-'.'16';
      } else {
        if($todayDate > 15) {
          $decreaseMonthEndDatePrevious = date('m');
          $decreaseMonthStartDatePrevious = date('m', strtotime(($today . "- 1 month")));
        } else {
          $decreaseMonthEndDatePrevious = date('m', strtotime(($today . "- 1 month")));
          $decreaseMonthStartDatePrevious = date('m', strtotime(($today . "- 2 month")));
        }
        $endDatePreviousPeriod = $yearDatePreviousPeriod.'-'.$decreaseMonthEndDatePrevious.'-'.'15';
        $startDatePreviousPeriod = $yearDatePreviousPeriod.'-'.$decreaseMonthStartDatePrevious.'-'.'16';
      }
      if($startDatePreviousPeriod < $karyawan->tgl_gabung) {
        $startDatePreviousPeriod = $karyawan->tgl_gabung;
      }
      $getPreviousPeriod = $rekap->rasio_absen($id, $startDatePreviousPeriod, $endDatePreviousPeriod, 'previousPeriod');
      $previousPeriod = $getPreviousPeriod['hari_kerja'];
      $explodeStarDatePreviousPeriod = explode('-', $startDatePreviousPeriod);   
      $explodeEndDatePreviousPeriod = explode('-', $endDatePreviousPeriod);      
      $previousDatePeriod = $explodeStarDatePreviousPeriod[2].'/'.$explodeStarDatePreviousPeriod[1].'/'.$explodeStarDatePreviousPeriod[0].' s/d '.$explodeEndDatePreviousPeriod[2].'/'.$explodeEndDatePreviousPeriod[1].'/'.$explodeEndDatePreviousPeriod[0];
      if($endDatePreviousPeriod < $karyawan->tgl_gabung) {
        $previousDatePeriod = '-';
      }

      $data['ijin']=DB::select(
        DB::raw('SELECT data_ijins.nama_ijin, COUNT(leaves.id) as jumlah_ijin
                  FROM
                    data_ijins
                  LEFT JOIN
                    leaves
                  ON
                    leaves.data_ijin_id = data_ijins.id
                    AND
                  leaves.tanggal_ijin BETWEEN "'.$tgl_awal.'" AND "'.$tgl_akhir.'"
                  WHERE
                  data_ijins.fullday = 1
                    AND
                  leaves.karyawan_id = '.$id.'
                    AND
                  leaves.statusatasan = "Diterima"
                  GROUP BY
                  data_ijins.id'
              ));
      $data['hari_kerja']=$absensi['hari_kerja'];
      $data['harus_hari_kerja']=$absensi['harus_hari_kerja'];
      $data['hari_aktif']=$absensi['hari_aktif'];
      $data['hari_ijin']=$absensi['hari_ijin'];
      $data['total_kerja']=$absensi['total_kerja'];//jam
      $data['jam_seharusnya']=$absensi['jam_seharusnya'];//jam
      $data['total_cuti']=$absensi['total_cuti'];//hari
      $data['total_dinas']=$absensi['total_dinas'];//jam
      $data['total_ijin']=$absensi['total_ijin'];//jam
      $data['total_mangkir']=$absensi['total_mangkir'];//hari
      $data['hari_libur']=$absensi['hari_libur'];
      $data['total_telat_cepat']=$absensi['total_telat_cepat'];//jam
      $data['persentase_kerja']=$absensi['persentase_kerja'];//dinas termasuk kerja
      $data['persentase_kerja_hari']=$absensi['persentase_kerja_hari'];//dinas termasuk kerja
      $data['persen_dinas']=$absensi['persen_dinas'];
      $data['persen_ijin']=$absensi['persen_ijin'];
      $data['persen_mangkir']=$absensi['persen_mangkir'];
      $data['hari_cepat']=$absensi['hari_cepat'];
      $data['currentPeriod'] = $currentPeriod;
      $data['previousPeriod'] = $previousPeriod;
      $data['tanggalCurrentPeriod'] = $currentDatePeriod;
      $data['tanggalPreviousPeriod'] = $previousDatePeriod;

      if($data['hari_kerja']==0){$data['hari_kerja']==1;}
      return view('dashboard/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=='hari_libur'){
        $sekarang=date('Y-m-d');
            $awal=$request->start;
            $banyak=$request->length;
            $banyak_colom=$request->iColumns;
            $kata_kunci_global=$request->sSearch;
            $echo=$request->draw;
            $kata_kunci=$request->search['value'];
            if( ($banyak<0) AND ($kata_kunci != "") ){
                $keyword='%'.$kata_kunci.'%';
                $item=hari_libur::where('tanggal','>',$sekarang)
                                  ->where('keterangan','like',$keyword)
                                  ->orderBy('tanggal','asc')
                                  ->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            }else if($kata_kunci != "" ) {
                $keyword='%'.$kata_kunci.'%';
                $item=hari_libur::where('tanggal','>',$sekarang)
                                  ->where('keterangan','like',$keyword)
                                  ->skip($awal)->take($banyak)->orderBy('tanggal','asc')->get();
                $total=hari_libur::where('tanggal','>',$sekarang)
                                  ->where('keterangan','like',$keyword)
                                  ->count();
                $data['recordsTotal']=$total;
                $data['recordsFiltered']=$total;

            } else if($banyak<0) {
                $item=hari_libur::where('tanggal','>',$sekarang)
                          ->orderBy('tanggal','asc')
                          ->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            } else {
                $item=hari_libur::where('tanggal','>',$sekarang)
                          ->skip($awal)->take($banyak)->orderBy('tanggal','asc')->get();
                $total=hari_libur::where('tanggal','>',$sekarang)
                          ->count();
                $data['recordsTotal']=$total;
                $data['recordsFiltered']=$total;
            }
            // $item->each(function($item) {
            //   $tgl=date_create($item->tanggal);
            //   $tanggal=date_format($tgl,"d M y");
            //   $item->tanggal=$tanggal;
            //
            // });

            $data['draw']=$echo;
            $data['data']=$item;
            return $data;
      }
      if($request->aksi=='ulangtahun'){
        $awal=$request->start;
        $banyak=$request->length;
        $banyak_colom=$request->iColumns;
        $kata_kunci_global=$request->sSearch;
        $echo=$request->draw;
        $sekarang=date('Y-m-d');
        $timestamp = strtotime($sekarang);
        $day = date('w', $timestamp);
        $penambah = 8-intval($day);
        $pengurang = intval($day)-1;

        $tanggal_awal = date("z", strtotime("-2 day", strtotime($sekarang)));
        $tanggal_akhir = date("z", strtotime("+10 day", strtotime($sekarang)));

        $tahun_ini = date('Y');
        $akhir_tahun = date("z", strtotime("+0 day", strtotime($tahun_ini.'-12-31')));

        if($tanggal_akhir < $tanggal_awal ){
          $tanggal_akhir = $tanggal_akhir + $akhir_tahun;
        }
        $kata_kunci=$request->search['value'];
        if( ($banyak<0) AND ($kata_kunci != "") ){
            $keyword='%'.$kata_kunci.'%';
            $item=karyawan::select('nama_karyawan','tgl_lahir')
                        ->where('nama_karyawan','like',$keyword)
                        ->where('status_kerja','Aktif')
                        ->whereNotIn('id', [94,32])
                        ->whereRaw("DAYOFYEAR(tgl_lahir) BETWEEN $tanggal_awal AND $tanggal_akhir");
                        $item=$item->orderByRaw("DAYOFYEAR(tgl_lahir) ASC")
                        ->get();
            $data['recordsTotal']=count($item);
            $data['recordsFiltered']=count($item);
        }else if($kata_kunci != "" ) {
            $keyword='%'.$kata_kunci.'%';
            $item=karyawan::select('nama_karyawan','tgl_lahir')
                        ->where('nama_karyawan','like',$keyword)
                        ->where('status_kerja','Aktif')
                        ->whereNotIn('id', [94,32])
                        ->whereRaw("DAYOFYEAR(tgl_lahir) BETWEEN $tanggal_awal AND $tanggal_akhir");
                        $item=$item->orderByRaw("DAYOFYEAR(tgl_lahir) ASC")->skip($awal)->take($banyak)->get();
            $total=karyawan::where('nama_karyawan','like',$keyword)
                        ->where('status_kerja','Aktif')
                        ->whereNotIn('id', [94,32])
                        ->whereBetween("DAYOFYEAR(tgl_lahir),[$tanggal_awal,$tanggal_akhir]");
                        $total=$total->orderByRaw("DAYOFYEAR(tgl_lahir) ASC")->get();
            $data['recordsTotal']=count($total);
            $data['recordsFiltered']=count($total);

        } else if($banyak<0) {
            $item=karyawan::select('nama_karyawan','tgl_lahir')
                        ->where('status_kerja','Aktif')
                        ->whereNotIn('id', [94,32])
                        ->whereRaw("DAYOFYEAR(tgl_lahir) BETWEEN $tanggal_awal AND $tanggal_akhir");
                        $item=$item->orderByRaw("DAYOFYEAR(tgl_lahir) ASC")
                      ->get();
            $data['recordsTotal']=count($item);
            $data['recordsFiltered']=count($item);
        } else {
            $item=karyawan::select('nama_karyawan','tgl_lahir')
                        ->where('status_kerja','Aktif')
                        ->whereNotIn('id', [94,32])
                        ->whereRaw("DAYOFYEAR(tgl_lahir) BETWEEN $tanggal_awal AND $tanggal_akhir");
            $item=$item->orderByRaw("DAYOFYEAR(tgl_lahir) ASC")
                      ->skip($awal)->take($banyak)->get();
            $total=karyawan::whereRaw("DAYOFYEAR(tgl_lahir) BETWEEN $tanggal_awal AND $tanggal_akhir")->whereNotIn('id', [94])->where('status_kerja','Aktif');

            $total=$total->orderByRaw("DAYOFYEAR(tgl_lahir) ASC")->get();
            $data['recordsTotal']=count($total);
            $data['recordsFiltered']=count($total);
        }
        $gh_x=$awal+1;
        $item->each(function($item) use (&$gh_x) {
            $item->setAttribute('nomer',$gh_x++);
            $b=explode('-',$item->tgl_lahir);

            $item->setAttribute('tgl_ultah',$item->tgl_lahir);
        });

        $data['draw']=$echo;
        $data['data']=$item;
        return $data;
      }
      if($request->aksi=='cuti'){
        $sekarang=date('Y-m-d');
            $awal=$request->start;
            $banyak=$request->length;
            $banyak_colom=$request->iColumns;
            $kata_kunci_global=$request->sSearch;
            $echo=$request->draw;
            $kata_kunci=$request->search['value'];
            $sekarang=date('Y-m-d');
            $sebelumnya = date("Y-m-d", strtotime("-1 day", strtotime($sekarang)));
            $akhir = date("Y-m-d", strtotime("+11 day", strtotime($sekarang)));
            $timestamp = strtotime($sekarang);
            $day = date('w', $timestamp);
            $penambah=8-intval($day);
            $pengurang=intval($day)-1;
            $tanggal_awal=date("z", strtotime("-1 day", strtotime($sekarang)));
            $tanggal_akhir=date("z", strtotime("+11 day", strtotime($sekarang)));

            $tahun_ini = date('Y');
            $akhir_tahun = date("z", strtotime("+0 day", strtotime($tahun_ini.'-12-31')));

            if($tanggal_akhir < $tanggal_awal ){
              $tanggal_akhir = $tanggal_akhir + $akhir_tahun;
            }

            if( ($banyak<0) AND ($kata_kunci != "") ){
                $keyword='%'.$kata_kunci.'%';
                $item=DB::table('ambilcutis')
                          ->select('nama_karyawan','tanggal_cuti','keperluan')
                          ->where('nama_karyawan','like',$keyword)
                          //->whereRaw("DAYOFYEAR(tanggal_cuti) BETWEEN $tanggal_awal AND $tanggal_akhir")
                          ->where('tanggal_cuti','>',$sebelumnya)
                          ->where('tanggal_cuti','<',$akhir)
                          ->where(function($query){
                            $query->where('ambilcutis.statusatasan','Diterima')
                                  ->where('ambilcutis.statushrd','Diterima');
                          })
                          ->where(function($query){
                            $query->where('ambilcutis.status_direksi','NOT LIKE','%Ditolak%')
                                  ->where('ambilcutis.status_direksi','NOT LIKE','%pending%')
                                  ->orWhereNull('status_direksi');
                          })
                          ->leftjoin('karyawans','ambilcutis.karyawan_id','=','karyawans.id')
                          ->orderBy("tanggal_cuti","ASC")
                          ->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            }else if($kata_kunci != "" ) {
                $keyword='%'.$kata_kunci.'%';
                $item=DB::table('ambilcutis')
                          ->select('nama_karyawan','tanggal_cuti','keperluan')
                          ->where('nama_karyawan','like',$keyword)
                          //->whereRaw("DAYOFYEAR(tanggal_cuti) BETWEEN $tanggal_awal AND $tanggal_akhir")
                          ->where('tanggal_cuti','>',$sebelumnya)
                          ->where('tanggal_cuti','<',$akhir)
                          ->where(function($query){
                            $query->where('ambilcutis.statusatasan','Diterima')
                                  ->where('ambilcutis.statushrd','Diterima');
                          })
                          ->where(function($query){
                            $query->where('ambilcutis.status_direksi','NOT LIKE','%Ditolak%')
                                  ->where('ambilcutis.status_direksi','NOT LIKE','%pending%')
                                  ->orWhereNull('status_direksi');
                          })
                          ->leftjoin('karyawans','ambilcutis.karyawan_id','=','karyawans.id')
                          ->orderBy("tanggal_cuti","ASC")
                          ->skip($awal)->take($banyak)->get();
                $total=DB::table('ambilcutis')
                          ->select('nama_karyawan','tanggal_cuti','keperluan')
                          ->where('nama_karyawan','like',$keyword)
                          //->whereRaw("DAYOFYEAR(tanggal_cuti) BETWEEN $tanggal_awal AND $tanggal_akhir")
                          ->where('tanggal_cuti','>',$sebelumnya)
                          ->where('tanggal_cuti','<',$akhir)
                          ->where(function($query){
                            $query->where('ambilcutis.statusatasan','Diterima')
                                  ->where('ambilcutis.statushrd','Diterima');
                          })
                          ->where(function($query){
                            $query->where('ambilcutis.status_direksi','NOT LIKE','%Ditolak%')
                                  ->where('ambilcutis.status_direksi','NOT LIKE','%pending%')
                                  ->orWhereNull('status_direksi');
                          })
                          ->leftjoin('karyawans','ambilcutis.karyawan_id','=','karyawans.id')
                          ->orderBy("tanggal_cuti","ASC")
                          ->get();
                $data['recordsTotal']=count($total);
                $data['recordsFiltered']=count($total);

            } else if($banyak<0) {
                $item=DB::table('ambilcutis')
                          ->select('nama_karyawan','tanggal_cuti','keperluan')
                          //->whereRaw("DAYOFYEAR(tanggal_cuti) BETWEEN $tanggal_awal AND $tanggal_akhir")
                          ->where('tanggal_cuti','>',$sebelumnya)
                          ->where('tanggal_cuti','<',$akhir)
                          ->where(function($query){
                            $query->where('ambilcutis.statusatasan','Diterima')
                                  ->where('ambilcutis.statushrd','Diterima');
                          })
                          ->where(function($query){
                            $query->where('ambilcutis.status_direksi','NOT LIKE','%Ditolak%')
                                  ->where('ambilcutis.status_direksi','NOT LIKE','%pending%')
                                  ->orWhereNull('status_direksi');
                          })
                          ->leftjoin('karyawans','ambilcutis.karyawan_id','=','karyawans.id')
                          ->orderBy("tanggal_cuti","ASC")
                          ->get();
                $data['recordsTotal']=count($item);
                $data['recordsFiltered']=count($item);
            } else {
                $item=DB::table('ambilcutis')
                          ->select('nama_karyawan','tanggal_cuti','keperluan')
                          //->whereRaw("DAYOFYEAR(tanggal_cuti) BETWEEN $tanggal_awal AND $tanggal_akhir")
                          ->where('tanggal_cuti','>',$sebelumnya)
                          ->where('tanggal_cuti','<',$akhir)
                          ->where(function($query){
                            $query->where('ambilcutis.statusatasan','Diterima')
                                  ->where('ambilcutis.statushrd','Diterima');
                          })
                          ->where(function($query){
                            $query->where('ambilcutis.status_direksi','NOT LIKE','%Ditolak%')
                                  ->where('ambilcutis.status_direksi','NOT LIKE','%pending%')
                                  ->orWhereNull('status_direksi');
                          })
                          ->leftjoin('karyawans','ambilcutis.karyawan_id','=','karyawans.id')
                          ->orderBy("tanggal_cuti","ASC")
                          ->skip($awal)->take($banyak)->get();
                $total=DB::table('ambilcutis')
                          ->select('nama_karyawan','tanggal_cuti','keperluan')
                          //->whereRaw("DAYOFYEAR(tanggal_cuti) BETWEEN $tanggal_awal AND $tanggal_akhir")
                          ->where('tanggal_cuti','>',$sebelumnya)
                          ->where('tanggal_cuti','<',$akhir)
                          ->where(function($query){
                            $query->where('ambilcutis.statusatasan','Diterima')
                                  ->where('ambilcutis.statushrd','Diterima');
                          })
                          ->where(function($query){
                            $query->where('ambilcutis.status_direksi','NOT LIKE','%Ditolak%')
                                  ->where('ambilcutis.status_direksi','NOT LIKE','%pending%')
                                  ->orWhereNull('status_direksi');
                          })
                          ->leftjoin('karyawans','ambilcutis.karyawan_id','=','karyawans.id')
                          ->orderBy("tanggal_cuti","ASC")
                          ->get();
                $data['recordsTotal']=count($total);
                $data['recordsFiltered']=count($total);
            }
            $data['draw']=$echo;
            $data['data']=$item;
            return $data;
            /*return json_encode($data);*/

      }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
