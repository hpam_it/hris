<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\karyawan;
use App\data_cuti;
use App\ambilcuti;
use App\jabatan;
use DB;
use App\Mail\approval;
use Mail;
use App\leave;
use App\data_ijin;
use Excel;

class daftarvisit extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function export_excel(){
         $daftar_cuti=leave::select('karyawans.nama_karyawan','karyawans.npp','data_ijins.nama_ijin','data_nasabahs.nama_nasabah','leaves.tanggal_ijin','leaves.jam_ijin','leaves.sampai_dengan','leaves.alamat','leaves.alasan','data_progresses.nama_progress','leaves.alasan_progress','leaves.nominal','leaves.tanggal_progress')
                     ->leftjoin('data_ijins','leaves.data_ijin_id','=','data_ijins.id')
                     ->leftjoin('karyawans','leaves.karyawan_id','=','karyawans.id')
                     ->leftjoin('karyawans as atasan','leaves.atasan','=','atasan.id')
                     ->leftjoin('data_progresses','leaves.progress_id','=','data_progresses.id')
                     ->leftjoin('data_nasabahs','leaves.nasabah_id','=','data_nasabahs.id')
                     ->where('leaves.statusatasan','Diterima')
                     ->where('data_ijins.hanya_marketing','Ya')
                     ->get()
                     ->toArray();
        $data_cuti=data_ijin::all()->toArray();;
        $karyawan=karyawan::select('id','npp','nama_karyawan')->get()->toArray();;

         Excel::create('data_visit', function($excel) use($daftar_cuti,$data_cuti,$karyawan) {
                 //mulai insert row
                 $excel->sheet('Pengajuan', function($sheet) use($daftar_cuti) {
                   $sheet->fromArray($daftar_cuti);

                 });
         })->export('xls');
     }


    public function index()
    {
      $data['halaman']='daftar-visit';
      return view('administrasi/daftar-visit/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
      if($request->aksi=='edit'){
          $item=leave::find($id);
          $data['karyawan']=karyawan::find($item->karyawan_id);
          $data['item']=$item;
          $data['jenis_ijin']=data_ijin::all();
          return view('administrasi/daftar-visit/edit_list',$data);
          //return $data;
        }

        if($request->aksi=='hapus_input'){
            $item=leave::find($id);
            $item->statushrd='batal';
            $item->statusatasan='batal';
            $item->save();
            return 1;
            //return $data;
          }

      if($request->aksi=="list-all"){
          $karyawan_id= Auth::id();
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $karyawan=karyawan::find($karyawan_id);
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              $item=leave::where(function($query) use ($keyword) {
                                $query->where('tanggal_ijin','like',$keyword)
                                      ->orWhere('alasan','like',$keyword)
                                      ->orWhere('karyawans.nama_karyawan','like',$keyword);
                              })
                            ->where('statusatasan','diterima')
                            ->where('data_ijins.hanya_marketing','Ya')
                            ->select('karyawans.nama_karyawan','leaves.id','leaves.tanggal_progress','leaves.nominal','leaves.alasan_progress','tanggal_ijin','alasan','data_ijins.nama_ijin','data_nasabahs.nama_nasabah','data_progresses.nama_progress','leaves.alamat as alamat_nasabah')
                            ->leftjoin('data_ijins','data_ijin_id','=','data_ijins.id')
                            ->leftjoin('karyawans','karyawan_id','karyawans.id')
                            ->leftjoin('data_progresses','leaves.progress_id','=','data_progresses.id')
                            ->leftjoin('data_nasabahs','leaves.nasabah_id','=','data_nasabahs.id');
                            if($karyawan->jabatan_id==15 AND ($karyawan->role=='user' OR $karyawan->role=='marketing' OR $karyawan->role=='' )){
                                  $item=$item->where('karyawans.divisi_id',$karyawan->divisi_id);
                              }
                            if($karyawan->jabatan_id==11 AND ($karyawan->role=='user' OR $karyawan->role=='marketing' OR $karyawan->role=='' )){
                                  $item=$item->where('karyawans.kantor_id',$karyawan->kantor_id);
                              }

                          $item=$item->orderBy('leaves.tanggal_ijin','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              $item=leave::where(function($query) use ($keyword) {
                                $query->where('tanggal_ijin','like',$keyword)
                                      ->orWhere('alasan','like',$keyword)
                                      ->orWhere('karyawans.nama_karyawan','like',$keyword);
                              })
                              ->where('statusatasan','diterima')
                              ->where('data_ijins.hanya_marketing','Ya')
                              ->select('karyawans.nama_karyawan','leaves.id','leaves.tanggal_progress','leaves.nominal','leaves.alasan_progress','tanggal_ijin','alasan','data_ijins.nama_ijin','data_nasabahs.nama_nasabah','data_progresses.nama_progress','leaves.alamat as alamat_nasabah')
                              ->leftjoin('data_ijins','data_ijin_id','=','data_ijins.id')
                              ->leftjoin('karyawans','karyawan_id','karyawans.id')
                              ->leftjoin('data_progresses','leaves.progress_id','=','data_progresses.id')
                              ->leftjoin('data_nasabahs','leaves.nasabah_id','=','data_nasabahs.id')
                              ->skip($awal)->take($banyak);
                              if($karyawan->jabatan_id==15 AND ($karyawan->role=='user' OR $karyawan->role=='marketing' OR $karyawan->role=='' )){
                                    $item=$item->where('karyawans.divisi_id',$karyawan->divisi_id);
                                }
                              if($karyawan->jabatan_id==11 AND ($karyawan->role=='user' OR $karyawan->role=='marketing' OR $karyawan->role=='' )){
                                    $item=$item->where('karyawans.kantor_id',$karyawan->kantor_id);
                                }

                            $item=$item->orderBy('leaves.tanggal_ijin','desc')->get();
              $total=leave::where(function($query) use ($keyword) {
                                $query->where('tanggal_ijin','like',$keyword)
                                      ->orWhere('alasan','like',$keyword)
                                      ->orWhere('karyawans.nama_karyawan','like',$keyword);
                              })
                              ->where('statusatasan','diterima')
                              ->where('data_ijins.hanya_marketing','Ya')
                              ->select('karyawans.nama_karyawan','leaves.id','leaves.tanggal_progress','leaves.nominal','leaves.alasan_progress','tanggal_ijin','alasan','data_ijins.nama_ijin','data_nasabahs.nama_nasabah','data_progresses.nama_progress','leaves.alamat as alamat_nasabah')
                              ->leftjoin('data_ijins','data_ijin_id','=','data_ijins.id')
                              ->leftjoin('karyawans','karyawan_id','karyawans.id')
                              ->leftjoin('data_progresses','leaves.progress_id','=','data_progresses.id')
                              ->leftjoin('data_nasabahs','leaves.nasabah_id','=','data_nasabahs.id');
                              if($karyawan->jabatan_id==15 AND ($karyawan->role=='user' OR $karyawan->role=='marketing' OR $karyawan->role=='' )){
                                    $total=$total->where('karyawans.divisi_id',$karyawan->divisi_id);
                                }
                              if($karyawan->jabatan_id==11 AND ($karyawan->role=='user' OR $karyawan->role=='marketing' OR $karyawan->role=='' )){
                                    $total=$total->where('karyawans.kantor_id',$karyawan->kantor_id);
                                }

                            $total=$total->orderBy('leaves.tanggal_ijin','desc')->get();
              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);

          } else if($banyak<0) {
              $item=leave::select('karyawans.nama_karyawan','leaves.id','leaves.tanggal_progress','leaves.nominal','leaves.alasan_progress','tanggal_ijin','alasan','data_ijins.nama_ijin','data_nasabahs.nama_nasabah','data_progresses.nama_progress','leaves.alamat as alamat_nasabah')
              ->leftjoin('data_ijins','data_ijin_id','=','data_ijins.id')
              ->where('statusatasan','diterima')
              ->where('data_ijins.hanya_marketing','Ya')
              ->leftjoin('karyawans','karyawan_id','karyawans.id')
              ->leftjoin('data_progresses','leaves.progress_id','=','data_progresses.id')
              ->leftjoin('data_nasabahs','leaves.nasabah_id','=','data_nasabahs.id');
              if($karyawan->jabatan_id==15 AND ($karyawan->role=='user' OR $karyawan->role=='marketing' OR $karyawan->role=='' )){
                    $item=$item->where('karyawans.divisi_id',$karyawan->divisi_id);
                }
              if($karyawan->jabatan_id==11 AND ($karyawan->role=='user' OR $karyawan->role=='marketing' OR $karyawan->role=='' )){
                    $item=$item->where('karyawans.kantor_id',$karyawan->kantor_id);
                }

            $item=$item->orderBy('leaves.tanggal_ijin','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
              $item=leave::select('karyawans.nama_karyawan','leaves.id','leaves.tanggal_progress','leaves.nominal','leaves.alasan_progress','tanggal_ijin','alasan','data_ijins.nama_ijin','data_nasabahs.nama_nasabah','data_progresses.nama_progress','leaves.alamat as alamat_nasabah')
              ->leftjoin('data_ijins','data_ijin_id','=','data_ijins.id')
              ->where('statusatasan','diterima')
              ->where('data_ijins.hanya_marketing','Ya')
              ->leftjoin('karyawans','karyawan_id','karyawans.id')
              ->leftjoin('data_progresses','leaves.progress_id','=','data_progresses.id')
              ->leftjoin('data_nasabahs','leaves.nasabah_id','=','data_nasabahs.id')
              ->skip($awal)->take($banyak);
              if($karyawan->jabatan_id==15 AND ($karyawan->role=='user' OR $karyawan->role=='marketing' OR $karyawan->role=='' )){
                    $item=$item->where('karyawans.divisi_id',$karyawan->divisi_id);
                }
              if($karyawan->jabatan_id==11 AND ($karyawan->role=='user' OR $karyawan->role=='marketing' OR $karyawan->role=='' )){
                    $item=$item->where('karyawans.kantor_id',$karyawan->kantor_id);
                }

            $item=$item->orderBy('leaves.tanggal_ijin','desc')->get();
              $total=leave::select('karyawans.nama_karyawan','leaves.id','leaves.tanggal_progress','leaves.nominal','leaves.alasan_progress','tanggal_ijin','alasan','data_ijins.nama_ijin','data_nasabahs.nama_nasabah','data_progresses.nama_progress','leaves.alamat as alamat_nasabah')
              ->leftjoin('data_ijins','data_ijin_id','=','data_ijins.id')
              ->where('statusatasan','diterima')
              ->where('data_ijins.hanya_marketing','Ya')
              ->leftjoin('karyawans','karyawan_id','karyawans.id')
              ->leftjoin('data_progresses','leaves.progress_id','=','data_progresses.id')
              ->leftjoin('data_nasabahs','leaves.nasabah_id','=','data_nasabahs.id');
              if($karyawan->jabatan_id==15 AND ($karyawan->role=='user' OR $karyawan->role=='marketing' OR $karyawan->role=='' )){
                    $total=$total->where('karyawans.divisi_id',$karyawan->divisi_id);
                }
              if($karyawan->jabatan_id==11 AND ($karyawan->role=='user' OR $karyawan->role=='marketing' OR $karyawan->role=='' )){
                    $total=$total->where('karyawans.kantor_id',$karyawan->kantor_id);
                }

            $total=$total->orderBy('leaves.tanggal_ijin','desc')->get();
              $data['recordsTotal']=count($total);
              $data['recordsFiltered']=count($total);
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x) {
              $item->setAttribute('nomer',$gh_x++);
              $item->setAttribute('action','<a href="'.url("daftar-ijin").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                          <i class="fa fa-edit"></i>
                      </a>
                      <button data-title="Batalkan Ijin Ini ?" data-toggle="confirmation" data-placement="left" data-url="'.url("daftar-ijin")."/".$item->id.'" class="konfirmasi tolak-ijin btn btn-md btn-icon-only red">
                          <i class="fa fa-times"></i>
                      </button>');
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
