<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\data_pajak;

class datapajak extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $data['halaman']='data-pajak';
    return view('data/pajak/index',$data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    if($request->aksi=='simpan_item'){
        $cek_item=data_pajak::where('pajak',$request->pajak)->count();
        if($cek_item>0){
            return 0;
        } else {
            data_pajak::create($request->all());
            return 1;
        }
      }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Request $request,$id)
  {
      if($request->aksi=='edit'){
          $data['item']=data_pajak::find($id);
          return view('data/pajak/edit_list',$data);
      }

      if($request->aksi=="list-all"){
          $awal=$request->start;
          $banyak=$request->length;
          $banyak_colom=$request->iColumns;
          $kata_kunci_global=$request->sSearch;
          $echo=$request->draw;
          $kata_kunci=$request->search['value'];
          if( ($banyak<0) AND ($kata_kunci != "") ){
              $keyword='%'.$kata_kunci.'%';
              $item=data_pajak::select('id','pajak')->where('pajak','like',$keyword)->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          }else if($kata_kunci != "" ) {
              $keyword='%'.$kata_kunci.'%';
              $item=data_pajak::select('id','pajak')->skip($awal)->where('pajak','like',$keyword)->take($banyak)->orderBy('id','desc')->get();
              $total=data_pajak::select('id','pajak')->where('pajak','like',$keyword)->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;

          } else if($banyak<0) {
              $item=data_pajak::select('id','pajak')->orderBy('id','desc')->get();
              $data['recordsTotal']=count($item);
              $data['recordsFiltered']=count($item);
          } else {
              $item=data_pajak::select('id','pajak')->skip($awal)->take($banyak)->orderBy('id','desc')->get();
              $total=data_pajak::select('id','pajak')->count();
              $data['recordsTotal']=$total;
              $data['recordsFiltered']=$total;
          }

          $gh_x=$awal+1;
          $item->each(function($item) use (&$gh_x) {
              $item->setAttribute('nomer',$gh_x++);
              $item->setAttribute('action','<a href="'.url("data-pajak").'/'.$item->id.'?aksi=edit" data-target="#ajax" data-toggle="modal" class="btn btn-md btn-icon-only green">
                          <i class="fa fa-edit"></i>
                      </a>
                      <button onclick="hapus_input('."'".url('data-pajak')."/".$item->id."'".');" class="btn btn-md btn-icon-only red">
                          <i class="fa fa-trash"></i>
                      </button>');
          });

          $data['draw']=$echo;
          $data['data']=$item;
          return $data;
          /*return json_encode($data);*/
      }

      if($request->aksi=='hapus_input'){
          data_pajak::find($id)->delete();
          return 1;
      }
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    if($request->aksi=='update_item'){
      $barang=data_pajak::find($id)->update($request->all());
      return 1;
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //
  }
}
