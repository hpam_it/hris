<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class penghasilan extends Model
{
    protected $fillable=['karyawan_id','gaji_pokok','adjustmen_gaji','tunjangan_makan','insentif','bonus','lembur','uang_dinas','thr','jkk','jk','bulan_pajak'];
}
