<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class data_keluarga extends Model
{
    protected $fillable = [
      'karyawan_id',
      'nama_anggota',
      'nik',
      'hubungan',
      'urutan',
      'tempat_lahir',
      'tgl_lahir',
      'gender'
    ];
}
