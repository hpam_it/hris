<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class followup extends Model
{
    protected $fillable=['karyawan_id','nasabah_id','progress_id','hasil_progress_id','keterangan_progress','tanggal_progress','tanggal_progress_berikutnya','nominal_progress','statusatasan','atasan'];
}
