<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class data_nasabah extends Model
{
    protected $fillable=['nama_nasabah','alamat','telpon','karyawan_id','status'];
}
