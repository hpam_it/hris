<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class jawaban extends Model
{
  protected $fillable=['soal_id','jawaban','target','responden','keterangan'];
}
