<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userinfo extends Model
{
    protected $connection = 'mysql2';
    protected $table = 'userinfo';
    protected $fillable=['userid','badgenumber','ssn','nama'];
}
