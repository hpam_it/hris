<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kantor extends Model
{
    protected $fillable=['kode_kantor','nama_kantor','nama_kota','alamat_kantor','telpon_kantor'];
}
