<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class soal extends Model
{
  protected $fillable=['pertanyaan','option','status','group','label_bawah','label_atas'];
}
