<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class import_absen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importAbsen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import absensi dari server 7.9';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
define('DB_SERVER_REMOTE','192.168.7.9');
define('DB_USER_REMOTE','root');
define('DB_PASSWORD_REMOTE','Pthpam25t@');
define('DB_NAME_REMOTE','import_absen');

//koneksi ke lokal db
define('DB_SERVER','127.0.0.1');
define('DB_USER','root');
define('DB_PASSWORD','NewPthpam25t@');
define('DB_NAME','hpam_hris');

class crud_remote{
	private $koneksi;

	public function __construct(){
  		//$this->koneksi = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
			$this->koneksi = new PDO("mysql:host=".DB_SERVER_REMOTE.";dbname=".DB_NAME_REMOTE."", DB_USER_REMOTE, DB_PASSWORD_REMOTE);
			$this->koneksi->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 	}

 	public function __destruct(){
		$this->koneksi = null;
 	}

	public function create(array $data){
		$db=get_class($this);
		$kolom=$this->kolom;
		$input="";
		$prepared="";
		$col=explode(',', $kolom);
		$panjang=count($col)-1;
		$x=0;
		//tentukan parameternya
		foreach ($col as $key=>$value) {
			if($key==$panjang){
				$prepared.=":$value";
			}else{
				$prepared.=":$value,";
			}
		}
		//bind parameter
		$insert = $this->koneksi->prepare("INSERT INTO ".$db." (".$kolom.")
    												VALUES (".$prepared.")");
		foreach ($col as $key) {
				if(array_key_exists($key, $data)){
					$insert->bindParam(':'.$key, $data[$key]);
				}else {
					$insert->bindParam(':'.$key,'');
				}
			}
		//$query="INSERT INTO ".$db."(".$kolom.") VALUES (".$input.")";
    $hasil=$insert->execute();
    //return $prepared;
		if($hasil){
				return $this->koneksi->lastInsertId();
			} else {
				return false;
		}
	}

	public function read($id){
		if($id > 0){
			$db=get_class($this);
			// $query="SELECT * FROM ".$db." WHERE id=".$id."";
			// $hasil=$this->koneksi->query($query);
			$read = $this->koneksi->prepare("SELECT * FROM ".$db." WHERE id=:id ");
			$read->bindParam(':id',$id);
			$read->execute();
			$result=$read->fetch(PDO::FETCH_ASSOC);
			return $result;
		} else {
			//gunakan id 0 untuk membaca semua
			$db=get_class($this);
			$read = $this->koneksi->prepare("SELECT * FROM ".$db);
			$read->execute();
			$result=$read->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

	}

	public function where($col,$val){
		$db=get_class($this);
		$where= $this->koneksi->prepare("SELECT * FROM ".$db." WHERE ".$col." = :column " );
		$where->bindParam(':column',$val);
		$where->execute();
		$result=$where->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function update($id,array $data){
		$db=get_class($this);
		$kolom=$this->kolom;
		//prepared
		$col=explode(',', $kolom);
		$set="";
		foreach ($data as $key=>$value) {
				if(array_search($key, $col)>=0){
					$set.=$key." = :$key,";
				}

			}
		$set_array=explode(',',$set);
		$panjang=count($set_array)-2;
		$set="";
		for($x=0;$x<=$panjang;$x++){
			if($x < $panjang){
				$set.=$set_array[$x]." ,";
			} else {
				$set.=$set_array[$x];
			}

		}

		//prepared statement
		$update=$this->koneksi->prepare("UPDATE $db SET $set ");
		//bind parameter
		foreach ($data as $key => $value) {
			$update->bindParam(':'.$key,$data[$key]);
		}

		if($update->execute()){
			return true;
		} else{
			return false;
		}
	}

	public function delete($id){
		$db=get_class($this);
		$query="DELETE FROM ".$db." WHERE id=:id";
		$hapus=$this->koneksi->prepare($query);
		$hapus->bindParam(':id',$id);
		if($hapus->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function runQuery($query, array $value){
		//runn query dengan hasil
		$hasil=$this->koneksi->prepare($query);
		$hasil->execute($value);
		$data = $hasil->fetchAll(PDO::FETCH_ASSOC);
		return $data;
	}

	public function aquery($query,array $value){
		//run query tanpa hasil
		$hasil=$this->koneksi->prepare($query);
		$result=$hasil->execute($value);
		return $result;
	}

}

class crud_lokal{
	private $koneksi;

	public function __construct(){
  		//$this->koneksi = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
			$this->koneksi = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME."", DB_USER, DB_PASSWORD);
			$this->koneksi->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 	}

 	public function __destruct(){
		$this->koneksi = null;
 	}

	public function create(array $data){
		$db=get_class($this);
		$kolom=$this->kolom;
		$input="";
		$prepared="";
		$col=explode(',', $kolom);
		$panjang=count($col)-1;
		$x=0;
		//tentukan parameternya
		foreach ($col as $key=>$value) {
			if($key==$panjang){
				$prepared.=":$value";
			}else{
				$prepared.=":$value,";
			}
		}
		//bind parameter
		$insert = $this->koneksi->prepare("INSERT INTO ".$db." (".$kolom.")
    												VALUES (".$prepared.")");
		foreach ($col as $key) {
				if(array_key_exists($key, $data)){
					$insert->bindParam(':'.$key, $data[$key]);
				}else {
					$insert->bindParam(':'.$key,'');
				}
			}
		//$query="INSERT INTO ".$db."(".$kolom.") VALUES (".$input.")";
    $hasil=$insert->execute();
    //return $prepared;
		if($hasil){
				return $this->koneksi->lastInsertId();
			} else {
				return false;
		}
	}

	public function read($id){
		if($id > 0){
			$db=get_class($this);
			// $query="SELECT * FROM ".$db." WHERE id=".$id."";
			// $hasil=$this->koneksi->query($query);
			$read = $this->koneksi->prepare("SELECT * FROM ".$db." WHERE id=:id ");
			$read->bindParam(':id',$id);
			$read->execute();
			$result=$read->fetch(PDO::FETCH_ASSOC);
			return $result;
		} else {
			//gunakan id 0 untuk membaca semua
			$db=get_class($this);
			$read = $this->koneksi->prepare("SELECT * FROM ".$db);
			$read->execute();
			$result=$read->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

	}

	public function where($col,$val){
		$db=get_class($this);
		$where= $this->koneksi->prepare("SELECT * FROM ".$db." WHERE ".$col." = :column " );
		$where->bindParam(':column',$val);
		$where->execute();
		$result=$where->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function update($id,array $data){
		$db=get_class($this);
		$kolom=$this->kolom;
		//prepared
		$col=explode(',', $kolom);
		$set="";
		foreach ($data as $key=>$value) {
				if(array_search($key, $col)>=0){
					$set.=$key." = :$key,";
				}

			}
		$set_array=explode(',',$set);
		$panjang=count($set_array)-2;
		$set="";
		for($x=0;$x<=$panjang;$x++){
			if($x < $panjang){
				$set.=$set_array[$x]." ,";
			} else {
				$set.=$set_array[$x];
			}

		}

		//prepared statement
		$update=$this->koneksi->prepare("UPDATE $db SET $set ");
		//bind parameter
		foreach ($data as $key => $value) {
			$update->bindParam(':'.$key,$data[$key]);
		}

		if($update->execute()){
			return true;
		} else{
			return false;
		}
	}

	public function delete($id){
		$db=get_class($this);
		$query="DELETE FROM ".$db." WHERE id=:id";
		$hapus=$this->koneksi->prepare($query);
		$hapus->bindParam(':id',$id);
		if($hapus->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function runQuery($query, array $value){
		//runn query dengan hasil
		$hasil=$this->koneksi->prepare($query);
		$hasil->execute($value);
		$data = $hasil->fetchAll(PDO::FETCH_ASSOC);
		return $data;
	}

	public function aquery($query,array $value){
		//run query tanpa hasil
		$hasil=$this->koneksi->prepare($query);
		$result=$hasil->execute($value);
		return $result;
	}

}

//nama table adalah nama clas dan nama field di variable kolom

class  absens extends crud_lokal{
	protected $kolom='userid,checktime';
}

// penggunaan
// $tabel = new namaTable;
// $tabel->read($id);
// $tabel->create(array('kolom'=>'value'));
// $tabel->where('kolom','value');
// $tabel->update($id,array('kolom'=>'value'));
// $tabel->delete($id);
// $query="SELECT * FROM table WHERE kolom = ? AND kolom2 = ?";
// $value=array(value1,value2)
// $tabel->runQuery($query,$value);//query dengan kembalian array hasil ex. select
// $query="UPDATE table SET kolom = ? , kolom2 = ?";
// $value=array(value1,value2)
// $tabel->aQuery($query,$value);//query tanpa kembalian hasil hanya true/false ex. update, delete
date_default_timezone_set("Asia/Jakarta");
//require "crud.php";
$tanggal_awal=date("Y-m-d H:i:s", strtotime(" -30 day"));
$tanggal_akhir=date("Y-m-d H:i:s", strtotime(" +1 day"));
// $tanggal_awal="2019-08-25 00:00:00";
// $tanggal_akhir="2019-08-27 00:00:00";
//table_remote
$absen_import=new crud_remote;
$import=$absen_import->runQuery("SELECT * FROM absens WHERE checktime >= ? AND checktime <= ? ",array($tanggal_awal,$tanggal_akhir));
$total=count($import) ;
if($total == 0){
  echo $total;
}else{
  foreach ($import as $key) {
    $absen= new crud_lokal;
    $cek=$absen->runQuery("SELECT * FROM absens WHERE userid = ? AND checktime = ?  ",array($key['userid'],$key['checktime']));
    if(count($cek) == 0){
      $insert = new absens;
      $insert->create(array('userid'=>$key['userid'],'checktime'=>$key['checktime']));
      echo "\n".$key['userid']." :  ".$key['checktime']." insert oke";
    }else{
      echo "\n".$key['userid']." :  ".$key['checktime']."  insert tidak";
    }
  }
  echo "\n".$total."\n";

    }
}
