<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use App\Mail\emailDokument;

class cekDokument extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dokument';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'cek apakah ada dokument yang akan expired dalam rentang waktu tertentu ( 3 bulan,2 bulan, 1 bulan, 15 hari, 5 hari, expired)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $email = 'hris.absensi@hpam.co.id';
      $bulan_sekarang = date('m');
      $hari_sekarang = date('d');
      $ultah = date("j F Y");
      $sekarang = date("Y-m-d");
      $bulan_6 = date('Y-m-d', strtotime("+6 month",strtotime($sekarang)));//6bulan
      $bulan_3 = date('Y-m-d', strtotime("+3 month",strtotime($sekarang)));//3bulan
      $bulan_2 = date('Y-m-d', strtotime("+2 month",strtotime($sekarang)));//2bulan
      $bulan_1 = date('Y-m-d', strtotime("+1 month",strtotime($sekarang)));//1bulan
      $hari_15 = date('Y-m-d', strtotime("+15 days",strtotime($sekarang)));//15 hari
      $hari_10 = date('Y-m-d', strtotime("+10 days",strtotime($sekarang)));//10 hari
      $hari_5 = date('Y-m-d', strtotime("+5 days",strtotime($sekarang)));//5 hari


      //6 bulan
      $dokumen_6_bulan = DB::table('kelengkapan_documents')
                  ->select('karyawans.nama_karyawan','karyawans.email','kelengkapan_documents.*')
                  ->leftjoin('karyawans','kelengkapan_documents.karyawan_id','karyawans.id')
                  ->where('kelengkapan_documents.tanggal_berakhir',$bulan_6)
                  ->where('karyawans.status_kerja','Aktif')
                  ->get();
      $dokumen_3_bulan = DB::table('kelengkapan_documents')
                  ->select('karyawans.nama_karyawan','karyawans.email','kelengkapan_documents.*')
                  ->leftjoin('karyawans','kelengkapan_documents.karyawan_id','karyawans.id')
                  ->where('kelengkapan_documents.tanggal_berakhir',$bulan_3)
                  ->where('karyawans.status_kerja','Aktif')
                  ->get();
      $dokumen_2_bulan = DB::table('kelengkapan_documents')
                  ->select('karyawans.nama_karyawan','karyawans.email','kelengkapan_documents.*')
                  ->leftjoin('karyawans','kelengkapan_documents.karyawan_id','karyawans.id')
                  ->where('kelengkapan_documents.tanggal_berakhir',$bulan_2)
                  ->where('karyawans.status_kerja','Aktif')
                  ->get();
      $dokumen_1_bulan = DB::table('kelengkapan_documents')
                  ->select('karyawans.nama_karyawan','karyawans.email','kelengkapan_documents.*')
                  ->leftjoin('karyawans','kelengkapan_documents.karyawan_id','karyawans.id')
                  ->where('kelengkapan_documents.tanggal_berakhir',$bulan_1)
                  ->where('karyawans.status_kerja','Aktif')
                  ->get();
      $dokumen_15_hari = DB::table('kelengkapan_documents')
                  ->select('karyawans.nama_karyawan','karyawans.email','kelengkapan_documents.*')
                  ->leftjoin('karyawans','kelengkapan_documents.karyawan_id','karyawans.id')
                  ->where('kelengkapan_documents.tanggal_berakhir',$hari_15)
                  ->where('karyawans.status_kerja','Aktif')
                  ->get();
      $dokumen_10_hari = DB::table('kelengkapan_documents')
                  ->select('karyawans.nama_karyawan','karyawans.email','kelengkapan_documents.*')
                  ->leftjoin('karyawans','kelengkapan_documents.karyawan_id','karyawans.id')
                  ->where('kelengkapan_documents.tanggal_berakhir',$hari_10)
                  ->where('karyawans.status_kerja','Aktif')
                  ->get();
      $dokumen_5_hari = DB::table('kelengkapan_documents')
                  ->select('karyawans.nama_karyawan','karyawans.email','kelengkapan_documents.*')
                  ->leftjoin('karyawans','kelengkapan_documents.karyawan_id','karyawans.id')
                  ->where('kelengkapan_documents.tanggal_berakhir',$hari_5)
                  ->where('karyawans.status_kerja','Aktif')
                  ->get();
      $dokumen_0_hari = DB::table('kelengkapan_documents')
                  ->select('karyawans.nama_karyawan','karyawans.email','kelengkapan_documents.*')
                  ->leftjoin('karyawans','kelengkapan_documents.karyawan_id','karyawans.id')
                  ->where('kelengkapan_documents.tanggal_berakhir',$sekarang)
                  ->where('karyawans.status_kerja','Aktif')
                  ->get();
      $dokumen_x_hari = DB::table('kelengkapan_documents')
                ->select('karyawans.nama_karyawan','karyawans.email','kelengkapan_documents.*')
                ->leftjoin('karyawans','kelengkapan_documents.karyawan_id','karyawans.id')
                ->where('kelengkapan_documents.tanggal_berakhir','<',$sekarang)
                ->where('karyawans.status_kerja','Aktif')
                ->get();
      //loop 6 bulan
      foreach ($dokumen_6_bulan as $key) {
        $sekarang_tgl = new \DateTime($sekarang);
        $expired_tgl = new \DateTime($key->tanggal_berakhir);
        $selisih_hari = $expired_tgl->diff($sekarang_tgl)->format("%a");
        $tgl_oke = date('j F Y', strtotime("+0 days",strtotime($key->tanggal_berakhir)));
        $content = [
                'nama_dokumen'=>$key->nama_dokumen,
                'nomor_dokumen'=>$key->nomor_dokumen,
                'tgl_expired'=>$tgl_oke,
                'nama_karyawan'=>$key->nama_karyawan,
                'scan_dokumen'=>$key->scan_dokumen,
                'jumlah_hari'=>$selisih_hari,
                'status'=>'akan berakhir dalam'
              ];
        if($key->email == NULL || $key->email == ''){
          Mail::to($email)->send(new emailDokument($content));
        }else{
          Mail::to($key->email)->send(new emailDokument($content));
          Mail::to($email)->send(new emailDokument($content));
        }

      }

      //loop 3 bulan
      foreach ($dokumen_3_bulan as $key) {
        $sekarang_tgl = new \DateTime($sekarang);
        $expired_tgl = new \DateTime($key->tanggal_berakhir);

        $selisih_hari = $expired_tgl->diff($sekarang_tgl)->format("%a");
        $tgl_oke = date('j F Y', strtotime("+0 days",strtotime($key->tanggal_berakhir)));
        $content = [
                'nama_dokumen'=>$key->nama_dokumen,
                'nomor_dokumen'=>$key->nomor_dokumen,
                'tgl_expired'=>$tgl_oke,
                'nama_karyawan'=>$key->nama_karyawan,
                'scan_dokumen'=>$key->scan_dokumen,
                'jumlah_hari'=>$selisih_hari,
                'status'=>'akan berakhir dalam'
              ];
        if($key->email == NULL || $key->email == ''){
          Mail::to($email)->send(new emailDokument($content));
        }else{
          Mail::to($key->email)->send(new emailDokument($content));
          Mail::to($email)->send(new emailDokument($content));
        }
      }

      //loop 2 bulan
      foreach ($dokumen_2_bulan as $key) {
        $sekarang_tgl = new \DateTime($sekarang);
        $expired_tgl = new \DateTime($key->tanggal_berakhir);

        $selisih_hari = $expired_tgl->diff($sekarang_tgl)->format("%a");
        $tgl_oke = date('j F Y', strtotime("+0 days",strtotime($key->tanggal_berakhir)));
        $content = [
                'nama_dokumen'=>$key->nama_dokumen,
                'nomor_dokumen'=>$key->nomor_dokumen,
                'tgl_expired'=>$tgl_oke,
                'nama_karyawan'=>$key->nama_karyawan,
                'scan_dokumen'=>$key->scan_dokumen,
                'jumlah_hari'=>$selisih_hari,
                'status'=>'akan berakhir dalam'
              ];
        if($key->email == NULL || $key->email == ''){
          Mail::to($email)->send(new emailDokument($content));
        }else{
          Mail::to($key->email)->send(new emailDokument($content));
          Mail::to($email)->send(new emailDokument($content));
        }
      }

      //loop 1 bulan
      foreach ($dokumen_1_bulan as $key) {
        $sekarang_tgl = new \DateTime($sekarang);
        $expired_tgl = new \DateTime($key->tanggal_berakhir);

        $selisih_hari = $expired_tgl->diff($sekarang_tgl)->format("%a");
        $tgl_oke = date('j F Y', strtotime("+0 days",strtotime($key->tanggal_berakhir)));
        $content = [
                'nama_dokumen'=>$key->nama_dokumen,
                'nomor_dokumen'=>$key->nomor_dokumen,
                'tgl_expired'=>$tgl_oke,
                'nama_karyawan'=>$key->nama_karyawan,
                'scan_dokumen'=>$key->scan_dokumen,
                'jumlah_hari'=>$selisih_hari,
                'status'=>'akan berakhir dalam'
              ];
        if($key->email == NULL || $key->email == ''){
          Mail::to($email)->send(new emailDokument($content));
        }else{
          Mail::to($key->email)->send(new emailDokument($content));
          Mail::to($email)->send(new emailDokument($content));
        }
      }

      //loop 15 hari
      foreach ($dokumen_15_hari as $key) {
        $sekarang_tgl = new \DateTime($sekarang);
        $expired_tgl = new \DateTime($key->tanggal_berakhir);

        $selisih_hari = $expired_tgl->diff($sekarang_tgl)->format("%a");
        $tgl_oke = date('j F Y', strtotime("+0 days",strtotime($key->tanggal_berakhir)));
        $content = [
                'nama_dokumen'=>$key->nama_dokumen,
                'nomor_dokumen'=>$key->nomor_dokumen,
                'tgl_expired'=>$tgl_oke,
                'nama_karyawan'=>$key->nama_karyawan,
                'scan_dokumen'=>$key->scan_dokumen,
                'jumlah_hari'=>$selisih_hari,
                'status'=>'akan berakhir dalam'
              ];
        if($key->email == NULL || $key->email == ''){
          Mail::to($email)->send(new emailDokument($content));
        }else{
          Mail::to($key->email)->send(new emailDokument($content));
          Mail::to($email)->send(new emailDokument($content));
        }
      }

      //loop 10 hari
      foreach ($dokumen_10_hari as $key) {
        $sekarang_tgl = new \DateTime($sekarang);
        $expired_tgl = new \DateTime($key->tanggal_berakhir);

        $selisih_hari = $expired_tgl->diff($sekarang_tgl)->format("%a");
        $tgl_oke = date('j F Y', strtotime("+0 days",strtotime($key->tanggal_berakhir)));
        $content = [
                'nama_dokumen'=>$key->nama_dokumen,
                'nomor_dokumen'=>$key->nomor_dokumen,
                'tgl_expired'=>$tgl_oke,
                'nama_karyawan'=>$key->nama_karyawan,
                'scan_dokumen'=>$key->scan_dokumen,
                'jumlah_hari'=>$selisih_hari,
                'status'=>'akan berakhir dalam'
              ];
        if($key->email == NULL || $key->email == ''){
          Mail::to($email)->send(new emailDokument($content));
        }else{
          Mail::to($key->email)->send(new emailDokument($content));
          Mail::to($email)->send(new emailDokument($content));
        }
      }

      //loop 5 hari
      foreach ($dokumen_5_hari as $key) {
        $sekarang_tgl = new \DateTime($sekarang);
        $expired_tgl = new \DateTime($key->tanggal_berakhir);

        $selisih_hari = $expired_tgl->diff($sekarang_tgl)->format("%a");
        $tgl_oke = date('j F Y', strtotime("+0 days",strtotime($key->tanggal_berakhir)));
        $content = [
                'nama_dokumen'=>$key->nama_dokumen,
                'nomor_dokumen'=>$key->nomor_dokumen,
                'tgl_expired'=>$tgl_oke,
                'nama_karyawan'=>$key->nama_karyawan,
                'scan_dokumen'=>$key->scan_dokumen,
                'jumlah_hari'=>$selisih_hari,
                'status'=>'akan berakhir dalam'
              ];
        if($key->email == NULL || $key->email == ''){
          Mail::to($email)->send(new emailDokument($content));
        }else{
          Mail::to($key->email)->send(new emailDokument($content));
          Mail::to($email)->send(new emailDokument($content));
        }
      }
      //loop 0 hari
      foreach ($dokumen_0_hari as $key) {
        $sekarang_tgl = new \DateTime($sekarang);
        $expired_tgl = new \DateTime($key->tanggal_berakhir);

        $selisih_hari = $expired_tgl->diff($sekarang_tgl)->format("%a");
        $tgl_oke = date('j F Y', strtotime("+0 days",strtotime($key->tanggal_berakhir)));
        $content = [
                'nama_dokumen'=>$key->nama_dokumen,
                'nomor_dokumen'=>$key->nomor_dokumen,
                'tgl_expired'=>$tgl_oke,
                'nama_karyawan'=>$key->nama_karyawan,
                'scan_dokumen'=>$key->scan_dokumen,
                'jumlah_hari'=>$selisih_hari,
                'status'=>'akan berakhir dalam'
              ];
        if($key->email == NULL || $key->email == ''){
          Mail::to($email)->send(new emailDokument($content));
        }else{
          Mail::to($key->email)->send(new emailDokument($content));
          Mail::to($email)->send(new emailDokument($content));
        }
      }

      //loop x hari
      foreach ($dokumen_x_hari as $key) {
        $sekarang_tgl = new \DateTime($sekarang);
        $expired_tgl = new \DateTime($key->tanggal_berakhir);

        $selisih_hari = $expired_tgl->diff($sekarang_tgl)->format("%a");
        $tgl_oke = date('j F Y', strtotime("+0 days",strtotime($key->tanggal_berakhir)));
        $content = [
                'nama_dokumen'=>$key->nama_dokumen,
                'nomor_dokumen'=>$key->nomor_dokumen,
                'tgl_expired'=>$tgl_oke,
                'nama_karyawan'=>$key->nama_karyawan,
                'scan_dokumen'=>$key->scan_dokumen,
                'jumlah_hari'=>$selisih_hari,
                'status'=>'sudah melewati masa berlakunya selama '
              ];
        if($key->email == NULL || $key->email == ''){
          Mail::to($email)->send(new emailDokument($content));
        }else{
          Mail::to($key->email)->send(new emailDokument($content));
          Mail::to($email)->send(new emailDokument($content));
        }
      }
    }
}
