<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class absensi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'absensi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ambil data import absensi ke database hris';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //class untuk aktifitas crud by zuliantoe@gmail.com
//silahkan digunakan untuk belajar, ada yang kurang jelas bisa email saya
//koneksi ke lokal db
define('DB_SERVER','127.0.0.1');
define('DB_USER','root');
define('DB_PASSWORD','NewPthpam25t@');
define('DB_NAME','hpam_hris');


class crud_lokal{
	private $koneksi;

	public function __construct(){
  		//$this->koneksi = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
			$this->koneksi = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME."", DB_USER, DB_PASSWORD);
			$this->koneksi->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 	}

 	public function __destruct(){
		$this->koneksi = null;
 	}

	public function create(array $data){
		$db=get_class($this);
		$kolom=$this->kolom;
		$input="";
		$prepared="";
		$col=explode(',', $kolom);
		$panjang=count($col)-1;
		$x=0;
		//tentukan parameternya
		foreach ($col as $key=>$value) {
			if($key==$panjang){
				$prepared.=":$value";
			}else{
				$prepared.=":$value,";
			}
		}
		//bind parameter
		$insert = $this->koneksi->prepare("INSERT INTO ".$db." (".$kolom.")
    												VALUES (".$prepared.")");
		foreach ($col as $key) {
				if(array_key_exists($key, $data)){
					$insert->bindParam(':'.$key, $data[$key]);
				}else {
					$insert->bindParam(':'.$key,'');
				}
			}
		//$query="INSERT INTO ".$db."(".$kolom.") VALUES (".$input.")";
    $hasil=$insert->execute();
    //return $prepared;
		if($hasil){
				return $this->koneksi->lastInsertId();
			} else {
				return false;
		}
	}

	public function read($id){
		if($id > 0){
			$db=get_class($this);
			// $query="SELECT * FROM ".$db." WHERE id=".$id."";
			// $hasil=$this->koneksi->query($query);
			$read = $this->koneksi->prepare("SELECT * FROM ".$db." WHERE id=:id ");
			$read->bindParam(':id',$id);
			$read->execute();
			$result=$read->fetch(PDO::FETCH_ASSOC);
			return $result;
		} else {
			//gunakan id 0 untuk membaca semua
			$db=get_class($this);
			$read = $this->koneksi->prepare("SELECT * FROM ".$db);
			$read->execute();
			$result=$read->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

	}

	public function where($col,$val){
		$db=get_class($this);
		$where= $this->koneksi->prepare("SELECT * FROM ".$db." WHERE ".$col." = :column " );
		$where->bindParam(':column',$val);
		$where->execute();
		$result=$where->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function update($id,array $data){
		$db=get_class($this);
		$kolom=$this->kolom;
		//prepared
		$col=explode(',', $kolom);
		$set="";
		foreach ($data as $key=>$value) {
				if(array_search($key, $col)>=0){
					$set.=$key." = :$key,";
				}

			}
		$set_array=explode(',',$set);
		$panjang=count($set_array)-2;
		$set="";
		for($x=0;$x<=$panjang;$x++){
			if($x < $panjang){
				$set.=$set_array[$x]." ,";
			} else {
				$set.=$set_array[$x];
			}

		}

		//prepared statement
		$update=$this->koneksi->prepare("UPDATE $db SET $set WHERE id=:idnya");
		//bind parameter
		$update->bindParam(':idnya',$id);
		foreach ($data as $key => $value) {
			$update->bindParam(':'.$key,$data[$key]);
		}

		if($update->execute()){
			return true;
		} else{
			return false;
		}
	}

	public function delete($id){
		$db=get_class($this);
		$query="DELETE FROM ".$db." WHERE id=:id";
		$hapus=$this->koneksi->prepare($query);
		$hapus->bindParam(':id',$id);
		if($hapus->execute()){
			return true;
		}else{
			return false;
		}
	}

	public function runQuery($query, array $value){
		//runn query dengan hasil
		$hasil=$this->koneksi->prepare($query);
		$hasil->execute($value);
		$data = $hasil->fetchAll(PDO::FETCH_ASSOC);
		return $data;
	}

	public function aquery($query,array $value){
		//run query tanpa hasil
		$hasil=$this->koneksi->prepare($query);
		$result=$hasil->execute($value);
		return $result;
	}

}

//nama table adalah nama clas dan nama field di variable kolom

class  sisa_cutis extends crud_lokal{
	protected $kolom='karyawan_id,data_cuti_id,periode_awal,periode_akhir,periode_tambahan,sisa,saldo_awal,jumlah_awal,diambil';
}

// penggunaan
// $tabel = new namaTable;
// $tabel->read($id);
// $tabel->create(array('kolom'=>'value'));
// $tabel->where('kolom','value');
// $tabel->update($id,array('kolom'=>'value'));
// $tabel->delete($id);
// $query="SELECT * FROM table WHERE kolom = ? AND kolom2 = ?";
// $value=array(value1,value2)
// $tabel->runQuery($query,$value);//query dengan kembalian array hasil ex. select
// $query="UPDATE table SET kolom = ? , kolom2 = ?";
// $value=array(value1,value2)
// $tabel->aQuery($query,$value);//query tanpa kembalian hasil hanya true/false ex. update, delete

date_default_timezone_set("Asia/Jakarta");
//require "crud.php";
// oke ini

$datanya=new crud_lokal;
$sekarang=date('Y-m-d');
$tahun_sekarang = date('Y');
$karyawan=$datanya->runQuery("SELECT id,tgl_gabung FROM karyawans WHERE status_kerja = ?",array('Aktif') );
$kemarin=intval($tahun_sekarang-1);
foreach ($karyawan as $key) {
  $awal_masuk=explode('-',$key['tgl_gabung']);
  //cek apakah tanggal valid
  $benar=checkdate($awal_masuk[1],$awal_masuk[2],date('Y'));
  if($benar){
    $tahun_ini=date('Y').'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
    $tahun_kemarin=$kemarin.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
  }else{
    // $sek=$awal_masuk['2']-1;
    // $ini=date('Y').'-'.$awal_masuk[1].'-'.$sek;
    $ini=date('Y').'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
    $tahun_ini=date('Y-m-d', strtotime("+0 day",strtotime($ini)));
    $itu=$kemarin.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
    $tahun_kemarin=date('Y-m-d', strtotime("+0 day",strtotime($itu)));
  }
  // CEK APAKAH SEKARANG LEBIH BESAR DARI  tgl seharusnya
  if(date('Y-m-d') >= $tahun_ini){
    //$data['cuti_tahunan_oke']='oke';
    $periode_awal=$tahun_ini;
  }else{
    //$data['cuti_tahunan_oke']='belum';
    $periode_awal=date('Y-m-d',strtotime($tahun_kemarin));

  }







  $depan=intval(date('Y')+1);
  $tahun_depan=$depan.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
  $periode_akhir=date('Y-m-d', strtotime("-1 day",strtotime("+1 Year",strtotime($periode_awal))));
  $periode_tambahan=date('Y-m-d', strtotime("+6 month",strtotime($periode_akhir)));
  //cari di sisa cuti apakah sudah ada periode sesuai tanggal sekarang
  $setahun = strtotime("+1 year",strtotime($key['tgl_gabung']));
  $data_cuti=$datanya->runQuery("SELECT * FROM data_cutis WHERE hitung=?",array('1'));
  foreach ($data_cuti as $data_cuti) {
    $saldo_awal=0;
    $sisacuti=$datanya->runQuery("SELECT COUNT(*) as sisa FROM sisa_cutis WHERE data_cuti_id = ? AND karyawan_id = ? AND ? BETWEEN periode_awal AND periode_akhir ",array($data_cuti['id'],$key['id'],$sekarang) );
    if(strtotime($sekarang) < $setahun ){
      //belum setahun
      if($sisacuti[0]['sisa'] > 0){

      }else{
        //insert sisanya 0('userid'=>$key['userid'],'checktime'=>$key['checktime'])
        $dtnya=new sisa_cutis;
        $dtnya->create(array('karyawan_id'=>$key['id'],'data_cuti_id'=>$data_cuti['id'],'periode_awal'=>"$periode_awal",'periode_akhir'=>"$periode_akhir",'periode_tambahan'=>"$periode_tambahan",'sisa'=>'0','saldo_awal'=>'0','jumlah_awal'=>'0','diambil'=>'0'));
      }
    }else{
      //sudah setahun
      if($sisacuti[0]['sisa'] > 0){

      }else{
        //insert sisanya 12
        // cek apakah sudah 5 tahun
        //cek apakah sudah 5 tahun / kelipatan 5 tahun
        $tgl1 = new DateTime($key['tgl_gabung']);
        $tgl2 = new DateTime($sekarang);
        $selisih_tahun=$tgl2->diff($tgl1);
        $total_tahun=$selisih_tahun->y;
        $ckck = new crud_lokal;
        $sisa_kemaren=$ckck->runQuery("SELECT id,sisa FROM sisa_cutis WHERE data_cuti_id = ? AND karyawan_id = ? AND periode_akhir < ? ORDER BY periode_awal DESC ",array($data_cuti['id'],$key['id'],$sekarang) );
        $sisanya = $data_cuti['jumlah'];
        $jumlah_awal=$data_cuti['jumlah'];
        $ada = count($sisa_kemaren);
        if($ada > 0 ){
          if($sisa_kemaren[0]['sisa'] < 0){
            $sisanya = $sisanya + $sisa_kemaren[0]['sisa'];
            $saldo_awal = $sisanya;
          }else{
            $saldo_awal = $sisanya + $sisa_kemaren[0]['sisa'];
          }
        }

        if($total_tahun < 5 ){
          //belum 5 tahun
          $sisa=$sisanya;
        }else{
          //sudah 5 tahun
          if($total_tahun%5 == 0){
            //cek apakah kelipatan 5
            $sisa=$sisanya + 9;
            $jumlah_awal=$data_cuti['jumlah']+9;
            $saldo_awal=$saldo_awal + 9;
          }else{
            $sisa=$sisanya;
          }

        }
        $dtnya=new sisa_cutis;
        $dtnya->create(array('karyawan_id'=>$key['id'],'data_cuti_id'=>$data_cuti['id'],'periode_awal'=>"$periode_awal",'periode_akhir'=>"$periode_akhir",'periode_tambahan'=>"$periode_tambahan",'sisa'=>$sisa,'saldo_awal'=>$saldo_awal,'jumlah_awal'=>$jumlah_awal,'diambil'=>'0'));
      }

    }
    //enol kan sisa cuti yang expired tetapi sisanya masih diatas 0
    $sisacuti_sekarang=$datanya->runQuery("SELECT id,sisa FROM sisa_cutis WHERE data_cuti_id = ? AND karyawan_id = ? AND periode_tambahan < ? ORDER BY periode_awal DESC",array($data_cuti['id'],$key['id'],$sekarang) );
    if(count($sisacuti_sekarang) > 0){
      if($sisacuti_sekarang[0]['sisa'] > 0){
        //update jadi 0
        $update = new sisa_cutis;
        $update->update($sisacuti_sekarang[0]['id'],array('sisa'=>0));
      }
    }


  }



}

    }
}
