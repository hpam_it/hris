<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use App\Mail\emailUlangTahun;

class cekUlangTahun extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ulangTahun';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cek siapa yang ulang tahun dan kirim email ke hpam all';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //cek yang ulang tahun hari ini
        $email = 'hpam@hpam.co.id';
        $bulan_sekarang = date('m');
        $hari_sekarang = date('d');
        $ultah = date("j F Y");
        $karyawan = DB::table('karyawans')
                    ->leftjoin('kantors','karyawans.kantor_id','kantors.id')
                    ->where('status_kerja','Aktif')
                    ->whereRaw('MONTH(tgl_lahir) = '.$bulan_sekarang)
                    ->whereRaw('DAY(tgl_lahir) = '.$hari_sekarang)
                    ->get();
        foreach ($karyawan as $key) {
          $content = [
                  'nama_karyawan'=>$key->nama_karyawan,
                  'tgl_ultah'=>$ultah,
                  'email'=>$key->email,
                  'telpon'=>$key->telpon_kantor
                ];
         Mail::to($email)->send(new emailUlangTahun($content));
        }

    }
}
