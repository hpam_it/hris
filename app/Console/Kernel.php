<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\cekUlangTahun',
        'App\Console\Commands\cekDokument',
//	'App\Console\Commands\import_absen',
//	'App\Console\Commands\absensi',

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('ulangTahun')->dailyAt('06:00');
        $schedule->command('dokument')->dailyAt('09:00');
//	$schedule->command('importAbsen')->twiceDaily(10, 16);
//	$schedule->command('absensi')->twiceDaily(10, 16);	
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}

