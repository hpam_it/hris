<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class purchase_request extends Model
{
    protected $fillable=['divisi_id','kantor_id','karyawan_id','atasan_id','tanggal_request','alasan','keterangan'];
}
