<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class leave extends Model
{
    protected $fillable=['karyawan_id','tanggal_ijin','data_ijin_id','jam_ijin','sampai_dengan','jumlah_hari','alasan','keterangan','atasan','statushrd','alasanhrd','statusatasan','alasanatasan','alamat','nasabah_id','progress_id','nominal','tanggal_progress','alasan_progress'];
}
