<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pajak extends Model
{
  protected $fillable=['karyawan_id','bulan_pajak','total_pengurang','penghasilan_netto','total_ptkp','pkp','pph_total','pph_bulansebelum','pph_bulanini'];
}
