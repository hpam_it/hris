<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hari_libur extends Model
{
    protected $fillable=['tanggal','keterangan'];
}
