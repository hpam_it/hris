<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class mailijin extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content,$file,$id_ijin)
    {
        //
        $this->content = $content;
        $this->filenya = $file;
        $this->id_ijin = $id_ijin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->filenya!=""){
          return $this->markdown('emails.terima_ijin')
                      ->subject( $this->content['title'] )
                      ->attach(url('lihat-file/'.$this->id_ijin),
                      [
                          'as' => $this->filenya,
                          'mime' => 'image/jpeg',
                      ]
                      )
                      ->with('content',$this->content);
        }else{
          return $this->markdown('emails.terima_ijin')
                      ->subject( $this->content['title'] )
                      ->with('content',$this->content);
        }

    }
}
