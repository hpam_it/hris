<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;

class emailDokument extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content)
    {

        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.dokumen')
                    ->subject('Dokumen '.$this->content['nama_dokumen'].' / '.$this->content['nama_karyawan'].' '.$this->content['status'].' '.$this->content['jumlah_hari'].' hari')
                    ->with('content',$this->content);
    }
}
