<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ambilcuti extends Model
{
    protected $fillable=[
              'karyawan_id',
              'data_cuti_id',
              'nomor_pengajuan',
              'tanggal_cuti',
              'tanggal_tanggal',
              'jumlah_hari',
              'keperluan',
              'alamat_cuti',
              'notelpon',
              'pic_pengganti',
              'atasan',
              'statusatasan',
              'alasan_atasan',
              'statuspic',
              'alasan_pic',
              'statushrd',
              'alasan_hrd',
              'statuscuti'
              ];
}
