<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pangkat extends Model
{
    protected $fillable=['kode_pangkat','nama_pangkat'];
}
