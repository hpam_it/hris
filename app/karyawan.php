<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class karyawan extends Authenticatable
{
  protected $fillable = [
      'nama_karyawan',
      'userid',
      'npp',
      'tgl_gabung',
      'status_kerja',
      'jabatan_id',
      'divisi_id',
      'kantor_id',
      'pangkat_id',
      'tempat_lahir',
      'tgl_lahir',
      'gender',
      'status_nikah',
      'status_pajak',
      'tanggungan',
      'agama',
      'nik',
      'npwp',
      'nama_bank',
      'nama_rekening',
      'nomor_rekening_payrol',
      'nomor_cif',
      'email',
      'email_pribadi',
      'password',
      'nomor_telpon',
      'nomor_ponsel',
      'nama_kontak_darurat',
      'nomor_kontak_darurat',
      'hubungan_kontak_darurat',
      'alamat_identitas',
      'alamat_sekarang',
      'role',
      'reset_token',
      'waktu_reset',
      'jamkerjaaktif'
  ];

  protected $hidden = [
      'password','remember_token','reset_token'
  ];
}
