<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class suratjalan extends Model
{
    protected $fillable=['tujuan','tipe','dari','keterangan','tanggal','diterima','dikirim','kota'];
}
