<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class divisi extends Model
{
  protected $fillable=['kode_divisi','nama_divisi'];
}
