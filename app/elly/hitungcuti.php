<?php
namespace App\elly;
use DB;
use App\karyawan;
use App\data_cuti;
use App\ambilcuti;
use App\jabatan;
use App\sisa_cuti;
class Hitungcuti
{
  static function ambil_sisa($id_karyawan,$datacuti_id,$tgltgl){
    $tahun_ini=date('Y');
    $id = $id_karyawan;
    $karyawan = karyawan::find($id);
    $awal_masuk=explode('-',$karyawan->tgl_gabung);
    $tahun_ini=date('Y').'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
    $depan=intval(date('Y')+1);
    $tahun_depan=$depan.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
    $kemarin=intval(date('Y')-1);
    $tahun_kemarin=$kemarin.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
    $kemarinnya=intval(date('Y')-2);
    $tahun_kemarinnya=$kemarinnya.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];

    if(date('Y-m-d') >= $tahun_ini){
      //$data['cuti_tahunan_oke']='oke';
      $periode_awal=date('Y-m-d',strtotime($tahun_ini));
      $periode_kemarin=date('Y-m-d',strtotime($tahun_kemarin));
      //$periode_akhir=$tahun_depan;
      $periode_akhir=date('Y-m-d', strtotime('-1 day',strtotime($tahun_depan)));
      $periode_tambahan=date('Y-m-d', strtotime("+6 month",strtotime($periode_akhir)));
    }else{
      //$data['cuti_tahunan_oke']='belum';
      $periode_awal=date('Y-m-d',strtotime($tahun_kemarin));
      $periode_kemarin=date('Y-m-d',strtotime($tahun_kemarinnya));
      //$periode_akhir=$tahun_ini;
      $periode_akhir=date('Y-m-d', strtotime('-1 day',strtotime($tahun_ini)));
      $periode_tambahan=date('Y-m-d', strtotime("+6 month",strtotime($periode_akhir)));
    }

    $periode_awal_depan = date('Y-m-d', strtotime('+1 day',strtotime($periode_akhir)));

    $datacuti=data_cuti::find($datacuti_id);
    //cek apakah sudah 5 tahun / kelipatan 5 tahun
    $tgl1 = new \DateTime($karyawan->tgl_gabung);
    $tgl2 = new \DateTime($periode_akhir);
    $selisih_tahun=$tgl2->diff($tgl1);
    $total_tahun=$selisih_tahun->y;
    $jumlah_awal=$datacuti->jumlah;

    //sisa sisa cuti
    $cutioke=data_cuti::select('data_cutis.nama_cuti','data_cutis.hitung','data_cutis.jumlah')
              ->leftjoin('sisa_cutis as sekarang',function($join) use ($periode_awal,$id){
                $join->on('data_cutis.id', '=', 'sekarang.data_cuti_id')
                      ->where('sekarang.periode_awal',$periode_awal)
                      ->where('sekarang.karyawan_id',$id);
              })
              ->leftjoin('sisa_cutis as kemarin',function($join) use ($periode_kemarin,$id){
                $join->on('data_cutis.id', '=', 'kemarin.data_cuti_id')
                      ->where('kemarin.periode_awal',$periode_kemarin)
                      ->where('kemarin.karyawan_id',$id);;
              })
              ->leftjoin(DB::raw("(SELECT
              IFNULL((SUM(ambilcutis.jumlah_hari)),0) as total_cuti,
              data_cuti_id
              FROM
              `ambilcutis`
              WHERE
              `karyawan_id` = ".$id." and
              `ambilcutis`.`statusatasan` = 'Diterima' and
              `ambilcutis`.`statushrd` = 'Diterima' and
              `ambilcutis`.`tanggal_cuti` between '".$periode_awal."' and '".$periode_akhir."' GROUP BY data_cuti_id) rekap"),
                function ($join) {
                    $join->on('data_cutis.id', '=', 'rekap.data_cuti_id');
                })
              ->addSelect('sekarang.jumlah_awal as sekarang_awal','sekarang.diambil as sekarang_ambil','sekarang.sisa as sekarang_sisa','sekarang.periode_tambahan as sekarang_tambahan')
              ->addSelect('kemarin.jumlah_awal as kemarin_awal','kemarin.diambil as kemarin_ambil','kemarin.sisa as kemarin_sisa','kemarin.periode_tambahan as kemarin_tambahan')
              ->addSelect('rekap.total_cuti');
              $cutioke=$cutioke->where('data_cutis.id',$datacuti_id);
              $cutioke=$cutioke->groupBy('data_cutis.id')
              ->first();
        $sisa_lalu=$cutioke->kemarin_sisa;
        $expired_lalu=$cutioke->kemarin_tambahan;
        $sisa_sekarang=$cutioke->sekarang_sisa;
        $expired_sekarang=$cutioke->sekarang_tambahan;
        //kelompokan tanggal masuk yang lalu apa sekarang
        $masuk_lalu = 0;
        $masuk_sekarang = 0;
        $masuk_grace_periode = 0;
        $masuk_depan = 0;
        foreach ($tgltgl as $key) {
          if($key <= $expired_lalu ){
            $masuk_lalu = $masuk_lalu+1;
          }else if($key > $expired_lalu AND $key <= $periode_akhir ){
            //masuk periode sekarang ini
            $masuk_sekarang = $masuk_sekarang+1;
          }else if($key > $periode_akhir AND $key <= $expired_sekarang){
            //masuk periode grace periode
            $masuk_grace_periode = $masuk_grace_periode+1;
          }else if($key > $expired_sekarang){
            //masuk periode berikutnya
            $masuk_depan = $masuk_depan + 1;
          }

        }
      //sekarang hitung tiap periode nya
      $sisa_cuti_setelah_ambil_sekarang=0;
      $sisa_cuti_setelah_ambil_periode_depan=0;
      if($sisa_lalu > 0){
        $sisa = $sisa_lalu - $masuk_lalu;
        if($sisa < 0){
          //cuti lebih besar dari sisa sekarang, maka ditambahkan ke sisa sekarang
          //untuk di kurangkan ke periode ini
          $masuk_sekarang = $masuk_sekarang - ($sisa); //minus sisanya jadi kalo minus minus sama dengan +
        }
      }else{
        $masuk_sekarang = $masuk_sekarang + $masuk_lalu; // gak punya tabungan cuti maka langsung masukan ke cuti sekarang
      }

      if($sisa_sekarang <=0 AND $masuk_sekarang > 0){
        // kalo sisa sekarang sudah 0 atau minus langsung return sisa ambil
        $sisa_cuti_setelah_ambil_sekarang = $sisa_sekarang - $masuk_sekarang;//sisa sekarang
        // untuk periode depan hitung sekalian
        // sisa periode depan adalah 12 - $masuk_sekarang + $masuk_grace_periode + $masuk_depan

        if($total_tahun%5 == 0 AND $total_tahun > 0){
          //cek apakah kelipatan 5
          $sisa_cuti_setelah_ambil_periode_depan=$datacuti->jumlah - $masuk_sekarang - $masuk_grace_periode - $masuk_depan + 8;
        }else{
          $sisa_cuti_setelah_ambil_periode_depan=$datacuti->jumlah - $masuk_sekarang - $masuk_grace_periode - $masuk_depan;
        }
      }else if($sisa_sekarang > 0){
        // kalo masih ada sisa kurangi sekarang
        $sisa_2=$sisa_sekarang - $masuk_sekarang;
        //kalo minus / 0 berarti sisanya minus langsung balikin aja
        if($sisa_2 <= 0){
          // minus atau 0 sisanya langsung aja
          $sisa_cuti_setelah_ambil_sekarang = $sisa_2;
          if($total_tahun%5 == 0 AND $total_tahun > 0 ){
            //cek apakah kelipatan 5
            $sisa_cuti_setelah_ambil_periode_depan=$datacuti->jumlah + $sisa_2 - $masuk_grace_periode - $masuk_depan + 8;
          }else{
            $sisa_cuti_setelah_ambil_periode_depan=$datacuti->jumlah + $sisa_2 - $masuk_grace_periode - $masuk_depan;
          }
        }else{
          //masih ada sisa / > 0 sisanya dikurangi grace periode dulu
          $sisa_grace_periode = $sisa_2;
          $sisa_3 = $sisa_grace_periode - $masuk_grace_periode;
          if($sisa_3 <= 0){
            //minus atau nol berarti sisa sekarang 0 dengan periode depan jumlah sisanya
            $sisa_cuti_setelah_ambil_sekarang=0;
            if($total_tahun%5 == 0 AND $total_tahun > 0 ){
              //cek apakah kelipatan 5
              $sisa_cuti_setelah_ambil_periode_depan=$datacuti->jumlah + $sisa_3 - $masuk_depan + 8;
            }else{
              $sisa_cuti_setelah_ambil_periode_depan=$datacuti->jumlah + $sisa_3 - $masuk_depan;
            }
          }else{
            //kalo lebih besar 0 sisa periode ini sisa_3 sisa periode depan kurang2i
            $sisa_cuti_setelah_ambil_sekarang=$sisa_3;
            if($total_tahun%5 == 0 AND $total_tahun > 0){
              //cek apakah kelipatan 5
              $sisa_cuti_setelah_ambil_periode_depan=$datacuti->jumlah - $masuk_depan + 8;
            }else{
              $sisa_cuti_setelah_ambil_periode_depan=$datacuti->jumlah - $masuk_depan;
            }
          }
        }

      }

      $data['periode_sekarang']=$sisa_cuti_setelah_ambil_sekarang;
      $data['periode_depan']=$sisa_cuti_setelah_ambil_periode_depan;
      return $data;
  }

  static function hitung_sisa($ambil_cuti){
    $cuti=ambilcuti::find($ambil_cuti);
    $tglcuti=ambilcuti::where('nomor_pengajuan',$cuti->nomor_pengajuan)->get();
    foreach ($tglcuti as $key) {
      //tentukan periode untuk bikin sisa cuti baru
      $karyawan=karyawan::find($key->karyawan_id);
      $tahun_cuti=date('Y', strtotime($key->tanggal_cuti));
      $awal_masuk=explode('-',$karyawan->tgl_gabung);
      $tahun_ini=$tahun_cuti.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
      $depan=intval($tahun_cuti+1);
      $tahun_depan=$depan.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
      $kemarin=intval($tahun_cuti-1);
      $tahun_kemarin=$kemarin.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
      $kemarinnya=intval($tahun_cuti-2);
      $tahun_kemarinnya=$kemarinnya.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
      //akhir tentukan periode_awal
      if($key->hitung==1){
      $cek=DB::table('sisa_cutis')
              ->where('data_cuti_id',$key->data_cuti_id)
              ->where('karyawan_id',$key->karyawan_id)
              ->whereRaw("'".$key->tanggal_cuti."' BETWEEN `periode_awal` AND `periode_tambahan`")
              ->orderBy('periode_awal','DESC')
              ->get();
      $berapa=count($cek);
      if($berapa>1){
        //berarti sudah masuk periode baru
        $ambils=DB::table('sisa_cutis')
                ->where('data_cuti_id',$key->data_cuti_id)
                ->where('karyawan_id',$key->karyawan_id)
                ->whereRaw("'".$key->tanggal_cuti."' BETWEEN `periode_awal` AND `periode_tambahan`")
                ->orderBy('periode_awal','DESC')
                ->first();
        $sisaambil=$ambils->sisa - 1;
        //kurangi satu cutinya
        DB::table('sisa_cutis')
            ->where('id', $ambils->id)
            ->update(['sisa' => $sisaambil]);

      }else{
        //masih di periode kemaren
        //cek apakah periode kemaren masih ada sisa
        $ambils=DB::table('sisa_cutis')
                ->where('data_cuti_id',$key->data_cuti_id)
                ->where('karyawan_id',$key->karyawan_id)
                ->whereRaw("'".$key->tanggal_cuti."' BETWEEN `periode_awal` AND `periode_tambahan`")
                ->orderBy('periode_awal','ASC')
                ->first();
        if(count($ambils) > 0){
            $sisaambil=$ambils->sisa;
          }else{
            $sisaambil=0;
          }
        if($sisaambil<=0){
          //habis atau minus maka bikin sisa cuti Baru untuk periode berikutnya
          if($key->tanggal_cuti >= $tahun_ini){
            //$data['cuti_tahunan_oke']='oke';
            $periode_awal=$tahun_ini;
            $periode_kemarin=$tahun_kemarin;
            $periode_akhir=date('Y-m-d', strtotime("-1 day",strtotime($tahun_depan)));
            $periode_tambahan=date('Y-m-d', strtotime("+6 month",strtotime($periode_akhir)));

          }else{
            //$data['cuti_tahunan_oke']='belum';
            $periode_awal=$tahun_kemarin;
            $periode_kemarin=$tahun_kemarinnya;
            $periode_akhir=date('Y-m-d', strtotime("-1 day",strtotime($tahun_ini)));
            $periode_tambahan=date('Y-m-d', strtotime("+6 month",strtotime($periode_akhir)));
          }

          $masuk_cuti = DB::table('sisa_cutis')
                  ->where('data_cuti_id',$key->data_cuti_id)
                  ->where('karyawan_id',$key->karyawan_id)
                  ->whereRaw("'".$key->tanggal_cuti."' BETWEEN `periode_awal` AND `periode_akhir`")
                  ->count();

          //cek terlebih dahulu apakah tanggl cuti masuk di periode lama atau baru

          if($masuk_cuti > 0 ){
            //masih di periode itu jangan dibikin sisa baru
            //tinggal di kurangin terus aja
            //kurangi satu cutinya
            $sisaambil=$sisaambil - 1;
            DB::table('sisa_cutis')
                ->where('id', $ambils->id)
                ->update(['sisa' => $sisaambil]);

          }else{
            //sudah masuk periode baru
            $datacuti=data_cuti::find($key->data_cuti_id);
            $sisa=$datacuti->jumlah + ($sisaambil-1) ;
            DB::table('sisa_cutis')->insert(
                [
                  'karyawan_id' => $key->karyawan_id,
                  'data_cuti_id' => $key->data_cuti_id,
                  'periode_awal' => $periode_awal,
                  'periode_akhir' => $periode_akhir,
                  'periode_tambahan' => $periode_tambahan,
                  'sisa'=>$sisa
                ]
              );
          }

        }else{
          //kurangi satu cutinya
          $sisaambil=$ambils->sisa - 1;
          DB::table('sisa_cutis')
              ->where('id', $ambils->id)
              ->update(['sisa' => $sisaambil]);
        }
      }
      }
    }
    return 'oke';
  }

  static function kurangi_cuti($tanggal,$karyawan_id,$datacuti_id){
    $karyawan=karyawan::find($karyawan_id);
    $tahun_cuti=date('Y', strtotime($tanggal));
    $awal_masuk=explode('-',$karyawan->tgl_gabung);
    $tahun_ini=$tahun_cuti.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
    $depan=intval($tahun_cuti+1);
    $tahun_depan=$depan.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
    $kemarin=intval($tahun_cuti-1);
    $tahun_kemarin=$kemarin.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
    $kemarinnya=intval($tahun_cuti-2);
    $tahun_kemarinnya=$kemarinnya.'-'.$awal_masuk[1].'-'.$awal_masuk['2'];
    $setahun=strtotime("+1 year",strtotime($karyawan->tgl_gabung));
    $cuti=data_cuti::find($datacuti_id);
    if($cuti->hitung=='1'){
        $cek=DB::table('sisa_cutis')
                ->where('data_cuti_id',$datacuti_id)
                ->where('karyawan_id',$karyawan_id)
                ->whereRaw("'".$tanggal."' BETWEEN `periode_awal` AND `periode_tambahan`")
                ->orderBy('periode_awal','DESC')
                ->get();

        $berapa=count($cek);
        if($berapa>1){
          //berarti sudah ada row periode cuti baru
          //cek apakah periode kemarin masih ada ?
          $kemarin=DB::table('sisa_cutis')
                  ->where('data_cuti_id',$datacuti_id)
                  ->where('karyawan_id',$karyawan_id)
                  ->whereRaw("'".$tanggal."' BETWEEN periode_awal AND periode_tambahan")
                  ->orderBy('periode_awal','ASC')
                  ->first();
          //tambahan revisi kurangi cuti 
          $sisaCuti = $kemarin->jumlah_awal-$kemarin->diambil;
          if($sisaCuti > 0){
            //kemarin masih, ambil yang kemarin
            $now = date('Y-m-d');
            if(strtotime($now) > strtotime($kemarin->periode_tambahan)){
              $sisaambil = 0;
            }else{
              $sisaambil=$sisaCuti- 1;
            }
            $diambil=$kemarin->diambil+1;
            //kurangi satu cutinya
            DB::table('sisa_cutis')
                ->where('id', $kemarin->id)
                ->update(['sisa' => $sisaambil,'diambil'=>$diambil]);
          }else{
            //kemarin habis, ambil yang sekarang
            $ambils=DB::table('sisa_cutis')
                    ->where('data_cuti_id',$datacuti_id)
                    ->where('karyawan_id',$karyawan_id)
                    ->whereRaw("'".$tanggal."' BETWEEN periode_awal AND periode_tambahan")
                    ->orderBy('periode_awal','DESC')
                    ->first();
            $sisaambil=$ambils->sisa - 1;
            $diambil=$ambils->diambil+1;
            //kurangi satu cutinya
            DB::table('sisa_cutis')
                ->where('id', $ambils->id)
                ->update(['sisa' => $sisaambil,'diambil'=>$diambil]);
          }

        }else{
          //masih di periode kemaren
          //belum ada row baru
          //cek apakah periode kemaren masih ada sisa
          $apasetahun = 0;
          $saldo_awal=0;
          if(strtotime($tanggal) < $setahun){
            //belum setahun
            $apasetahun = 0;
          }else{
            //sudah setahun
            $apasetahun = 1;

          }


          $ambils=DB::table('sisa_cutis')
                  ->where('data_cuti_id',$datacuti_id)
                  ->where('karyawan_id',$karyawan_id)
                  ->whereRaw("'".$tanggal."' BETWEEN `periode_awal` AND `periode_tambahan`")
                  ->orderBy('periode_awal','DESC')
                  ->first();

          if(count($ambils) > 0){
                    //sudah ada row sisa cuti periode kemaren
                    $sisaambil=$ambils->sisa;
                    //baru satu row dan apakah sudah setahun ? kalo belum setahun jangan bikin baru
                    //ini jangan bikin row baru dulu biar sampai habis, nanti akan dibikin otomatis

            }else{
                    //belum ada row kemaren
                    $sisaambil=0;
            }
            //cek apakah cuti sudah seharusnya di periode baru ? kalo belum tolong jangan bikin sisa cuti baru, minusin aja yang sekarang aja
            $sekarang_sudah = DB::table('sisa_cutis')
                    ->where('data_cuti_id',$datacuti_id)
                    ->where('karyawan_id',$karyawan_id)
                    ->whereRaw("'".$tanggal."' BETWEEN `periode_awal` AND `periode_akhir`")
                    ->orderBy('periode_awal','DESC')
                    ->first();

          if($sisaambil<=0 AND count($sekarang_sudah) < 1 ){
            //habis atau minus periode sebelumnya dan sudah masuk periode baru maka bikin sisa cuti Baru
            //ini cek dulu apakah sudah setahun apa belum
            if($apasetahun == 0){
              //belum setahun
              $sisa=-1;
              $jumlah_awal=0;
            }else{
              //sudah setahun
              $datacuti=data_cuti::find($datacuti_id);
              //cek apakah sudah 5 tahun / kelipatan 5 tahun
              $tgl1 = new \DateTime($karyawan->tgl_gabung);
              $tgl2 = new \DateTime($tanggal);
              $selisih_tahun=$tgl2->diff($tgl1);
              $total_tahun=$selisih_tahun->y;
              $jumlah_awal=$datacuti->jumlah;
              if($total_tahun < 5 ){
                //belum 5 tahun
                $sisa=$datacuti->jumlah + $sisaambil - 1;
                $saldo_awal=$datacuti->jumlah + $sisaambil;
              }else{
                //sudah 5 tahun
                if($total_tahun%5 == 0){
                  //cek apakah kelipatan 5
                  $sisa=$datacuti->jumlah + $sisaambil + 8;
                  $jumlah_awal=$datacuti->jumlah+9;
                  $saldo_awal=$datacuti->jumlah + $sisaambil + 9;
                }else{
                  $sisa=$datacuti->jumlah + $sisaambil - 1;
                }

              }

            }
            if(date('Y-m-d') >= $tahun_ini){
              //$data['cuti_tahunan_oke']='oke';
              $periode_awal=date('Y-m-d',strtotime($tahun_ini));
              $periode_kemarin=date('Y-m-d',strtotime($tahun_kemarin));
              //$periode_akhir=$tahun_depan;

              $periode_akhir=date('Y-m-d', strtotime('-1 day',strtotime($tahun_depan)));
              $periode_tambahan=date('Y-m-d', strtotime("+6 month",strtotime($periode_akhir)));
            }else{
              //$data['cuti_tahunan_oke']='belum';
              $periode_awal=date('Y-m-d',strtotime($tahun_kemarin));
              $periode_kemarin=date('Y-m-d',strtotime($tahun_kemarinnya));
              //$periode_akhir=$tahun_ini;

              $periode_akhir=date('Y-m-d', strtotime('-1 day',strtotime($tahun_ini)));
              $periode_tambahan=date('Y-m-d', strtotime("+6 month",strtotime($periode_akhir)));
            }
            DB::table('sisa_cutis')->insert(
                [
                  'karyawan_id' => $karyawan_id,
                  'data_cuti_id' => $datacuti_id,
                  'periode_awal' => $periode_awal,
                  'periode_akhir' => $periode_akhir,
                  'periode_tambahan' => $periode_tambahan,
                  'jumlah_awal' => $jumlah_awal,
                  'saldo_awal' => $saldo_awal,
                  'diambil'=>1,
                  'sisa'=>$sisa
                ]
              );
          }else{
            //kurangi satu cutinya
            $sisaambil=$ambils->sisa - 1;
            $diambil=$ambils->diambil+1;
            DB::table('sisa_cutis')
                ->where('id', $ambils->id)
                ->update(['sisa' => $sisaambil,'diambil'=>$diambil]);
          }
        }
    }
  }


  static function cek_approval($ambilcuti_id){
    $ambilcuti=ambilcuti::find($ambilcuti_id);
    if($ambilcuti->statusatasan=='Diterima'){
      $atasan=1;
    }else{
      $atasan=0;
    }

    if($ambilcuti->statushrd=='Diterima'){
      $hrd=1;
    }else{
      $hrd=0;
    }
    //jika butuh persetujuan direksi
    if($ambilcuti->persetujuan_direksi==1){
      //cek apakah ada pending / Ditolak
      $pending = stripos($ambilcuti->status_direksi,'pending');
      if($pending > 0){
        $direksi_pending=1;
      }else{
        $direksi_pending=0;
      }
      $reject = stripos($ambilcuti->status_direksi,'Ditolak');
      if($reject > 0){
        $direksi_reject=1;
      }else{
        $direksi_reject=0;
      }
      if($atasan==1 AND $hrd==1 AND $direksi_pending == 0 AND $direksi_reject == 0){
        return 1;
      }else{
        return 0;
      }
    }else{
      if($atasan==1 AND $hrd==1){
        return 1;
      }else{
        return 0;
      }
    }
  }
}
