<?php
namespace App\elly;
use DB;
use App\karyawan;
use App\jabatan;
use App\divisi;
use App\kantor;
use App\struktur;

class Helper
{
  static function balik_tgl($tgl,$pemisah){
    $tanggal = explode('-',$tgl);
    return $tanggal[2].$pemisah.$tanggal[1].$pemisah.$tanggal[0];
  }

  static function tgl_bulan($tgl,$pemisah){
    $tanggal = explode('-',$tgl);
    switch ($tanggal[1]) {
      case '1':
        return $tanggal[2].$pemisah.'Jan'.$pemisah.$tanggal[0];
        break;
      case '2':
        return $tanggal[2].$pemisah.'Feb'.$pemisah.$tanggal[0];
        break;
      case '3':
        return $tanggal[2].$pemisah.'Mar'.$pemisah.$tanggal[0];
        break;
      case '4':
        return $tanggal[2].$pemisah.'Apr'.$pemisah.$tanggal[0];
        break;
      case '5':
        return $tanggal[2].$pemisah.'Mei'.$pemisah.$tanggal[0];
        break;
      case '6':
        return $tanggal[2].$pemisah.'Jun'.$pemisah.$tanggal[0];
        break;
      case '7':
        return $tanggal[2].$pemisah.'Jul'.$pemisah.$tanggal[0];
        break;
      case '8':
        return $tanggal[2].$pemisah.'Agu'.$pemisah.$tanggal[0];
        break;
      case '9':
        return $tanggal[2].$pemisah.'Sep'.$pemisah.$tanggal[0];
        break;
      case '10':
        return $tanggal[2].$pemisah.'Okt'.$pemisah.$tanggal[0];
        break;
      case '11':
        return $tanggal[2].$pemisah.'Nop'.$pemisah.$tanggal[0];
        break;
      case '12':
        return $tanggal[2].$pemisah.'Des'.$pemisah.$tanggal[0];
        break;
      default:
        return '00-00-0000';
        break;
    }
  }
  static function balik_tgl_jam($tgl,$pemisah){
    $tanggal=explode(' ',$tgl);
    $tanggalnya=explode('-',$tanggal[0]);
    $jamnya=explode(':',$tanggal[1]);
    return $tanggalnya[2].$pemisah.$tanggalnya[1].$pemisah.$tanggalnya[0].' '.$jamnya[0].':'.$jamnya[1];
  }
  static function cariatasan($karyawanid){
    $karyawan=karyawan::find($karyawanid);
    //cek dulu apakah strukturnya sudah ada
    $cekstruktur = struktur::where('divisi_id',$karyawan->divisi_id)
                          ->where('jabatan_id',$karyawan->jabatan_id)
                          ->where('kantor_id',$karyawan->kantor_id)
                          ->count();

    if($cekstruktur == 0){
      //belum ada struktur
      return 'struktur kosong';
    }else{
      $struktur = struktur::where('divisi_id',$karyawan->divisi_id)
                            ->where('jabatan_id',$karyawan->jabatan_id)
                            ->where('kantor_id',$karyawan->kantor_id)
                            ->first();

      if($struktur->atasan === 0){
        //atasannya diri sendiri atau tidak punya atasan
        return $karyawanid;
      }else{
        //ada atasan di struktur
        $struktur_atasan = struktur::find($struktur->atasan);
        //cek dulu apakah ada karyawan dengan struktur atasan itu
        $cekatasan = karyawan::where('divisi_id',$struktur_atasan->divisi_id)
                              ->where('jabatan_id',$struktur_atasan->jabatan_id)
                              ->where('kantor_id',$struktur_atasan->kantor_id)
                              ->where('status_kerja','Aktif')
                              ->count();
        if($cekatasan > 0){
          //ada karyawannya
          $atasan = karyawan::where('divisi_id',$struktur_atasan->divisi_id)
                                ->where('jabatan_id',$struktur_atasan->jabatan_id)
                                ->where('kantor_id',$struktur_atasan->kantor_id)
                                ->where('status_kerja','Aktif')
                                ->first();
            return $atasan->id;
        }else{
          //tidak ada atasannya
          return 'atasan kosong';
        }
      }
    }
  }

  static function caridireksi($karyawanid){
    $karyawan = karyawan::find($karyawanid);
    $struktur = struktur::where('divisi_id',$karyawan->divisi_id)
                          ->where('jabatan_id',$karyawan->jabatan_id)
                          ->where('kantor_id',$karyawan->kantor_id)
                          ->first();
        ///atasan level pertama
        $direksi=0;
        if($struktur->atasan == 0){
          $atasan_sekarang = $struktur->id;
        }else{
          $atasan_sekarang = $struktur->atasan;
        }

        while($atasan_sekarang >=0){
            $atasan = struktur::find($atasan_sekarang);
            //cari karyawannya dan apakah role nya direktur ?
            $atasan_karyawan = karyawan::where('divisi_id',$atasan->divisi_id)
                              ->where('jabatan_id',$atasan->jabatan_id)
                              ->where('kantor_id',$atasan->kantor_id)
                              ->where('status_kerja','Aktif')
                              ->first();
            if($atasan->atasan==0){
              $atasan_sekarang = -1;
            }
            if($atasan_karyawan->role=='direktur'){
              $direksi = $atasan_karyawan->id;
              $atasan_sekarang = -1;
            }else {
              $atasan_sekarang = $atasan->atasan;
            }

        }

        return $direksi;
  }
}
