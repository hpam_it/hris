<?php
namespace App\elly;
use DB;
class Rekapabsensi
{
    public function rasio_absen($karyawan, $tgl_awal, $tgl_akhir, $param)
    {
        $total_jam_kerja = 0;
        $total_harus = 0;
        $total_cuti = 0;
        $total_dinas = 0;
        $total_ijin = 0;
        $total_mangkir = 0;
        $harus_hari_kerja = 0;
        $hari_kerja = 0;
        $hari_ijin = 0;
        $libur_nasional = 0;
        $hari_aktif = 0;
        $hari_ijin = 0;
        $ijin_semua = 0;
        $ayo = $tgl_awal;
        while (strtotime($ayo) <= strtotime($tgl_akhir))
        {
            $ada_ijin = 0;
            $timestamp = strtotime($ayo);
            $day = date('w', $timestamp);
            $libur = DB::table('hari_liburs')->where('tanggal', $ayo)->first();
            $absen = DB::table('karyawans')->addSelect(DB::raw('MIN(absens.checktime) as cek_in'))
                ->addSelect(DB::raw('MAX(absens.checktime) as cek_out'))
                ->addSelect(DB::raw('DATE(absens.checktime) as tanggal'))
                ->leftjoin('absens', 'karyawans.userid', '=', 'absens.userid')
                ->addSelect('karyawans.jamkerjaaktif')
                ->whereRaw("DATE(absens.checktime) = '$ayo' ")->where('karyawans.id', $karyawan)->groupBy('tanggal')
                ->first();
            if (count($absen) == 0)
            {
                $absen = (object)[];
                $absen->cek_in = '00:00:00';
                $absen->cek_out = '00:00:00';
                $absen->tanggal = $ayo;
            }
            $cek_ijin = DB::table('leaves')->select('leaves.*', 'data_ijins.nama_ijin', 'data_ijins.hitung')
                ->where('leaves.karyawan_id', $karyawan)->where('leaves.tanggal_ijin', $ayo)->where('leaves.statusatasan', 'Diterima')
                ->leftjoin('data_ijins', 'leaves.data_ijin_id', '=', 'data_ijins.id')
                ->get();
            $cek_ijin_fullday = DB::table('leaves')->select('leaves.*', 'data_ijins.nama_ijin', 'data_ijins.hitung')
                ->where('leaves.karyawan_id', $karyawan)->where('leaves.tanggal_ijin', $ayo)->where('leaves.statusatasan', 'Diterima')
                ->leftjoin('data_ijins', 'leaves.data_ijin_id', '=', 'data_ijins.id')
                ->where('data_ijins.fullday', '1')
                ->get();
            $cek_cuti = DB::table('ambilcutis')->where('karyawan_id', $karyawan)->where('tanggal_cuti', '=', $ayo)->where('statusatasan', '=', 'Diterima')
                ->where('statushrd', '=', 'Diterima')->where(function ($query)
            {
                $query->where('status_direksi', 'NOT LIKE', '%pending%')
                    ->where('status_direksi', 'NOT LIKE', '%Ditolak%')
                    ->orWhereNull('status_direksi');
            })
                ->first();
            //cek apakah ada custom jam
            $dt_kar = DB::table('karyawans')->where('id', $karyawan)->first();
            $jam_aktif = $dt_kar->jamkerjaaktif;

            if ($jam_aktif != '')
            {
                //ada
                $jam_aktifnya = array_map('intval', explode(',', $jam_aktif));
                $jamkerja = DB::table('jam_kerjas')->where('tgl_mulai', '<=', $ayo)->where('tgl_selesai', '>=', $ayo)->whereIn('id', $jam_aktifnya)->orderBy('tgl_mulai', 'DESC')
                    ->first();

            }
            else
            {
                //gak ada
                $jamkerja = DB::table('jam_kerjas')->where('tgl_mulai', '<=', $ayo)->where('tgl_selesai', '>=', $ayo)->orderBy('tgl_mulai', 'DESC')
                    ->first();
            }

            $isijin = count($cek_ijin);
            $iscuti = count($cek_cuti);
            $isijin = count($cek_ijin);
            $isijinfull = count($cek_ijin_fullday);
            $ijin_mulai;
            $ijin_selesai;
            $ijinn = [];
            if ($isijin > 0)
            {
                $se = 0;
                foreach ($cek_ijin as $ijine)
                {

                    $ijin_mulai = $ijine->jam_ijin;
                    $ijin_selesai = $ijine->sampai_dengan;
                    if ($ijin_mulai == NULL or $ijin_mulai == '')
                    {
                        $ijin_mulai = $jamkerja->masuk;
                    }

                    if ($ijin_selesai == NULL or $ijin_selesai == '')
                    {
                        $ijin_selesai = $jamkerja->pulang;
                    }

                    //hitung durasi ijin
                    $c_in = new \Datetime($absen->cek_in);
                    $c_out = new \Datetime($absen->cek_out);
                    $c_mulai = new \Datetime($absen->tanggal . ' ' . $ijin_mulai);
                    $c_selesai = new \Datetime($absen->tanggal . ' ' . $ijin_selesai);

                    $j_istirahat = new \DateTime($absen->tanggal . ' ' . $jamkerja->jam_istirahat);
                    $m_pulang = new \DateTime($absen->tanggal . ' ' . $jamkerja->maxpulang);
                    $b_reak = explode(':', $jamkerja->istirahat);
                    $_istirahat = ($b_reak[0] * 3600) + ($b_reak[1] * 60) + $b_reak[2];
                    $_cek_in = new \DateTime($absen->cek_in);
                    $_masuk = new \DateTime($absen->tanggal . ' ' . $jamkerja->masuk);
                    $_pulang = new \DateTime($absen->tanggal . ' ' . $jamkerja->pulang);
                    $_toleransi = new \DateTime($absen->tanggal . ' ' . $jamkerja->toleransi);
                    $_norm = explode(':', $jamkerja->jam_normal);
                    $_normal = ($_norm[0] * 3600) + ($_norm[1] * 60) + $_norm[2];
                    if (($c_mulai > $_masuk) and ($c_mulai <= $_toleransi))
                    {
                        $dinas = $c_mulai->diff($c_selesai);
                    }
                    else if ($c_mulai <= $_masuk)
                    {
                        $dinas = $_masuk->diff($c_selesai);
                    }
                    else if ($c_mulai > $_toleransi and $c_selesai > $m_pulang)
                    {
                        $dinas = $c_mulai->diff($m_pulang);
                    }
                    else if ($c_mulai > $_toleransi)
                    {
                        $dinas = $c_mulai->diff($c_selesai);
                    }

                    $selisih_dinas = $dinas->format('%H:%I:%S');
                    $to = explode(':', $selisih_dinas);
                    $detik_dinas = ($to[0] * 3600) + ($to[1] * 60) + $to[2];

                    if (($c_mulai > $j_istirahat) or ($c_selesai < $j_istirahat))
                    {
                        $detik_dinas = $detik_dinas;
                    }
                    else
                    {
                        $detik_dinas = $detik_dinas - $_istirahat;
                    }
                    if ($detik_dinas >= $_normal)
                    {
                        $detik_dinas = $_normal;
                    }
                    elseif ($detik_dinas < 0)
                    {
                        $detik_dinas = $detik_dinas;
                    }
                    else
                    {
                        $detik_dinas = $detik_dinas;
                    }
                    //jika ijinnya termasuk hitung jam kerja
                    if ($ijine->hitung == '1')
                    {
                        if ($c_mulai < $c_in)
                        {
                            $absen->cek_in = $ayo . " " . $ijin_mulai;
                        }
                        if ($c_selesai > $c_out)
                        {
                            $absen->cek_out = $ayo . " " . $ijin_selesai;
                        }
                        $total_dinas += $detik_dinas;
                    }
                    else
                    {
                        $total_ijin += $detik_dinas;
                        $ada_ijin = 1;
                    }
                    $se++;
                }
            }

            if ($absen->cek_in == '00:00:00' and $absen->cek_out == '00:00:00')
            {
                if ($iscuti > 0)
                {
                    $absen->jam_kerja = '00:00:00';
                }
                else if ($isijinfull > 0)
                {
                    $absen->jam_kerja = '00:00:00';
                    $hari_ijin += 1;
                }
                else
                {
                    if (count($libur) > 0)
                    {
                        $absen->jam_kerja = '00:00:00';
                    }
                    else
                    {
                        if ($day == '0' || $day == '6')
                        {
                            $absen->jam_kerja = '00:00:00';
                        }
                        else
                        {
                            $absen->jam_kerja = '00:00:00';
                            $total_mangkir += 1;
                        }

                    }
                }

            }
            elseif ($absen->tanggal != null)
            {

                $absen->masuk = $jamkerja->masuk;
                $absen->pulang = $jamkerja->pulang;
                $absen->istirahat = $jamkerja->istirahat;
                $absen->jamnormal = $jamkerja->jam_normal;
                $absen->toleransi = $jamkerja->toleransi;
                $absen->maxpulang = $jamkerja->maxpulang;
                $timestamp = strtotime($absen->tanggal);
                $masuk = new \DateTime($absen->tanggal . ' ' . $absen->masuk);
                $pulang = new \DateTime($absen->tanggal . ' ' . $absen->pulang);
                $jam_istirahat = new \DateTime($absen->tanggal . ' ' . $jamkerja->jam_istirahat);
                $toleransi = new \DateTime($absen->tanggal . ' ' . $absen->toleransi);
                $maxpulang = new \DateTime($absen->tanggal . ' ' . $absen->maxpulang);
                $cek_in = new \Datetime($absen->cek_in);
                $cek_out = new \Datetime($absen->cek_out);
                //convert istirahat ke detik
                $break = explode(':', $absen->istirahat);
                $istirahat = ($break[0] * 3600) + ($break[1] * 60) + $break[2];
                $norm = explode(':', $absen->jamnormal);
                $normal = ($norm[0] * 3600) + ($norm[1] * 60) + $norm[2];
                if (($cek_in > $masuk) and ($cek_in < $toleransi))
                {
                    $selisih = $cek_in->diff($cek_out);
                    $kerja = $selisih->format('%H:%I:%S');
                }
                else if ($cek_in <= $masuk and $cek_out >= $masuk)
                {
                    $selisih = $masuk->diff($cek_out);
                    $kerja = $selisih->format('%H:%I:%S');
                }
                else if ($cek_in > $toleransi and $cek_out > $maxpulang)
                {
                    $selisih = $cek_in->diff($maxpulang);
                    $kerja = $selisih->format('%H:%I:%S');
                }
                else if ($cek_in > $toleransi)
                {
                    $selisih = $cek_in->diff($cek_out);
                    $kerja = $selisih->format('%H:%I:%S');
                }
                else
                {
                    $kerja = '00:00:00';
                }
                $krj = explode(':', $kerja);
                $detikkerja = ($krj[0] * 3600) + ($krj[1] * 60) + $krj[2];
                if (($cek_in > $jam_istirahat) or ($cek_out < $jam_istirahat))
                {
                    $totalkerja = $detikkerja;
                }
                else
                {
                    $totalkerja = $detikkerja - $istirahat;
                }

                if ($totalkerja >= $normal)
                {
                    $kerjanya = $normal;
                }
                elseif ($totalkerja < 0)
                {
                    $kerjanya = $detikkerja;
                }
                else
                {
                    $kerjanya = $totalkerja;
                }
                $absen->jam_kerja = gmdate("H:i:s", $kerjanya);
                if ($day == '0' || $day == '6')
                {
                    $absen->jam_kerja = '00:00:00';
                }
                else
                {
                    $total_jam_kerja += $kerjanya;
                }
                $absen->cek_in = $cek_in->format('H:i:s');
                $absen->cek_out = $cek_out->format('H:i:s');
            }
            //cek kalender libur nasional
            if (count($libur) > 0)
            {
                $libur_nasional++;
            }
            else
            {
                if ($day == '0' || $day == '6')
                {
                }
                else
                {
                    if($param == 'oldVersion' && $iscuti != 1) {
                        $hari_aktif += 1;
                    }
                    if ($iscuti <= 0)
                    {
                        $total_harus += 8;
                        $harus_hari_kerja += 1;
                        if ($absen->jam_kerja == '08:00:00')
                        {
                            if($param == 'oldVersion') {
                                $hari_kerja += 1;
                            } else {
                                $cek_ijin = DB::table('leaves')->select('leaves.*', 'data_ijins.nama_ijin', 'data_ijins.hitung')
                                ->where('leaves.karyawan_id', $karyawan)->where('leaves.tanggal_ijin', $ayo)->where('leaves.statusatasan', 'Diterima')
                                ->leftjoin('data_ijins', 'leaves.data_ijin_id', '=', 'data_ijins.id')
                                ->get();
                                if($cek_ijin->first()) {
                                    if($cek_ijin->first()->data_ijin_id == 4 || $cek_ijin->first()->data_ijin_id == 6 || $cek_ijin->first()->data_ijin_id == 7 || $cek_ijin->first()->data_ijin_id == 10){
                                        $hari_kerja += 1;
                                    }
                                } else {
                                    $hari_kerja += 1;
                                }
                            }
                        }

                    }
                    else
                    {
                        if($param == 'oldVersion') {
                            $total_cuti += 1;
                        }
                    }

                }

            }
            if ($ada_ijin == 1)
            {
                $ijin_semua += 1;
            }

            $ayo = date("Y-m-d", strtotime("+1 day", strtotime($ayo)));
        }

        $data['hari_kerja'] = $hari_kerja;
        $data['harus_hari_kerja'] = $harus_hari_kerja;
        $data['hari_aktif'] = $hari_aktif;
        $data['hari_ijin'] = $ijin_semua;
        $data['total_kerja'] = round(($total_jam_kerja / 3600) - ($total_dinas / 3600) , 2); //jam
        $data['jam_seharusnya'] = round($total_harus, 2); //jam
        $data['total_cuti'] = $total_cuti; //hari
        $data['total_dinas'] = round($total_dinas / 3600, 2); //jam
        $data['total_ijin'] = round($total_ijin / 3600, 2); //jam
        $data['total_mangkir'] = $total_mangkir; //hari
        $data['hari_libur'] = $libur_nasional;
        if ($total_harus <= 0)
        {
            $data['total_telat_cepat'] = 0; //jam
            $data['persentase_kerja'] = 0; //dinas termasuk kerja
            $data['persentase_kerja_hari'] = 0; //dinas termasuk kerja
            $data['persen_dinas'] = 0;
            $data['persen_ijin'] = 0;
            $data['persen_mangkir'] = 0;
        }
        else
        {
            $data['total_telat_cepat'] = round($total_harus - ($total_jam_kerja / 3600) - ($total_mangkir * 8) , 2); //jam
            $data['persentase_kerja'] = round((($total_jam_kerja / 3600) / $total_harus) * 100, 2); //dinas termasuk kerja
            $data['persentase_kerja_hari'] = round(($hari_kerja / $harus_hari_kerja) * 100, 2); //dinas termasuk kerja
            $data['persen_dinas'] = round((($total_dinas / 3600) / $total_harus) * 100, 2);
            $data['persen_ijin'] = round((($total_ijin / 3600) / $total_harus) * 100, 2);
            $data['persen_mangkir'] = round(((($total_mangkir * 8) + ($total_harus - ($total_jam_kerja / 3600) - ($total_mangkir * 8))) / $total_harus) * 100, 2);
        }

        $data['hari_cepat'] = $harus_hari_kerja - $hari_kerja - $hari_ijin - $total_mangkir;

        return $data;
    }
}

