<?php $__env->startSection('title'); ?>
Pengajuan Izin
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-5">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Pengajuan Izin</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" class="form-horizontal" id="form_input"  action="<?php echo e(url('ijin')); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-4 control-label">Karyawan</label>
                          <div class=" col-md-8" >
                            <select name="karyawan_id" id="karyawan_id" class="form-control bs-select" />
                              <?php $__currentLoopData = $karyawan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $karyawan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($karyawan->id); ?>"><?php echo e($karyawan->nama_karyawan); ?> / <?php echo e($karyawan->npp); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-4 control-label">Jenis Izin</label>
                          <div class=" col-md-8" >
                            <select name="data_ijin_id" id="data_ijin_id" class="form-control bs-select" />
                                <option value="">Pilih</option>
                              <?php $__currentLoopData = $jenis_ijin; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jenis_ijin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($jenis_ijin->id); ?>"><?php echo e($jenis_ijin->nama_ijin); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Tanggal Izin</label>
                          <div class=" col-md-8" >
                            <div class="input-group input-normal date date-picker" data-date="" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-calendar"></i>
                                  </button>
                            </span>
                            <input type="text" readonly name="tanggal_ijin" id="tanggal_ijin" class="form-control" required>

                          </div>
                          </div>
                      </div>
                      <div class="form-group jam">
                          <label class="control-label col-md-4">Jam Mulai</label>
                          <div class=" col-md-8" >
                            <div class="input-group input-normal" >
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-clock-o"></i>
                                  </button>
                            </span>
                            <input type="text" readonly name="jam_ijin" id="jam_ijin" class="form-control timepicker" required>

                          </div>
                          </div>
                      </div>
                      <div class="form-group jam">
                          <label class="control-label col-md-4">Jam selesai</label>
                          <div class=" col-md-8" >
                            <div class="input-group input-normal" >
                            <span class="input-group-btn">
                                  <button  class="btn default" type="button">
                                          <i class="fa fa-clock-o"></i>
                                  </button>
                            </span>
                            <input type="text" readonly name="sampai_dengan" id="sampai_dengan" class="form-control timepicker" required>

                          </div>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Alasan Izin</label>
                          <div class=" col-md-8" >
                            <input type="text" name="alasan" id="alasan" class="form-control" placeholder="Alasan Izin" required="true">
                          </div>
                      </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-7">
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Pengajuan Izin</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                  <div class="tabbable-custom ">
                      <ul class="nav nav-tabs ">
                          <li class="active">
                              <a href="#ijin-pending" data-toggle="tab">Pending</a>
                          </li>
                          <li>
                              <a href="#ijin-batal" data-toggle="tab">Batal</a>
                          </li>
                          <li>
                              <a href="#ijin-ok" data-toggle="tab">Disetujui</a>
                          </li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane active" id="ijin-pending">
                          <table id="table-ijin-pending" data-kolom='[
                            { "data": "nomer","orderable":false },
                            { "data": "tanggal_ijin","orderable":false,render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" ) },
                            { "data": "jam_ijin","orderable":false },
                            { "data": "sampai_dengan","orderable":false },
                            { "data": "nama_ijin","orderable":false },
                            { "data": "alasan","orderable":false }
                            ]'
                            action="<?php echo e(url('ijin')); ?>"

                            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                          <thead>
                          <tr>
                              <th class="all">No</th>
                              <th class="min-mobile-p">Tanggal</th>
                              <th class="min-tablet-l">Mulai</th>
                              <th class="min-tablet-l">Selesai</th>
                              <th class="min-tablet-l">Nama Izin</th>
                              <th class="min-tablet-l">Alasan</th>
                          </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          </table>
                        </div>
                        <div class="tab-pane" id="ijin-batal">
                          <table id="table-ijin-batal" data-kolom='[
                            { "data": "nomer","orderable":false },
                            { "data": "tanggal_ijin","orderable":false },
                            { "data": "jam_ijin","orderable":false },
                            { "data": "sampai_dengan","orderable":false },
                            { "data": "nama_ijin","orderable":false },
                            { "data": "alasan","orderable":false },
                            { "data": "alasanhrd","orderable":false },
                            { "data": "alasanatasan","orderable":false }
                            ]'

                            action="<?php echo e(url('ijin')); ?>"

                            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                          <thead>
                          <tr>
                              <th class="all">No.</th>
                              <th class="min-mobile-p">Tanggal</th>
                              <th class="min-tablet-l">Mulai</th>
                              <th class="min-tablet-l">Selesai</th>
                              <th class="min-tablet-l">Nama Izin</th>
                              <th class="min-tablet-l">Alasan</th>
                              <th class="none">Alasan HRD</th>
                              <th class="none">Alasan Atasan</th>
                          </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          </table>
                        </div>
                        <div class="tab-pane" id="ijin-ok">
                          <table id="table-ijin-oke" data-kolom='[
                            { "data": "nomer","orderable":false },
                            { "data": "tanggal_ijin","orderable":false },
                            { "data": "jam_ijin","orderable":false },
                            { "data": "sampai_dengan","orderable":false },
                            { "data": "nama_ijin","orderable":false },
                            { "data": "alasan","orderable":false }
                            ]'

                            action="<?php echo e(url('ijin')); ?>"

                            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                          <thead>
                          <tr>
                              <th class="all">No.</th>
                              <th class="min-mobile-p">Tanggal</th>
                              <th class="min-tablet-l">Mulai</th>
                              <th class="min-tablet-l">Selesai</th>
                              <th class="min-tablet-l">Nama Izin</th>
                              <th class="min-tablet-l">Alasan</th>
                          </tr>
                          </thead>
                          <tbody>

                          </tbody>
                          </table>
                        </div>
                      </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>