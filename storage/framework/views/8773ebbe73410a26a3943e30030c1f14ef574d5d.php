<?php $__env->startComponent('mail::message'); ?>
# <?php echo e($content['title']); ?>

<?php echo e($content['text']); ?>

<?php $__env->startComponent('mail::panel'); ?>
  Nama Karyawan : <?php echo e($content['nama_karyawan']); ?><br>
  Kantor : <?php echo e($content['nama_kantor']); ?><br>
  Divisi : <?php echo e($content['nama_divisi']); ?><br>
  Tanggal Request : <?php echo e($content['tanggal_request']); ?><br>
  Keterangan :
  <br><?php echo $content['keterangan'] ?>
  <br>Alasan : <?php echo e($content['alasan']); ?><br>
<?php echo $__env->renderComponent(); ?>

<?php $__env->startComponent('mail::button', ['url' => 'https://hris.hpam.co.id/list-request' ]); ?>
<?php echo e($content['button']); ?>

<?php echo $__env->renderComponent(); ?>

Thanks,
<?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>
