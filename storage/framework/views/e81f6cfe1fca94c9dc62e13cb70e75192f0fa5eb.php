<?php $__env->startSection('title'); ?>
Data Divisi
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Input Data Karyawan</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" class="form-horizontal" id="form_input"  action="<?php echo e(url('data-karyawan')); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                      <div class="tabbable-custom ">
                                          <ul class="nav nav-tabs ">
                                              <li class="active">
                                                  <a href="#data_diri" data-toggle="tab">Data Diri</a>
                                              </li>
                                              <li>
                                                  <a href="#info_kontak" data-toggle="tab">Kontak</a>
                                              </li>
                                              <li>
                                                  <a href="#info_kepegawaian" data-toggle="tab">Kepegawaian</a>
                                              </li>
                                              <li>
                                                  <a href="#info_bank" data-toggle="tab">Bank</a>
                                              </li>
                                          </ul>
                                          <div class="tab-content">
                                              <div class="tab-pane active" id="data_diri">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Nama Sesuai ID</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nama_karyawan" id="nama_karyawan" class="pertama form-control" placeholder="Nama Lengkap" autofocus required="true">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tempat Lahir</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" placeholder="Tempat Lahir" autofocus required="true">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tanggal Lahir</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="tgl_lahir" id="tgl_lahir" class="date-picker form-control" placeholder="Tanggal Lahir" autofocus required="true">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">NIK</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nik" id="nik" class="form-control" placeholder="Nomor Induk Kependudukan"  required="true">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Alamat Sesuai ID</label>
                                                    <div class=" col-md-8" >
                                                      <textarea class="form-control" name="alamat_identitas" required></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Alamat Sekarang</label>
                                                    <div class=" col-md-8" >
                                                      <textarea class="form-control" name="alamat_sekarang" required></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">NPWP</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="npwp" id="npwp" class="form-control" placeholder="NPWP" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status Pajak</label>
                                                    <div class="col-md-8" >
                                                      <select name="status_pajak" id="status_pajak" >
                                                            <?php $__currentLoopData = $pajak; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pajak): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                              <option value="<?php echo e($pajak->pajak); ?>"><?php echo e($pajak->pajak); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Jenis Kelamin</label>
                                                    <div class="col-md-8" >
                                                      <select name="gender" id="gender" class="form-control" required >
                                                          <option value="L">Laki-Laki</option>
                                                          <option value="P">Perempuan</option>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status Nikah</label>
                                                    <div class=" col-md-8" >
                                                      <select name="status_nikah" id="status_nikah" class="form-control" required >
                                                          <option value="Lajang">Lajang</option>
                                                          <option value="Kawin">Kawin</option>
                                                          <option value="Janda">Janda</option>
                                                          <option value="Duda">Duda</option>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Agama</label>
                                                    <div class=" col-md-8" >
                                                      <select name="agama" id="agama" class="form-control" required>
                                                          <option value="Islam">Islam</option>
                                                          <option value="Kristen">Kristen</option>
                                                          <option value="Katolik">Katolik</option>
                                                          <option value="Budha">Buda</option>
                                                          <option value="Hindu">Hindu</option>
                                                          <option value="Konghucu">Konghucu</option>
                                                          <option value="Aliran Kepercayaan">Aliran Kepercayaan</option>
                                                      </select>
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_kontak">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">No Telpon</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nomor_telpon" id="nomor_telpon" class="form-control" placeholder="Nomor Telpon" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">No Handphone</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nomor_ponsel" id="nomor_ponsel" class="form-control" placeholder="Nomor Handphone"  required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Email Pribadi</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="email_pribadi" id="email_pribadi" class="form-control" placeholder="Email Pribadi"  required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nama_kontak_darurat" id="nama_kontak_darurat" class="form-control" placeholder="Nama Kontak Darurat" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input type="tel" name="nomor_kontak_darurat" id="nomor_kontak_darurat" class="form-control" placeholder="Nomor Kontak Darurat" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Hubungan Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="hubungan_kontak_darurat" id="hubungan_kontak_darurat" class="form-control" placeholder="Hubungan Kontak Darurat"  required >
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_kepegawaian">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Pegawai</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="npp" id="npp" class="form-control" placeholder="NPP" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Email Kantor</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="email" id="email" class="form-control" placeholder="Email Kantor" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Password</label>
                                                    <div class=" col-md-8" >
                                                      <input type="password" name="passwordnya" id="passwordnya" class="form-control" placeholder="Password untuk akses sistem" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Jabatan</label>
                                                    <div class=" col-md-8" >
                                                      <select name="jabatan_id" id="jabatan_id" class="form-control" required>
                                                          <option value="0">Pilih</option>
                                                          <?php $__currentLoopData = $jabatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jabatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($jabatan->id); ?>"><?php echo e($jabatan->nama_jabatan); ?></option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Divisi</label>
                                                    <div class=" col-md-8" >
                                                      <select name="divisi_id" id="divisi_id" class="form-control" required>
                                                          <option value="0">Pilih</option>
                                                          <?php $__currentLoopData = $divisi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $divisi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($divisi->id); ?>"><?php echo e($divisi->nama_divisi); ?></option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Kantor</label>
                                                    <div class=" col-md-8" >
                                                      <select name="kantor_id" id="kantor_id" class="form-control" required>
                                                          <option value="0">Pilih</option>
                                                          <?php $__currentLoopData = $kantor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kantor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($kantor->id); ?>"><?php echo e($kantor->nama_kantor); ?></option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Pangkat</label>
                                                    <div class=" col-md-8" >
                                                      <select name="pangkat_id" id="pangkat_id" class="form-control" required>
                                                          <option value="0">Pilih</option>
                                                          <?php $__currentLoopData = $pangkat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pangkat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($pangkat->id); ?>"><?php echo e($pangkat->nama_pangkat); ?></option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Role</label>
                                                    <div class=" col-md-8" >
                                                      <select name="role" id="role" class="form-control" required>
                                                            <option value="user">User</option>
                                                            <option value="admin">Admin</option>
                                                      </select>
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_bank">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nama Bank</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nama_bank" id="nama_bank" class="form-control" placeholder="Nama Bank"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nama Rekening</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nama_rekening" id="nama_rekening" class="form-control" placeholder="Nama Rekening"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Rekening</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nomor_rekening_payrol" id="nomor_rekening_payrol" class="form-control" placeholder="Nomor Rekening"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor CIF</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nomor_cif" id="nomor_cif" class="form-control" placeholder="Nomor CIF" >
                                                    </div>
                                                </div>
                                              </div>
                                          </div>
                                      </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_karyawan();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>List Data Kantor</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer","orderable":false },
                      { "data": "nama_karyawan","orderable":false },
                      { "data": "npp","orderable":false },
                      { "data": "nomor_ponsel","orderable":false},
                      { "data": "tambahan","orderable":false},
                      { "data": "action","orderable":false }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">NO</th>
                        <th class="min-mobile-p">Nama</th>
                        <th class="min-tablet-l">Npp</th>
                        <th class="min-tablet-l">Ponsel</th>
                        <th class="none"></th>
                        <th class="none">Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>