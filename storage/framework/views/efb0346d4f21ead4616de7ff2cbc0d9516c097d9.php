
<?php $__env->startSection('title'); ?>
List Survey
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Hasil Survey</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                  <table id="table-list-aja" data-kolom='[
                    { "data": "nama_group","orderable":false },
                    { "data": "keterangan","orderable":false },
                    { "data": "action","orderable":false}
                    ]'
                    action="<?php echo e(url('data-survey')); ?>"
                    class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                  <thead>
                  <tr>
                      <th class="all">Nama Survey</th>
                      <th class="min-mobile-p">Keterangan</th>
                      <th class="min-tablet-l">Action</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  </table>
                </div>
            </div>
          </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>