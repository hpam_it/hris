<?php $__env->startComponent('mail::message'); ?>
# <?php echo e($content['title']); ?>

<?php echo e($content['text']); ?>

<?php $__env->startComponent('mail::panel'); ?>
  Nama : <?php echo e($content['nama']); ?><br>
  Tanggal Cuti : <?php echo e($content['tanggal']); ?><br>
  Jenis Cuti : <?php echo e($content['cuti']); ?><br>
  Keperluan : <?php echo e($content['keperluan']); ?><br>
  Alamat Cuti : <?php echo e($content['alamat_cuti']); ?><br>
  Kontak : <?php echo e($content['kontak_cuti']); ?><br>
  Pengganti : <?php echo e($content['pengganti']); ?><br>
<?php echo $__env->renderComponent(); ?>

<?php $__env->startComponent('mail::button', ['url' => 'http://192.168.7.9' ]); ?>
<?php echo e($content['button']); ?>

<?php echo $__env->renderComponent(); ?>

Thanks,
<?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>
