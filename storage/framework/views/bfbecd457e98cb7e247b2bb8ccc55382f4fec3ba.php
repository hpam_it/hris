
<?php $__env->startSection('title'); ?>
Approval Request
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>List Purchase Request</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                  <table id="table-modal" class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                  <thead>
                  <tr>
                      <th >Kantor</th>
                      <th >Divisi</th>
                      <th >Karyawan</th>
                      <th >Tanggal Request</th>
                      <th >Kategori</th>
                      <th >Keterangan</th>
                      <th >Alasan</th>
                      <th >Action</th>
                  </tr>

                  </thead>
                  <tbody>
                    <?php $__currentLoopData = $item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                        <td><?php echo e($item->nama_kantor); ?></td>
                        <td><?php echo e($item->nama_divisi); ?></td>
                        <td><?php echo e($item->nama_karyawan); ?></td>
                        <td><?php echo e($item->tanggal_request); ?></td>
                        <td><?php echo e($item->kategori); ?></td>
                        <td><?php echo $item->keterangan; ?></td>
                        <td><?php echo e($item->alasan); ?></td>
                        <td><?php echo $item->action ?></td>
                      </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                  </table>
                </div>
            </div>
          </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>