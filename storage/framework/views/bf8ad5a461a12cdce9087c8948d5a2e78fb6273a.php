<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Progress</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"   action="<?php echo e(url('data-progress')); ?>/<?php echo e($item->id); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="update_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="control-label">Nama Progress</label>
                          <input type="text" value="<?php echo e($item->nama_progress); ?>" name="nama_progress" id="nama_progress" class="pertama form-control" placeholder="Nama progress" autofocus required="true">
                      </div>
                      <div class="form-group">
                          <label class="control-label">Butuh Alasan</label>
                          <select name="butuh_alasan" id="butuh_alasan" class="form-control" required="true">
                            <option <?php if($item->butuh_alasan=='Tidak'): ?> selected <?php endif; ?> value="Tidak">Tidak</option>
                            <option <?php if($item->butuh_alasan=='Ya'): ?> selected <?php endif; ?>  value="Ya">Ya</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label class="control-label">Butuh Nominal</label>
                          <select name="butuh_nominal" id="butuh_nominal" class="form-control" required="true">
                            <option <?php if($item->butuh_nominal=='Tidak'): ?> selected <?php endif; ?> value="Tidak">Tidak</option>
                            <option <?php if($item->butuh_nominal=='Ya'): ?> selected <?php endif; ?>  value="Ya">Ya</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label class="control-label">Butuh Tanggal</label>
                          <select name="butuh_tanggal" id="butuh_tanggal" class="form-control" required="true">
                            <option <?php if($item->butuh_tanggal=='Tidak'): ?> selected <?php endif; ?> value="Tidak">Tidak</option>
                            <option <?php if($item->butuh_tanggal=='Ya'): ?> selected <?php endif; ?>  value="Ya">Ya</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label class="control-label">Sebagai Hasil Progress Visit</label>
                          <select name="progress_visit" id="progress_visit" class="form-control" required="true">
                            <option <?php if($item->progress_visit=='Tidak'): ?> selected <?php endif; ?> value="Tidak">Tidak</option>
                            <option <?php if($item->progress_visit=='Ya'): ?> selected <?php endif; ?>  value="Ya">Ya</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label class="control-label">Sebagai Progress Followup</label>
                          <select name="progress_followup" id="progress_followup" class="form-control" required="true">
                            <option <?php if($item->progress_followup=='Tidak'): ?> selected <?php endif; ?> value="Tidak">Tidak</option>
                            <option <?php if($item->progress_followup=='Ya'): ?> selected <?php endif; ?>  value="Ya">Ya</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label class="control-label">Sebagai Hasil Progress Followup</label>
                          <select name="hasil_progress_followup" id="hasil_progress_followup" class="form-control" required="true">
                            <option <?php if($item->hasil_progress_followup=='Tidak'): ?> selected <?php endif; ?> value="Tidak">Tidak</option>
                            <option <?php if($item->hasil_progress_followup=='Ya'): ?> selected <?php endif; ?>  value="Ya">Ya</option>
                          </select>
                      </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
