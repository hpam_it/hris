<?php $__env->startSection('title'); ?>
Data Struktur
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Input Data Struktur</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  action="<?php echo e(url('data-struktur')); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Jabatan</label>
                            <select name="jabatan_id" id="jabatan_id" class="form-control bs-select" data-live-search="true" >
                                <option value="0">Pilih</option>
                                <?php $__currentLoopData = $jabatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jabatans): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <option value="<?php echo e($jabatans->id); ?>"><?php echo e($jabatans->nama_jabatan); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nama Divisi</label>
                            <select name="divisi_id" id="divisi_id" class="form-control bs-select" data-live-search="true" >
                                <option value="0">Pilih</option>
                                <?php $__currentLoopData = $divisi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $divisis): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <option value="<?php echo e($divisis->id); ?>"><?php echo e($divisis->nama_divisi); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kantor</label>
                            <select name="kantor_id" id="kantor_id" class="form-control bs-select" data-live-search="true" >
                                <option value="0">Pilih</option>
                                <?php $__currentLoopData = $kantor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kantors): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <option value="<?php echo e($kantors->id); ?>"><?php echo e($kantors->nama_kantor); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <h3 class="form-section">Atasan</h3>
                        <div class="form-group">
                            <label class="control-label">Nama Jabatan</label>
                            <select name="jabatan_atasan" id="jabatan_atasan" class="form-control bs-select" data-live-search="true" >
                                <option value="0">Pilih</option>
                                <?php $__currentLoopData = $jabatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jabatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <option value="<?php echo e($jabatan->id); ?>"><?php echo e($jabatan->nama_jabatan); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nama Divisi</label>
                            <select name="divisi_atasan" id="divisi_atasan" class="form-control bs-select" data-live-search="true" >
                                <option value="0">Pilih</option>
                                <?php $__currentLoopData = $divisi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $divisi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <option value="<?php echo e($divisi->id); ?>"><?php echo e($divisi->nama_divisi); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kantor</label>
                            <select name="kantor_atasan" id="kantor_atasan" class="form-control bs-select" data-live-search="true" >
                                <option value="0">Pilih</option>
                                <?php $__currentLoopData = $kantor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kantor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <option value="<?php echo e($kantor->id); ?>"><?php echo e($kantor->nama_kantor); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Daftar Struktur</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list"
                      data-kolom='[
                        { "data": "nomer" },
                        { "data": "nama_jabatan" },
                        { "data": "nama_divisi" },
                        { "data": "nama_kantor" },
                        { "data": "kode_struktur" },
                        { "data": "jabatan_atasan" },
                        { "data": "divisi_atasan" },
                        { "data": "kantor_atasan" },
                        { "data": "kode_atasan" },
                        { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">No.</th>
                        <th class="min-mobile-p">Jabatan</th>
                        <th class="min-tablet-p">Divisi</th>
                        <th class="min-mobile-p">Kantor</th>
                        <th class="min-mobile-p">Kode Jabatan</th>
                        <th class="none">Jabatan Atasan</th>
                        <th class="none">Divisi Atasan</th>
                        <th class="none">Kantor Atasan</th>
                        <th class="none">Kode Jabatan Atasan</th>
                        <th class="none">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>