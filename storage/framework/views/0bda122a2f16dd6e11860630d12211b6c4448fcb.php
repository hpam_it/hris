<?php $__env->startComponent('mail::message'); ?>
# <?php echo e($content['title']); ?>

<?php echo e($content['text']); ?>

<?php $__env->startComponent('mail::panel'); ?>
  Gunakan link dibawah untuk mereset password anda
  <br><a href="<?php echo e(config('app.url').$content['link']); ?>" >Reset Now</a>
  <br>Link akan expired dalam 24 jam.
  <br>Sementar link hanya bisa di akses dari jaringan lokal PT.HPAM
<?php echo $__env->renderComponent(); ?>

<?php $__env->startComponent('mail::button', ['url' => "<?php echo e(config('app.url')); ?>" ]); ?>
<?php echo e($content['button']); ?>

<?php echo $__env->renderComponent(); ?>

Thanks,
<?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>
