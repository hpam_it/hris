<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Terima Permohonan Cuti</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="terimacuti"  class="form-horizontal" action="<?php echo e(url('approve-cuti')); ?>/<?php echo e($item->id); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <?php echo e(method_field('PATCH')); ?>

                    <input type="hidden" name="aksi" value="terima">
                    <input type="hidden" name="status" value="<?php echo e($status); ?>">
                    <table class="table">
                        <tr>
                            <td width="30%" >Nama Karyawan</td>
                            <td> : <?php echo e($item->nama_karyawan); ?></td>
                        </tr>
                        <tr>
                            <td>PIC Pengganti</td>
                            <td> : <?php echo e($item->nama_pengganti); ?></td>
                        </tr>
                        <tr>
                            <td>Atasan Langsung</td>
                            <td> : <?php echo e($item->nama_atasan); ?></td>
                        </tr>
                        <tr>
                            <td>Tanggal Cuti</td>
                            <td> : <?php echo e($item->tanggal_tanggal); ?></td>
                        </tr>
                        <tr>
                            <td>Jumlah Hari Cuti</td>
                            <td> : <?php echo e($item->jumlah_hari); ?></td>
                        </tr>
                        <tr>
                            <td>Keperluan</td>
                            <td> : <?php echo e($item->keperluan); ?></td>
                        </tr>
                        <tr>
                            <td>Jenis Cuti</td>
                            <td> : <?php echo e($item->nama_cuti); ?></td>
                        </tr>

                    </table>
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-5 control-label">Apakah Anda Menyetujui ?</label>
                          <div class="col-md-3">
                            <select name="keputusan" id="keputusan" class="form-control">
                              <option value="Diterima" >YA</option>
                              <option value="Ditolak" >TIDAK</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">Alasan</label>
                          <div class="col-md-9">
                            <textarea class="form-control" name="alasan" id="alasan"></textarea>
                          </div>
                      </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="terimacuti();" type="button" class="simpan_edit btn blue">Save</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
            <div class="table-responsive" style="padding:10px;">
              <table class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
              <thead>
              <tr>
                  <th class="all" colspan="2" rowspan="2">Ketentuan</th>
                  <th colspan="8">Periode</th>
              </tr>
              <tr>
                  <th class="all text-center" colspan="4">Lalu</th>
                  <th class="all text-center" colspan="4">Sekarang</th>
              </tr>
              <tr>
                  <th class="all">Jenis Cuti</th>
                  <th class="min-tablet-l text-center">Jumlah</th>
                  <th class="min-tablet-l text-center">Jumlah</th>
                  <th class="min-tablet-l text-center">Terpakai</th>
                  <th class="min-tablet-l text-center">Berakhir</th>
                  <th class="min-tablet-l text-center">Sisa</th>
                  <th class="min-tablet-l text-center">Jumlah</th>
                  <th class="min-tablet-l text-center">Terpakai</th>
                  <th class="min-tablet-l text-center">Berakhir</th>
                  <th class="min-tablet-l text-center">Sisa</th>
              </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $cuti_oke; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $oke): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <th class="all"><?php echo e($oke->nama_cuti); ?></th>
                    <th class="min-mobile-p text-center"><?php echo e($oke->jumlah); ?></th>
                    <th class="min-mobile-p text-center"><?php echo e($oke->kemarin_awal); ?></th>
                    <th class="min-tablet-l text-center"><?php echo e($oke->kemarin_ambil); ?></th>
                    <?php $tgl_expired = date_create($oke->kemarin_tambahan); $sekarang=date('Y-m-d'); ?>
                    <th class="min-tablet-l text-center"><?php if($oke->hitung==1): ?> <?php echo e(date_format($tgl_expired,"d-m-Y")); ?> <?php endif; ?></th>
                    <th class="min-tablet-l text-center"><?php echo e($oke->kemarin_sisa); ?></th>
                    <th class="min-mobile-p text-center"><?php if($oke->hitung==1): ?> <?php echo e($oke->sekarang_awal); ?> <?php endif; ?></th>
                    <th class="min-tablet-l text-center"><?php if($oke->hitung==1): ?> <?php echo e($oke->sekarang_ambil); ?> <?php else: ?> <?php echo e($oke->total_cuti); ?><?php endif; ?></th>
                    <?php $tgl_expired = date_create($oke->sekarang_tambahan); $sekarang=date('Y-m-d'); ?>
                    <th class="min-tablet-l text-center"><?php if($oke->hitung==1): ?> <?php echo e(date_format($tgl_expired,"d-m-Y")); ?> <?php endif; ?></th>
                    <th class="min-tablet-l text-center"><?php echo e($oke->sekarang_sisa); ?></th>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td colspan="10"><p><?php echo e($pesan); ?></p></td>
                </tr>

              </tbody>
              </table>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
