<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">List Asuransi</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
          <form method="POST" class="form-horizontal"  enctype="multipart/form-data" id="form_modal"  action="<?php echo e(url('data-karyawan')); ?> ">
               <?php echo e(csrf_field()); ?>

               <input type="hidden" name="aksi" value="simpan_asuransi" />
               <input type="hidden" name="karyawan_id" value="<?php echo e($id_karyawan); ?>" />
               <div class="form-body">
                 <div class="form-group">
                     <label class="control-label col-md-4">Nama Asuransi</label>
                     <div class=" col-md-8" >
                       <input type="text" name="nama_asuransi" id="nama_asuransi" class="form-control" placeholder="Nama Asuransi" required >
                     </div>
                 </div>
                 <div class="form-group">
                   <label class="control-label col-md-4">Nomor</label>
                     <div class=" col-md-8" >
                       <input type="text" name="nomor_asuransi" id="nomor_asuransi" class="form-control" placeholder="Nomor Asuransi" required >
                     </div>
                 </div>
                 <div class="form-group">
                     <label class="control-label col-md-4">Scan</label>
                     <div class=" col-md-8" >
                       <input type="file" name="scan_kartu" id="scan_kartu" class="form-control"  required >
                     </div>
                 </div>
               </div>
               <div class="form-actions">
                   <button type="button" onclick="simpan_dokumen();" class="simpan_input btn green">Simpan</button>
                   <input type="reset" class="btn default" value="Reset"/>
               </div>
           </form>

         </div>
       </div>
       <div class="row">
         <div class="col-md-12 tempat-table-modal">
           List Asuransi
           <table id="table-modal"
             class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
           <thead>
           <tr>
               <th class="all">NO</th>
               <th class="min-mobile-p">Nama Asuransi</th>
               <th class="min-tablet-l">Nomor</th>
               <th class="none">Scan Kartu</th>
               <th class="min-tablet-l">Action</th>
           </tr>
           </thead>
             <?php $nomor=1; ?>
             <tbody>

               <?php $__currentLoopData = $dokument; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dokument): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <tr>
                <td><?php echo e($nomor++); ?></td>
                <td><?php echo e($dokument->nama_asuransi); ?></td>
                <td><?php echo e($dokument->nomor_asuransi); ?></td>
                <td><a target="_blank" href="<?php echo e(Storage::url($dokument->scan_kartu)); ?>" >Download</a></td>
                <td>
                  <button  data-title="Hapus Dokumen ?" data-toggle="confirmation" data-placement="left" data-url="<?php echo e(url("data-karyawan")); ?>/<?php echo e($dokument->id); ?>?aksi=hapus_asuransi" class="konfirmasi hapus-dokumen btn btn-md btn-icon-only red">
                      <i class="fa fa-trash"></i>
                  </button>
                </td>
              </tr>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
             </tbody>
           </table>
         </div>
       </div>
</div>
<div class="modal-footer">

</div>
<script>
$("#table-modal").dataTable();
$('.konfirmasi').on('click', function () {
  $(this).confirmation('show');

  $('.hapus-dokumen').on('confirmed.bs.confirmation', function () {
      var url=$(this).attr('data-url');
      hapus_input_modal(url);
      //alert(url);
  });

} );
</script>
