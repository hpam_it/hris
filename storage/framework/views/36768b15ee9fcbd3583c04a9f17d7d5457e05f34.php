<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Terima Permohonan Ijin</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="terimacuti"  class="form-horizontal" action="<?php echo e(url('approve-ijin')); ?>/<?php echo e($item->id); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="terima">
                    <input type="hidden" name="status" value="<?php echo e($status); ?>">
                    <table class="table">
                        <tr>
                            <td width="30%" >Nama Karyawan</td>
                            <td> : <?php echo e($item->nama_karyawan); ?></td>
                        </tr>
                        <tr>
                            <td>Atasan Langsung</td>
                            <td> : <?php echo e($item->nama_atasan); ?></td>
                        </tr>
                        <tr>
                            <td>Tanggal Ijin</td>
                            <td> : <?php echo e($item->tanggal_ijin); ?></td>
                        </tr>
                        <tr>
                            <td>Jam Mulai</td>
                            <td> : <?php echo e($item->jam_ijin); ?></td>
                        </tr>
                        <tr>
                            <td>Selesai</td>
                            <td> : <?php echo e($item->sampai_dengan); ?></td>
                        </tr>
                        <tr>
                            <td>Jenis Ijin</td>
                            <td> : <?php echo e($item->nama_ijin); ?></td>
                        </tr>
                        <tr>
                            <td>Alasan</td>
                            <td> : <?php echo e($item->alasan); ?></td>
                        </tr>

                    </table>
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-5 control-label">Apakah Anda Menyetujui ?</label>
                          <div class="col-md-3">
                            <select name="keputusan" id="keputusan" class="form-control">
                              <option value="Diterima" >YA</option>
                              <option value="Ditolak" >TIDAK</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-3">Alasan</label>
                          <div class="col-md-9">
                            <textarea class="form-control" name="alasan" id="alasan"></textarea>
                          </div>
                      </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="terimacuti();" type="button" class="simpan_edit btn blue">Save</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
