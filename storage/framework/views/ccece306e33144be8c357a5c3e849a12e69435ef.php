<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Jenis Cuti</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  action="<?php echo e(url('data-cuti')); ?>/<?php echo e($item->id); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="update_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="control-label">Nama Cuti</label>
                          <input type="text" name="nama_cuti" id="nama_cuti" value="<?php echo e($item->nama_cuti); ?>" class="pertama form-control" placeholder="Nama Cuti" autofocus required="true">
                      </div>
                      <div class="form-group">
                          <label class="control-label">Jumlah Hari</label>
                          <input type="number" name="jumlah" id="jumlah" value="<?php echo e($item->jumlah); ?>" class="form-control" placeholder="Jumlah" required="true">
                      </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
