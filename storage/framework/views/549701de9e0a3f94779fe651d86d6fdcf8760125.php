<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Jadwal</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
          <form class="form-horizontal" method="PATCH" id="form_update"  action="<?php echo e(url('absensi')); ?>/<?php echo e($jadwal->id); ?> ">
               <?php echo e(csrf_field()); ?>

               <input type="hidden"  name="aksi" value="update_jadwal">
               <div class="form-body">
                 <div class="col-md-6">
                     <div class="form-group">
                         <label class="control-label col-md-3">Jam Kerja</label>
                         <div class="col-md-9">
                             <input type="text" value="<?php echo e($jadwal->nama_jamkerja); ?>" class="form-control pertama" placeholder="Nama Jam Kerja" name="nama_jamkerja" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="control-label col-md-3">Masuk (Jam)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" value="<?php echo e($jadwal->masuk); ?>" name="masuk" required=""/>
                         </div>
                     </div>
                     <div class=form-group>
                         <label class="control-label col-md-3">Pulang (Jam)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" value="<?php echo e($jadwal->pulang); ?>" name="pulang" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="control-label col-md-3">Toleransi (Jam)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" value="<?php echo e($jadwal->toleransi); ?>" name="toleransi" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="control-label col-md-3">Max-Pulang (Jam)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" value="<?php echo e($jadwal->maxpulang); ?>" name="maxpulang" required=""/>
                         </div>
                     </div>

                   </div>
                   <div class="col-md-6">

                     <div class="form-group">
                         <label class="control-label col-md-3">Istirahat (waktu)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" value="<?php echo e($jadwal->istirahat); ?>" name="istirahat" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="control-label col-md-3">Mulai Istirahat (Jam)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" name="jam_istirahat" value="<?php echo e($jadwal->jam_istirahat); ?>" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="control-label col-md-3">Total Kerja (waktu)</label>
                         <div class="col-md-9">
                             <input type="text" class="form-control timepicker" value="<?php echo e($jadwal->jam_normal); ?>" name="jam_normal" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                         <label class="control-label col-md-3">Masa Berlaku</label>
                         <div class="col-md-4">
                             <input type="text" class="form-control date-picker" value="<?php echo e($jadwal->tgl_mulai); ?>" name="tgl_mulai" required=""/>
                         </div>
                         <div class="col-md-1 text-center">
                           S/D
                         </div>
                         <div class="col-md-4">
                             <input type="text" class="form-control date-picker" value="<?php echo e($jadwal->tgl_selesai); ?>" name="tgl_selesai" required=""/>
                         </div>
                     </div>
                     <div class="form-group">
                       <div class="col-md-12 text-right">
                         <button type="button" onclick="update_input();" class="simpan_input btn green">Submit</button>
                         <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                       </div>
                     </div>
                   </div>
             </div>

           </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
<script>
$('.date-picker').datepicker({
rtl: App.isRTL(),
orientation: "left",
format:"yyyy-mm-dd",
autoclose: true
});

$('.timepicker').timepicker({
      autoclose: true,
      minuteStep: 5,
      showSeconds: false,
      showMeridian: false
  });
</script>
