<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>HRIS | <?php echo $__env->yieldContent('title'); ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #3 for " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
          <link href="assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/layouts/layout3/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="img/favicon.png" /> </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-menu-fixed">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <!-- BEGIN HEADER -->
                    <div class="page-header">
                        <div class="page-header-top">
                            <div class="container">
                                <!-- BEGIN LOGO -->
                                <div class="page-logo">
                                       <span class="judul-logo"> Human Resource Database</span>
                                </div>
                                <!-- END LOGO -->
                                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                                <a href="javascript:;" class="menu-toggler"></a>
                                <!-- END RESPONSIVE MENU TOGGLER -->
                            </div>
                        </div>
                        <!-- BEGIN HEADER MENU -->
                        <div class="page-header-menu">
                            <div class="container">
                                <!-- BEGIN HEADER SEARCH BOX -->
                                <!-- END HEADER SEARCH BOX -->
                                <!-- BEGIN MEGA MENU -->
                                <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                                <div class="hor-menu  ">
                                    <ul class="nav navbar-nav">

                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown <?php if($halaman == 'dashboard'): ?> active <?php endif; ?>">
                                            <a href="<?php echo e(url('dashboard')); ?>"> Dashboard</a>
                                        </li>
                                        <?php if(Auth::user()->role=='admin'): ?>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown <?php if($halaman == 'data-divisi' || $halaman == 'data-jabatan' || $halaman == 'data-pangkat' || $halaman == 'data-kantor' || $halaman == 'data-status-karyawan' || $halaman == 'data-karyawan' ): ?> active <?php endif; ?>">
                                            <a href="javascript:;">Data
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li aria-haspopup="true" class=" <?php if($halaman=='data-calendar'): ?> active <?php endif; ?> ">
                                                    <a href="<?php echo e(url('data-calendar')); ?>" class="nav-link  ">
                                                        <i class="icon-paper-clip"></i> Calendar
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" <?php if($halaman=='data-cuti'): ?> active <?php endif; ?> ">
                                                    <a href="<?php echo e(url('data-cuti')); ?>" class="nav-link  ">
                                                        <i class="icon-paper-clip"></i> Cuti
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" <?php if($halaman=='data-divisi'): ?> active <?php endif; ?> ">
                                                    <a href="<?php echo e(url('data-divisi')); ?>" class="nav-link  ">
                                                        <i class="icon-paper-clip"></i> Divisi
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" <?php if($halaman=='data-jabatan'): ?> active <?php endif; ?> ">
                                                    <a href="<?php echo e(url('data-jabatan')); ?>" class="nav-link  ">
                                                        <i class="icon-users"></i> Jabatan
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" <?php if($halaman=='data-struktur'): ?> active <?php endif; ?> ">
                                                    <a href="<?php echo e(url('data-struktur')); ?>" class="nav-link  ">
                                                        <i class="icon-users"></i> Struktur
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" <?php if($halaman=='data-pangkat'): ?> active <?php endif; ?> ">
                                                    <a href="<?php echo e(url('data-pangkat')); ?>" class="nav-link  ">
                                                        <i class="icon-notebook"></i> Pangkat </a>
                                                </li>
                                                <li aria-haspopup="true" class=" <?php if($halaman=='data-status-karyawan'): ?> active <?php endif; ?> ">
                                                    <a href="<?php echo e(url('data-status-karyawan')); ?>" class="nav-link  ">
                                                        <i class="icon-star"></i> Status Karyawan
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" <?php if($halaman=='data-pajak'): ?> active <?php endif; ?> ">
                                                    <a href="<?php echo e(url('data-pajak')); ?>" class="nav-link  ">
                                                        <i class="icon-star"></i> Status Pajak
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" <?php if($halaman=='data-kantor'): ?> active <?php endif; ?> ">
                                                    <a href="<?php echo e(url('data-kantor')); ?>" class="nav-link  ">
                                                        <i class="icon-star"></i> Kantor
                                                    </a>
                                                </li>
                                                <li aria-haspopup="true" class=" <?php if($halaman=='data-karyawan'): ?> active <?php endif; ?> ">
                                                    <a href="<?php echo e(url('data-karyawan')); ?>" class="nav-link  ">
                                                        <i class="icon-star"></i> Karyawan
                                                    </a>
                                                </li>

                                            </ul>
                                        </li>
                                        <?php endif; ?>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown <?php if($halaman == 'ambilcuti'): ?> active <?php endif; ?>">
                                            <a href="<?php echo e(url('ambilcuti')); ?>">Pengajuan Cuti</a>
                                        </li>
                                        <!-- <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown <?php if($halaman == 'ijin'): ?> active <?php endif; ?>">
                                            <a href="<?php echo e(url('ijin')); ?>">Ijin</a>
                                        </li> -->
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown <?php if($halaman == 'logabsen'): ?> active <?php endif; ?>">
                                            <a href="<?php echo e(url('logabsen')); ?>">Log Absen</a>
                                        </li>
                                          <?php if(Auth::user()->role=='admin'): ?>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown <?php if($halaman == 'absensi'): ?> active <?php endif; ?>">
                                            <a href="<?php echo e(url('absensi')); ?>">Absensi</a>
                                        </li>
                                        <?php endif; ?>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown <?php if( $halaman =='approve-cuti' || $halaman=='daftar-cuti' || $halaman=='daftar-ijin' ): ?> active <?php endif; ?>">
                                            <a href="javascript:;">Administrasi
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-left">
                                                <li aria-haspopup="true" class=" <?php if($halaman=='approve-cuti'): ?> active <?php endif; ?> ">
                                                    <a href="<?php echo e(url('approve-cuti')); ?>" class="nav-link  ">
                                                        <i class="icon-paper-clip"></i>Approve Cuti
                                                    </a>
                                                </li>
                                                <!-- <li aria-haspopup="true" class=" <?php if($halaman=='daftar-cuti'): ?> active <?php endif; ?> ">
                                                    <a href="<?php echo e(url('daftar-cuti')); ?>" class="nav-link  ">
                                                        <i class="icon-paper-clip"></i>Daftar Cuti
                                                    </a>
                                                </li> -->
                                                <!-- <li aria-haspopup="true" class=" <?php if($halaman=='daftar-ijin'): ?> active <?php endif; ?> ">
                                                    <a href="<?php echo e(url('daftar-ijin')); ?>" class="nav-link  ">
                                                        <i class="icon-paper-clip"></i>Daftar Ijin
                                                    </a>
                                                </li> -->

                                            </ul>
                                        </li>
                                        <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                                            <a href="<?php echo e(url('logout')); ?>"> Logout
                                                <span class="arrow"></span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                                <!-- END MEGA MENU -->
                            </div>
                        </div>
                        <!-- END HEADER MENU -->
                    </div>
                    <!-- END HEADER -->
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">
                                <?php echo $__env->yieldContent('content'); ?>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            <div class="page-wrapper-row">
                <div class="page-wrapper-bottom">
                    <!-- BEGIN FOOTER -->
                    <!-- BEGIN INNER FOOTER -->
                    <div class="page-footer">
                        <div class="container"> 2017 &copy; PT HPAM
                        </div>
                    </div>
                    <div class="scroll-to-top">
                        <i class="icon-arrow-up"></i>
                    </div>
                    <!-- END INNER FOOTER -->
                    <!-- END FOOTER -->
                </div>
            </div>
        </div>
        <!-- modal container  -->
        <div class="modal fade" id="ajax" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content modal-lg">
                    <div class="modal-body">
                        <img src="assets/global/img/loading-spinner-grey.gif" alt="" class="loading">
                        <span> &nbsp;&nbsp;Loading... </span>
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN QUICK NAV -->

        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<script src="../assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
          <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
        <script src="assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="js/jquery.number.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
         <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="js/gh.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="assets/layouts/layout3/scripts/demo.min.js" type="text/javascript"></script>

        <!-- END THEME LAYOUT SCRIPTS -->

    </body>

</html>
