<?php $__env->startComponent('mail::message'); ?>
# <?php echo e($content['title']); ?>

<?php echo e($content['text']); ?>


<?php $__env->startComponent('mail::panel'); ?>
  Nama : <?php echo e($content['nama']); ?><br>
  Tanggal Progress : <?php echo e($content['tanggal']); ?><br>
  Nama Progress : <?php echo e($content['progress']); ?><br>
  Nasabah : <?php echo e($content['nasabah']); ?><br>
  Nama Progress : <?php echo e($content['progress']); ?><br>
  Hasil Progress : <?php echo e($content['hasil_progress']); ?><br>
  Nominal : <?php echo e($content['nominal']); ?><br>
  Tanggal Progress Selanjutnya : <?php echo e($content['tanggal_progress']); ?><br>
  Keterangan : <?php echo e($content['keterangan']); ?>


<?php echo $__env->renderComponent(); ?>

<?php $__env->startComponent('mail::button', ['url' => 'https://hris.hpam.co.id/approve-ijin' ]); ?>
<?php echo e($content['button']); ?>

<?php echo $__env->renderComponent(); ?>

Terima Kasih,

<?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>
