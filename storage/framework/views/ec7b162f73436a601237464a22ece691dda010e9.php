<?php $__env->startComponent('mail::message'); ?>
# <?php echo e($content['title']); ?>

<?php echo e($content['text']); ?>


<?php $__env->startComponent('mail::panel'); ?>
  Nama : <?php echo e($content['nama']); ?><br>
  Tanggal Izin : <?php echo e($content['tanggal']); ?><br>
  Jenis Izin : <?php echo e($content['jenis_ijin']); ?><br>
  Keperluan : <?php echo e($content['alasan']); ?><br>
  <?php if($content['hanya_marketing']=='Ya'): ?>
    Nasabah : <?php echo e($content['nasabah']); ?><br>
    Alamat : <?php echo e($content['alamat']); ?><br>
    Progress : <?php echo e($content['progress']); ?><br>
    Nominal : <?php echo e($content['nominal']); ?><br>
    Tanggal Progress Selanjutnya : <?php echo e($content['tanggal_progress']); ?><br>
    Alasan progress : <?php echo e($content['alasan_progress']); ?>

  <?php endif; ?>

<?php echo $__env->renderComponent(); ?>

<?php $__env->startComponent('mail::button', ['url' => 'https://hris.hpam.co.id/approve-ijin' ]); ?>
<?php echo e($content['button']); ?>

<?php echo $__env->renderComponent(); ?>

Terima Kasih,

<?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>
