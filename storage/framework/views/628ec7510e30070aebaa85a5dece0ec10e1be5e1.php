<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data karyawan</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" class="form-horizontal" id="form_update_karyawan"  action="<?php echo e(url('data-karyawan')); ?>">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="update_item" >
                    <input type="hidden" name="id_karyawan" value="<?php echo e($karyawan->id); ?>" >
                    <div class="form-body">
                      <div class="tabbable-custom ">
                                          <ul class="nav nav-tabs ">
                                              <li class="active">
                                                  <a href="#data_diri2" data-toggle="tab">Data Diri</a>
                                              </li>
                                              <li>
                                                  <a href="#info_kontak2" data-toggle="tab">Kontak</a>
                                              </li>
                                              <li>
                                                  <a href="#info_kepegawaian2" data-toggle="tab">Kepegawaian</a>
                                              </li>
                                              <li>
                                                  <a href="#info_bank2" data-toggle="tab">Bank</a>
                                              </li>
                                          </ul>
                                          <div class="tab-content">
                                              <div class="tab-pane active" id="data_diri2">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Nama Sesuai ID</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" value="<?php echo e($karyawan->nama_karyawan); ?>" name="nama_karyawan" id="nama_karyawan" class="pertama form-control" placeholder="Nama Lengkap" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tempat Lahir</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="tempat_lahir" value="<?php echo e($karyawan->tempat_lahir); ?>" id="tempat_lahir" class="form-control" placeholder="Tempat Lahir" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tanggal Lahir</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="tgl_lahir" value="<?php echo e($karyawan->tgl_lahir); ?>" id="tgl_lahir" class="date-picker form-control" placeholder="Tanggal Lahir" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">NIK</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nik" id="nik" value="<?php echo e($karyawan->nik); ?>" class="form-control" placeholder="Nomor Induk Kependudukan"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Alamat Sesuai ID</label>
                                                    <div class=" col-md-8" >
                                                      <textarea class="form-control"  name="alamat_identitas" ><?php echo e($karyawan->alamat_identitas); ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Alamat Sekarang</label>
                                                    <div class=" col-md-8" >
                                                      <textarea class="form-control"  name="alamat_sekarang" ><?php echo e($karyawan->alamat_sekarang); ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">NPWP</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="npwp" value="<?php echo e($karyawan->npwp); ?>" id="npwp" class="form-control" placeholder="NPWP"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status Pajak</label>
                                                    <div class="col-md-8" >
                                                      <select name="status_pajak" id="status_pajak" >
                                                            <?php $__currentLoopData = $pajak; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pajak): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                              <option <?php if($pajak->pajak==$karyawan->status_pajak): ?> selected <?php endif; ?> value="<?php echo e($pajak->pajak); ?>"><?php echo e($pajak->pajak); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tanggungan</label>
                                                    <div class=" col-md-8" >
                                                      <input type="number" value="<?php echo e($karyawan->tanggungan); ?>" name="tanggungan" id="tanggungan" class="form-control" placeholder="Banyaknya Tanggungan" required >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Jenis Kelamin</label>
                                                    <div class="col-md-8" >
                                                      <select name="gender" id="gender" class="form-control" >
                                                          <option <?php if($karyawan->gender=='L'): ?> selected <?php endif; ?> value="L">Laki-Laki</option>
                                                          <option <?php if($karyawan->gender=='P'): ?> selected <?php endif; ?>  value="P">Perempuan</option>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status Nikah</label>
                                                    <div class=" col-md-8" >
                                                      <select name="status_nikah" id="status_nikah" class="form-control" >
                                                          <option <?php if($karyawan->status_nikah=='Lajang'): ?> selected <?php endif; ?>  value="Lajang">Lajang</option>
                                                          <option <?php if($karyawan->status_nikah=='Menikah'): ?> selected <?php endif; ?>  value="Menikah">Menikah</option>
                                                          <option <?php if($karyawan->status_nikah=='Sendiri'): ?> selected <?php endif; ?>  value="Sendiri">Sendiri</option>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Agama</label>
                                                    <div class=" col-md-8" >
                                                      <select name="agama" id="agama" class="form-control" >
                                                          <option >Pilih</option>
                                                          <option <?php if(stripos($karyawan->agama,"Islam") !== false ): ?> selected <?php endif; ?> value="Islam">Islam</option>
                                                          <option <?php if(stripos($karyawan->agama,"Kristen") !== false ): ?> selected <?php endif; ?> value="Kristen">Kristen</option>
                                                          <option <?php if(stripos($karyawan->agama,"Katholik") !== false ): ?> selected <?php endif; ?> value="Katholik">Katholik</option>
                                                          <option <?php if(stripos($karyawan->agama,"Budha") !== false ): ?> selected <?php endif; ?> value="Budha">Budha</option>
                                                          <option <?php if(stripos($karyawan->agama,"Hindu") !== false ): ?> selected <?php endif; ?> value="Hindu">Hindu</option>
                                                          <option <?php if(stripos($karyawan->agama,"Konghucu") !== false ): ?> selected <?php endif; ?> value="Konghucu">Konghucu</option>
                                                          <option <?php if(stripos($karyawan->agama,"Aliran Kepercayaan") !== false ): ?> selected <?php endif; ?> value="Aliran Kepercayaan">Aliran Kepercayaan</option>
                                                      </select>
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_kontak2">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">No Telpon</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" value="<?php echo e($karyawan->nomor_telpon); ?>" name="nomor_telpon" id="nomor_telpon" class="form-control" placeholder="Nomor Telpon" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">No Handphone</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" value="<?php echo e($karyawan->nomor_ponsel); ?>" name="nomor_ponsel" id="nomor_ponsel" class="form-control" placeholder="Nomor Handphone" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Email Pribadi</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" autocomplete="new-password" name="email_pribadi" value="<?php echo e($karyawan->email_pribadi); ?>" id="email_pribadi" class="form-control" placeholder="Email Pribadi"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="nama_kontak_darurat" value="<?php echo e($karyawan->nama_kontak_darurat); ?>" id="nama_kontak_darurat" class="form-control" placeholder="Nama Kontak Darurat" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input type="tel" name="nomor_kontak_darurat" value="<?php echo e($karyawan->nomor_kontak_darurat); ?>" id="nomor_kontak_darurat" class="form-control" placeholder="Nomor Kontak Darurat" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Hubungan Kontak Darurat</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="hubungan_kontak_darurat" value="<?php echo e($karyawan->hubungan_kontak_darurat); ?>" id="hubungan_kontak_darurat" class="form-control" placeholder="Hubungan Kontak Darurat" >
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_kepegawaian2">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Pegawai</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="npp" id="npp" value="<?php echo e($karyawan->npp); ?>" class="form-control" placeholder="NPP" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">User Absen</label>
                                                    <div class=" col-md-8" >
                                                      <select class="form-control bs-select" data-live-search='true' name="userid">
                                                            <option value="0">Belum Ada</option>
                                                          <?php $__currentLoopData = $userinfo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $userinfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option <?php if($userinfo->userid == $karyawan->userid): ?> selected <?php endif; ?> value="<?php echo e($userinfo->userid); ?>"><?php echo e($userinfo->badgenumber); ?>/<?php echo e($userinfo->nama); ?></option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Tanggal Gabung</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" name="tgl_gabung" value="<?php echo e($karyawan->tgl_gabung); ?>" id="tgl_gabung" class="date-picker  form-control" placeholder="Tanggal Gabung">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Email Kantor</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" autocomplete="new-password" name="email" id="email" value="<?php echo e($karyawan->email); ?>" class="form-control" placeholder="Email Kantor" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Password</label>
                                                    <div class=" col-md-8" >
                                                      <input type="password" autocomplete="new-password" name="passwordnya" id="passwordny" class="form-control" placeholder="Password untuk akses sistem" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Jabatan</label>
                                                    <div class=" col-md-8" >
                                                      <select name="jabatan_id" id="jabatan_id" class="form-control" >
                                                          <option value="0">Pilih</option>
                                                          <?php $__currentLoopData = $jabatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jabatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option <?php if($jabatan->id==$karyawan->jabatan_id): ?> selected <?php endif; ?> value="<?php echo e($jabatan->id); ?>"><?php echo e($jabatan->nama_jabatan); ?></option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Divisi</label>
                                                    <div class=" col-md-8" >
                                                      <select name="divisi_id" id="divisi_id" class="form-control" >
                                                          <option value="0">Pilih</option>
                                                          <?php $__currentLoopData = $divisi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $divisi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option <?php if($divisi->id==$karyawan->divisi_id): ?> selected <?php endif; ?> value="<?php echo e($divisi->id); ?>"><?php echo e($divisi->nama_divisi); ?></option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Kantor</label>
                                                    <div class=" col-md-8" >
                                                      <select name="kantor_id" id="kantor_id" class="form-control" >
                                                          <option value="0">Pilih</option>
                                                          <?php $__currentLoopData = $kantor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kantor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option <?php if($kantor->id==$karyawan->kantor_id): ?> selected <?php endif; ?> value="<?php echo e($kantor->id); ?>"><?php echo e($kantor->nama_kantor); ?></option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Role</label>
                                                    <div class=" col-md-8" >
                                                      <select name="role" id="role" class="form-control" required>
                                                            <option  <?php if($karyawan->role=='user'): ?> selected <?php endif; ?> value="user">User</option>
                                                            <option  <?php if($karyawan->role=='admin'): ?> selected <?php endif; ?> value="admin">Admin</option>
                                                            <option  <?php if($karyawan->role=='sekretaris'): ?> selected <?php endif; ?> value="sekretaris">Sekretaris</option>
                                                            <option  <?php if($karyawan->role=='finance'): ?> selected <?php endif; ?> value="finance">Finance</option>
                                                            <option  <?php if($karyawan->role=='marketing'): ?> selected <?php endif; ?> value="marketing">Marketing</option>
                                                            <option  <?php if($karyawan->role=='direktur'): ?> selected <?php endif; ?> value="direktur">Direktur</option>
                                                            <?php if(Auth::user()->role=='superadmin'): ?>
                                                            <option  <?php if($karyawan->role=='superadmin'): ?> selected <?php endif; ?> value="superadmin">superadmin</option>
                                                            <?php endif; ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Pangkat</label>
                                                    <div class=" col-md-8" >
                                                      <select name="pangkat_id" id="pangkat_id" class="form-control">
                                                          <option value="0">Pilih</option>
                                                          <?php $__currentLoopData = $pangkat; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pangkat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option <?php if($pangkat->id==$karyawan->pangkat_id): ?> selected <?php endif; ?> value="<?php echo e($pangkat->id); ?>"><?php echo e($pangkat->nama_pangkat); ?></option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Status</label>
                                                    <div class=" col-md-8" >
                                                      <select name="status_kerja" id="status_kerja" class="form-control" required>
                                                          <?php $__currentLoopData = $status; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option <?php if($karyawan->status_kerja==$status->status_karyawan): ?> selected <?php endif; ?> value="<?php echo e($status->status_karyawan); ?>"><?php echo e($status->status_karyawan); ?></option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Jam Kerja</label>
                                                    <div class=" col-md-8" >
                                                      <select name="jamkerja[]" multiple class="form-control bs-select">
                                                          <option value="">Pilih jam kerja</option>
                                                          <?php $__currentLoopData = $jamkerja; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jamkerja): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option <?php if($jamkerja->selected=='ya'): ?> selected <?php endif; ?> value="<?php echo e($jamkerja->id); ?>" ><?php echo e($jamkerja->nama_jamkerja); ?></option>
                                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                      </select>
                                                    </div>
                                                </div>
                                              </div>
                                              <div class="tab-pane" id="info_bank2">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nama Bank</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" value="<?php echo e($karyawan->nama_bank); ?>" name="nama_bank" id="nama_bank" class="form-control" placeholder="Nama Bank"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nama Rekening</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" value="<?php echo e($karyawan->nama_rekening); ?>" name="nama_rekening" id="nama_rekening" class="form-control" placeholder="Nama Rekening"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor Rekening</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" value="<?php echo e($karyawan->nomor_rekening_payrol); ?>" name="nomor_rekening_payrol" id="nomor_rekening_payrol" class="form-control" placeholder="Nomor Rekening"  >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Nomor CIF</label>
                                                    <div class=" col-md-8" >
                                                      <input type="text" value="<?php echo e($karyawan->nomor_cif); ?>" name="nomor_cif" id="nomor_cif" class="form-control" placeholder="Nomor CIF" >
                                                    </div>
                                                </div>
                                              </div>
                                          </div>
                                      </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="update_karyawan();" class="simpan_input btn green">Submit</button>
                        <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
