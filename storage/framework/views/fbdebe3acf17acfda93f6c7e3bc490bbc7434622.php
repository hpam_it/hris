
<?php $__env->startSection('title'); ?>
Approval Ijin
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="icon-grid"></i> Daftar Pengajuan Izin</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list-aja" action="<?php echo e(url('approve-ijin')); ?>"  data-kolom='[
                      { "data": "nomer","orderable":false },
                      { "data": "nama_karyawan","orderable":false },
                      { "data": "nama_ijin","orderable":false },
                      { "data": "tanggal_ijin","orderable":false,render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" )  },
                      { "data": "jam_ijin","orderable":false },
                      { "data": "sampai_dengan","orderable":false },
                      { "data": "alasan","orderable":false },
                      { "data": "action","orderable":false }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">No.</th>
                        <th class="min-mobile-p">Nama</th>
                        <th class="min-tablet-l">Jenis Izin</th>
                        <th class="min-tablet-l">Tanggal</th>
                        <th class="min-tablet-l">Mulai</th>
                        <th class="min-tablet-l">Selesai</th>
                        <th class="min-tablet-l">Keperluan</th>
                        <th class="min-tablet-l">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>