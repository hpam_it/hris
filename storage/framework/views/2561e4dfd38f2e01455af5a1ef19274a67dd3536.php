<?php $__env->startSection('title'); ?>
Daftar Ijin
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Daftar Izin</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                  <table id="table-list-aja" action="<?php echo e(url('daftar-ijin')); ?>"  data-kolom='[
                    { "data": "nomer","orderable":false },
                    { "data": "nama_karyawan","orderable":false },
                    { "data": "tanggal_ijin","orderable":false,render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" ) },
                    { "data": "alasan","orderable":false }
                    ]'
                    class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                  <thead>
                  <tr>
                      <th class="all">No.</th>
                      <th class="min-mobile-p">Nama Karyawan</th>
                      <th class="min-mobile-p">Tanggal</th>
                      <th class="min-tablet-l">Alasan</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  </table>
                  <a href="<?php echo e(url('/')); ?>/daftarijin/export" type="button" class="simpan_input btn green">Excel</a>
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>