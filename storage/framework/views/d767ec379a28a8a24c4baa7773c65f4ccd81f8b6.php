
<?php $__env->startSection('title'); ?>
Purchase Request
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="icon-grid"></i>Purchase Request</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" class="form-horizontal" id="form_input"  action="<?php echo e(url('simpan-request')); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-4 control-label">Kantor</label>
                          <div class=" col-md-8" >
                            <input class="form-control" type="text" readonly="true" value="<?php echo e($kantor); ?>" />
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Divisi</label>
                          <div class=" col-md-8" >
                            <input class="form-control" type="text" readonly="true" value="<?php echo e($divisi); ?>" />
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Kategori</label>
                          <div class=" col-md-8" >
                            <select name="kategori" class="form-control bs-select" required />
                                <option value="">Pilih</option>
                                <option value="Komputer">Komputer</option>
                                <option value="Kendaraan">Kendaraan</option>
                                <option value="Lainnya">Lainnya</option>
                            </select>
                          </div>
                      </div>

                      <div class="form-group">
                          <label class="control-label col-md-4">Keterangan</label>
                          <div class=" col-md-8" >
                            <textarea  class="form-control summernote-html" name="keterangan"></textarea>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Alasan</label>
                          <div class=" col-md-8" >
                            <textarea class="form-control" name="alasan"></textarea>
                          </div>
                      </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
        <div class="col-md-12">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Daftar Purchase Request</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                  <table
                  id="table-ijin-pending"
                  data-kolom='[
                    { "data": "nomer","orderable":false },
                    { "data": "tanggal_request","orderable":false },
                    { "data": "kategori","orderable":false },
                    { "data": "keterangan","orderable":false},
                    { "data": "alasan","orderable":false},
                    { "data": "status","orderable":false}
                    ]'
                    action="<?php echo e(url('table-request')); ?>"
                    class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                  <thead>
                  <tr>
                      <th class="all">No.</th>
                      <th class="min-mobile-p">Tanggal Request</th>
                      <th class="min-tablet-l">Kategori</th>
                      <th class="none">Keterangan</th>
                      <th class="none">Alasan</th>
                      <th class="min-tablet-l">Status</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  </table>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>