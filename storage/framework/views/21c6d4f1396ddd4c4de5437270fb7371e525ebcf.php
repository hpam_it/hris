<?php $__env->startSection('title'); ?>
Dashboard
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid home">
  <div class="row">
  <div class="col-md-6">
    <div class="portlet box yellow" >
        <div class="portlet-title">
            <div class="caption">
            <i class="fa fa-tasks"></i>Rasio Absensi</div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body" >
            <table class="table table-bordered">
                <tr>
                  <td>Hari Kerja (Year to Date)</td>
                  <td><?php echo e($hari_aktif); ?></td>
                </tr>
                <tr>
                  <td>Hari Kerja Sesuai</td>
                  <td><?php echo e($hari_kerja); ?></td>
                </tr>
                <tr>
                  <td>Cuti</td>
                  <td><?php echo e($total_cuti); ?></td>
                </tr>

                <?php $__currentLoopData = $ijin; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ijin): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($ijin->nama_ijin); ?></td>
                  <td><?php echo e($ijin->jumlah_ijin); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td>Datang Telat/Pulang Cepat</td>
                  <td><?php echo e($hari_cepat); ?></td>
                </tr>
                <tr>
                  <td>Mangkir</td>
                  <td><?php echo e($total_mangkir); ?></td>
                </tr>
                <tr>
                    <th>Rasio Hari Kerja Sesuai</th>
                    <th><?php echo e($persentase_kerja_hari); ?>%</th>
                </tr>
                <tr>
                    <th>Rasio Jam Kerja</th>
                    <th><?php echo e($persentase_kerja); ?>%</th>
                </tr>
                <tr>
                    <td>Absen Periode Sebelumnya (<b><?php echo e($tanggalPreviousPeriod); ?></b>)</td>
                    <td><?php echo e($previousPeriod); ?> Hari</td>
                </tr>
                <tr>
                    <td>Absen Periode Sekarang (<b><?php echo e($tanggalCurrentPeriod); ?></b>)</td>
                    <td><?php echo e($currentPeriod); ?> Hari</td>
                </tr>
            </table>
        </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="portlet box blue" >
        <div class="portlet-title">
            <div class="caption">
            <i class="fa fa-power-off"></i>Cuti Minggu Ini</div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body" >
              <table id="dashboard-cuti" data-kolom='[
                { "data": "nama_karyawan" },
                { "data": "tanggal_cuti" ,render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" )}
                ]'

                action='<?php echo e(url('dashboard')); ?>'
                class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
              <thead>
              <tr>
                  <th class="all text-center">Nama Karyawan</th>
                  <th class="min-mobile-p text-center">Tanggal</th>
              </tr>
              </thead>
              <tbody>

              </tbody>
              </table>
        </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="portlet box green" >
        <div class="portlet-title">
            <div class="caption">
            <i class="fa fa-gift"></i>Ulang Tahun</div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body" >
          <table id="dashboard-ultah" data-kolom='[
            { "data": "nama_karyawan" },
            { "data": "tgl_ultah",render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM", "id" ) }
            ]'

            action='<?php echo e(url('dashboard')); ?>'
            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
          <thead>
          <tr>
              <th class="all text-center">Nama karyawan</th>
              <th class="min-mobile-p text-center">Tanggal</th>
          </tr>
          </thead>
          <tbody>

          </tbody>
          </table>
        </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="portlet box red" >
        <div class="portlet-title">
            <div class="caption">
            <i class="fa fa-plane"></i>Hari Libur</div>
            <div class="tools"> </div>
        </div>
        <div class="portlet-body" >
          <table id="dashboard-libur" data-kolom='[
            { "data": "tanggal",render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" ) },
            { "data": "keterangan" }
            ]'

            action='<?php echo e(url('dashboard')); ?>'
            class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
          <thead>
          <tr>
              <th class="all text-center">Tanggal</th>
              <th class="min-mobile-p text-center">Keterangan</th>
          </tr>
          </thead>
          <tbody>

          </tbody>
          </table>
        </div>
    </div>
  </div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>