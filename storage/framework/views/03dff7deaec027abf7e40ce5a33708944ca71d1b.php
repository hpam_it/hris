<?php $__env->startSection('title'); ?>
Data Divisi
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Input Data Pangkat</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  action="<?php echo e(url('data-pangkat')); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Pangkat</label>
                            <input type="text" name="nama_pangkat" id="nama_pangkat" class="pertama form-control" placeholder="Nama Pangkat" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kode Pangkat</label>
                            <input type="text" name="kode_pangkat" id="kode_pangkat" class="form-control" placeholder="Kode Pangkat" required="true">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>List Data Divisi</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer" },
                      { "data": "nama_pangkat" },
                      { "data": "kode_pangkat" },
                      { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">NO</th>
                        <th class="min-mobile-p">Nama Divisi</th>
                        <th class="min-tablet-l">Kode</th>
                        <th class="none">Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>