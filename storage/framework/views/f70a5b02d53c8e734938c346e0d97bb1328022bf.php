<?php $__env->startSection('title'); ?>
Data progress
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Input Data Progress</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  action="<?php echo e(url('data-progress')); ?>">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Progress</label>
                            <input type="text" name="nama_progress" id="nama_progress" class="pertama form-control" placeholder="Nama progress" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Butuh Alasan</label>
                            <select name="butuh_alasan" id="butuh_alasan" class="form-control" required="true">
                              <option value="Tidak">Tidak</option>
                              <option value="Ya">Ya</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Butuh Nominal</label>
                            <select name="butuh_nominal" id="butuh_nominal" class="form-control" required="true">
                              <option value="Tidak">Tidak</option>
                              <option value="Ya">Ya</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Butuh Tanggal</label>
                            <select name="butuh_tanggal" id="butuh_tanggal" class="form-control" required="true">
                              <option value="Tidak">Tidak</option>
                              <option value="Ya">Ya</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Sebagai Hasil Progress Visit</label>
                            <select name="progress_visit" id="progress_visit" class="form-control" required="true">
                              <option value="Tidak">Tidak</option>
                              <option value="Ya">Ya</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Sebagai Progress Followup</label>
                            <select name="progress_followup" id="progress_followup" class="form-control" required="true">
                              <option value="Tidak">Tidak</option>
                              <option value="Ya">Ya</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Sebagai Hasil Progress Followup</label>
                            <select name="hasil_progress_followup" id="hasil_progress_followup" class="form-control" required="true">
                              <option value="Tidak">Tidak</option>
                              <option value="Ya">Ya</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>List Data Progress</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer","orderable":false },
                      { "data": "nama_progress","orderable":false },
                      { "data": "butuh_alasan","orderable":false },
                      { "data": "butuh_nominal","orderable":false },
                      { "data": "butuh_tanggal","orderable":false },
                      { "data": "progress_visit","orderable":false },
                      { "data": "progress_followup","orderable":false },
                      { "data": "hasil_progress_followup","orderable":false },
                      { "data": "action","orderable":false }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">No.</th>
                        <th class="min-mobile-p">Nama Progress</th>
                        <th class="min-mobile-l">Alasan</th>
                        <th class="min-tablet-l">Nominal</th>
                        <th class="min-tablet-l">Tanggal</th>
                        <th class="min-tablet-l">Visit</th>
                        <th class="min-tablet-l">Followup</th>
                        <th class="min-tablet-l">Hasil Followup</th>
                        <th class="min-tablet-l">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>