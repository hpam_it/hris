<?php $__env->startSection('title'); ?>
Pengaturan Absen
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Pengturan Absensi</div>
                </div>
                <div class="portlet-body form">
                  <div class="tabbable-custom ">
                      <ul class="nav nav-tabs ">
                          <li class="active">
                              <a href="#data-absen" data-toggle="tab">Data Absen</a>
                          </li>
                          <li>
                              <a href="#import-log" data-toggle="tab">Import Log</a>
                          </li>
                          <li>
                              <a href="#jadwal" data-toggle="tab">Jadwal</a>
                          </li>
                      </ul>
                      <div class="tab-content">
                          <div class="tab-pane active" id="data-absen">
                            <!-- Berisi data absensi karyawan sesuai jadwal yang berlaku -->
                            <table id="table-absen"
                              data-kolom='[
                                  {"data":"nama_karyawan","orderable":false},
                                  {"data":"npp","orderable":false},
                                  {"data":"tanggal","orderable":false},
                                  {"data":"masuk","orderable":false},
                                  {"data":"pulang","orderable":false},
                                  {"data":"cek_in","orderable":false},
                                  {"data":"cek_out","orderable":false},
                                  {"data":"jam_kerja","orderable":false}
                              ]'

                              action="<?php echo e(url('absensi')); ?>"
                              class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th colspan="8">
                                  Tanggal :
                                  <input style="padding:4px;" type="text" class="date-picker" id="absen_start">
                                  S/D
                                  <input style="padding:4px; margin-right:10px;"Type="text" class="date-picker" id="absen_end">
                                  Departement : <select style="padding:4px;"  id="absen_divisi">
                                                  <option value="all">All</option>
                                                  <?php $__currentLoopData = $divisi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $divisi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                  <option value="<?php echo e($divisi->id); ?>"><?php echo e($divisi->nama_divisi); ?></option>
                                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select> &nbsp;
                                  <button type="button" onclick="filter_absen(listabsen)" class="btn btn-md blue" >Filter</button>
                                </th>
                            </tr>
                            <tr>
                                <th class="min-mobile-p">Nama</th>
                                <th class="min-tablet-l">Npp</th>
                                <th class="min-mobile-p">Tanggal</th>
                                <th class="min-tablet-l">Masuk</th>
                                <th class="min-tablet-l">Pulang</th>
                                <th class="min-mobile-p">In</th>
                                <th class="min-mobile-p">Out</th>
                                <th class="min-tablet-l">Jam Kerja</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            </table>
                          </div>
                          <div class="tab-pane" id="import-log">
                            <form method="POST" class="form-horizontal" id="form_import"  action="<?php echo e(url('absensi')); ?> ">
                                 <?php echo e(csrf_field()); ?>

                                 <input type="hidden" name="aksi" value="import-log">
                                 <div class="form-body">
                                     <div class="form-group">
                                         <label class="control-label col-md-2">Tanggal Awal</label>
                                        <div class="col-md-2">
                                          <input type="text" name="awal" id="tgl_awal" class="form-control date-picker"  required="true">
                                        </div>
                                        <div class="col-md-1">
                                          S/D
                                        </div>
                                        <div class="col-md-2">
                                          <input type="text" name="akhir" id="tgl_akhir" class="form-control date-picker"  required="true">
                                        </div>
                                     </div>
                                     <div class="form-group">
                                       <label class="control-label col-md-2">Progress</label>
                                       <div class="col-md-2">
                                         <input type="text" name="posisi" id="sekarang" class="form-control"  readonly value="0">
                                       </div>
                                       <div class="col-md-1">
                                         Dari
                                       </div>
                                       <div class="col-md-2">
                                         <input type="text" id="total" class="form-control" readonly  value="0">
                                       </div>
                                     </div>
                                 </div>
                                 <div class="form-actions">
                                     <button type="button" onclick="import_log();" class="simpan_input btn green">Import</button>
                                 </div>
                             </form>
                          </div>
                          <div class="tab-pane" id="jadwal">
                            <div class="row">
                                <div class="col-md-12">
                                       <form class="form-horizontal" method="POST" id="form_input"  action="<?php echo e(url('absensi')); ?> ">
                                            <?php echo e(csrf_field()); ?>

                                            <input type="hidden"  name="aksi" value="simpan-jadwal">
                                            <div class="form-body">
                                              <div class="col-md-6">
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Jam Kerja</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control" placeholder="Nama Jam Kerja" name="nama_jamkerja" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Masuk (Jam)</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control timepicker" name="masuk" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class=form-group>
                                                      <label class="control-label col-md-3">Pulang (Jam)</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control timepicker" name="pulang" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Toleransi (Jam)</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control timepicker" name="toleransi" required=""/>
                                                      </div>
                                                  </div>

                                                </div>
                                                <div class="col-md-6">
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Max-Pulang (Jam)</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control timepicker" name="maxpulang" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Istirahat (waktu)</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control timepicker" name="istirahat" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Total Kerja (waktu)</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control timepicker" name="jam_normal" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Masa Berlaku</label>
                                                      <div class="col-md-4">
                                                          <input type="text" class="form-control date-picker" name="tgl_mulai" required=""/>
                                                      </div>
                                                      <div class="col-md-1 text-center">
                                                        S/D
                                                      </div>
                                                      <div class="col-md-4">
                                                          <input type="text" class="form-control date-picker" name="tgl_selesai" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                    <div class="col-md-12 text-right">
                                                      <button type="button" onclick="simpan_jadwal();" class="simpan_input btn green">Submit</button>
                                                    </div>
                                                  </div>
                                                </div>
                                          </div>

                                        </form>

                                  </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet box blue" id="tempat_table" >
                                        <div class="portlet-title">
                                            <div class="caption">
                                            <i class="fa fa-gift"></i>List Jadwal</div>
                                            <div class="tools"> </div>
                                        </div>
                                        <div class="portlet-body" id="list_input" >
                                            <table id="table-list" data-kolom='[
                                              { "data": "nomer" },
                                              { "data": "nama_jamkerja" },
                                              { "data": "masuk" },
                                              { "data": "pulang" },
                                              { "data": "toleransi" },
                                              { "data": "maxpulang" },
                                              { "data": "tgl_mulai" },
                                              { "data": "tgl_selesai" },
                                              { "data": "jam_normal" },
                                              { "data": "istirahat" },
                                              { "data": "action" }
                                              ]'
                                              class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="all">NO</th>
                                                <th class="min-mobile-p">Jam Kerja</th>
                                                <th class="min-tablet-l">Masuk</th>
                                                <th class="min-tablet-l">Pulang</th>
                                                <th class="none">Toleransi</th>
                                                <th class="none">Max Pulang</th>
                                                <th class="min-tablet-l">Tanggal Mulai</th>
                                                <th class="min-tablet-l">Tanggal Selesai</th>
                                                <th class="none">Jam Normal</th>
                                                <th class="none">Istirahat</th>
                                                <th class="none">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>


                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>