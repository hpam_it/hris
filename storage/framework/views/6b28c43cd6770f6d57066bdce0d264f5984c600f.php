<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Jenis Izin</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  action="<?php echo e(url('data-ijin')); ?>/<?php echo e($item->id); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="update_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Izin</label>
                            <input type="text" value="<?php echo e($item->nama_ijin); ?>" name="nama_ijin" id="nama_ijin" class="pertama form-control" placeholder="Nama ijin" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Hitung Jam Kerja</label>
                            <select name="hitung" class="form-control" id="hitung" required>
                              <option <?php if($item->hitung=='0'): ?> selected <?php endif; ?> value="0">Tidak</option>
                              <option <?php if($item->hitung=='1'): ?> selected <?php endif; ?> value="1">Hitung</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Sehari Penuh</label>
                            <select name="fullday" class="form-control" id="fullday" required>
                              <option <?php if($item->fullday=='0'): ?> selected <?php endif; ?>  value="0">Tidak</option>
                              <option <?php if($item->fullday=='1'): ?> selected <?php endif; ?>  value="1">Ya</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
