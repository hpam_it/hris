<?php $__env->startSection('title'); ?>
Data Target Survey
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Input Target</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  action="<?php echo e(url('target-survey')); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Karyawan</label>
                            <select class="form-control selectpicker" data-live-search="true" name="karyawan_id" id="karyawan_id" >
                                  <?php $__currentLoopData = $karyawan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $karyawan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($karyawan->id); ?>"><?php echo e($karyawan->nama_karyawan); ?></option>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Group Survey</label>
                            <select class="form-control" name="group_id" id="group_id" >
                                  <?php $__currentLoopData = $group; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($group->id); ?>"><?php echo e($group->nama_group); ?></option>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            <select class="form-control" name="status" id="status" >
                                    <option value="1">Aktif</option>
                                    <option value="0">Off</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Daftar Target Survey</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nama_karyawan" },
                      { "data": "nama_group" },
                      { "data": "status_target" },
                      { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">Nama karyawan</th>
                        <th class="min-mobile-p">Nama group</th>
                        <th class="min-tablet-l">Status</th>
                        <th class="min-tablet-l">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>