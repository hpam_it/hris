
<?php $__env->startSection('title'); ?>
Pengaturan Absen
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="icon-settings"></i>Pengturan Absensi</div>
                </div>
                <div class="portlet-body form">
                  <div class="tabbable-custom ">
                      <ul class="nav nav-tabs ">
                          <li class="active">
                              <a href="#data-absen" data-toggle="tab">Data Absensi</a>
                          </li>
                          <li>
                              <a href="#rekap_absensi" data-toggle="tab">Rekap Absensi</a>
                          </li>
                          <li>
                              <a href="#import-log" data-toggle="tab">Import Log</a>
                          </li>
                          <li>
                              <a href="#jadwal" data-toggle="tab">Jadwal</a>
                          </li>
                          <li>
                              <a href="#rekap-cuti" data-toggle="tab">Rekap Cuti</a>
                          </li>
                      </ul>
                      <div class="tab-content">
                          <div class="tab-pane active" id="data-absen">
                            <!-- Berisi data absensi karyawan sesuai jadwal yang berlaku -->
                            <table id="table-absen"
                              data-kolom='[
                                  {"data":"nama_karyawan","orderable":false},
                                  {"data":"npp","orderable":false},
                                  {"data":"tanggal","orderable":false,render: $.fn.dataTable.render.moment( "YYYY-MM-DD", "Do MMM YYYY", "id" )},
                                  {"data":"cek_in","orderable":false},
                                  {"data":"cek_out","orderable":false},
                                  {"data":"jam_kerja","orderable":false},
                                  {"data":"keterangan","orderable":false}
                              ]'

                              action="<?php echo e(url('absensi')); ?>"
                              class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th colspan="6">
                                  Tanggal :
                                  <input style="padding:4px;" type="text" class="date-picker" id="absen_start">
                                  S/D
                                  <input style="padding:4px; margin-right:10px;"Type="text" class="date-picker" id="absen_end">
                                  Departement : <select style="padding:4px;"  id="absen_divisi">
                                                  <option value="all">All</option>
                                                  <?php $__currentLoopData = $divisi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $divisi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                  <option value="<?php echo e($divisi->id); ?>"><?php echo e($divisi->nama_divisi); ?></option>
                                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select> &nbsp;
                                  <button type="button" onclick="filter_absen(listabsen)" class="btn btn-md blue" >Filter</button>

                                </th>
                            </tr>
                            <tr>
                                <th class="min-mobile-p">Nama</th>
                                <th class="min-tablet-l">NPP</th>
                                <th class="min-mobile-p">Tanggal</th>
                                <th class="min-mobile-p">Jam Masuk</th>
                                <th class="min-mobile-p">Jam Keluar</th>
                                <th class="min-tablet-l">Jam Kerja</th>
                                <th class="min-tablet-l">Keterangan</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            </table>
                          </div>
                          <!-- rekap absensi -->
                          <div class="tab-pane" id="rekap_absensi">
                            <!-- Berisi data absensi karyawan sesuai jadwal yang berlaku -->
                            <table id="table-rekap"
                              data-kolom='[
                                  {"data":"nama_karyawan"},
                                  {"data":"npp","orderable":false},
                                  {"data":"nama_divisi","orderable":false},
                                  {"data":"nama_jabatan","orderable":false},
                                  {"data":"hari_aktif","orderable":false},
                                  {"data":"hari_kerja","orderable":false},
                                  {"data":"total_cuti"},
                                  {"data":"hari_cepat"},
                                  {"data":"hari_ijin"},
                                  {"data":"total_mangkir"},
                                  {"data":"persentase_kerja_hari"},
                                  {"data":"persentase_kerja"}
                              ]'

                              action="<?php echo e(url('rasio_karyawan')); ?>"
                              class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th colspan="7">
                                  Tanggal :
                                  <input style="padding:4px;" type="text" class="date-picker" id="rekap_start">
                                  S/D
                                  <input style="padding:4px; margin-right:10px;"Type="text" class="date-picker" id="rekap_end">
                                  Departement : <select style="padding:4px;"  id="rekap_divisi">
                                                  <option value="all">All</option>
                                                  <?php $__currentLoopData = $divisi2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $divisi2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                  <option value="<?php echo e($divisi2->id); ?>"><?php echo e($divisi2->nama_divisi); ?></option>
                                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select> &nbsp;
                                  Status Kerja : <select style="padding:4px;"  id="filter">
                                                    <?php $__currentLoopData = $status_kerja; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $status_kerja): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                      <option value="<?php echo e($status_kerja->status_karyawan); ?>"><?php echo e($status_kerja->status_karyawan); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                  <button type="button" onclick="filter_absen(rekapabsensi)" class="btn btn-md blue" >Filter</button>

                                </th>
                            </tr>
                            <tr>
                                <th class="min-mobile-p">Nama</th>
                                <th class="min-tablet-l">NPP</th>
                                <th class="min-mobile-p">Divisi</th>
                                <th class="min-mobile-p">Jabatan</th>
                                <th class="min-mobile-p">Hari Kerja</th>
                                <th class="min-mobile-p">Hari Kerja<br>Sesuai</th>
                                <th class="min-tablet-l">Cuti</th>
                                <th class="min-tablet-l">Telat<br>Pulang Cepat</th>
                                <th class="min-tablet-l">Izin</th>
                                <th class="min-tablet-l">Mangkir</th>
                                <th class="min-tablet-l">Rasio<br>Hari Kerja</th>
                                <th class="min-tablet-l">Rasio<br>Jam Kerja</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            </table>
                          </div>
                          <!-- end rekap absensi -->
                          <div class="tab-pane" id="import-log">
                            <form method="POST" class="form-horizontal" id="form_import"  action="<?php echo e(url('absensi')); ?> ">
                                 <?php echo e(csrf_field()); ?>

                                 <input type="hidden" name="aksi" value="import-log">
                                 <div class="form-body">
                                     <div class="form-group">
                                         <label class="control-label col-md-2">Tanggal Awal</label>
                                        <div class="col-md-2">
                                          <input type="text" name="awal" id="tgl_awal" class="form-control date-picker"  required="true">
                                        </div>
                                        <div class="col-md-1">
                                          S/D
                                        </div>
                                        <div class="col-md-2">
                                          <input type="text" name="akhir" id="tgl_akhir" class="form-control date-picker"  required="true">
                                        </div>
                                     </div>
                                     <div class="form-group">
                                       <label class="control-label col-md-2">Progress</label>
                                       <div class="col-md-2">
                                         <input type="text" name="posisi" id="sekarang" class="form-control"  readonly value="0">
                                       </div>
                                       <div class="col-md-1">
                                         Dari
                                       </div>
                                       <div class="col-md-2">
                                         <input type="text" id="total" class="form-control" readonly  value="0">
                                       </div>
                                     </div>
                                 </div>
                                 <div class="form-actions">
                                     <button type="button" onclick="import_log();" class="simpan_input btn green">Import</button>
                                 </div>
                             </form>
                          </div>
                          <div class="tab-pane" id="jadwal">
                            <div class="row">
                                <div class="col-md-12">
                                       <form class="form-horizontal" method="POST" id="form_input"  action="<?php echo e(url('absensi')); ?> ">
                                            <?php echo e(csrf_field()); ?>

                                            <input type="hidden"  name="aksi" value="simpan-jadwal">
                                            <div class="form-body">
                                              <div class="col-md-5">
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Jam Kerja</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control" placeholder="Nama Jam Kerja" name="nama_jamkerja" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Masuk (Jam)</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control timepicker" name="masuk" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class=form-group>
                                                      <label class="control-label col-md-3">Pulang (Jam)</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control timepicker" name="pulang" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Toleransi (Jam)</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control timepicker" name="toleransi" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Max-Pulang (Jam)</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control timepicker" name="maxpulang" required=""/>
                                                      </div>
                                                  </div>
                                                </div>
                                                <div class="col-md-7">

                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Istirahat (waktu)</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control timepicker" name="istirahat" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Mulai Istirahat (Jam)</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control timepicker" name="jam_istirahat" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Total Kerja (waktu)</label>
                                                      <div class="col-md-9">
                                                          <input type="text" class="form-control timepicker" name="jam_normal" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                      <label class="control-label col-md-3">Masa Berlaku</label>
                                                      <div class="col-md-4">
                                                          <input type="text" class="form-control date-picker" name="tgl_mulai" required=""/>
                                                      </div>
                                                      <div class="col-md-1 text-center">
                                                        S/D
                                                      </div>
                                                      <div class="col-md-4">
                                                          <input type="text" class="form-control date-picker" name="tgl_selesai" required=""/>
                                                      </div>
                                                  </div>
                                                  <div class="form-group">
                                                    <div class="col-md-12 text-right">
                                                      <button type="button" onclick="simpan_jadwal();" class="simpan_input btn green">Submit</button>
                                                    </div>
                                                  </div>
                                                </div>
                                          </div>

                                        </form>

                                  </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12">
                                    <div class="portlet box blue" id="tempat_table" >
                                        <div class="portlet-title">
                                            <div class="caption">
                                            <i class="fa fa-gift"></i>List Jadwal</div>
                                            <div class="tools"> </div>
                                        </div>
                                        <div class="portlet-body" id="list_input" >
                                            <table id="table-list" data-kolom='[
                                              { "data": "nomer" },
                                              { "data": "nama_jamkerja" },
                                              { "data": "masuk" },
                                              { "data": "pulang" },
                                              { "data": "toleransi" },
                                              { "data": "maxpulang" },
                                              { "data": "tgl_mulai" },
                                              { "data": "tgl_selesai" },
                                              { "data": "jam_normal" },
                                              { "data": "istirahat" },
                                              { "data": "action" }
                                              ]'
                                              class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th class="all">NO</th>
                                                <th class="min-mobile-p">Jam Kerja</th>
                                                <th class="min-tablet-l">Masuk</th>
                                                <th class="min-tablet-l">Pulang</th>
                                                <th class="none">Toleransi</th>
                                                <th class="none">Max Pulang</th>
                                                <th class="min-tablet-l">Tanggal Mulai</th>
                                                <th class="min-tablet-l">Tanggal Selesai</th>
                                                <th class="none">Jam Normal</th>
                                                <th class="none">Istirahat</th>
                                                <th class="none">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>


                          </div>
                          <div class="tab-pane" id="rekap-cuti">
                            <div class="row">
                                <div class="col-md-12">
                                  <div class="portlet box blue" id="tempat_table" >
                                      <div class="portlet-title">
                                          <div class="caption">
                                          <i class="fa fa-gift"></i>Rekap Cuti</div>
                                          <div class="tools"> </div>
                                      </div>
                                      <div class="portlet-body"  >
                                          <div class="table-responsive">
                                          <table id="table-rekap-cuti"
                                          action="<?php echo e(url('rekap-cuti')); ?>"
                                          data-kolom='[
                                            { "data": "nama_karyawan" },
                                            { "data": "nama_cuti" },
                                            { "data": "jumlah_awal" },
                                            { "data": "saldo_awal" },
                                            { "data": "diambil"},
                                            { "data": "saldo_cuti"}
                                            ]'
                                            class="table table-striped table-bordered table-hover " width="100%"  cellspacing="0" width="100%">
                                          <thead>
                                          <tr>
                                              <th class="all">Nama Karyawan</th>
                                              <th class="min-tablet-l">Nama Cuti</th>
                                              <th class="min-tablet-l">Jumlah Cuti</th>
                                              <th class="min-tablet-l">Saldo Awal</th>
                                              <th class="min-tablet-l">Cuti Diambil</th>
                                              <th class="min-tablet-l">Sisa Cuti</th>
                                          </tr>
                                          </thead>
                                          <tbody>

                                          </tbody>
                                          </table>
                                        </div>
                                      </div>
                                  </div>
                                  <!-- <button class="btn btn-md" onclick="hitung_cuti_rekap('<?php echo e(url('hitung-cuti')); ?>')">Hitung Cuti</button> -->
                                </div>
                            </div>
                          </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>