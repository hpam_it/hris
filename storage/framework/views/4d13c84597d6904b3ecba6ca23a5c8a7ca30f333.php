<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Divisi</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  action="<?php echo e(url('data-jabatan')); ?>/<?php echo e($item->id); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="update_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="control-label">Nama Jabatan</label>
                          <input type="text" value="<?php echo e($item->nama_jabatan); ?>" name="nama_jabatan" id="nama_jabatan" class="pertama form-control" placeholder="Nama Jabatan" autofocus required="true">
                      </div>
                      <div class="form-group">
                          <label class="control-label">Kode Jabatan</label>
                          <input type="text" value="<?php echo e($item->kode_jabatan); ?>" name="kode_jabatan" id="kode_jabatan" class="form-control" placeholder="Kode Jabatan" required="true">
                      </div>
                      <div class="form-group">
                          <label class="control-label">Wewenang</label>
                          <textarea  name="wewenang" id="wewenang" class="form-control" required="true"><?php echo e($item->wewenang); ?></textarea>
                      </div>
                      <div class="form-group">
                          <label class="control-label">Tanggung Jawab</label>
                          <textarea name="tanggung_jawab" id="tanggung_jawab" class="form-control" required="true"><?php echo e($item->tanggung_jawab); ?></textarea>
                      </div>
                      <div class="form-group">
                          <label class="control-label">Atasan</label>
                          <select name="atasan" id="atasan" class="form-control bs-select">
                              <option value="0">Pilih</option>
                              <?php $__currentLoopData = $jabatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jabatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option <?php if($jabatan->id==$item->atasan ): ?> selected <?php endif; ?> value="<?php echo e($jabatan->id); ?>"><?php echo e($jabatan->nama_jabatan); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>
                      </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
