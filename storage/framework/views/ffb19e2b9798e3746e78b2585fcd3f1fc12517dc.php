<?php $__env->startSection('title'); ?>
Data Nasabah
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Input Data Nasabah</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  action="<?php echo e(url('data-nasabah')); ?>">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Nasabah</label>
                            <input type="text" name="nama_nasabah" id="nama_nasabah" class="pertama form-control" placeholder="Nama nasabah" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Alamat</label>
                            <textarea name="alamat" id="alamat" class="form-control" placeholder="Alamat nasabah" required="true"></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nomor Telepon</label>
                            <input type="text" name="telpon" id="telpon" class="form-control" placeholder="Nomor telpon nasabah" required="true">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>List Data Nasabah</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer","orderable":false },
                      { "data": "nama_nasabah","orderable":false },
                      { "data": "alamat","orderable":false },
                      { "data": "telpon","orderable":false },
                      { "data": "action","orderable":false }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">No.</th>
                        <th class="min-mobile-p">Nama Nasabah</th>
                        <th class="min-mobile-l">Alamat</th>
                        <th class="min-tablet-p">No. Telpon</th>
                        <th class="min-tablet-l">Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>