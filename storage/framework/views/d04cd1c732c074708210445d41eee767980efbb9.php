<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Nasabah</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"   action="<?php echo e(url('data-nasabah')); ?>/<?php echo e($item->id); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="update_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Nasabah</label>
                            <input value="<?php echo e($item->nama_nasabah); ?>" type="text" name="nama_nasabah" id="nama_nasabah" class="pertama form-control" placeholder="Nama nasabah" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Alamat</label>
                            <textarea name="alamat" id="alamat" class="form-control" placeholder="Alamat nasabah"required="true"><?php echo e($item->alamat); ?></textarea>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nomor Telepon</label>
                            <input type="text" value="<?php echo e($item->telpon); ?>" name="telpon" id="telpon" class="form-control" placeholder="Nomor telpon nasabah" required="true">
                        </div>
                    </div>
                    <div class="form-actions">
                    <button onclick="update_input();" type="button" class="simpan_edit btn blue">Save changes</button>
                    <button  type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
