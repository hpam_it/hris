<?php $__env->startSection('title'); ?>
Pengajuan Cuti
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Pengajuan Cuti</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" class="form-horizontal" id="form_input"  action="<?php echo e(url('ambilcuti')); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                      <div class="form-group">
                          <label class="col-md-4 control-label">Karyawan</label>
                          <div class=" col-md-8" >
                            <select name="karyawan_id" id="karyawan_id" class="form-control bs-select" />
                                <option value="">Pilih</option>
                              <?php $__currentLoopData = $karyawan2; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $karyawan2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($karyawan2->id); ?>"><?php echo e($karyawan2->nama_karyawan); ?> / <?php echo e($karyawan2->npp); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Tanggal Cuti</label>
                          <div class=" col-md-8" >
                            <input type="text" name="tanggal_cuti" id="tanggal_cuti" class="date-picker-multi form-control" placeholder="Hari Pertama Cuti" required="true">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Jumlah Hari Cuti (Hari Kerja)</label>
                          <div class=" col-md-8" >
                            <input type="number" name="jumlah_hari" id="jumlah_hari" class="form-control" placeholder="Jumlah Hari Kerja" required="true">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Jenis Cuti</label>
                          <div class=" col-md-8" >
                            <select name="data_cuti_id" id="data_cuti_id" class="form-control bs-select" />
                                <option value="">Pilih</option>
                              <?php $__currentLoopData = $data_cuti; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data_cuti): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($data_cuti->id); ?>"><?php echo e($data_cuti->nama_cuti); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Keperluan</label>
                          <div class=" col-md-8" >
                            <input type="text" name="keperluan" id="keperluan" class="form-control" placeholder="Keperluan Cuti"  required="true">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">Alamat Cuti</label>
                          <div class=" col-md-8" >
                            <textarea name="alamat_cuti" id="alamat_cuti" class="form-control" required="true"></textarea>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">No Telpon</label>
                          <div class=" col-md-8" >
                            <input type="text" name="notelpon" id="notelpon" class="form-control" placeholder="Nomor Telpon cuti"  required="true">
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label col-md-4">PIC Pengganti</label>
                          <div class=" col-md-8" >
                            <select name="pic_pengganti" id="pic_pengganti" class="form-control bs-select" />
                                <option value="">Pilih</option>
                              <?php $__currentLoopData = $karyawan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $karyawan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($karyawan->id); ?>"><?php echo e($karyawan->nama_karyawan); ?> / <?php echo e($karyawan->npp); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                          </div>
                      </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>List Pengajuan Cuti</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer","orderable":false },
                      { "data": "nama_karyawan","orderable":false },
                      { "data": "tanggal_cuti","orderable":false },
                      { "data": "jumlah_hari","orderable":false},
                      { "data": "keperluan","orderable":false},
                      { "data": "alamat_cuti","orderable":false},
                      { "data": "notelpon","orderable":false},
                      { "data": "pengganti","orderable":false},
                      { "data": "action","orderable":false }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">NO</th>
                        <th class="min-mobile-p">Nama</th>
                        <th class="min-tablet-l">Tanggal Cuti</th>
                        <th class="none">HK</th>
                        <th class="none">Keperluan</th>
                        <th class="none">Alamat Cuti</th>
                        <th class="none">Telpon</th>
                        <th class="none">PIC Pengganti</th>
                        <th class="none">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>