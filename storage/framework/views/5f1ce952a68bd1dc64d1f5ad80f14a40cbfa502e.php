<?php $__env->startSection('title'); ?>
Data Calendar
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Input Data Calendar</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  class="form-horizontal" action="<?php echo e(url('data-calendar')); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal</label>
                            <div class="col-md-9">
                              <input type="text" name="tanggal" id="tanggal" class="date-picker pertama form-control" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Keterangan</label>
                            <div class="col-md-9">
                              <input type="text" name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan Libur" required="true">
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_calendar();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
            <!-- list calender -->

            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>List calendar</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer" },
                      { "data": "tanggal" },
                      { "data": "keterangan" },
                      { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">NO</th>
                        <th class="min-mobile-p">Tanggal</th>
                        <th class="min-tablet-l">Keterangan</th>
                        <th class="none">Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div class="col-md-7">
            <div class="portlet box blue" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>List Calendar</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <div id="calendar" class="has-toolbar fc fc-ltr fc-unthemed">
                      
	                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>