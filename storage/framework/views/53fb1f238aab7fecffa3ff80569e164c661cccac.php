<?php $__env->startSection('title'); ?>
Data PTKPS
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>Input Data PTKP</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  action="<?php echo e(url('data-ptkp')); ?> ">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama PTKP</label>
                            <input type="text" name="nama" id="nama" class="pertama form-control" placeholder="Nama PTKP" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Nilai PTKP</label>
                            <input type="text" name="nilai" id="nilai" class="form-control" placeholder="Nilai PTKP" required="true">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class="fa fa-gift"></i>List Data PTKP</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer" },
                      { "data": "nama" },
                      { "data": "nilai", render: $.fn.dataTable.render.number( ".", ",", 0, "Rp" ) },
                      { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="all">NO</th>
                        <th class="min-mobile-p">Nama PTKP</th>
                        <th class="min-tablet-l">Nilai</th>
                        <th class="none">Action</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>