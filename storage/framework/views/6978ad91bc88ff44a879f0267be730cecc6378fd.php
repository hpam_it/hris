<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Edit Data Struktur</h4>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12">
           <form method="PATCH" id="form_update"  action="<?php echo e(url('data-struktur')); ?>/<?php echo e($struktur->id); ?> ">
             <?php echo e(csrf_field()); ?>

             <input type="hidden" name="aksi" value="simpan_item">
             <div class="form-body">
                 <div class="form-group">
                     <label class="control-label">Nama Jabatan</label>
                     <select name="jabatan_id" id="jabatan_id" class="form-control bs-select" data-live-search="true" >
                         <option value="0">Pilih</option>
                         <?php $__currentLoopData = $jabatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jabatans): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <option <?php if($struktur->jabatan_id == $jabatans->id ): ?> selected <?php endif; ?> value="<?php echo e($jabatans->id); ?>"><?php echo e($jabatans->nama_jabatan); ?></option>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </select>
                 </div>
                 <div class="form-group">
                     <label class="control-label">Nama Divisi</label>
                     <select name="divisi_id" id="divisi_id" class="form-control bs-select" data-live-search="true" >
                         <option value="0">Pilih</option>
                         <?php $__currentLoopData = $divisi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $divisis): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <option <?php if($struktur->divisi_id == $divisis->id ): ?> selected <?php endif; ?> value="<?php echo e($divisis->id); ?>"><?php echo e($divisis->nama_divisi); ?></option>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </select>
                 </div>
                 <div class="form-group">
                     <label class="control-label">Kantor</label>
                     <select name="kantor_id" id="kantor_id" class="form-control bs-select" data-live-search="true" >
                         <option value="0">Pilih</option>
                         <?php $__currentLoopData = $kantor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kantors): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <option <?php if($struktur->kantor_id == $kantors->id ): ?> selected <?php endif; ?> value="<?php echo e($kantors->id); ?>"><?php echo e($kantors->nama_kantor); ?></option>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </select>
                 </div>
                 <h3 class="form-section">Atasan</h3>
                 <div class="form-group">
                     <label class="control-label">Nama Jabatan</label>
                     <select name="jabatan_atasan" id="jabatan_atasan" class="form-control bs-select" data-live-search="true" >
                         <option value="0">Pilih</option>
                         <?php $__currentLoopData = $jabatan; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $jabatan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <option <?php if($struktur->jabatan_atasan == $jabatan->id ): ?> selected <?php endif; ?> value="<?php echo e($jabatan->id); ?>"><?php echo e($jabatan->nama_jabatan); ?></option>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </select>
                 </div>
                 <div class="form-group">
                     <label class="control-label">Nama Divisi</label>
                     <select name="divisi_atasan" id="divisi_atasan" class="form-control bs-select" data-live-search="true" >
                         <option value="0">Pilih</option>
                         <?php $__currentLoopData = $divisi; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $divisi): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <option <?php if($struktur->divisi_atasan == $divisi->id ): ?> selected <?php endif; ?> value="<?php echo e($divisi->id); ?>"><?php echo e($divisi->nama_divisi); ?></option>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </select>
                 </div>
                 <div class="form-group">
                     <label class="control-label">Kantor</label>
                     <select name="kantor_atasan" id="kantor_atasan" class="form-control bs-select" data-live-search="true" >
                         <option value="0">Pilih</option>
                         <?php $__currentLoopData = $kantor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kantor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <option <?php if($struktur->kantor_atasan == $kantor->id ): ?> selected <?php endif; ?> value="<?php echo e($kantor->id); ?>"><?php echo e($kantor->nama_kantor); ?></option>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </select>
                 </div>
             </div>
             <div class="form-actions">
                 <button type="button" onclick="update_input();" class="simpan_input btn green">Submit</button>
                 <button data-dismiss="modal" type="button" class="btn default">Cancel</button>
             </div>
            </form>
        </div>
    </div>
</div>
<div class="modal-footer">

</div>
