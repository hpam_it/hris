<?php $__env->startSection('title'); ?>
Data Jenis Izin
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green" id="box_form_input">
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Input Data Jenis izin</div>
                </div>
                <div class="portlet-body form">
                <!-- BEGIN FORM-->
               <form method="POST" id="form_input"  action="<?php echo e(url('data-ijin')); ?>">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="aksi" value="simpan_item">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Nama Izin</label>
                            <input type="text" name="nama_ijin" id="nama_ijin" class="pertama form-control" placeholder="Nama ijin" autofocus required="true">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Hitung Jam Kerja</label>
                            <select name="hitung" class="form-control" id="hitung" required>
                              <option value="0">Tidak</option>
                              <option value="1">Hitung</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Sehari Penuh</label>
                            <select name="fullday" class="form-control" id="fullday" required>
                              <option value="0">Tidak</option>
                              <option value="1">YA</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Hanya Untuk Marketing</label>
                            <select name="hanya_marketing" class="form-control" id="hanya_marketing" required>
                              <option value="Tidak">Tidak</option>
                              <option value="Ya">Ya</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="button" onclick="simpan_input();" class="simpan_input btn green">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                </form>
                <!-- END FORM-->
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box blue" id="tempat_table" >
                <div class="portlet-title">
                    <div class="caption">
                    <i class=" icon-grid"></i>Daftar Jenis Izin</div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body" id="list_input" >
                    <table id="table-list" data-kolom='[
                      { "data": "nomer" },
                      { "data": "nama_ijin" },
                      { "data": "hitung_kerja" },
                      { "data": "sehari_penuh" },
                      { "data": "hanya_marketing" },
                      { "data": "action" }
                      ]'
                      class="table table-striped table-bordered table-hover dt-responsive" width="100%"  cellspacing="0" width="100%">
                    <thead>
                      <tr>
                          <th class="all">No.</th>
                          <th class="min-mobile-p">Nama Izin</th>
                          <th class="min-tablet-l">Hitung Jam Kerja</th>
                          <th class="min-tablet-l">Sehari Penuh</th>
                          <th class="min-tablet-l">Hanya Marketing</th>
                          <th class="none">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('home', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>