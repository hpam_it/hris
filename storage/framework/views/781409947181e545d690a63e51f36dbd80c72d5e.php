<?php $__env->startComponent('mail::message'); ?>
# <?php echo e($content['title']); ?>

<?php echo e($content['text']); ?>

<?php $__env->startComponent('mail::panel'); ?>
  Nama : <?php echo e($content['nama']); ?><br>
  Tanggal Izin : <?php echo e($content['tanggal']); ?><br>
  Jenis Izin : <?php echo e($content['jenis_ijin']); ?><br>
  Keperluan : <?php echo e($content['alasan']); ?><br>
<?php echo $__env->renderComponent(); ?>

<?php $__env->startComponent('mail::button', ['url' => 'http://192.168.7.9' ]); ?>
<?php echo e($content['button']); ?>

<?php echo $__env->renderComponent(); ?>

Terima Kasih,

<?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>
