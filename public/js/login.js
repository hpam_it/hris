function reset_password(){
  var email=$('#email_reset').val();
  $.ajax({
            type:"POST",
            url: $("#form_reset_password").attr('action'),
            data:$("#form_reset_password").serialize(),
                  beforeSend:function(){
                      App.blockUI({
                  target: '#form_reset_password',
                  animate: true
              });
                  },
            success: function(response){
              if(response=='oke'){
                 alert('Email reset telah dikirim ke email anda');
                 App.unblockUI('#form_reset_password');
                 $(location).attr('href','/');
              }else{
                App.unblockUI('#form_reset_password');
                alert('Email tidak terdaftar');
              }


          },
               error: function(){
                  alert('Email tidak terdaftar');
                  App.unblockUI('#form_reset_password');
          }
        });
}

function ganti_password(){
  $.ajax({
            type:"POST",
            url: $("#form_ganti_password").attr('action'),
            data:$("#form_ganti_password").serialize(),
                  beforeSend:function(){
                      App.blockUI({
                  target: '#form_ganti_password',
                  animate: true
              });
                  },
            success: function(response){
              if(response=='oke'){
                 alert('Password Anda telah di ganti, silahkan login dengan password baru anda');
                 App.unblockUI('#form_ganti_password');
                 $(location).attr('href','/');
              }else{
                App.unblockUI('#form_ganti_password');
                alert('Email salah atau link expired');
              }

          },
               error: function(){
                  alert('Email salah atau link expired');
                  App.unblockUI('#form_ganti_password');
          }
        });
}
