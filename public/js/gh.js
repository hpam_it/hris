$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function notifikasi(teks,jenis){
    swal("PESAN",teks,jenis);
}

//untuk confirmasi di data table
$('#tempat_table tbody').on('click','.konfirmasi', function () {
  $(this).confirmation('show');

  $('.hapus-input').on('confirmed.bs.confirmation', function () {
      var url=$(this).attr('data-url');
      hapus_input(url);
  });

  $('.tolak-ijin').on('confirmed.bs.confirmation', function () {
      var url=$(this).attr('data-url');
      hapus_input(url);
  });

} );


//datatable
var kolom =  eval($("#table-list").attr('data-kolom'));
var tablemodal = $("#table-modal").dataTable();
var url1 = $("#form_input").attr('action');
if(url1 == undefined){
  url1 = 'http://localhost';
}

var listTable=$("#table-list").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url1.replace(/\s/g, "")+"/0?aksi=list-all",
            "columns": kolom,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});
//list table tanpa form
var kolom2 =  eval($("#table-list-aja").attr('data-kolom'));
var url3 =$("#table-list-aja").attr('action');
if(url3==undefined){
  url3 = 'http://localhost';
}
var listTableAja=$("#table-list-aja").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url3.replace(/\s/g, "")+"/0?aksi=list-all",
            "columns": kolom2,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});


//list table dashboard-cuti
var kolom10 =  eval($("#dashboard-cuti").attr('data-kolom'));
var url30 =$("#dashboard-cuti").attr('action');
if(url30==undefined){
  url30 = 'http://localhost';
}
var listTablecuti=$("#dashboard-cuti").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url30.replace(/\s/g, "")+"/0?aksi=cuti",
            "columns": kolom10,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});

//list table dashboard-task
var kolom11 =  eval($("#dashboard-task").attr('data-kolom'));
var url31 =$("#dashboard-task").attr('action');
if(url31==undefined){
  url31 = 'http://localhost';
}
var listTabletask=$("#dashboard-task").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url31.replace(/\s/g, "")+"/0?aksi=task",
            "columns": kolom11,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});

//list table ultah
var kolom12 =  eval($("#dashboard-ultah").attr('data-kolom'));
var url32 =$("#dashboard-ultah").attr('action');
if(url32==undefined){
  url32 = 'http://localhost';
}
var listTableultah=$("#dashboard-ultah").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url32.replace(/\s/g, "")+"/0?aksi=ulangtahun",
            "columns": kolom12,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});

//list table hari_libur
var kolom13 =  eval($("#dashboard-libur").attr('data-kolom'));
var url33 =$("#dashboard-libur").attr('action');
if(url33==undefined){
  url33 = 'http://localhost';
}
var listTablelibur=$("#dashboard-libur").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url33.replace(/\s/g, "")+"/0?aksi=hari_libur",
            "columns": kolom13,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});

//list table ijin pending
var kolom_ijin0 = eval($("#table-ijin-pending").attr('data-kolom'));
var url_ijin0 =$("#table-ijin-pending").attr('action');
if(url_ijin0==undefined){
  url_ijin0 = 'http://localhost';
}
var list_ijin_pending=$("#table-ijin-pending").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url_ijin0.replace(/\s/g, "")+"/0?aksi=list_all&status=pending",
            "columns": kolom_ijin0,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});

//table dinas selesai
var kolom_ijin09 = eval($("#table-dinas-selesai").attr('data-kolom'));
var url_ijin09 =$("#table-dinas-selesai").attr('action');
if(url_ijin09==undefined){
  url_ijin09 = 'http://localhost';
}
var listDinasSelesai=$("#table-dinas-selesai").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url_ijin09.replace(/\s/g, "")+"/0?aksi=list_all&status=selesai",
            "columns": kolom_ijin09,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});

//list dinas progress
var kolom_ijin10 = eval($("#table-dinas-progress").attr('data-kolom'));
var url_ijin10 =$("#table-dinas-progress").attr('action');
if(url_ijin10==undefined){
  url_ijin10 = 'http://localhost';
}
var listDinasProgress=$("#table-dinas-progress").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url_ijin10.replace(/\s/g, "")+"/0?aksi=list_all&status=progress",
            "columns": kolom_ijin10,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});


//list table ijin batal
var kolom_ijin1 =  eval($("#table-ijin-batal").attr('data-kolom'));
var url_ijin1 =$("#table-ijin-batal").attr('action');
if(url_ijin1==undefined){
  url_ijin1 = 'http://localhost';
}
var list_ijin_batal=$("#table-ijin-batal").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url_ijin1.replace(/\s/g, "")+"/0?aksi=list_all&status=batal",
            "columns": kolom_ijin1,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});


//list table ijin oke
var kolom_ijin2 =  eval($("#table-ijin-oke").attr('data-kolom'));
var url_ijin2 =$("#table-ijin-oke").attr('action');
if(url_ijin2==undefined){
  url_ijin2 = 'http://localhost';
}
var list_ijin_oke=$("#table-ijin-oke").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url_ijin2.replace(/\s/g, "")+"/0?aksi=list_all&status=oke",
            "columns": kolom_ijin2,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});

//list table data absen
var kolom4 =  eval($("#table-absen").attr('data-kolom'));
var url4 =$("#table-absen").attr('action');
if(url4==undefined){
  url4 = 'http://localhost';
}
var listabsen=$("#table-absen").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax":{
            "url": url4.replace(/\s/g, "")+"/0?aksi=list-absen",
            "data": function ( d ) {
                d.startdate = $("#absen_start").val();
                d.enddate = $("#absen_end").val();
                d.divisi = $("#absen_divisi").val();
                // d.custom = $('#myInput').val();
                // etc
              }
            },
            "columns": kolom4,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});

var kolom122 =  eval($("#table-rekap-cuti").attr('data-kolom'));
var url33 =$("#table-rekap-cuti").attr('action');
if(url33==undefined){
  url33 = 'http://localhost';
}
var listTableRekapCuti=$("#table-rekap-cuti").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url33.replace(/\s/g, "/"),
            "columns": kolom122,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});


//rekap absensi
var kolom123 =  eval($("#table-rekap").attr('data-kolom'));
var url123 =$("#table-rekap").attr('action');
if(url123==undefined){
  url123 = 'http://localhost';
}
var rekapabsensi=$("#table-rekap").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax":{
            "url": url123.replace(/\s/g, ""),
            "data": function ( d ) {
                d.startdate = $("#rekap_start").val();
                d.enddate = $("#rekap_end").val();
                d.divisi = $("#rekap_divisi").val();
                d.filter = $("#filter").val();
                // d.custom = $('#myInput').val();
                // etc
              }
            },
            "columns": kolom123,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});



//list akomodasi selesai
var kolom_akomodasi1 = eval($("#table-akomodasi-selesai").attr('data-kolom'));
var url_akomodasi1 =$("#table-akomodasi-selesai").attr('action');
if(url_akomodasi1==undefined){
  url_akomodasi1 = 'http://localhost';
}
var listAkomodasiSelesai=$("#table-akomodasi-selesai").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url_akomodasi1.replace(/\s/g, "")+"/0?aksi=list_all&status=akomodasi_selesai",
            "columns": kolom_akomodasi1,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});


//list akomodasi progress
var kolom_akomodasi2 = eval($("#table-akomodasi-progress").attr('data-kolom'));
var url_akomodasi2 =$("#table-akomodasi-progress").attr('action');
if(url_akomodasi2==undefined){
  url_akomodasi2 = 'http://localhost';
}
var listAkomodasiProgress=$("#table-akomodasi-progress").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url_akomodasi2.replace(/\s/g, "")+"/0?aksi=list_all&status=akomodasi_progress",
            "columns": kolom_akomodasi2,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});


///approval

//list akomodasi progress
var kolom_approvalSebelum = eval($("#table-approval-sebelum").attr('data-kolom'));
var url_approvalSebelum =$("#table-approval-sebelum").attr('action');
if(url_approvalSebelum==undefined){
  url_approvalSebelum = 'http://localhost';
}
var listApprovalSebelum=$("#table-approval-sebelum").dataTable({
            "processing": true,
        	"serverSide": true,
        	"ajax": url_approvalSebelum.replace(/\s/g, "")+"/?aksi=sebelum",
            "columns": kolom_approvalSebelum,
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },
            buttons: [
                { extend: 'print', className: 'btn default' },
                { extend: 'pdf', className: 'btn default' },
                { extend: 'csv', className: 'btn default' }
            ],
            responsive: {
                details: {

                }
            },

            "order": [
                [0, 'asc']
            ],

            "lengthMenu": [
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
                [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 5,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
});


//filter absen

function filter_absen(a){
  a.api().ajax.reload();
}
//date picker
jQuery(document).ready(function() {
  if (jQuery().datepicker) {
  $('.date-picker').datepicker({
    rtl: App.isRTL(),
    orientation: "left",
    format:"yyyy-mm-dd",
    autoclose: true
 });

 $('.date-picker-multi').datepicker({
   rtl: App.isRTL(),
   orientation: "left",
   format:"yyyy-mm-dd",
   multidate:true,
   multidateSeparator:",",
   autoclose: true
 });

 $( document ).scroll(function(){
            $('#ajax .date-picker').datepicker('place'); //#modal is the id of the modal
        });
}
if (jQuery().timepicker) {
  $('.timepicker').timepicker({
                autoclose: true,
                minuteStep: 5,
                showSeconds: false,
                showMeridian: false
            });
  $('.timepicker').parent('.input-group').on('click', '.input-group-btn', function(e){
        e.preventDefault();
        $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
  });

  $( document ).scroll(function(){
             $('#ajax .timepicker').datepicker('place'); //#modal is the id of the modal
         });
}
//bootstrap select
$('.bs-select').selectpicker({
            iconBase: 'fa',
            tickIcon: 'fa-check',
            size:5
});

$('.angka-angka').number( true, 0,',','.' );


});
//cek_stok($("#barang_id").val());
//format number live ngetik

//untuk enter2 dan ngubah jadi huruf capital
$(document).on('keypress','.form-control',function(e){

  if (e.which==13){
    if(inputs.eq( inputs.index(this) ).is("textarea")){

    }else{
      e.preventDefault();
      var inputs = $(this).closest('form').find(':input');
      if(inputs.eq( inputs.index(this)+ 1 ).is("button")){
      	var form = $(this).closest('form')[0];
      	if (form.id=='form_input'){
      		simpan_input();
      	} else if (form.id=='form_update'){
      		update_input();
      	}
      } else {
      	inputs.eq( inputs.index(this)+ 1 ).focus();
      }
    }

  }

});
// $(document).on('keyup','.form-control',function(e){
//   if (e.which != 13){
//      this.value = this.value.toUpperCase();
//   }
//
// });

$(".select-value").keypress(function(e){
  if (e.which==13){
    var inputs = $(this).closest('form').find(':input');
    inputs.eq( inputs.index(this)+ 1 ).focus();
  }

});

function print_surat(a){
  //$.download(a);
  window.location.href=a;
}
//simpan semua inputan
function simpan_input(){
	var kosong=0;
   			 	$('#form_input').find('input').each(function(){
    				if($(this).prop('required')){

        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}

    					}
				});
                $('#form_input').find('select').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
                $('#form_input').find('textarea').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else {
				$.ajax({
            			type:"POST",
            			url: $("#form_input").attr('action'),
            			data:$("#form_input").serialize(),
                        beforeSend:function(){
                            App.blockUI({
                				target: '#box_form_input',
                				animate: true
            				});
                        },
            			success: function(response){
                    console.log(response);
            				if(response==0){
            					alert('data sudah pernah di input');
                      App.unblockUI('#box_form_input');
            				} else if(response==2) {
                                alert('jumlah barang kebanyakan');
                                App.unblockUI('#box_form_input');
                            }else {
            					App.unblockUI('#box_form_input');
            					listTable.api().ajax.reload();
                      list_ijin_pending.api().ajax.reload();
            					$("#form_input")[0].reset();
                      $('.bs-select').selectpicker('refresh');
            					$(".pertama").focus();
                      alert('Data Berhasil disimpan');
            				}


            		},
                     error: function(){
                        alert('gagal menyimpan data');
                        App.unblockUI('#box_form_input');
                            }
            	});
				}
}

function simpan_cuti(){
	var kosong=0;
   			 	$('#form_input').find('input').each(function(){
    				if($(this).prop('required')){

        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}

    					}
				});
                $('#form_input').find('select').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
                $('#form_input').find('textarea').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else {
				$.ajax({
            			type:"POST",
            			url: $("#form_input").attr('action'),
            			data:$("#form_input").serialize(),
                        beforeSend:function(){
                            App.blockUI({
                				target: '#box_form_input',
                				animate: true
            				});
                        },
            			success: function(response){
                    console.log(response);
            				if(response==0){
            					alert('data gagal di input');
                      App.unblockUI('#box_form_input');
            				} else if(response== 'pending') {
                                alert('Anda masih mempunyai pengajuan cuti yang masih dalam status pending');
                                App.unblockUI('#box_form_input');
                    }else if(response== 'bulan') {
                                alert('Mohon ajukan cuti dalam bulan yang sama');
                                App.unblockUI('#box_form_input');
                    }else if(response== 'batas') {
                                alert('batas maksimum pengajuan cuti adalah 2 bulan yang lalu dan 6 bulan yang akan datang');
                                App.unblockUI('#box_form_input');
                    }else if(response == 1){
            					App.unblockUI('#box_form_input');
            					listTable.api().ajax.reload();
                      list_ijin_pending.api().ajax.reload();
            					$("#form_input")[0].reset();
                      $('.bs-select').selectpicker('refresh');
            					$(".pertama").focus();
                      alert('Data Berhasil disimpan');
            				}


            		},
                     error: function(){
                        alert('gagal menyimpan data');
                        App.unblockUI('#box_form_input');
                            }
            	});
				}
}

//simpan jadwal
function simpan_jadwal(){
	var kosong=0;
   			 	$('#form_input').find('input').each(function(){
    				if($(this).prop('required')){

        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}

    					}
				});
                $('#form_input').find('select').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
                $('#form_input').find('textarea').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else {
				$.ajax({
            			type:"POST",
            			url: $("#form_input").attr('action'),
            			data:$("#form_input").serialize(),
                        beforeSend:function(){
                            App.blockUI({
                				target: '#form_input',
                				animate: true
            				});
                        },
            			success: function(response){
                    console.log(response);
            				if(response==0){
              					App.unblockUI('#form_input');
                        alert('Jadwal Gagal disimpan');
                      }else {
              					App.unblockUI('#form_input');
              					listTable.api().ajax.reload();
              					$("#form_input")[0].reset();
                        $('.bs-select').selectpicker('refresh');
              					$(".pertama").focus();
            				}


            		},
                     error: function(e){
                       console.log(e);
                        alert('gagal menyimpan data');
                        App.unblockUI('#form_input');
                            }
            	});
				}
}

//simpan karyawan
function simpan_karyawan(){
	var kosong=0;
   			 	$('#form_input').find('input').each(function(){
    				if($(this).prop('required')){

        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}

    					}
				});
                $('#form_input').find('select').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
                $('#form_input').find('textarea').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else {
				$.ajax({
            			type:"POST",
            			url: $("#form_input").attr('action'),
            			data:$("#form_input").serialize(),
                        beforeSend:function(){
                            App.blockUI({
                				target: '#box_input_form',
                				animate: true
            				});
                        },
            			success: function(response){
                    if(response=='gabung'){
                      alert('Status kerja jangan aktif, karena tanggal gabung lebih besar dari sekarang');
                                App.unblockUI('#box_input_form');
                    }else if(response=='nik'){
            					alert('NIK sudah terdaftar');
                                App.unblockUI('#box_input_form');
            				} else if(response=='NPP') {
                                alert('NPP sudah terdaftar');
                                App.unblockUI('#box_input_form');
                            }else if(response=='ok'){
            					App.unblockUI('#box_input_form');
            					listTable.api().ajax.reload();
            					$("#form_input")[0].reset();
                                $('.bs-select').selectpicker('refresh');
            					$(".pertama").focus();
                      alert('Data Berhasil disimpan');
            				}


            		},
                     error: function(){
                        alert('gagal menyimpan data');
                        App.unblockUI('#box_input_form');
                            }
            	});
				}
}
//update karyawan
function update_karyawan(){
	var kosong=0;
   			 	$('#form_update_karyawan').find('input').each(function(){
    				if($(this).prop('required')){

        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}

    					}
				});
                $('#form_update_karyawan').find('select').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
                $('#form_update_karyawan').find('textarea').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else {
				$.ajax({
            			type:"POST",
            			url: $("#form_update_karyawan").attr('action'),
            			data:$("#form_update_karyawan").serialize(),
                        beforeSend:function(){
                            App.blockUI({
                				target: '#form_update_karyawan',
                				animate: true
            				});
                        },
            			success: function(response){
            				if(response=='nik'){
            					alert('NIK sudah terdaftar');
                                App.unblockUI('#box_input_form');
            				} else if(response=='NPP') {
                                alert('NPP sudah terdaftar');
                                App.unblockUI('#box_input_form');
                            }else if(response=='ok'){
            					App.unblockUI('#form_update_karyawan');
            					listTable.api().ajax.reload();
                      $("#ajax").modal('hide');

            				}


            		},
                     error: function(){
                        alert('gagal menyimpan data');
                        App.unblockUI('#form_update_karyawan');
                            }
            	});
				}
}

//update password
function update_password(){
  $.ajax({
            type:"POST",
            url: $("#form_update").attr('action'),
            data:$("#form_update").serialize(),
                  beforeSend:function(){
                      App.blockUI({
                  target: '#box_input_form',
                  animate: true
              });
                  },
            success: function(response){
                alert('Password Berhasil di Update');
                App.unblockUI('#box_input_form');
                $("#passwordnya").val('');
              },
            error: function(){
                alert('gagal menyimpan data');
                App.unblockUI('#box_input_form');
              }
        });
}
//simpan_keluarga
function simpan_keluarga(){
	var kosong=0;
   			 	$('#form_modal').find('input').each(function(){
    				if($(this).prop('required')){

        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}

    					}
				});
                $('#form_modal').find('select').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
                $('#form_modal').find('textarea').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else {
				$.ajax({
            			type:"POST",
            			url: $("#form_modal").attr('action'),
            			data:$("#form_modal").serialize(),
                        beforeSend:function(){
                            App.blockUI({
                				target: '#form_modal',
                				animate: true
            				});
                        },
            			success: function(response){
            				if(response=='ada'){
            					alert('NIK sudah terdaftar');
                                App.unblockUI('#form_modal');
            				} else if(response=='gagal') {
                                alert('GAGAL input data');
                                App.unblockUI('#form_modal');
                            }else if(response=='ok'){
                    					App.unblockUI('#form_modal');
                              $("#ajax").modal('hide');
            				       }


            		},
                     error: function(){
                        alert('gagal menyimpan data');
                        App.unblockUI('#form_modal');
                            }
            	});
				}
}


//simpan dokumen

function simpan_dokumen(){
	var kosong=0;
   			 	$('#form_modal').find('input').each(function(){
    				if($(this).prop('required')){

        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}

    					}
				});
                $('#form_modal').find('select').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
                $('#form_modal').find('textarea').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else {
				$.ajax({
            			type:"POST",
                  cache: false,
                  contentType: false,
                  processData: false,
                  dataType: 'json',
            			url: $("#form_modal").attr('action'),
            			data:new FormData($("#form_modal")[0]),//untuk file upload harus gini
                        beforeSend:function(){
                            App.blockUI({
                				target: '#form_modal',
                				animate: true
            				});
                        },
            			success: function(response){
                    //var response=JSON.parse(hasilnya);
            				if(response['hasil']=='ada'){
            					alert('dokumen sudah ada');
                                App.unblockUI('#form_modal');
            				} else if(response['hasil']=='gagal') {
                                alert('GAGAL input data');
                                App.unblockUI('#form_modal');
                            }else if(response['hasil']=='ok'){
                    					App.unblockUI('#form_modal');
                              $("#ajax").modal('hide');
            				       }


            		},
                error: function(e){
                       console.log(e);
                        alert('gagal menyimpan data 2');
                        App.unblockUI('#form_modal');
                        $("#ajax").modal('hide');
                            }
            	});
				}
}

//simpan calendar
function simpan_calendar(){
	var kosong=0;
   			 	$('#form_input').find('input').each(function(){
    				if($(this).prop('required')){

        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}

    					}
				});
                $('#form_input').find('select').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
                $('#form_input').find('textarea').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else {
				$.ajax({
            			type:"POST",
            			url: $("#form_input").attr('action'),
            			data:$("#form_input").serialize(),
                        beforeSend:function(){
                            App.blockUI({
                				target: '#box_input_form',
                				animate: true
            				});
                        },
            			success: function(response){
                    console.log(response);
            				if(response==0){
            					alert('data sudah pernah di input');
                                App.unblockUI('#box_input_form');
            				} else {
            					App.unblockUI('#box_input_form');
            					$("#form_input")[0].reset();
                                $('.bs-select').selectpicker('refresh');
                       $('#calendar').fullCalendar('refetchEvents');
            				}


            		},
                     error: function(){
                        alert('gagal menyimpan data');
                        App.unblockUI('#box_input_form');
                            }
            	});
				}
}




function hapus_input(a){
	$.ajax({
        type:"GET",
        url: a,
        data:{
          aksi:'hapus_input'
        },
        beforeSend:function(){
        	App.blockUI({
            	target: '#tempat_table',
            	animate: true
            	});
            },
        success: function(response){
            if(response != 1){
            	alert('gagal hapus data');
                App.unblockUI('#tempat_table');

            } else {
            	App.unblockUI('#tempat_table');
            	listTable.api().ajax.reload();
              alert('berhasil menghapus data');
            }


           },
        error: function(){
            alert('gagal hapus data');
            App.unblockUI('#tempat_table');
        }
    });
}
//hapus input

function hapus_input_modal(a){
	$.ajax({
        type:"GET",
        url: a,
        beforeSend:function(){
        	App.blockUI({
            	target: '#tempat_table_modal',
            	animate: true
            	});
            },
        success: function(response){
            if(response != 1){
            	alert('gagal hapus data');
                App.unblockUI('#tempat_table_modal');

            } else {
            	App.unblockUI('#tempat_table_modal');
            	$("#ajax").modal('hide');
              alert('berhasil menghapus data');
            }


           },
        error: function(){
            alert('gagal hapus data');
            App.unblockUI('#tempat_table');
        }
    });
}

function update_input(){
var kosong=0;
   			 	$('#form_update').find('input').each(function(){
    				if($(this).prop('required')){

        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}

    					}
				});
                $('#form_update').find('textarea').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
                $('#form_update').find('select').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else {
				$.ajax({
            			type:"PATCH",
            			url: $("#form_update").attr('action'),
            			data:$("#form_update").serialize(),
                        beforeSend:function(){
                            App.blockUI({
                				target: '#ajax',
                				animate: true
            				});
                        },
            			success: function(response){
            				if(response!=1){
                      console.log(response);
            					alert('update data gagal');
                                App.unblockUI('#ajax');
            				} else {
            					App.unblockUI('#ajax');
            					listTable.api().ajax.reload();
            					$("#ajax").modal('hide');
                      alert('berhasil update data');
            				}


            		  },
                        error: function(){
                                alert('update data gagal');
                                App.unblockUI('#ajax');
                                $("#ajax").modal('hide');
                                }
            	});
				}
}
//simpan calendar
function hitung_cuti_rekap(url){
				$.ajax({
            			type:"POST",
            			url: url,
            			data: { tahun:'2018' },
                        beforeSend:function(){
                            App.blockUI({
                				target: '#rekap_cuti',
                				animate: true
            				});
                        },
            			success: function(response){
                    console.log(response);
                    alert('sukses');
            					App.unblockUI('#rekap_cuti');
            		},
                     error: function(){
                        alert('gagal');
                        App.unblockUI('#rekap_cuti');
                            }
            	});
}
//terima cuti
function terimacuti(){
var kosong=0;
   			 	$('#terimacuti').find('input').each(function(){
    				if($(this).prop('required')){

        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}

    					}
				});
                $('#terimacuti').find('textarea').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
                $('#terimacuti').find('select').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else {
				$.ajax({
            			type:"PATCH",
            			url: $("#terimacuti").attr('action'),
            			data:$("#terimacuti").serialize(),
                        beforeSend:function(){
                            App.blockUI({
                				target: '#terimacuti',
                				animate: true
            				});
                        },
            			success: function(response){
            				if(response!='ok'){
            					alert('Akses Ditolak');
                                App.unblockUI('#ajax');
            				} else {
            					App.unblockUI('#terimacuti');
            					listTableAja.api().ajax.reload();
            					$("#ajax").modal('hide');
                      alert('Update data berhasil');
            				}


            		  },
                        error: function(){
                                alert('Gagal Simpan Data');
                                App.unblockUI('#terimacuti');
                                $("#ajax").modal('hide');
                                }
            	});
				}
}

//simpan calendar
function import_log(){
        var sekarang=$('#sekarang').val();
        var akhir=$('#tgl_akhir').val();
        akhir=new Date(akhir);
        $('#total').val(akhir.toLocaleDateString());
        if(sekarang==0){
          sekarang=new Date($('#tgl_awal').val());
        //  d.toLocaleDateString()
          $('#sekarang').val(sekarang.toLocaleDateString());
        }else{
          sekarang=new Date($('#sekarang').val());
        }

				$.ajax({
            			type:"POST",
            			url: $("#form_import").attr('action'),
            			data:$("#form_import").serialize(),
                  beforeSend:function(){
                      App.blockUI({
                          target: '#form_import',
                          animate: true
                      });
                  },
            			success: function(response){
                  console.log(response);
                  if(sekarang < akhir){
                    var besok=new Date(sekarang);
                    besok.setDate(besok.getDate() + 1);
                    $('#sekarang').val(besok.toLocaleDateString());
                    App.unblockUI('#form_import');
                    import_log();
                  }else{
                    App.unblockUI('#form_import');
                    $('#sekarang').val(0);
                    $('#total').val(0);
                    alert('Selesai Bos');

                  }

            		},
                error: function(e){
                  App.unblockUI('#form_import');
                  console.log(e);
                }
            	});
}





// var listTableLaporan=$("#table-laporan").dataTable({
//             "processing": true,
//             "serverSide": true,
//             "ajax": {
//                 "url": $("#form_input").attr('action').replace(/\s/g, "")+"/0",
//                 "data": function ( d ) {
//                     d.barang_id=ambil_id();
//                     d.aksi="list-all";
//                     d.tanggal_1=tanggal_awal();
//                     d.tanggal_2=tanggal_akhir();
//                     d.spk_id=ambil_spk();
//                     d.suplyer_id=ambil_suplyer();
//                     d,bagian_id=ambil_bagian();
//                 }
//             },
//             "columns": kolomlaporan,
//             "language": {
//                 "aria": {
//                     "sortAscending": ": activate to sort column ascending",
//                     "sortDescending": ": activate to sort column descending"
//                 },
//                 "emptyTable": "No data available in table",
//                 "info": "Showing _START_ to _END_ of _TOTAL_ entries",
//                 "infoEmpty": "No entries found",
//                 "infoFiltered": "(filtered1 from _MAX_ total entries)",
//                 "lengthMenu": "_MENU_ entries",
//                 "search": "Search:",
//                 "zeroRecords": "No matching records found"
//             },
//             buttons: [
//                 { extend: 'print', className: 'btn default' },
//                 { extend: 'pdf', className: 'btn default' },
//                 { extend: 'csv', className: 'btn default' }
//             ],
//             responsive: {
//                 details: {
//
//                 }
//             },
//
//             "order": [
//                 [0, 'asc']
//             ],
//
//             "lengthMenu": [
//                 [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, -1],
//                 [5, 10, 15, 20, 30, 40, 50, 60, 70, 80, 90, 100, "All"] // change per page values here
//             ],
//             // set the initial value
//             "pageLength": 5,
//
//             "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
// });
//
// function laporan(){
//     listTableLaporan.api().ajax.reload();
// }

//calendar
$('#calendar').fullCalendar({
  header: {
  left: 'prev,next today',
  center: 'title',
  right: 'month'
  },
  navLinks: true, // can click day/week names to navigate views
  businessHours: true, // display business hours
  editable: false,
  locale: 'id',
  events: '/data-calendar/1/?aksi=json'

});

$('#calendar-absen').fullCalendar({
  header: {
  left: 'prev,next today',
  center: 'title',
  right: 'month'
  },
  navLinks: true, // can click day/week names to navigate views
  businessHours: true, // display business hours
  editable: false,
  locale: 'id',
  events: '/logabsen/1/?aksi=json'

});

$('#calendar-libur').fullCalendar({
  header: {
  left: 'prev,next today',
  center: 'title',
  right: 'month'
  },
  navLinks: true, // can click day/week names to navigate views
  businessHours: true, // display business hours
  editable: false,
  locale: 'id',
  events: '/dashboard/1/?aksi=hari_libur'

});
$('#calendar-ultah').fullCalendar({
  header: {
  left: 'prev,next today',
  center: 'title',
  right: 'month'
  },
  locale: 'id',
  navLinks: true, // can click day/week names to navigate views
  businessHours: true, // display business hours
  editable: false,
  events: '/dashboard/1/?aksi=ulangtahun'

});


//hitung jumlah hari cuti
function hitung_hari(tanggal){
  var hari = tanggal.split(",");
  var total = hari.length;
  $("#jumlah_hari").val(total);
}


function pilih_karyawan(id){
  var karyawan_id=$("#karyawan_id").val();
  $.ajax({
            type:"GET",
            url: '/input-pajak/'+id,
            data:{karyawan_id:id,aksi:'pilih_karyawan'},
            beforeSend:function(){
                App.blockUI({
                    target: '#data_karyawan',
                    animate: true
                });
            },
            success: function(response){
            console.log(response);
            var hasil = JSON.parse(response);
              App.unblockUI('#data_karyawan');
              //npwp,nik,status_nikah,tanggungan,jabatan,gaji_pokok,bulan_pajak
              $('#npwp').val(hasil.npwp);
              $('#nik').val(hasil.nik);
              $('#status_nikah').val(hasil.status_nikah);
              $('#tanggungan').val(hasil.tanggungan);
              $('#jabatan').val(hasil.jabatan);
              if(hasil.gaji_pokok!=null){
                $('#gaji_pokok').val(hasil.gaji_pokok);
              }else{
                $('#gaji_pokok').val("");
              }
              $('#bulan_pajak').val(hasil.bulan_pajak);
          },
          error: function(e){
            App.unblockUI('#data_karyawan');
            console.log(e);
          }
        });

}
//cek required data karyawan
function cek_tab_karyawan(){
  var karyawan_id=$("#karyawan_id").val();
  var gaji_pokok=$("#gaji_pokok").val();
  var bulan_pajak=$("#bulan_pajak").val();
  var error=0;
  if(karyawan_id == 0 || karyawan_id == 0 || gaji_pokok == "" || gaji_pokok == 0 || bulan_pajak == "" || bulan_pajak =='0000-00-00'){
    error=1;
  }

  return error;
}

function cek_tab_penghasilan(){
  var adjustmen_gaji=$("#adjustmen_gaji").val();
  var tunjangan_makan=$("#tunjangan_makan").val();
  var insentif=$("#insentif").val();
  var bonus=$("#bonus").val();
  var lembur=$("#lembur").val();
  var uang_dinas=$("#uang_dinas").val()
  var thr=$("#thr").val();
  var error=0;
  if( adjustmen_gaji=="" || tunjangan_makan=="" || insentif=="" || bonus=="" || lembur=="" || uang_dinas=="" || thr==""){
    error=1;
  }
  return error;
}

function cek_tab_pengurang(){
  var iuran_pensiun=$("#iuran_pensiun").val();
  var iuran_jht=$("#iuran_jht").val();
  var error=0
  if(iuran_jht==null || iuran_pensiun==null){
    error=1;
  }
  return error;
}

function hitung_ptkp(){

var hasil=0;
    $('#ptkp').find('.data_ptkp').each(function(){
      hasil+=$(this).val()*$(this).attr('data-nilai');
    });
  $('#total_ptkp').val(hasil);
}


function pilih_ptkp(){
  var bulan_pajak=$("#bulan_pajak").val();
  var karyawan_id=$("#karyawan_id").val();
  $.ajax({
            type:"GET",
            url: '/input-pajak/'+karyawan_id,
            data:{
              karyawan_id:karyawan_id,
              bulan_pajak:bulan_pajak,
              aksi:'pilih_ptkp'
            },
            beforeSend:function(){
                App.blockUI({
                    target: '#ptkp',
                    animate: true
                });
            },
            success: function(response){
            console.log(response);
            App.unblockUI('#ptkp');
            //ambil data qty
            var hasil=JSON.parse(response);
            for (x in hasil) {
                $('#'+x).val(hasil[x]);
                console.log(hasil[x]);
              }
              setTimeout(function () {
                hitung_ptkp();
              }, 200);

          },
          error: function(e){
            App.unblockUI('#ptkp');
            console.log(e);
          }
        });

}

//event onshow tab
$('.nav-tabs a').on('shown.bs.tab', function(event){
    var x = $(event.target).attr('href');
    if(x=='#ijin-pending'){

      list_ijin_pending.api().ajax.reload();
    }else if(x=='#ijin-batal'){

      list_ijin_batal.api().ajax.reload();
    }else if(x=='#ijin-ok'){

      list_ijin_oke.api().ajax.reload();
    }else if(x=='#penghasilan'){
      var gapok=$("#gaji_pokok").val();
      var jkk=gapok*0.03;
      var jk=gapok*0.01;
      $('#jkk').val(jkk);
      $('#jk').val(jk);
    }else if(x=='#pengurang'){
      var gaji_pokok=$("#gaji_pokok").val();
      var jkk=gaji_pokok*0.03;
      var jk=gaji_pokok*0.01;
      var adjustmen_gaji=$("#adjustmen_gaji").val();
      var tunjangan_makan=$("#tunjangan_makan").val();
      var insentif=$("#insentif").val();
      var bonus=$("#bonus").val();
      var lembur=$("#lembur").val();
      var uang_dinas=$("#uang_dinas").val()
      var thr=$("#thr").val();
      var karyawan_id=$("#karyawan_id").val();
      var bulan_pajak=$("#bulan_pajak").val();
      $.ajax({
                type:"GET",
                url: '/input-pajak/'+karyawan_id,
                data:{
                  aksi:'hitung_biaya',
                  gaji_pokok:gaji_pokok,
                  jkk:jkk,
                  jk:jk,
                  adjustmen_gaji:adjustmen_gaji,
                  tunjangan_makan:tunjangan_makan,
                  insentif:insentif,
                  bonus:bonus,
                  lembur:lembur,
                  uang_dinas:uang_dinas,
                  thr:thr,
                  karyawan_id:karyawan_id,
                  bulan_pajak:bulan_pajak
                },
                beforeSend:function(){
                    App.blockUI({
                        target: '#pengurang',
                        animate: true
                    });
                },
                success: function(response){
                App.unblockUI('#pengurang');
                console.log(response);
                var hasil = JSON.parse(response);
                  //npwp,nik,status_nikah,tanggungan,jabatan,gaji_pokok,bulan_pajak
                  $("#biaya_jabatan").val(hasil.biaya_jabatan);
                  $("#total_penghasilan").val(hasil.total_penghasilan);
              },
              error: function(e){
                App.unblockUI('#pengurang');
                console.log(e);
              }
            });


    }else if(x=='#ptkp'){
      pilih_ptkp();
    }
});

$('.nav-tabs a').on('hide.bs.tab', function(event){
    var x = $(event.target).attr('href');
    if(x=='#data_karyawan'){
      var error_karyawan=cek_tab_karyawan();
      if (error_karyawan==1){
        alert('Mohon lengkapi Tab data karyawan');
        event.preventDefault();
      }
    }else if(x=='#penghasilan'){
      var error_penghasilan=cek_tab_penghasilan();
      if (error_penghasilan==1){
        alert('Mohon lengkapi Tab data penghasilan');
        event.preventDefault();
      }
    }else if(x=="#pengurang"){
      var error_pengurang=cek_tab_pengurang();
      if (error_pengurang==1){
        alert('Mohon lengkapi Tab data pengurang');
        event.preventDefault();
      }
    }
});

//simpan input pajak
function simpan_pajak(){
	var kosong=0;
   			 	$('#form_input').find('input').each(function(){
    				if($(this).prop('required')){

        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}

    					}
				});
                $('#form_input').find('select').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
                $('#form_input').find('textarea').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else {
				$.ajax({
            			type:"POST",
            			url: $("#form_input").attr('action'),
            			data:$("#form_input").serialize(),
                        beforeSend:function(){
                            App.blockUI({
                				target: '#box_form_input',
                				animate: true
            				});
                        },
            			success: function(response){
                    console.log(response);
            				if(response==0){
            					 alert('data sudah pernah di input');
                       App.unblockUI('#box_form_input');
            				}else{
            					App.unblockUI('#box_form_input');
            					listTable.api().ajax.reload();
            					$("#form_input")[0].reset();
                      $('.bs-select').selectpicker('refresh');
            					$(".pertama").focus();
                      $("#data_karyawan").tab('show');
            				}


            		},
                     error: function(){
                        alert('gagal menyimpan data');
                        App.unblockUI('#box_form_input');
                            }
            	});
				}
}

function pilih_tanggal(jenis){
  if(jenis=='tanggal'){
    $('.cuti-tanggal').removeClass('hidden');
    $('.cuti-range').addClass('hidden');
  }else if(jenis=='range'){
    $('.cuti-range').removeClass('hidden');
    $('.cuti-tanggal').addClass('hidden');
  }else{
    $('.cuti-tanggal').addClass('hidden');
    $('.cuti-range').addClass('hidden');
  }
}

function pilih_ijin(){
  var hanya_marketing=$('#data_ijin_id option:selected').attr('hanya-marketing');
  if( hanya_marketing=='Ya'){
    //show form tambahan untuk marketing
    $(".hanya-marketing").show();
    $("#nasabah_id").attr('required',true);
    $("#alamat").attr('required',true);
    $("#progress_id").attr('required',true);
  }else{
    //hide form tambahan untuk marketing
    $(".hanya-marketing").hide();
    $("#nasabah_id").removeAttr('required');
    $("#alamat").removeAttr('required');
    $("#progress_id").removeAttr('required');
  }
}

function pilih_progress(){
  var butuh_alasan=$('#progress_id option:selected').attr('butuh-alasan');
  var butuh_nominal=$('#progress_id option:selected').attr('butuh-nominal');
  var butuh_tanggal=$('#progress_id option:selected').attr('butuh-tanggal');

  if(butuh_alasan=="Ya"){
    $('.alasan-progress').show();
    $("#alasan_progress").attr('required',true);
  }else{
    $('.alasan-progress').hide();
    $("#alasan_progress").removeAttr('required');
  }

  if(butuh_nominal=="Ya"){
    $('.nominal').show();
    $("#nominal").attr('required',true);
  }else{
    $('.nominal').hide();
    $("#nominal").removeAttr('required');
  }

  if(butuh_tanggal=="Ya"){
    $('.tanggal-progress').show();
    $("#tanggal_progress").attr('required',true);
  }else{
    $('.tanggal-progress').hide();
    $("#tanggal_progress").removeAttr('required');
  }

}

function pilih_followup(){
  var butuh_nominal=$('#hasil_progress_id option:selected').attr('butuh-nominal');
  var butuh_tanggal=$('#hasil_progress_id option:selected').attr('butuh-tanggal');


  if(butuh_nominal=="Ya"){
    $('.nominal').show();
    $("#nominal_progress").attr('required',true);
  }else{
    $('.nominal').hide();
    $("#nominal_progress").removeAttr('required');
  }

  if(butuh_tanggal=="Ya"){
    $('.tanggal-progress').show();
    $("#tanggal_progress_berikutnya").attr('required',true);
  }else{
    $('.tanggal-progress').hide();
    $("#tanggal_progress_berikutnya").removeAttr('required');
  }

}

$('.summernote').summernote({height: 300});
//ikut survey
function ikut_survey(){
var kosong=0;

				if (kosong==1){
					alert('semua form harus di isi');
				}else {
				$.ajax({
            			type:"POST",
            			url: $("#ikut_survey").attr('action'),
            			data:$("#ikut_survey").serialize(),
                        beforeSend:function(){
                            App.blockUI({
                				target: '#full',
                				animate: true
            				});
                        },
            			success: function(response){
            				if(response!='ok'){
            					alert('Mohon Periksa Kembali');
                                App.unblockUI('#full');
            				} else {
            					App.unblockUI('#ikut_survey');
            					listTableAja.api().ajax.reload();
            					$("#full").modal('hide');
                      alert('Berhasi disimpan');
            				}


            		  },
                        error: function(){
                                alert('Gagal Simpan Data');
                                App.unblockUI('#full');
                                $("#full").modal('hide');
                                }
            	});
				}
}
var FormRepeater = function () {

    return {
        //main function to initiate the module
        init: function () {
        	$('.mt-repeater').each(function(){
                $(this).repeater({
        			show: function () {
	                	$(this).slideDown();
                        $('.date-picker').datepicker({
                            rtl: App.isRTL(),
                            orientation: "left",
                            autoclose: true
                        });
                        $('.angka-angka').number( true, 0,',','.' );
		            },

		            hide: function (deleteElement) {
		                if(confirm('Are you sure you want to delete this element?')) {
		                    $(this).slideUp(deleteElement);
		                }
		            },

		            ready: function (setIndexes) {

		            }

        		});
        	});
        }

    };

}();
function simpan_dinas(){
	var kosong=0;
   			 	$('#form_input').find('input').each(function(){
    				if($(this).prop('required')){

        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}

    					}
				});
                $('#form_input').find('select').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
                $('#form_input').find('textarea').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else{
				$.ajax({
            			type:"POST",
            			url: $("#form_input").attr('action'),
            			data:$("#form_input").serialize(),
                        beforeSend:function(){
                            App.blockUI({
                				target: '#box_form_input',
                				animate: true
            				});
                        },
            			success: function(response){
                    console.log(response);
            				if(response==0){
            					alert('data gagal di input');
                      App.unblockUI('#box_form_input');
            				}else if(response == 1){
            					App.unblockUI('#box_form_input');
                      alert('Data Berhasil disimpan');
            					listDinasProgress.api().ajax.reload();
                      listDinasSelesai.api().ajax.reload();
            					$("#form_input")[0].reset();
                      $('.bs-select').selectpicker('refresh');
            					$(".pertama").focus();

            				}


            		},
                     error: function(){
                        alert('gagal menyimpan data');
                        App.unblockUI('#box_form_input');
                            }
            	});
				}
}

function pilih_sesuatu(id,url,lokasi){
  $.ajax({
            type:"POST",
            url: url,
            data:{id:id},
                  beforeSend:function(){
                  App.blockUI({
                  target: '#'+lokasi,
                  animate: true
              });
                  },
            success: function(response){
              //console.log(response);
              $('#'+lokasi).html(response);
              App.unblockUI('#'+lokasi);
          },
           error: function(){
              alert('gagal menampilkan data');
              App.unblockUI('#'+lokasi);
          }
        });
}

function simpan_akomodasi(){
	var kosong=0;
   			 	$('#form_input').find('input').each(function(){
    				if($(this).prop('required')){

        				if( ! $(this).val() ){
        					kosong=1;
        					$(this).focus();
        					return false;
        				}

    					}
				});
                $('#form_input').find('select').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
                $('#form_input').find('textarea').each(function(){
                    if($(this).prop('required')){

                        if( ! $(this).val() ){
                            kosong=1;
                            $(this).focus();
                            return false;
                        }

                        }
                });
				if (kosong==1){
					alert('semua form harus di isi');
				}else{
				$.ajax({
            			type:"POST",
            			url: $("#form_input").attr('action'),
            			data:$("#form_input").serialize(),
                        beforeSend:function(){
                            App.blockUI({
                				target: '#box_form_input',
                				animate: true
            				});
                        },
            			success: function(response){
                    console.log(response);
            				if(response==0){
            					alert('data gagal di input');
                      App.unblockUI('#box_form_input');
            				}else if(response == 1){
            					App.unblockUI('#box_form_input');
                      alert('Data Berhasil disimpan');
            					listAkomodasiProgress.api().ajax.reload();
                      listAkomodasiSelesai.api().ajax.reload();
            					$("#form_input")[0].reset();
                      $('.bs-select').selectpicker('refresh');
            					$(".pertama").focus();
                      location.reload();

            				}


            		},
                     error: function(){
                        alert('gagal menyimpan data');
                        App.unblockUI('#box_form_input');
                            }
            	});
				}
}

///simpan untuk ijin

function simpan_ijin(){
	var kosong=0;
  var nama_form = "";
  var thisForm = $('#form-input');
  var jamMulaiVal = document.forms['form_ijin']['jam_ijin'].value;
  var jamSelesaiVal = document.forms['form_ijin']['sampai_dengan'].value;
  var jamMulai = jamMulaiVal.split(':');
  var jamSelesai = jamSelesaiVal.split(':');
  var totJamMulai = parseInt(jamMulai[0])+parseInt(jamMulai[1])/60;
  var totJamSelesai = parseInt(jamSelesai[0])+parseInt(jamSelesai[1])/60;
  //alert("Test "+jamMulai[0]);
  $('#form-input').find('input').each(function(){
    	if($(this).prop('required')){
        if( $(this).val()=='' ){
        		kosong=1;
            nama_form=$(this).attr('nama_form');
        		$(this).focus();
        		return false;
          }

    	}
	});
  $('#form-input').find('select').each(function(){
      if($(this).prop('required')){
          if( ! $(this).val() ){
              kosong=1;
              nama_form=$(this).attr('nama_form');
              $(this).focus();
              return false;
          }

      }
  });
  $('#form-input').find('textarea').each(function(){
      if($(this).prop('required')){

          if( $(this).val()=='' ){
              kosong=1;
              nama_form=$(this).attr('nama_form');
              $(this).focus();
              return false;
          }

          }
  });
				if (kosong==1){
					notifikasi(nama_form+" harus di isi","warning");
				}else if(totJamMulai > totJamSelesai){
                    			notifikasi("Jam mulai tidak boleh lebih dari jam selesai","warning");
                		}else if(parseInt(jamSelesai[0]) == 0){
                     			notifikasi("Jam selesai tidak boleh melebihi jam 23:59","warning");
                		}else if(parseInt(jamMulai[0]) == 0){
                     			notifikasi("Jam mulai tidak boleh melebihi jam 23:59","warning");
				}else {
				$.ajax({
      			type:"POST",
      			url: $("#form-input").attr('action'),
      			data:new FormData(thisForm[0]),
            cache:false,
            contentType: false,
            processData: false,
                  beforeSend:function(){
                      App.blockUI({
                        target: '#box_form_input',
                        animate: true
                    });
                  },
      			success: function(response){
              console.log(response);
              var hasil = response;
                  if(hasil.sukses == 0){
                    //ini error
                    App.unblockUI('#box_form_input');
                    	notifikasi(hasil.error,'warning');

                  }else if(hasil.sukses == 1){
                    // ini tidak eror
                     	App.unblockUI('#box_form_input');
                      listTable.api().ajax.reload();
                      list_ijin_pending.api().ajax.reload();
            					$("#form-input")[0].reset();
                      $('.bs-select').selectpicker('refresh');
            					$(".pertama").focus();
                      notifikasi(hasil.pesan,'success');
                  }else{
                    App.unblockUI('#box_form_input');
                    notifikasi('gagal menyimpan data','error');

                  }
          		},
                error: function(){
                  App.unblockUI('#box_form_input');
                notifikasi('gagal menyimpan data','error');

                    }
            	});
				}
}

$("#ajax").on('shown.bs.modal', function(){
  $('.bs-select').selectpicker({
              iconBase: 'fa',
              tickIcon: 'fa-check',
              size:5
  });
});

jQuery(document).ready(function() {
    FormRepeater.init();
});
